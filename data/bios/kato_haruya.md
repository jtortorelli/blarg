Actor with an eternally boyish appearance (think Matthew Broderick) who worked exclusively for Toho. Kato regularly played supporting roles, usually providing comic relief. He is best seen as the easily spooked younger member of the diamond gang in _Dogora, the Space Monster_ (1964).

His career trajectory was a familiar one for actors of his generation; he appeared regularly for Toho throughout the 50s and 60s, and after his contract was released he retired from film and acted in television during the 70s and into the 80s.

In 2009 Kato appeared in public, attending a screening of _Godzilla_ and _The Mysterians_ and giving interviews with suit actor Haruo Nakajima. According to a volume from Yosensha's "Special Effects" series, Kato passed away in 2015 or 2016.