[
  title: "Moon Over Tao",
  path: "moon-over-tao-1997",
  original_title: "タオの月",
  transliteration: "Tao No Tsuki",
  translation: "Moon of Tao",
  release_date: ~D[1997-11-29],
  runtime: 96,
  produced_by: "Bandai Visual",
  staff: [
    [roles: "Director", people: "Keita Amemiya"],
    [roles: "Screenplay", people: ["Toru Tanaka", "Hajime Matsumoto", "Keita Amemiya"]],
    [roles: "Photography", people: "Hiroshi Kidokoro"],
    [roles: "Lighting", people: "Yoshimi Hosaka"],
    [roles: "Art", people: "Akihiko Iguchi"],
    [roles: "Character Design", people: "Keita Amemiya"],
    [roles: "Editor", people: "Shinichi Fushima"],
    [roles: "Music", people: ["Buddy-Zoo", "Koichi Ota", "Shinji Kinoshita"]],
    [roles: "Sound", people: "Atsushi Sugiyama"]
  ],
  top_billed_cast: [
    [actor: "Toshiyuki Nagashima", characters: "Suikyo"],
    [actor: "Hiroshi Abe", characters: "Hayate"],
    [actor: "Sayaka Yoshino", characters: "Renge"],
    [actor: "Yuko Moriyama", characters: ["Marien", "Kuzuto", "Abira"]],
    [actor: "Kei Tani", characters: "Tadaoki"],
    [actor: "Takaaki Enoki", characters: "Kakugyo"]
  ],
  other_cast: [
    [actor: "Yukijiro Hotaru", characters: "Swordsmith"],
    [actor: "Kunihiko Ida", characters: "Guard"]
  ],
  synopsis: true
]
