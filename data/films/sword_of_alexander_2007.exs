[
  title: "The Sword of Alexander",
  path: "sword-of-alexander-2007",
  original_title: "大帝の剣",
  transliteration: "Taitei No Ken",
  translation: "Sword of the Emperor",
  release_date: ~D[2007-04-07],
  runtime: 110,
  produced_by: [
    "Enterbrain",
    "Toei",
    "Toei Video",
    "Sky Perfect Well Think",
    "TV Asahi",
    "Geo",
    "Asahi Broadcasting",
    "Yahoo! JAPAN",
    "Me TV",
    "Hiroshima Home TV",
    "Kyushu Asahi Broadcasting",
    "Hokkaido TV",
    "Comic Toranoana",
    "Tommy Walker"
  ],
  staff: [
    [roles: "Director", people: "Yukihiko Tsutsumi"],
    [roles: "Original Story", people: "Baku Yumemakura"],
    [roles: "Screenplay", people: "Akira Amasawa"],
    [roles: "Music", people: "Akira Mitake"],
    [roles: "Photography", people: "Satoru Karasawa"],
    [roles: "Lighting", people: "Akio Kimura"],
    [roles: "Art Director", people: "Hisao Inagaki"],
    [roles: "Sound", people: "Yasushi Tanaka"],
    [roles: "Editor", people: "Nobuyuki Ito"]
  ],
  top_billed_cast: [
    [actor: "Hiroshi Abe", characters: "Genkuro Yorozu"],
    [actor: "Kyoko Hasegawa", characters: "Ran / Mai"],
    [actor: "Kankuro Kudo", characters: "Sasuke"],
    [actor: "Meisa Kuroki", characters: "Botan"],
    [actor: "Toru Emori", characters: "Narrator"],
    [actor: "Aya Sugimoto", characters: "Himeyasha"],
    [actor: "Masahiko Tsugawa", characters: "Yukimura Sanada"],
    [actor: "Riki Takeuchi", characters: ["Gin", "Dakusha"]]
  ],
  other_cast: [
    [actor: "Kenichi Endo", characters: ["Mountain Man", "Dakusha"]],
    [actor: "Masakatsu Funaki", characters: "Bandit Chieftan"],
    [actor: "Hirotaro Honda", characters: "Saizo"],
    [actor: "Ai Maeda", characters: "Kidnapped Girl"],
    [actor: "Naomasa Musaka", characters: "Kuromushi"],
    [actor: "Koji Okura", characters: "Toji"],
    [actor: "Taro Suwa", characters: "Villager"],
    [actor: "Takashi Taniguchi", characters: "Fujidera"],
    [actor: "Yu Tokui", characters: "Toshitsune Maeda"]
  ],
  synopsis: true
]
