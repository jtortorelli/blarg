[
  title: "Shinobi: Heart Under Blade",
  path: "shinobi-heart-under-blade-2005",
  release_date: ~D[2005-09-17],
  runtime: 101,
  produced_by: [
    "Shochiku",
    "Nippon TV Network",
    "Eisei Gekijo",
    "Yomiuri TV Broadcasting",
    "Yomiuri Shimbun Tokyo"
  ],
  staff: [
    [roles: "Director", people: "Ten Shimoyama"],
    [roles: "Original Story", people: "Futaro Yamada"],
    [roles: "Screenplay", people: "Kenya Hirata"],
    [roles: "Photography", people: "Masashi Komori"],
    [roles: "Lighting", people: "Koichi Watanabe"],
    [roles: "Art", people: "Toshihiro Isomi"],
    [roles: "Sound", people: "Hajime Suzuki"],
    [roles: "Editor", people: "Isao Kawase"],
    [roles: "Music", people: "Taro Iwashiro"],
    [roles: "Action Director", people: "Yuji Shimomura"]
  ],
  top_billed_cast: [
    [actor: "Yukie Nakama", characters: "Oboro"],
    [actor: "Joe Odagiri", characters: "Gennosuke Koga"],
    [actor: "Tomoka Kurotani", characters: "Kagero"],
    [actor: "Erika Sawajiri", characters: "Hotarubi"],
    [actor: "Renji Ishibashi", characters: "Tenkai Nankobo"],
    [actor: "Kazuo Kitamura", characters: "Ieyasu Tokugawa"],
    [actor: "Kippei Shiina", characters: "Tenzen Yakushiji"]
  ],
  other_cast: [
    [actor: "Shun Ito", characters: "Nenki Mino"],
    [actor: "Hoka Kinoshita", characters: "Saemon Kisaragi"],
    [actor: "Mitsuki Koga", characters: "Koshiro Chikuma"],
    [actor: "Lily", characters: "Ogen"],
    [actor: "Takeshi Masu", characters: "Hyoma Muroga"],
    [actor: "Yutaka Matsushige", characters: "Hanzo Hattori"],
    [actor: "Kenji Miyoshi", characters: "Saemon Kisaragi"],
    [actor: "Osami Nabe", characters: "Man Pulling Wagon"],
    [actor: "Toshiya Nagasawa", characters: "Munenori Yagyu"],
    [actor: "Masaki Nishina", characters: "Jubei Yagyu"],
    [actor: "Tak Sakaguchi", characters: "Yashamaru"],
    [actor: "Minoru Terada", characters: "Danjo Koga"]
  ],
  synopsis: true
]
