[
  title: "Message from Space",
  path: "message-from-space-1978",
  original_title: "宇宙からのメッセージ",
  transliteration: "Uchū Kara No Messēji",
  translation: "Message from Space",
  release_date: ~D[1978-04-29],
  runtime: 105,
  produced_by: ["Toei"],
  staff: [
    [roles: "Director", people: "Kinji Fukasaku"],
    [roles: "Special Effects Director", people: "Nobuo Yajima"],
    [
      roles: "Original Story",
      people: ["Shotaro Ishinomori", "Masahiro Noda", "Kinji Fukasaku", "Hiro Matsuda"]
    ],
    [roles: "Screenplay", people: "Hiro Matsuda"],
    [roles: "Photography", people: "Toru Nakajima"],
    [roles: "Music", people: "Kenichiro Morioka"],
    [roles: "Art", people: "Michio Mikami"],
    [roles: "Lighting", people: "Shigeru Wakaki"],
    [roles: "Sound", people: "Teruhiko Arakawa"],
    [roles: "Editor", people: "Isamu Ichida"],
    [roles: "Visual Effects", people: "Minoru Nakano"],
    [roles: "Mecha Design", people: "Shotaro Ishinomori"]
  ],
  top_billed_cast: [
    [actor: "Vic Morrow", characters: "General Garuda"],
    [actor: "Etsuko Shihomi", characters: "Princess Emeralida"],
    [actor: "Philip Casnoff", characters: "Aaron Solar"],
    [actor: "Peggy Lee Brennan", characters: "Meia Long"],
    [actor: "Hiroyuki Sanada", characters: "Shiro Hongo"],
    [actor: "Seijun Okabe", characters: "Jack"],
    [actor: "Isamu Shimizu", characters: "Beba-2"],
    [actor: "Hideyo Amamoto", characters: "Elder Dark"],
    [actor: "Junkichi Orimoto", characters: "Elder Kido"],
    [actor: "Noboru Mitani", characters: "Kamesasa"],
    [actor: "Sanda Sugiyama", characters: "Big Sam"],
    [actor: "Harumi Sone", characters: "Lazar"],
    [actor: "Jerry Ito", characters: "Earth Federation Commander"],
    [actor: "Tetsuro Tamba", characters: "Earth Federation Chairman Ernest Noguchi"],
    [actor: "Nenji Kobayashi", characters: "Space Patrol Constable Fox"],
    [actor: "William Ross", characters: "Meia's Private Spacecraft Captain"],
    [actor: "Hirohisa Nakata", characters: "Earth Federation Officer"],
    [actor: "Machiko Soga", characters: "Beba-2 (Voice)"],
    [actor: "Daisuke Awaji", characters: "Earth Federation Officer"],
    [actor: "Mikio Narita", characters: "Prince Rockseia XII"],
    [actor: "Makoto Sato", characters: "Urocco"],
    [actor: "Takayuki Akutagawa", characters: "Narrator"],
    [actor: "Shinichi Chiba", characters: "Prince Hans"]
  ],
  synopsis: true
]
