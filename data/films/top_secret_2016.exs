[
  title: "The Top Secret",
  path: "top-secret-2016",
  original_title: "秘密 THE TOP SECRET",
  transliteration: "Himitsu The Top Secret",
  translation: "Secret: The Top Secret",
  release_date: ~D[2016-08-06],
  runtime: 148,
  produced_by: ["Shochiku", "Kinoshita Group", "WOWOW", "J Storm", "Hakusensha", "GYAO"],
  staff: [
    [roles: "Director", people: "Keishi Otomo"],
    [roles: "Original Story", people: "Reiko Shimizu"],
    [
      roles: "Screenplay",
      people: ["Izumi Takahashi", "Keishi Otomo", "Sork-jun Lee", "Sun-mee Kim"]
    ],
    [roles: "Music", people: "Naoki Sato"],
    [roles: "Photography", people: "Takuro Ishizaka"],
    [roles: "Lighting", people: "Shori Hirano"],
    [roles: "Art", people: "Hajime Hashimoto"],
    [roles: "Sound", people: "Fusao Yuwaki"],
    [roles: "Editor", people: "Tsuyoshi Imai"],
    [roles: "VFX Supervisor", people: "Ichijun Kosaka"]
  ],
  synopsis: true,
  top_billed_cast: [
    [actor: "Toma Ikuta", characters: "Tsuyoshi Maki"],
    [actor: "Masaki Okada", characters: "Ikko Aoki"],
    [actor: "Koji Kikkawa", characters: "Kiyotaka Kainuma"],
    [actor: "Tori Matsuzaka", characters: "Katsuhiro Suzuki"],
    [actor: "Lisa Oda", characters: "Kinuko Tsuyuguchi"],
    [actor: "Koji Okura", characters: "Takashi Imai"],
    [actor: "Haruka Kinami", characters: "Nanako Amachi"],
    [actor: "Yusuke Hirayama", characters: "Yasufumi Okabe"],
    [actor: "Masaki Miura", characters: "Kiyoshi Yamaji"],
    [actor: "Mantaro Koichi", characters: "Advisory Committee Chair Asano"],
    [actor: "Chiaki Kuriyama", characters: "Yukiko Miyoshi"],
    [actor: "Lily Franky", characters: "Junichiro Saito"],
    [actor: "Kippei Shiina", characters: "Koichi Tsuyuguchi"],
    [actor: "Nao Omori", characters: "Shunsuke Manabe"]
  ],
  other_cast: [
    [actor: "Yuki Izumisawa", characters: "Shin Tanaka"],
    [actor: "Ayumu Mochitsuki", characters: "Manabu Hirai"],
    [actor: "Tomoya Moeno", characters: "Suspect"],
    [actor: "Ian Moore", characters: "Priest"]
  ]
]
