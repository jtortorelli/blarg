[
  title: "Samurai II: Duel at Ichijoji Temple",
  path: "samurai-ii-duel-at-ichijoji-temple-1955",
  original_title: "續宮本武蔵 一乘寺の決斗",
  transliteration: "Zoku Miyamoto Musashi Ichijōji No Kettō",
  translation: "Continued Miyamoto Musashi: Duel of Ichijoji Temple",
  release_date: ~D[1955-07-12],
  runtime: 103,
  preceded_by: {"Samurai I: Musashi Miyamoto", 1954},
  followed_by: {"Samurai III: Duel at Ganryu Island", 1956},
  produced_by: "Toho",
  staff: [
    [roles: "Director", people: "Hiroshi Inagaki"],
    [roles: "Original Story", people: "Eiji Yoshikawa"],
    [roles: "Dramatization", people: "Hideji Hojo"],
    [roles: "Screenplay", people: ["Tokuhei Wakao", "Hiroshi Inagaki"]],
    [roles: "Photography", people: "Jun Yasumoto"],
    [roles: "Art Director", people: "Kisaku Ito"],
    [roles: "Sound", people: "Choshichiro Mikami"],
    [roles: "Lighting", people: "Shigeru Mori"],
    [roles: "Music", people: "Ikuma Dan"],
    [roles: "Assistant Director", people: "Jun Fukuda"],
    [roles: "Editor", people: "Hidefumi Oi"]
  ],
  top_billed_cast: [
    [actor: "Toshiro Mifune", characters: "Musashi Miyamoto"],
    [actor: "Koji Tsuruta", characters: "Kojiro Sasaki"],
    [actor: "Mariko Okada", characters: "Akemi"],
    [actor: "Kaoru Yachigusa", characters: "Otsu"],
    [actor: "Michio Kogure", characters: "Dayu Yoshino"],
    [actor: "Mitsuko Mito", characters: "Oko"],
    [actor: "Akihiko Hirata", characters: "Seijuro Yoshioka"],
    [actor: "Daisuke Kato", characters: "Toji Gion"],
    [actor: "Kurouemon Onoue", characters: "Takuan the Priest"],
    [actor: "Sachio Sakai", characters: "Matahachi"],
    [actor: "Yu Fujiki", characters: "Shichiro Yoshioka"],
    [actor: "Machiko Kitagawa", characters: "Tayu Kobosatsu"],
    [actor: "Eiko Miyoshi", characters: "Osugi"],
    [actor: "Eijiro Tono", characters: "Baiken Shishido"],
    [actor: "Kento Ida", characters: "Jotaro"],
    [actor: "Akira Tani", characters: "Gonroku Kawarano"],
    [actor: "Ko Mihashi", characters: "Koetsu Honami"],
    [actor: "Kuninori Kodo", characters: "Nitsukan the Monk"],
    [actor: "Keiko Kondo", characters: "Wataru Hagerin"],
    [actor: "Hisako Takihana", characters: "Myoshuni"],
    [actor: "Yoshibumi Tajima", characters: "Heisuke Otaguro"],
    [actor: "Ren Yamamoto", characters: "Riichi Kawakami"],
    [actor: "Etsuro Saijo", characters: "Kurato Kobashi"],
    [actor: "Ryu Kuze", characters: "Kikaku Kanehara"],
    [actor: "Torahiko Hamada", characters: "Jurozaemon Oike"],
    [actor: "Yoshio Inaba", characters: "Ryohei Ueda"],
    [actor: "Giro Sakurai", characters: "Shigesaku Nishino"],
    [actor: "Fuminto Matsuo", characters: "Jinbei Ichikawa"],
    [actor: "Yasuhisa Tsutsumi", characters: "Yoshioka School Samurai"],
    [actor: "Ren Imaizumi", characters: "Yoshioka School Samurai"],
    [actor: "Sokichi Maki", characters: "Genzaemon Mibu"],
    [actor: "Kenzo Tabu", characters: "Kensuke Kenshi"]
  ],
  other_cast: [
    [actor: "Takuzo Kumagai", characters: "Yoshioka School Samurai"],
    [actor: "Yoshio Katsube", characters: "Passerby"],
    [actor: "Haruo Nakajima", characters: "Litter Bearer"],
    [actor: "Keiji Sakakida", characters: "Yoshioka School Manservant"],
    [actor: "Haruya Sakamoto", characters: "Yoshioka School Samurai"],
    [actor: "Hideo Shibuya", characters: "Yoshioka School Samurai"],
    [actor: "Jiro Suzukawa", characters: "Yoshioka School Samurai"],
    [actor: "Kamayuki Tsubono", characters: "Samurai on Bridge"],
    [actor: "Mitsuo Tsuda", characters: "Yoshioka School Samurai"]
  ],
  credits: true,
  synopsis: true
]
