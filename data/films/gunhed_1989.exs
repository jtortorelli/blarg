[
  title: "Gunhed",
  path: "gunhed-1989",
  original_title: "ガンヘッド",
  transliteration: "Ganheddo",
  translation: "Gunhed",
  release_date: ~D[1989-07-22],
  runtime: 100,
  produced_by: ["Toho", "Sunrise", "Bandai", "Kadokawa Shoten", "IMAGICA"],
  staff: [
    [roles: "Director", people: "Masato Harada"],
    [roles: "Special Effects Director", people: "Koichi Kawakita"],
    [roles: "Producer", people: ["Tomoyuki Tanaka", "Eiji Yamamura"]],
    [roles: "Screenplay", people: ["Masato Harada", "James Bannon"]],
    [roles: "Photography", people: "Junichi Fujisawa"],
    [roles: "Art", people: "Fumio Ogawa"],
    [roles: "Sound", people: "Teiichi Sato"],
    [roles: "Lighting", people: "Tsuyoshi Awakihara"],
    [roles: "Editor", people: "Yoshitami Kuroiwa"],
    [roles: "Music", people: "Toshiyuki Honda"]
  ],
  top_billed_cast: [
    [actor: "Masahiro Takashima", characters: "Brooklyn"],
    [actor: "Brenda Bakke", characters: "Nim"],
    [actor: "Aya Enjoji", characters: "Baby"],
    [actor: "Eugene Harada", characters: "Seven"],
    [actor: "Kaori Mizushima", characters: "Eleven"],
    [actor: "James B. Thompson", characters: "Barabbas"],
    [actor: "Yosuke Saito", characters: "Boxer"],
    [actor: "Doll Nguyen", characters: "Boomerang"],
    [actor: "Jay Kabira", characters: "Bombay"],
    [actor: "Randy Reyes", characters: "Gunhed (Voice)"],
    [actor: "Mickey Curtis", characters: "Bansho"]
  ],
  synopsis: true
]
