[
  title: "Ring",
  path: "ring-1998",
  original_title: "リング",
  transliteration: "Ringu",
  translation: "Ring",
  release_date: ~D[1998-01-31],
  runtime: 95,
  followed_by: {"Rasen", 1998},
  produced_by: ["Kadokawa Shoten", "Pony Canyon", "Toho", "IMAGICA", "Asmik", "Omega Project"],
  staff: [
    [roles: "Director", people: "Hideo Nakata"],
    [roles: "Original Story", people: "Koji Suzuki"],
    [roles: "Screenplay", people: "Hiroshi Takahashi"],
    [roles: "Photography", people: "Junichiro Hayashi"],
    [roles: "Art", people: "Iwao Saito"],
    [roles: "Lighting", people: "Nobuo Maehara"],
    [roles: "Music", people: "Kenji Kawai"],
    [roles: "Editor", people: "Nobuyuki Takahashi"]
  ],
  top_billed_cast: [
    [actor: "Nanako Matsushima", characters: "Reiko Asakawa"],
    [actor: "Miki Nakatani", characters: "Mai Takano"],
    [actor: "Hiroyuki Sanada", characters: "Ryuji Takayama"]
  ],
  other_cast: [
    [actor: "Daisuke Ban", characters: "Heihachiro Ikuma"],
    [actor: "Lee Jong-ho", characters: "Komiya"],
    [actor: "Miwako Kaji", characters: "Kazue Yamamura"],
    [actor: "Masako", characters: "Shizuko Yamamura"],
    [actor: "Yutaka Matsushige", characters: "Kenzo Yoshino"],
    [actor: "Katsumi Muramatsu", characters: "Koichi Asakawa"],
    [actor: "Yoichi Numata", characters: "Takashi Yamamura"],
    [actor: "Yoko Oshima", characters: "Reiko's Aunt"],
    [actor: "Rikiya Otaka", characters: "Yoichi Asakawa"],
    [actor: "Hitomi Sato", characters: "Masami Kurahashi"],
    [actor: "Kiriko Shimizu", characters: "Yoshimi Oishi"],
    [actor: "Toshihiko Takeda", characters: "Young Takashi Yamamura"],
    [actor: "Yuko Takeuchi", characters: "Tomoko Oishi"],
    [actor: "Hiroyuki Tanabe", characters: "Hayatsu"],
    [actor: "Yurei Yanagi", characters: "Okazaki"]
  ],
  synopsis: true
]
