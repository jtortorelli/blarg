[
  title: "Lupin the Third: The Castle of Cagliostro",
  path: "lupin-the-third-the-castle-of-cagliostro-1979",
  original_title: "ルパン三世 カリオストロの城",
  transliteration: "Rupan Sansei Kariosutoro No Shiro",
  translation: "Lupin the Third: Castle of Cagliostro",
  release_date: ~D[1979-12-15],
  runtime: 100,
  produced_by: "Tokyo Movie",
  staff: [
    [roles: "Director", people: "Hayao Miyazaki"],
    [roles: "Original Story", people: "Monkey Punch"],
    [roles: "Screenplay", people: ["Hayao Miyazaki", "Haruya Yamazaki"]],
    [roles: "Music", people: "Yuji Ono"],
    [roles: "Art", people: "Shichiro Kobayashi"],
    [roles: "Photography", people: "Hirofumi Takahashi"],
    [roles: "Sound", people: "Satoshi Kato"],
    [roles: "Editor", people: "Tsurubuchi Yumitsu"]
  ],
  top_billed_cast: [
    [actor: "Yasuo Yamada", characters: "Lupin the Third"],
    [actor: "Eiko Masuyama", characters: "Fujiko Mine"],
    [actor: "Kiyoshi Kobayashi", characters: "Daisuke Jigen"],
    [actor: "Makio Inoue", characters: "Goemon Ishikawa"],
    [actor: "Goro Naya", characters: "Inspector Zenigata"],
    [actor: "Sumi Shimamoto", characters: "Clarisse"],
    [actor: "Taro Ishida", characters: "Count Cagliostro"],
    [actor: "Kohei Miyauchi", characters: "Gardener"],
    [actor: "Ichiro Nagai", characters: "Jodot"]
  ],
  synopsis: true
]
