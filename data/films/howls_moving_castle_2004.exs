[
  title: "Howl's Moving Castle",
  path: "howls-moving-castle-2004",
  original_title: "ハウルの動く城",
  transliteration: "Hauru No Ugoku Shiro",
  translation: "Howl's Moving Castle",
  release_date: ~D[2004-11-20],
  runtime: 119,
  produced_by: [
    "Tokuma Shoten",
    "Studio Ghibli",
    "Nippon TV",
    "Dentsu",
    "Disney",
    "Mitsubishi Corporation",
    "Toho"
  ],
  staff: [
    [roles: "Director", people: "Hayao Miyazaki"],
    [roles: "Producer", people: "Toshio Suzuki"],
    [roles: "Original Story", people: "Diana Wynn Jones"],
    [roles: "Screenplay", people: "Hayao Miyazaki"],
    [roles: "Music", people: "Joe Hisaishi"],
    [roles: "Art Director", people: ["Yoji Takeshige", "Noboru Yoshida"]],
    [roles: "Photography", people: "Atsushi Okui"],
    [roles: "Sound", people: "Kazuhiro Wakabayashi"],
    [roles: "Editor", people: "Takeshi Seyama"]
  ],
  synopsis: true,
  top_billed_cast: [
    [actor: "Chieko Baisho", characters: "Sophie"],
    [actor: "Takuya Kimura", characters: "Howl"],
    [actor: "Akihiro Miwa", characters: "The Witch of the Waste"],
    [actor: "Tatsuya Gashuin", characters: "Calcifer"],
    [actor: "Ryunosuke Kamiki", characters: "Markl"],
    [actor: "Mitsunori Isaki", characters: "Kosho"],
    [actor: "Yo Oizumi", characters: "Scarecrow Turnip"],
    [actor: "Akira Otsuka", characters: "The King"],
    [actor: "Daijiro Harada", characters: "Heen"],
    [actor: "Haruko Kato", characters: "Suliman"]
  ],
  other_cast: [
    [actor: "Rio Kanno", characters: "Madge"],
    [actor: "Yayoi Kazuki", characters: "Lettie"],
    [actor: "Mayuno Yasokawa", characters: "Honey"]
  ]
]
