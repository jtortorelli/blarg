[
  title: "Returner",
  path: "returner-2002",
  release_date: ~D[2002-08-31],
  runtime: 116,
  produced_by: ["Fuji Television", "Toho", "Amuse Pictures", "ROBOT", "Shirogumi", "IMAGICA"],
  staff: [
    [roles: ["Director", "Screenplay", "VFX"], people: "Takashi Yamazaki"],
    [roles: "Screenplay", people: "Kenya Hirata"],
    [roles: "Music", people: "Akihiko Matsumoto"],
    [roles: "Photography", people: ["Kozo Shibasaki", "Akira Sako"]],
    [roles: "Lighting", people: "Nariaki Ueda"],
    [roles: "Sound", people: ["Chuji Sato", "Yasushi Takana"]],
    [roles: "Art", people: "Anri Jojo"],
    [roles: "Editor", people: "Takuya Taguchi"],
    [roles: "Stunts", people: "Yuji Shimomura"],
    [roles: "Visual Effects Director", people: "Kiyoko Shibuya"],
    [roles: "Special Effects Director", people: "Katsuhiro Onoe"]
  ],
  top_billed_cast: [
    [actor: "Takeshi Kaneshiro", characters: "Miyamoto"],
    [actor: "Anne Suzuki", characters: "Millie"],
    [actor: "Yukiko Okamoto", characters: "Dr. Yagi"],
    [actor: "Mitsu Murata", characters: "Mizoguchi Thug"],
    [actor: "Kisuke Iida", characters: "Karasawa"],
    [actor: "Kazuya Shimizu", characters: "Murakami"],
    [actor: "Chiharu Kawai", characters: "Liu's Interpeter"],
    [actor: "Dean Harrington", characters: "Dr. Brown"],
    [actor: "Xiaoqun Zhao", characters: "Chinese Slave Merchant"],
    [actor: "Kanata Hongo", characters: "Young Miyamoto"],
    [actor: "Hoshi Ishida", characters: "Shifan"],
    [actor: "Masaya Takahashi", characters: "Liu Laoban"],
    [actor: "Shinji Higuchi", characters: "UFO Researcher"],
    [actor: "Makoto Kamiya", characters: "Wounded Future Soldier"],
    [actor: "Kirin Kiki", characters: "Xie"],
    [actor: "Goro Kishitani", characters: "Mizoguchi"]
  ],
  synopsis: true
]
