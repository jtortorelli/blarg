[
  title: "Godzilla 2000",
  path: "godzilla-2000-1999",
  original_title: "ゴジラ2000 ミレニアム",
  transliteration: "Gojira 2000 Mireniamu",
  translation: "Godzilla 2000 Millennium",
  release_date: ~D[1999-12-11],
  runtime: 108,
  preceded_by: {"Godzilla VS Destroyer", 1995},
  followed_by: {"Godzilla X Megaguirus", 2000},
  produced_by: "Toho",
  staff: [
    [roles: "Director", people: "Takao Okawara"],
    [roles: "Special Effects", people: "Kenji Suzuki"],
    [roles: "Producer", people: "Shogo Tomiyama"],
    [roles: "Screenplay", people: ["Hiroshi Kashiwabara", "Wataru Mimura"]],
    [roles: "Photography", people: "Yudai Kato"],
    [roles: "Art", people: "Takeshi Shimizu"],
    [roles: "Sound", people: "Teiichi Sato"],
    [roles: "Lighting", people: "Tsuyoshi Awakihara"],
    [roles: "Editor", people: "Yoshiyuki Okuhara"],
    [roles: "Music", people: "Takayuki Hattori"],
    [roles: "Physical Effects Assistant", people: "Katsuhiro Onoe"]
  ],
  top_billed_cast: [
    [actor: "Takehiro Murata", characters: "Yuji Shinoda"],
    [actor: "Hiroshi Abe", characters: "Mitsuo Katagiri"],
    [actor: "Naomi Nishida", characters: "Yuki Ichinose"],
    [actor: "Takeo Nakahara", characters: "SDF Chief Takada"],
    [actor: "Mayu Suzuki", characters: "Io Shinoda"],
    [actor: "Takeshi Obayashi", characters: "Secretary Gonno"],
    [actor: "Shiro Namiki", characters: "Minister Shiozaki"],
    [actor: "Sakae Kimura", characters: "Captain Ogawa"],
    [actor: "Shiro Sano", characters: "Shiro Miyasaka"]
  ],
  other_cast: [
    [actor: "Bengal", characters: "Sonoda"],
    [actor: "Makoto Ito", characters: "Orga"],
    [actor: "Tsutomu Kitagawa", characters: "Godzilla"],
    [actor: "Yoichi Nukumizu", characters: "Man on Train"],
    [actor: "Koichi Ueda", characters: "Power Plant Director (Voice)"]
  ],
  synopsis: true
]
