[
  title: "Godzilla vs. Gigan",
  path: "godzilla-vs-gigan-1972",
  original_title: "地球攻撃命令 ゴジラ対ガイガン",
  transliteration: "Chikyū Kōgeki Meirei Gojira Tai Gaigan",
  translation: "Earth Destruction Order: Godzilla Against Gigan",
  aliases: "Godzilla on Monster Island",
  release_date: ~D[1972-03-12],
  runtime: 89,
  preceded_by: {"Godzilla vs. Hedorah", 1971},
  followed_by: {"Godzilla vs. Megalon", 1973},
  produced_by: "Toho",
  staff: [
    [roles: "Director", people: "Jun Fukuda"],
    [roles: "Producer", people: "Tomoyuki Tanaka"],
    [roles: "Screenplay", people: "Shinichi Sekizawa"],
    [roles: "Photography", people: "Kiyoshi Hasegawa"],
    [roles: "Art", people: "Yoshifumi Honda"],
    [roles: "Sound", people: "Fumio Yanoguchi"],
    [roles: "Lighting", people: "Kojiro Sato"],
    [roles: "Music", people: "Akira Ifukube"],
    [roles: "Special Effects", people: "Teruyoshi Nakano"],
    [roles: "Editor", people: "Yoshio Tamura"]
  ],
  top_billed_cast: [
    [actor: "Hiroshi Ishikawa", characters: "Gengo Kotaka"],
    [actor: "Tomoko Umeda", characters: "Machiko Shima"],
    [actor: "Yuriko Hishimi", characters: "Tomoko Tomoe"],
    [actor: "Minoru Takashima", characters: "Shosaku Takasugi"],
    [actor: "Zan Fujita", characters: "Fumio Sudo"],
    [actor: "Toshiaki Nishizawa", characters: "Kubota"],
    [actor: "Kunio Murai", characters: "Takeshi Shima"],
    [actor: "Gen Shimizu", characters: "Defense Commander"],
    [actor: "Kuniko Ashihara", characters: "Mrs. Sudo"],
    [actor: "Zeko Nakamura", characters: "Priest"],
    [actor: "Shosei Muto", characters: "Kotaka's Editor"],
    [actor: "Naoya Kusakawa", characters: "Policeman"],
    [actor: "Wataru Omae", characters: "Alien Henchman"],
    [actor: "Haruo Nakajima", characters: "Godzilla"],
    [actor: "Koetsu Omiya", characters: "Anguirus"],
    [actor: "Kanta Ina", characters: "King Ghidorah"],
    [actor: "Kenpachiro Satsuma", characters: "Gigan"]
  ],
  other_cast: [
    [actor: "Tadashi Okabe", characters: "Policeman"]
  ],
  credits: true,
  synopsis: true
]
