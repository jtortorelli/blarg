[
  title: "The Golden Bat",
  path: "golden-bat-1966",
  original_title: "黄金 バット",
  transliteration: "Ōgon Batto",
  translation: "Golden Bat",
  release_date: ~D[1966-12-21],
  runtime: 73,
  produced_by: "Toei",
  staff: [
    [roles: "Director", people: "Hajime Sato"],
    [roles: "Original Story", people: "Takeo Nagamatsu"],
    [roles: "Screenplay", people: "Susumu Takaku"],
    [roles: "Photography", people: "Yoshikazu Yamasawa"],
    [roles: "Sound", people: "Yosuke Uchida"],
    [roles: "Lighting", people: "Kenzo Ginya"],
    [roles: "Art", people: "Shinichi Eno"],
    [roles: "Music", people: "Shunsuke Kikuchi"],
    [roles: "Editor", people: "Fumio Anda"]
  ],
  top_billed_cast: [
    [actor: "Shinichi Chiba", characters: "Dr. Yamatone"],
    [actor: "Hirohisa Nakata", characters: "Shimizu"],
    [actor: "Emily Hatoyama", characters: "Emily Pearl"],
    [actor: "Wataru Yamakawa", characters: "Akira Kazehaya"],
    [actor: "Tadahiko Sato", characters: "Golden Bat"],
    [actor: "Koji Sekiyama", characters: "Nazo"],
    [actor: "Yoichi Numata", characters: "Keloid"],
    [actor: "Keiko Kuni", characters: "Piranha"],
    [actor: "Keiichi Kitagawa", characters: "Jackal"],
    [actor: "Akira Katayama", characters: "Astronomer"],
    [actor: "Andrew Hughes", characters: "Dr. Pearl"],
    [actor: "Kosaku Okano", characters: "Nakamura"],
    [actor: "Yukio Aoshima", characters: "Policeman"],
    [actor: "Hisako Tsukuba", characters: "Naomi Akiyama"]
  ],
  credits: true,
  synopsis: true
]
