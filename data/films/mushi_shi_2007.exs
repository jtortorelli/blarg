[
  title: "Mushi-shi",
  path: "mushi-shi-2007",
  original_title: "蟲師",
  transliteration: "Mushishi",
  translation: "Bug Master",
  release_date: ~D[2007-03-24],
  runtime: 131,
  produced_by: [
    "Tohokushinsha",
    "Bandai Networks",
    "Toshiba Entertainment",
    "Pyramid Films",
    "Yahoo! JAPAN",
    "Tokyu Recreation",
    "Ogura Office"
  ],
  staff: [
    [roles: "Original Story", people: "Yuki Urushibara"],
    [roles: ["Director", "Screenplay"], people: "Katsuhiro Otomo"],
    [roles: "Screenplay", people: "Sadayuki Murai"],
    [roles: "Photography", people: "Takahide Shibanushi"],
    [roles: "Lighting", people: "Tatsuya Osada"],
    [roles: "Art", people: "Noriyoshi Ikeya"],
    [roles: "VFX Supervisor", people: "Nobuaki Koga"],
    [roles: "Music", people: "Kuniaki Haishima"],
    [roles: "Sound", people: "Zenda Kohara"],
    [roles: "Editor", people: "Soichi Ueno"]
  ],
  top_billed_cast: [
    [actor: "Joe Odagiri", characters: "Ginko"],
    [actor: "Nao Omori", characters: "Nijiro"],
    [actor: "Yu Aoi", characters: "Tanyu"],
    [actor: "Lily", characters: "Shoya Hostess"],
    [actor: "Reisen Ri", characters: "Tama"],
    [actor: "Makiko Esumi", characters: "Nui"]
  ],
  other_cast: [
    [actor: "Hideyuki Inada", characters: "Yoki"],
    [actor: "Keiko Inoue", characters: "Jochutake"],
    [actor: "Makiko Kuno", characters: "Mahi's Mother"],
    [actor: "Reia Moriyama", characters: "Mahi"],
    [actor: "Baku Numata", characters: "Nui's Husband"],
    [actor: "Hiro Otaka", characters: "Nui's Former Husband"],
    [actor: "Rintaro", characters: "Shoya Host"],
    [actor: "Shinji Rokkaku", characters: "Makino"],
    [actor: "Hideo Sakaki", characters: "Mahi's Father"],
    [actor: "Kazumasa Taguchi", characters: "Sakuji"],
    [actor: "Kenichiro Tanabe", characters: "Bunzo Shibayama"],
    [actor: "Masayo Umezawa", characters: "Shoya Maid"]
  ],
  synopsis: true
]
