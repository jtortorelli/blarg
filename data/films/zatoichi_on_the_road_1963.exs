[
  title: "Zatoichi on the Road",
  path: "zatoichi-on-the-road-1963",
  original_title: "座頭市喧嘩旅",
  transliteration: "Zatōichi Kenkatabi",
  translation: "Zatoichi Fighting Journey",
  release_date: ~D[1963-11-30],
  runtime: 88,
  preceded_by: {"Zatoichi the Fugitive", 1963},
  followed_by: {"Zatoichi and the Chest of Gold", 1964},
  produced_by: "Daiei",
  staff: [
    [roles: "Director", people: "Kimiyoshi Yasuda"],
    [roles: "Original Story", people: "Kan Shimozawa"],
    [roles: "Screenplay", people: "Minoru Inuzaka"],
    [roles: "Photography", people: "Shozo Honda"],
    [roles: "Sound", people: "Sakae Nagaoka"],
    [roles: "Lighting", people: "Hiroshi Mima"],
    [roles: "Art", people: "Yoshinobu Nishioka"],
    [roles: "Music", people: "Akira Ifukube"],
    [roles: "Editor", people: "Kanji Suganuma"]
  ],
  top_billed_cast: [
    [actor: "Shintaro Katsu", characters: "Zatoichi"],
    [actor: "Shiho Fujimura", characters: "Omitsu"],
    [actor: "Ryuzo Shimada", characters: "Jingoro"],
    [actor: "Matasaburo Niwa", characters: "Yamada"],
    [actor: "Yutaka Nakamura", characters: "Matsu"],
    [actor: "Reiko Fujiwara", characters: "Ohisa"],
    [actor: "Sonosuke Sawamura", characters: "Boss Tobei"],
    [actor: "Koichi Mizuhara", characters: "Kamashichi"],
    [actor: "Shosaku Sugiyama", characters: "Boss Hikozo"],
    [actor: "Yusaku Terashima", characters: "Passenger"],
    [actor: "Yoshio Yoshida", characters: "Boss Tomegoro"],
    [actor: "Tokio Oki", characters: "Sanzo"],
    [actor: "Yukio Horikita", characters: "Kawachi"],
    [actor: "Gen Kimura", characters: "Kiyono"],
    [actor: "Ichi Koshikawa", characters: "Kisuke"],
    [actor: "Gen Kuroki", characters: "Ronin"]
  ],
  other_cast: [
    [actor: "Teruko Omi", characters: "Mistress"]
  ],
  credits: true,
  synopsis: true
]
