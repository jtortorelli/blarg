[
  title: "Samaritan Zatoichi",
  path: "samaritan-zatoichi-1968",
  original_title: "座頭市喧嘩太鼓",
  transliteration: "Zatōichi Kenkadaiko",
  translation: "Zatoichi War Drum",
  release_date: ~D[1968-12-28],
  runtime: 82,
  preceded_by: {"Zatoichi and the Fugitives", 1968},
  followed_by: {"Zatoichi Meets Yojimbo", 1970},
  produced_by: "Daiei",
  staff: [
    [roles: "Director", people: "Kenji Misumi"],
    [roles: "Original Story", people: "Kan Shimozawa"],
    [roles: "Screenplay", people: ["Kiyokata Saruwaka", "Hisashi Sugiura", "Tetsuro Yoshida"]],
    [roles: "Photography", people: "Fujio Morita"],
    [roles: "Sound", people: "Yukio Kaibara"],
    [roles: "Lighting", people: "Shunji Kurokawa"],
    [roles: "Art", people: "Akira Naito"],
    [roles: "Music", people: "Sei Ikeno"],
    [roles: "Editor", people: "Toshio Taniguchi"],
    [roles: "Theme Song Performer", people: "Shintaro Katsu"]
  ],
  top_billed_cast: [
    [actor: "Shintaro Katsu", characters: "Zatoichi"],
    [actor: "Yoshiko Mita", characters: "Osode"],
    [actor: "Makoto Sato", characters: "Kashiwazaki"],
    [actor: "Ko Nishimura", characters: "Sosuke Saruya"],
    [actor: "Takuya Fujioka", characters: "Shinsuke"],
    [actor: "Chocho Miyako", characters: "Ohaya"],
    [actor: "Akira Shimizu", characters: "Boss Kumakichi"],
    [actor: "Ryutaro Gomi", characters: "Boss Sashichi"],
    [actor: "Saburo Date", characters: "Kanzo"],
    [actor: "Rokko Tora", characters: "Choji"],
    [actor: "Ryoichi Tamagawa", characters: "Chohachi"],
    [actor: "Machiko Soga", characters: "Osen"],
    [actor: "Osamu Okawa", characters: "Tokuji"],
    [actor: "Shosaku Sugiyama", characters: "Kinsuke"],
    [actor: "Gen Kimura", characters: "Daisaku Igawa"],
    [actor: "Teruko Omi", characters: "Osho"],
    [actor: "Kazue Tamaki", characters: "Innkeeper"],
    [actor: "Yukio Horikita", characters: "Isoyoshi"],
    [actor: "Yuji Hamada", characters: "Heiba"],
    [actor: "Jun Katsumura", characters: "Yakuza"],
    [actor: "Gen Kuroki", characters: "Samurai"],
    [actor: "Masayuki Nagatomo", characters: "Peasant Child"]
  ],
  credits: true,
  synopsis: true
]
