[
  title: "The Living Skeleton",
  path: "living-skeleton-1968",
  original_title: "吸血髑髏船",
  transliteration: "Kyūketsu Dokurosen",
  translation: "Blood Sucking Skeleton Ship",
  release_date: ~D[1968-11-09],
  runtime: 81,
  produced_by: "Shochiku",
  staff: [
    [roles: "Director", people: "Koki Matsuno"],
    [roles: "Screenplay", people: ["Kikuma Shimoizaka", "Kyuzo Kobayashi"]],
    [roles: "Photography", people: ["Masayuki Kato", "Takashi Akamatsu"]],
    [roles: "Art", people: "Kyohei Morita"],
    [roles: "Music", people: "Noboru Nishiyama"],
    [roles: "Lighting", people: "Takehiko Sakuma"],
    [roles: "Editor", people: "Kazuo Ota"],
    [roles: "Sound", people: "Hideo Kobayashi"]
  ],
  top_billed_cast: [
    [actor: "Kikko Matsuoka", characters: ["Yoriko", "Saeko"]],
    [actor: "Yasunori Irikawa", characters: "Mochizuki"],
    [actor: "Masumi Okada", characters: "Father Akashi / Tanuma"],
    [actor: "Asao Uchida", characters: "Eijiri"],
    [actor: "Asao Koike", characters: "Tsuji"],
    [actor: "Toshihiko Yamamoto", characters: "Ono"],
    [actor: "Keijiro Kikiyo", characters: "Policeman"],
    [actor: "Kaori Taniguchi", characters: "Mayumi"],
    [actor: "Keiko Yanagawa", characters: "Sanae Suetsugu"],
    [actor: "Nobuo Kaneko", characters: "Suetsugu"],
    [actor: "Ko Nishimura", characters: "Nishizato"]
  ],
  credits: true,
  synopsis: true
]
