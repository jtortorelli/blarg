[
  title: "Espy",
  path: "espy-1974",
  original_title: "エスパイ",
  transliteration: "Esupai",
  translation: "Espy",
  release_date: ~D[1974-12-28],
  runtime: 94,
  produced_by: "Toho",
  staff: [
    [roles: "Director", people: "Jun Fukuda"],
    [roles: "Co-Director", people: "Kenjiro Omori"],
    [roles: "Special Effects Director", people: "Teruyoshi Nakano"],
    [roles: "Producer", people: ["Tomoyuki Tanaka", "Fumio Tanaka"]],
    [roles: "Original Story", people: "Sakyo Komatsu"],
    [roles: "Screenplay", people: "Ei Ogawa"],
    [roles: "Photography", people: ["Shoji Ueda", "Kazumi Hara"]],
    [roles: "Art", people: "Shinobu Muraki"],
    [roles: "Sound", people: "Toshiya Ban"],
    [roles: "Lighting", people: "Masakuni Morimoto"],
    [roles: "Music", people: ["Masaaki Hirao", "Kensuke Kyo"]],
    [roles: "Editor", people: "Michiko Ikeda"]
  ],
  top_billed_cast: [
    [actor: "Hiroshi Fujioka", characters: "Yoshio Tamura"],
    [actor: "Kaoru Yumi", characters: "Maria Harada"],
    [actor: "Masao Kusakari", characters: "Jiro Miki"],
    [actor: "Eiji Okada", characters: "Master Sarabad"],
    [actor: "Goro Mutsumi", characters: "Teraoka"],
    [actor: "Katsumasa Uchida", characters: "Goro Tatsumi"],
    [actor: "Luna Takamura", characters: "Julietta"],
    [actor: "Hatsuo Yamaya", characters: "Boru"],
    [actor: "Jimmy Shaw", characters: "Godnov"],
    [actor: "Andrew Hughes", characters: "Espy Chief"],
    [actor: "Steve Green", characters: "Baltonian Prime Minister"],
    [actor: "Willy Dorsey", characters: "Abdullah"],
    [actor: "Ralph Jesser", characters: "Evil Espy"],
    [actor: "Yoshio Katsube", characters: "Man in Garage"],
    [actor: "Yuzo Kayama", characters: "Hojo"],
    [actor: "Tomisaburo Wakayama", characters: "Ulrov"]
  ],
  other_cast: [
    [actor: "Robert Dunham", characters: "Pilot"]
  ],
  credits: true,
  synopsis: true
]
