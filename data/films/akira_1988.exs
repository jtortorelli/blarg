[
  title: "Akira",
  path: "akira-1988",
  original_title: "アキラ",
  transliteration: "Akira",
  translation: "Akira",
  release_date: ~D[1988-07-16],
  runtime: 124,
  produced_by: [
    "Kodansha",
    "Mainichi Broadcasting System",
    "Bandai",
    "Hakuhodo",
    "Toho",
    "Laserdisc",
    "Sumitomo",
    "Tokyo Movie Shinsha"
  ],
  staff: [
    [roles: "Director", people: "Katsuhiro Otomo"],
    [roles: "Screenplay", people: ["Katsuhiro Otomo", "Izo Hashimoto"]],
    [roles: "Art Director", people: "Toshiharu Mizutani"],
    [roles: "Photography", people: "Katsuji Misawa"],
    [roles: "Sound", people: "Susumu Aketagawa"],
    [roles: "Music", people: "Tsutomu Ohashi"],
    [roles: "Editor", people: "Takeshi Seyama"]
  ],
  synopsis: true,
  top_billed_cast: [
    [actor: "Mitsuo Iwata", characters: "Shotaro Kaneda"],
    [actor: "Nozomu Sasaki", characters: "Tetsuo Shima"],
    [actor: "Mami Koyama", characters: "Kei"],
    [actor: "Tetsusho Genda", characters: "Ryu"],
    [actor: "Hiroshi Otake", characters: "Nezu"],
    [actor: "Koichi Kitamura", characters: "Miyako"],
    [actor: "Yuriko Fuchizaki", characters: "Kaori"],
    [actor: "Masaaki Okura", characters: "Yamagata"],
    [actor: "Takeshi Kusao", characters: "Kai"],
    [actor: "Yosuke Akimoto", characters: "Bartender"],
    [actor: "Kazuhiro Kamifuji", characters: "Masaru"],
    [actor: "Tatsuhiko Nakamura", characters: "Takashi"],
    [actor: "Fukue Ito", characters: "Kiyoko"],
    [actor: "Taro Ishida", characters: "Captain Shikishima"],
    [actor: "Mizuho Suzuki", characters: "Doctor Onishi"]
  ]
]
