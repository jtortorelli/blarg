[
  title: "Ultraman",
  path: "ultraman-2004",
  release_date: ~D[2004-12-18],
  runtime: 97,
  produced_by: [
    "Tsuburaya Productions",
    "Bandai",
    "Bandai Visual",
    "TBS",
    "Chubu Nippon Broadcasting",
    "Nippon Shuppan Hanbai",
    "Dentsu",
    "Shochiku"
  ],
  staff: [
    [roles: "Director", people: "Kazuya Konaka"],
    [roles: "Special Effects Director", people: "Yuichi Kikuchi"],
    [roles: "Music Supervisor", people: "Takahiro Matsumoro"],
    [roles: "Producer", people: "Kazuo Tsuburaya"],
    [roles: "Music", people: ["Masazumi Ozawa", "Daisuke Ikeda", "Shingo Kamata"]],
    [roles: "Screenplay", people: "Keiichi Hasegawa"],
    [roles: ["Photography", "VFX Supervisor"], people: "Shinichi Oka"],
    [roles: "Art Director", people: "Tetsuzo Ozawa"],
    [roles: "Lighting", people: "Masakatsu Izumi"],
    [roles: "Sound", people: ["Yutaka Tsurumaki", "Jin Tsurumaki"]],
    [roles: "Editor", people: "Akira Matsuki"]
  ],
  top_billed_cast: [
    [actor: "Tetsuya Bessho", characters: "Shunichi Maki"],
    [actor: "Kyoko Toyama", characters: "Sara Mizuhara"],
    [actor: "Kenya Osumi", characters: ["Takafumi Udo", "The One (Voice)"]],
    [actor: "Nae Yuki", characters: "Yoko Maki"],
    [actor: "Ryohei Hirota", characters: "Tsugumu Maki"],
    [actor: "Eisuke Tsunoda", characters: "Ippei"],
    [actor: "Yumiko Sato", characters: "Yuriko"],
    [actor: "Kengo Komada", characters: "Announcer"],
    [actor: "Keiji Hasegawa", characters: "Ultraman the Next"],
    [actor: "Satoshi Yamamoto", characters: ["The One (Stage 2)", "The One (Stage 3)"]],
    [actor: "Junya Iwamoto", characters: "The One (Stage 1)"],
    [actor: "Hideyuki Tanaka", characters: "Ultraman the Next (Voice)"],
    [actor: "Katsuyuki Konishi", characters: "The One (Voice)"],
    [actor: "Daisuke Ryu", characters: "Sogabe"],
    [actor: "Toshiya Nagasawa", characters: "Tsuyoshi Kurashima"],
    [actor: "Masao Kusakari", characters: "Manjome"]
  ],
  other_cast: [
    [actor: "Yoichi Okamura", characters: "Shopkeeper"],
    [actor: "Edo Yamaguchi", characters: "SDF Air Commander"],
    [actor: "Makiko Yasumuro", characters: "Shopkeeper's Wife"]
  ],
  synopsis: true
]
