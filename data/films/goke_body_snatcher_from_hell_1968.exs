[
  title: "Goke, Body Snatcher from Hell",
  path: "goke-body-snatcher-from-hell-1968",
  original_title: "吸血鬼ゴケミドロ",
  transliteration: "Kyūketsuki Gokemidoro",
  translation: "Vampire Gokemidoro",
  release_date: ~D[1968-08-14],
  runtime: 84,
  produced_by: "Shochiku",
  staff: [
    [roles: "Director", people: "Hajime Sato"],
    [roles: "Screenplay", people: ["Susumu Takaku", "Kyuzo Kobayashi"]],
    [roles: "Photography", people: "Shizuo Hirase"],
    [roles: "Art", people: "Yunota Yoshino"],
    [roles: "Music", people: "Shunsuke Kikuchi"],
    [roles: "Lighting", people: "Tatsuo Aomoto"],
    [roles: "Editor", people: "Akimitsu Terada"],
    [roles: "Sound", people: "Hiroshi Nakamura"]
  ],
  top_billed_cast: [
    [actor: "Teruo Yoshida", characters: "Sugisaka"],
    [actor: "Tomomi Sato", characters: "Kazumi Asakura"],
    [actor: "Hideo Ko", characters: "Hirofumi Teraoka (The Hijacker)"],
    [actor: "Masaya Takahashi", characters: "Toshiyuki Saga"],
    [actor: "Nobuo Kaneko", characters: "Tokuyasu"],
    [actor: "Eizo Kitamura", characters: "Gozo Mano"],
    [actor: "Yuko Kusunoki", characters: "Noriko Tokuyasu"],
    [actor: "Kazuo Kato", characters: "Hyakutake"],
    [actor: "Hiroyuki Nishimoto", characters: "Airplane Captain"],
    [actor: "Toshihiko Yamamoto", characters: "Matsumiya"],
    [actor: "Kathy Horan", characters: "Mrs. Neal"]
  ],
  other_cast: [
    [actor: "Harold S. Conway", characters: "UK Ambassador"]
  ],
  credits: true,
  synopsis: true
]
