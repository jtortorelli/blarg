[
  title: "Rescue Wings",
  path: "rescue-wings-2008",
  original_title: "空へ 救いの翼 RESCUE WINGS",
  transliteration: "Sorae Sukui No Tsubasa Rescue Wings",
  translation: "To the Sky, Wings of Salvation: Rescue Wings",
  release_date: ~D[2008-12-13],
  runtime: 108,
  produced_by: [
    "First Pictures",
    "Kadokawa Films",
    "Northern Japan Maritime Affairs",
    "Vision West",
    "Amuse Soft Entertainment",
    "Sankei Shimbun"
  ],
  staff: [
    [roles: "Director", people: "Masaaki Tezuka"],
    [roles: "Original Story", people: "Tommy Otsuka"],
    [
      roles: "Screenplay",
      people: ["Tadashi Naito", "Seishi Minakami", "Masaaki Tezuka", "Kazuki Omori"]
    ],
    [roles: "Photography", people: "Yudai Kato"],
    [roles: "Lighting", people: "Nobuhiro Shibata"],
    [roles: "Art", people: "Yukiharu Seshimo"],
    [roles: "Sound", people: "Soichi Inoue"],
    [roles: "Editor", people: "Akimasa Kawashima"],
    [roles: "Music", people: "Kaoru Wada"]
  ],
  top_billed_cast: [
    [actor: "Yuko Takayama", characters: "Haruka Kawashima"],
    [actor: "Dai Watanabe", characters: "Kotaro Senami"],
    [actor: "Shunya Isaka", characters: "Ryuhei Oda"],
    [actor: "Ken Kaneko", characters: "Tsuyoshi Yokosuka"],
    [actor: "Taiki Nakabayashi", characters: "Kenji Osaka"],
    [actor: "Shunta Nakamura", characters: "Daisuke Kakizaki"],
    [actor: "Junichi Haruta", characters: "Shigeo Waseda"],
    [actor: "Ichirota Miyakawa", characters: "Harusame Deputy Chief"],
    [actor: "Seina Suzuki", characters: "Aoi Katsunuma"],
    [actor: "Saki Seto", characters: "Meiko Yokoyama"],
    [actor: "Miyoko Asada", characters: "Chieko Kawashima"],
    [actor: "Masatoshi Nakamura", characters: "Ijima"],
    [actor: "Yoshino Kimura", characters: "Mina Takasu"],
    [actor: "Tomokazu Miura", characters: "Yasuo Kikuta"]
  ],
  synopsis: true
]
