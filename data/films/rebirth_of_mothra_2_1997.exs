[
  title: "Rebirth of Mothra 2",
  path: "rebirth-of-mothra-2-1997",
  original_title: "モスラ2 海底の大決戦",
  transliteration: "Mosura 2 Kaitei No Kessen",
  translation: "Mothra 2: Sea Battle",
  release_date: ~D[1997-12-13],
  runtime: 100,
  preceded_by: {"Rebirth of Mothra", 1996},
  followed_by: {"Rebirth of Mothra 3", 1998},
  produced_by: "Toho",
  staff: [
    [roles: "Director", people: "Kunio Miyoshi"],
    [roles: "Special Effects Director", people: "Koichi Kawakita"],
    [roles: "Original Story", people: "Tomoyuki Tanaka"],
    [roles: "Producer", people: "Shogo Tomiyama"],
    [roles: "Screenplay", people: "Masumi Suetani"],
    [roles: "Music", people: "Toshiyuki Watanabe"],
    [roles: "Photography", people: "Yoshinori Sekiguchi"],
    [roles: "Art", people: "Takeshi Shimizu"],
    [roles: "Sound Director", people: "Noboru Ikeda"],
    [roles: "Lighting", people: "Teruo Osawa"],
    [roles: "Editor", people: "Miho Yoneda"],
    [roles: "Assistant Director", people: "Masaaki Tezuka"],
    [roles: "Special Effects Assistant Director", people: "Kenji Suzuki"]
  ],
  top_billed_cast: [
    [actor: "Megumi Kobayashi", characters: "Moll"],
    [actor: "Sayaka Yamaguchi", characters: "Lora"],
    [actor: "Hikari Mitsushima", characters: "Shiori Uranai"],
    [actor: "Masanao Shimada", characters: "Yoji Miyagi"],
    [actor: "Masaki Otake", characters: "Kohei Watakuchi"],
    [actor: "Atsushi Okuno", characters: "Mikio Kotani"],
    [actor: "Hajime Okayama", characters: "Junichi Nagase"],
    [actor: "Maho Nonami", characters: "Yuna"],
    [actor: "Aki Hano", characters: "Belvera"]
  ],
  other_cast: [
    [actor: "Fumie Hosokawa", characters: "Akiko Kimura"],
    [actor: "Misako Konno", characters: "Toshiko Uranai"],
    [actor: "Masahiro Sato", characters: "Tatsuzo Itoman"],
    [actor: "Mizuho Yoshida", characters: "Dagara"]
  ],
  synopsis: true
]
