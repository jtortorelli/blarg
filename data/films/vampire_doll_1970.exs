[
  title: "Vampire Doll",
  path: "vampire-doll-1970",
  original_title: "幽霊屋敷の恐怖 血を吸う人形",
  transliteration: "Yūreiyashiki No Kyōfu Chiwosū Ningyō",
  translation: "Horror of Haunted House: Bloodsucking Doll",
  release_date: ~D[1970-07-04],
  runtime: 71,
  followed_by: {"Lake of Dracula", 1971},
  produced_by: "Toho",
  staff: [
    [roles: "Director", people: "Michio Yamamoto"],
    [roles: "Producer", people: ["Tomoyuki Tanaka", "Fumio Tanaka"]],
    [roles: "Screenplay", people: ["Ei Ogawa", "Hiroshi Nagano"]],
    [roles: "Photography", people: "Kazumi Hara"],
    [roles: "Art", people: "Yoshifumi Honda"],
    [roles: "Sound", people: "Minoru Tomita"],
    [roles: "Lighting", people: "Kojiro Sato"],
    [roles: "Music", people: "Riichiro Manabe"],
    [roles: "Editor", people: "Koichi Iwashita"]
  ],
  top_billed_cast: [
    [actor: "Kayo Matsuo", characters: "Keiko Sagawa"],
    [actor: "Akira Nakao", characters: "Hiroshi Takagi"],
    [actor: "Yukiko Kobayashi", characters: "Yuko Nonomura"],
    [actor: "Yoko Minakaze", characters: "Shizu Nonomura"],
    [actor: "Atsuo Nakamura", characters: "Kazuhiko Sagawa"],
    [actor: "Kaku Takashina", characters: "Genzo"],
    [actor: "Shin Hamamura", characters: "Officer"],
    [actor: "Kenzo Sekiguchi", characters: "Gas Station Attendant"],
    [actor: "Sachio Sakai", characters: "Taxi Driver"],
    [actor: "Tadao Futami", characters: "Coolie"],
    [actor: "Jun Usami", characters: "Dr. Junnosuke Yamaguchi"]
  ],
  credits: true,
  synopsis: true
]
