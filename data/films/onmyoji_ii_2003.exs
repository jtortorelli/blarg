[
  title: "Onmyoji II",
  path: "onmyoji-ii-2003",
  original_title: "陰陽師II",
  transliteration: "Onmyōji II",
  translation: "Yin Yang Master II",
  release_date: ~D[2003-10-04],
  runtime: 115,
  preceded_by: {"Onmyoji", 2001},
  produced_by: ["Tohokushinsha", "TBS", "Dentsu", "Kadokawa Shoten", "Toho", "MBS"],
  staff: [
    [roles: "Director", people: "Yojiro Takita"],
    [roles: "Original Story", people: "Baku Yumemakura"],
    [roles: "Screenplay", people: ["Itaru Era", "Baku Yumemakura", "Yojiro Takita"]],
    [roles: "Photography", people: "Takeshi Hamada"],
    [roles: "Lighting", people: "Tatsuya Osada"],
    [roles: "Art", people: "Kyoko Heya"],
    [roles: "Sound", people: "Osamu Onodera"],
    [roles: "Editor", people: "Nobuko Tomita"],
    [roles: "Music", people: "Shigeru Umebayashi"],
    [roles: "Makeup Effects Supervisor", people: "Tomo Haraguchi"],
    [roles: "Special Effects Director", people: "Katsuhiro Onoe"]
  ],
  top_billed_cast: [
    [actor: "Mansai Nomura", characters: "Abe No Seimei"],
    [actor: "Hideaki Ito", characters: "Minamoto No Hiromasa"],
    [actor: "Eriko Imai", characters: "Mitsumushi"],
    [actor: "Kyoko Fukada", characters: "Fujiwara No Himiko"],
    [actor: "Yuko Kotegawa", characters: "Tsukiyomi"],
    [actor: "Kiichi Nakai", characters: "Genkaku"]
  ],
  other_cast: [
    [actor: "Dai Fuji", characters: "Tajikarao"],
    [actor: "Yukijiro Hotaru", characters: "The Mikado"],
    [actor: "Masato Ibu", characters: "Fujiwara No Yasumaro"],
    [actor: "Hayato Ichihara", characters: "Susa"],
    [actor: "Ayumu Saito", characters: "Fujiwara No Kanemichi"],
    [actor: "Hiromitsu Suzuki", characters: "Hira No Tamenari"],
    [actor: "Tatsuo Yamada", characters: "Sanzen No Yukinori"],
    [actor: "Kenji Yamaki", characters: "Tachibana No Ukon"],
    [actor: "Kazutoshi Yokoyama", characters: "Susano-o"]
  ],
  synopsis: true
]
