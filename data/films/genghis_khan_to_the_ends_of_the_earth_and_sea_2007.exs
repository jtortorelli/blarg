[
  title: "Genghis Khan: To the Ends of the Earth and Sea",
  path: "genghis-khan-to-the-ends-of-the-earth-and-sea-2007",
  original_title: "蒼き狼 地果て海尽きるまで",
  transliteration: "Aoki Ōkami Chisate Umitsukiru Made",
  translation: "Blue Wolf: To the Ends of the Earth and Sea",
  release_date: ~D[2007-03-03],
  runtime: 136,
  produced_by: [
    "Haruki Kadokawa Office",
    "Avex Entertainment",
    "Shochiku",
    "Fields",
    "FM Tokyo",
    "Yomiuri Shimbun Tokyo",
    "Yahoo",
    "Japan Airlines",
    "HIS"
  ],
  staff: [
    [roles: "Director", people: "Shinichiro Sawai"],
    [roles: "Executive Producer", people: "Haruki Kadokawa"],
    [roles: "Original Story", people: "Seiichi Morimura"],
    [roles: "Screenplay", people: ["Takehiro Nakajima", "Shoichi Maruyama"]],
    [roles: "Music", people: ["Taro Iwashiro", "Kiyoshi Yoshikawa"]],
    [roles: "Photography", people: "Yonezo Maeda"],
    [roles: "Lighting", people: "Kazuo Yabe"],
    [roles: "Sound", people: "Kenichi Beniya"],
    [roles: "Art Director", people: ["Katsumi Nakazawa", "Shigeyuki Kondo"]],
    [roles: "Editor", people: "Akimasa Kawashima"]
  ],
  top_billed_cast: [
    [actor: "Takashi Sorimachi", characters: "Temujin / Genghis Khan"],
    [actor: "Rei Kikukawa", characters: "Borte"],
    [actor: "Mayumi Wakamura", characters: "Hoelun"],
    [actor: "Kenichi Matsuyama", characters: "Jochi"],
    [actor: "Ara", characters: "Kulan"],
    [actor: "Yoshihiko Hakamada", characters: "Qasar"],
    [actor: "Eugene Nomura", characters: "Bo'orchu"],
    [actor: "Yusuke Hirayama", characters: "Jamuqa"],
    [actor: "Sosuke Ikematsu", characters: "Young Temujin"],
    [actor: "Togo Shimura", characters: "Munrik"],
    [actor: "Toshiya Nagasawa", characters: "Dayan"],
    [actor: "Shunsuke Kariya", characters: "Charaka"],
    [actor: "Kazuko Imai", characters: "Coaquchin"],
    [actor: "Ryo Karato", characters: "Yege Chiled"],
    [actor: "Satoshi Jimbo", characters: "Talgutei"],
    [actor: "Takeshi Obayashi", characters: "Caravan Leader"],
    [actor: "Hisaya Wakabayashi", characters: "Teichal"],
    [actor: "Kairi Narita", characters: "Belgutei"],
    [actor: "Kachiwo Endo", characters: "Todoen-Girte"],
    [actor: "Kohei Kumai", characters: "Young Jamuqa"],
    [actor: "Aimi Takeishi", characters: "Young Borte"],
    [actor: "Rihoko Shimomiya", characters: "Temulun"],
    [actor: "Shohei Yamazaki", characters: "Bekter"],
    [actor: "Naoki Hosaka", characters: "Yesukhei Baghatur"],
    [actor: "Takaaki Enoki", characters: "Dei Seichun"],
    [actor: "Masahiko Tsugawa", characters: "Khequchu"],
    [actor: "Hiroki Matsukata", characters: "Toghrul Khan"]
  ],
  synopsis: true
]
