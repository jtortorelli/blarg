[
  title: "Spirited Away",
  path: "spirited-away-2001",
  original_title: "千と千尋の神隠し",
  transliteration: "Sen To Chihiro No Kamekakushi",
  translation: "Sen and Chihiro's Great Disappearance",
  release_date: ~D[2001-07-20],
  runtime: 124,
  produced_by: [
    "Tokuma Shoten",
    "Studio Ghibli",
    "Nippon TV",
    "Dentsu",
    "Disney",
    "Tohokushinsha",
    "Mitsubishi Corporation"
  ],
  staff: [
    [roles: "Director", people: "Hayao Miyazaki"],
    [roles: "Producer", people: "Toshio Suzuki"],
    [roles: "Executive Producer", people: "Yasuyoshi Tokuma"],
    [roles: ["Original Story", "Screenplay"], people: "Hayao Miyazaki"],
    [roles: "Music", people: "Joe Hisaishi"],
    [roles: "Art Director", people: "Yoji Takeshige"],
    [roles: "Assistant Art Director", people: "Noboru Yoshida"],
    [roles: "Photography", people: "Atsushi Okui"],
    [roles: "Sound", people: "Kazuhiro Wakabayashi"],
    [roles: "Editor", people: "Takeshi Seyama"]
  ],
  top_billed_cast: [
    [actor: "Rumi Hiiragi", characters: "Chihiro Ogino"],
    [actor: "Miyu Irino", characters: "Haku"],
    [actor: "Mari Natsuki", characters: ["Yubaba", "Zeniba"]],
    [actor: "Takashi Naito", characters: "Akio Ogino"],
    [actor: "Yasuko Sawaguchi", characters: "Yuko Ogino"],
    [actor: "Tatsuya Gashuin", characters: "Aogaeru"],
    [actor: "Ryunosuke Kamiki", characters: "Bo"],
    [actor: "Yumi Tamai", characters: "Lin"],
    [actor: "Yo Oizumi", characters: "Bandaigaeru"],
    [actor: "Koba Hayashi", characters: "River God"],
    [actor: "Tsunehiko Kamijo", characters: "Chichiyaku"],
    [actor: "Takehiko Ono", characters: "Aniyaku"],
    [actor: "Bunta Sugawara", characters: "Kamaji"]
  ],
  synopsis: true
]
