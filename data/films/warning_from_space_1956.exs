[
  title: "Warning from Space",
  path: "warning-from-space-1956",
  original_title: "宇宙人東京に現わる",
  transliteration: "Uchūjin Tōkyō Ni Arawaru",
  translation: "Space Men Appear in Tokyo",
  release_date: ~D[1956-01-29],
  runtime: 82,
  produced_by: "Daiei",
  staff: [
    [roles: "Director", people: "Koji Shima"],
    [roles: "Producer", people: "Masaichi Nagata"],
    [roles: "Original Story", people: "Gentaro Nakajima"],
    [roles: "Screenplay", people: "Hideo Oguni"],
    [roles: "Photography", people: "Kimio Watanabe"],
    [roles: "Sound", people: "Norikazu Nishii"],
    [roles: "Lighting", people: "Koichi Kubota"],
    [roles: "Art", people: "Shigeo Kanno"],
    [roles: "Music", people: "Seitaro Omori"],
    [roles: "Editor", people: "Toyo Suzuki"]
  ],
  top_billed_cast: [
    [actor: "Keizo Kawasaki", characters: "Dr. Toru Isobe"],
    [actor: "Toyomi Karita", characters: ["Hikari Aozora", "Ginko Amano"]],
    [actor: "Satoshi Yagisawa", characters: "No. 2 Pairan"],
    [actor: "Isao Yamagata", characters: "Dr. Eisuke Matsuda"],
    [actor: "Shozo Nanbu", characters: "Dr. Naotaro Isobe"],
    [actor: "Bontaro Miake", characters: "Dr. Yoshio Komura"],
    [actor: "Mieko Nagai", characters: "Tomoko Komura"],
    [actor: "Kiyoko Hirai", characters: "Kiyoko Matsuda"],
    [actor: "Fumiko Okamura", characters: "Hostess"],
    [actor: "Junji Kawahara", characters: "Dr. Takashima"],
    [actor: "Sho Natsuki", characters: "Pairan"],
    [actor: "Yasushi Sugita", characters: "Reporter"],
    [actor: "Shizuharu Izumi", characters: "Industrial Thief"]
  ],
  credits: true,
  synopsis: true
]
