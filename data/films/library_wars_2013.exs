[
  title: "Library Wars",
  path: "library-wars-2013",
  original_title: "図書館戦争",
  transliteration: "Toshōkan Sensō",
  translation: "Library War",
  release_date: ~D[2013-04-27],
  runtime: 128,
  followed_by: {"Library Wars: The Last Mission", 2015},
  produced_by: [
    "TBS TV",
    "Kadokawa Shoten",
    "Toho",
    "J Storm",
    "Sedic International",
    "CBC",
    "MBS",
    "WOWOW",
    "Mainichi Shimbun",
    "HBC"
  ],
  staff: [
    [roles: "Director", people: "Shinsuke Sato"],
    [roles: "Original Story", people: "Hiro Arikawa"],
    [roles: "Screenplay", people: "Akiko Nogi"],
    [roles: "Music", people: "Yu Takami"],
    [roles: "Photography", people: "Taro Kawazu"],
    [roles: "Art", people: "Iwao Saito"],
    [roles: "Sound", people: "Kazujiko Yokono"],
    [roles: "Editor", people: "Tsuyoshi Imai"],
    [roles: "VFX Supervisor", people: "Makoto Kamiya"],
    [roles: "Action Director", people: "Yuji Shimomura"]
  ],
  top_billed_cast: [
    [actor: "Junichi Okada", characters: "Atsushi Dojo"],
    [actor: "Nana Eikura", characters: "Iku Kasahara"],
    [actor: "Kei Tanaka", characters: "Mikihisa Komaki"],
    [actor: "Sota Fukushi", characters: "Hikaru Tezuka"],
    [actor: "Naomi Nishida", characters: "Maki Origuchi"],
    [actor: "Jun Hashimoto", characters: "Ryusuke Genda"],
    [actor: "Chiaki Kuriyama", characters: "Asako Shibasaki"],
    [actor: "Koji Ishizaka", characters: "Iwao Nishina"]
  ],
  other_cast: [
    [actor: "Joji Abe", characters: "Shinsuke Itagaki"],
    [actor: "Kazuyuki Aijima", characters: "Bitani"],
    [actor: "Kazuki Namioka", characters: "Shindo"],
    [actor: "Motoki Ochiai", characters: "Hiraga's Assistant"],
    [actor: "Kyusaku Shimada", characters: "Hiraga"],
    [actor: "Kazuma Suzuki", characters: "Kenji Takeyama"],
    [actor: "Ryushin Tei", characters: "Akiya Ogata"]
  ],
  synopsis: true
]
