[
  title: "Alive",
  path: "alive-2003",
  release_date: ~D[2003-06-21],
  produced_by: [
    "KSS",
    "Eisei Gekijo",
    "NTT Learning Systems",
    "The Klockworx",
    "Hammers",
    "Nikkatsu",
    "Suplex",
    "Skyworks",
    "Napalm Films"
  ],
  staff: [
    [roles: "Director", people: "Ryuhei Kitamura"],
    [roles: "Screenplay", people: ["Ryuhei Kitamura", "Yudai Yamaguchi", "Isao Kiriyama"]],
    [roles: "Original Story", people: "Tsutomu Takahashi"],
    [roles: "Photography", people: "Takumi Furuya"],
    [roles: "Lighting", people: "Fumihiko Tamura"],
    [roles: "Art", people: "Yuji Hayashida"],
    [roles: "Editor", people: "Shuichi Kakesu"],
    [roles: "Music", people: ["Nobuhiko Morino", "Daisuke Yano"]],
    [roles: "Action Choreographer", people: "Yuji Shimomura"],
    [roles: "2nd Unit Director", people: "Yudai Yamaguchi"],
    [roles: "Title Design", people: "Ryuhei Kitamura"]
  ],
  top_billed_cast: [
    [actor: "Hideo Sakaki", characters: "Tenshu Yashiro"],
    [actor: "Ryo", characters: "Yurika Saegusa"],
    [actor: "Koyuki", characters: "Asuka Saegusa"],
    [actor: "Shun Sugata", characters: "Matsuda"],
    [actor: "Erika Oda", characters: "Misako Hara"],
    [actor: "Tak Sakaguchi", characters: "Zeros"],
    [actor: "Jun Kunimura", characters: "Kojima"],
    [actor: "Renji Ishibashi", characters: "Chief"],
    [actor: "Bengal", characters: "Tokutake"],
    [actor: "Tetta Sugimoto", characters: "Gondo"]
  ],
  other_cast: [
    [actor: "Yuichiro Arai", characters: "SWAT"],
    [actor: "Kenji Matsuda", characters: "SWAT Leader"],
    [actor: "Minoru Matsumoto", characters: "SWAT"],
    [actor: "Kazuhito Oba", characters: "SWAT"],
    [actor: "Yoji Tanaka", characters: "Hair Cut Guard"]
  ],
  synopsis: true
]
