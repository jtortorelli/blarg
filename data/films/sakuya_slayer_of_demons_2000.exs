[
  title: "Sakuya, Slayer of Demons",
  path: "sakuya-slayer-of-demons-2000",
  original_title: "さくや妖怪伝",
  transliteration: "Sakuya Yokaiden",
  translation: "Sakuya Yokai Legend",
  aliases: "Sakuya: The Sword of Darkness",
  release_date: ~D[2000-08-12],
  runtime: 88,
  produced_by: ["Warner Bros.", "Towani"],
  staff: [
    [roles: ["Original Story", "Director"], people: "Tomo Haraguchi"],
    [roles: "Special Effects Director", people: "Shinji Higuchi"],
    [roles: "Special Effects Supervisor", people: "Katsuhiro Onoe"],
    [roles: "Screenplay", people: "Kimiaki Mitsumasu"],
    [roles: "Photography", people: "Shoji Ehara"],
    [roles: "Music", people: "Kenji Kawai"],
    [roles: "Art", people: "Tetsuo Harada"],
    [roles: "Sound", people: "Toyohira Nakaji"],
    [roles: "Lighting", people: "Hiroyoshi Tsuno"],
    [roles: "Editor", people: "Hiroshi Okuda"]
  ],
  top_billed_cast: [
    [actor: "Nozomi Ando", characters: "Sakuya Sakaki"],
    [actor: "Shuichi Yamauchi", characters: "Taro Sakaki"],
    [actor: "Kyusaku Shimada", characters: "Shuzo Nikarasu"],
    [actor: "Keiichiro Sakagi", characters: "Hyoe Enki"],
    [actor: "Moeko Ezawa", characters: "Old Woman"],
    [actor: "Shinya Tsukamoto", characters: "Puppeteer"],
    [actor: "Yuki Kuroda", characters: "Shigeyuki Yamato"],
    [actor: "Hidehiko Ishikura", characters: "Zennosuke Tachibana"],
    [actor: "Hiroshi Fujioka", characters: "Yoshiaki Sakaki"],
    [actor: "Naoto Takenaka", characters: "Narrator"],
    [actor: "Tetsuro Tamba", characters: "Naoki Ii"],
    [actor: "Keiko Matsuzaka", characters: "The Spider Queen"]
  ],
  synopsis: true
]
