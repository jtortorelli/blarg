[
  title: "Rebirth of Mothra",
  path: "rebirth-of-mothra-1996",
  original_title: "モスラ",
  transliteration: "Mosura",
  translation: "Mothra",
  release_date: ~D[1996-12-14],
  runtime: 106,
  followed_by: {"Rebirth of Mothra 2", 1997},
  produced_by: "Toho",
  staff: [
    [roles: "Director", people: "Okihiro Yoneda"],
    [roles: "Special Effects Director", people: "Koichi Kawakita"],
    [roles: "Original Story", people: "Tomoyuki Tanaka"],
    [roles: "Producer", people: "Shogo Tomiyama"],
    [roles: "Screenplay", people: "Masumi Suetani"],
    [roles: "Music", people: "Toshiyuki Watanabe"],
    [roles: "Photography", people: "Yoshinori Sekiguchi"],
    [roles: "Art", people: "Kyoko Heya"],
    [roles: "Sound", people: "Kazuo Miyauchi"],
    [roles: "Lighting", people: "Teruo Osawa"],
    [roles: "Editor", people: "Nobuo Ogawa"],
    [roles: "Special Effects Assistant Director", people: "Kenji Suzuki"]
  ],
  top_billed_cast: [
    [actor: "Megumi Kobayashi", characters: "Moll"],
    [actor: "Sayaka Yamaguchi", characters: "Lora"],
    [actor: "Aki Hano", characters: "Belvera"],
    [actor: "Kazuki Futami", characters: "Taiki Goto"],
    [actor: "Maya Fujisawa", characters: "Wakaba Goto"],
    [actor: "Nagare Hagiwara", characters: "Yoshinori Tagawa"],
    [actor: "Akira Terao", characters: "Doctor"],
    [actor: "Hitomi Takahashi", characters: "Makiko Goto"],
    [actor: "Kenjiro Nashimoto", characters: "Yuichi Goto"]
  ],
  other_cast: [
    [actor: "Mizuho Yoshida", characters: "Death Ghidorah"]
  ],
  synopsis: true
]
