[
  title: "Princess Mononoke",
  path: "princess-mononoke-1997",
  original_title: "もののけ姫",
  transliteration: "Mononoke Hime",
  translation: "Princess Mononoke",
  release_date: ~D[1997-07-12],
  runtime: 133,
  produced_by: ["Tokuma Shoten", "Nippon TV Network", "Dentsu", "Studio Ghibli"],
  staff: [
    [roles: "Director", people: "Hayao Miyazaki"],
    [roles: "Producer", people: "Toshio Suzuki"],
    [roles: "Executive Producer", people: "Yasuyoshi Tokuma"],
    [roles: ["Original Story", "Screenplay"], people: "Hayao Miyazaki"],
    [roles: "Music", people: "Joe Hisaishi"],
    [
      roles: "Art",
      people: ["Nizo Yamamoto", "Naoya Tanaka", "Yoji Takeshige", "Satoshi Kuroda", "Kazuo Oga"]
    ],
    [roles: "Photography", people: "Atsushi Okui"],
    [roles: "Sound", people: "Shuji Inoue"],
    [roles: "Editor", people: "Takeshi Seyama"]
  ],
  top_billed_cast: [
    [actor: "Yoji Matsuda", characters: "Ashitaka"],
    [actor: "Yuriko Ishida", characters: "San"],
    [actor: "Yuko Tanaka", characters: "Lady Eboshi"],
    [actor: "Kaoru Kobayashi", characters: "Jiko-bo"],
    [actor: "Masahiko Nishimura", characters: "Koroku"],
    [actor: "Tsunehiko Kamijo", characters: "Gonza"],
    [actor: "Sumi Shimamoto", characters: "Toki"],
    [actor: "Tetsu Watanabe", characters: "Mountain Wolf"],
    [actor: "Makoto Sato", characters: "Nago"],
    [actor: "Akira Nagoya", characters: "Cow Herder"],
    [actor: "Akihiro Miwa", characters: "Moro"],
    [actor: "Mitsuko Mori", characters: "Oracle"],
    [actor: "Hisaya Morishige", characters: "Okkoto"]
  ],
  synopsis: true
]
