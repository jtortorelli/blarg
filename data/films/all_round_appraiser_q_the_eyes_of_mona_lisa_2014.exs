[
  title: "All-Round Appraiser Q: The Eyes of Mona Lisa",
  path: "all-round-appraiser-q-the-eyes-of-mona-lisa-2014",
  original_title: "万能鑑定士Q モナ・リザの瞳",
  transliteration: "Honnō Kanteishi Q Mona Riza No Hitomi",
  translation: "Universal Appraiser Q: Mona Lisa's Eyes",
  release_date: ~D[2014-05-31],
  runtime: 119,
  produced_by: [
    "TBS",
    "Kadokawa",
    "Toho",
    "Horipro",
    "Dentsu",
    "Twins Japan",
    "MBS",
    "CBC TV",
    "WOWOW",
    "RKB",
    "HBC",
    "GyaO!"
  ],
  staff: [
    [roles: "Director", people: "Shinsuke Sato"],
    [roles: "Original Story", people: "Keisuke Matsuoka"],
    [roles: "Screenplay", people: "Manabu Uda"],
    [roles: "Music", people: ["Yuri Habuka", "Takashi Omama"]],
    [roles: "Photography", people: "Taro Kawazu"],
    [roles: "Art Director", people: "Iwao Saito"],
    [roles: "Sound", people: "Kazujiko Yokono"],
    [roles: "Editor", people: "Hitomi Kato"],
    [roles: "VFX Supervisor", people: "Minami Tsujino"]
  ],
  top_billed_cast: [
    [actor: "Haruka Ayase", characters: "Riko Usuda"],
    [actor: "Tori Matsuzaka", characters: "Yuma Ogasawara"],
    [actor: "Eriko Hatsune", characters: "Misa Ryusendera"],
    [actor: "Hiroaki Mukami", characters: "Naoyuki Asahina"],
    [actor: "Pierre Deladonchamps", characters: "Richard Bre"],
    [actor: "Jun Hashimoto", characters: "Koyo Ogino"],
    [actor: "Seminosuke Murasugi", characters: "Satoshi Yamada"],
    [actor: "Kazuya Kojima", characters: "Yuzen Kiyabu"],
    [actor: "Tomoya Moeno", characters: "Shimada"],
    [actor: "Kazuyuki Aijima", characters: "Doctor"],
    [actor: "Nana Eikura", characters: "Thrift Store Clerk"],
    [actor: "Kazue Tsunogae", characters: "Kaori Egoshi"]
  ],
  other_cast: [
    [actor: "Kazuhide Kobayashi", characters: "Detective"],
    [actor: "Masanobu Sakata", characters: "Detective"],
    [actor: "Donpei Tsuchihira", characters: "Priest"]
  ],
  synopsis: true
]
