[
  title: "Sayonara, Jupiter",
  path: "sayonara-jupiter-1984",
  original_title: "さよならジュピター",
  transliteration: "Sayonara Jupitā",
  translation: "Farewell Jupiter",
  release_date: ~D[1984-03-17],
  runtime: 129,
  produced_by: ["Toho", "Io"],
  staff: [
    [roles: ["General Director", "Original Story", "Screenplay"], people: "Sakyo Komatsu"],
    [roles: "Director", people: "Koji Hashimoto"],
    [roles: "Special Effects Director", people: "Koichi Kawakita"],
    [roles: "Producer", people: ["Tomoyuki Tanaka", "Sakyo Komatsu"]],
    [roles: "Co-Producer", people: ["Fumio Tanaka", "Shiro Fujiwara"]],
    [roles: "Music", people: "Kentaro Haneda"],
    [roles: "Photography", people: "Kazumi Hara"],
    [roles: "Art", people: "Kazuo Takenaka"],
    [roles: "Sound", people: "Shotaro Yoshida"],
    [roles: "Lighting", people: "Shinji Kojima"],
    [roles: "Editor", people: "Nobuo Ogawa"],
    [roles: "Special Effects Assistant Director", people: "Eiichi Asada"]
  ],
  top_billed_cast: [
    [actor: "Tomokazu Miura", characters: "Eiji Honda"],
    [actor: "Diane d'Angély", characters: "Maria Basehart"],
    [actor: "Miyuki Ono", characters: "Anita"],
    [actor: "Rachel Huggett", characters: "Dr. Millicent Willem"],
    [actor: "Paul Taiga", characters: "Peter"],
    [actor: "Kim Bass", characters: "Booker"],
    [actor: "Marc Panther", characters: "Carlos Angeles"],
    [actor: "Ron Irwin", characters: "Capt. Kinn"],
    [actor: "William Tapier", characters: "Edward Webb"],
    [actor: "Akihiko Hirata", characters: "Dr. Inoue"],
    [actor: "Masumi Okada", characters: "Muhammad Mansur"],
    [actor: "Hisaya Morishige", characters: "Earth Federation President"]
  ],
  other_cast: [
    [actor: "Andrew Hughes", characters: "Senator Shadillic"]
  ],
  synopsis: true
]
