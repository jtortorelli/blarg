[
  title: "Matango",
  path: "matango-1963",
  original_title: "マタンゴ",
  transliteration: "Matango",
  aliases: "Attack of the Mushroom People",
  release_date: ~D[1963-08-11],
  runtime: 89,
  produced_by: "Toho",
  staff: [
    [roles: "Director", people: "Ishiro Honda"],
    [roles: "Special Effects Director", people: "Eiji Tsuburaya"],
    [roles: "Producer", people: "Tomoyuki Tanaka"],
    [
      roles: "Original Story",
      people: ["Shinichi Hoshi", "Masami Fukushima", "William Hope Hodgson"]
    ],
    [roles: "Screenplay", people: "Takeshi Kimura"],
    [roles: "Photography", people: "Hajime Koizumi"],
    [roles: "Art", people: "Shigekazu Ikuno"],
    [roles: "Sound", people: "Fumio Yanoguchi"],
    [roles: "Lighting", people: "Shoshichi Kojima"],
    [roles: "Music", people: "Sadao Bekku"],
    [roles: "Editor", people: "Reiko Kaneko"],
    [roles: "Special Effects Photography", people: "Sadamasa Arikawa"],
    [roles: "Special Effects Assistant Director", people: "Teruyoshi Nakano"]
  ],
  top_billed_cast: [
    [actor: "Akira Kubo", characters: "Kenji Murai"],
    [actor: "Kumi Mizuno", characters: "Mami Sekiguchi"],
    [actor: "Hiroshi Koizumi", characters: "Naoyuki Sakuta"],
    [actor: "Kenji Sahara", characters: "Senzo Koyama"],
    [actor: "Hiroshi Tachikawa", characters: "Etsuro Yoshida"],
    [actor: "Yoshio Tsuchiya", characters: "Masafumi Kasai"],
    [actor: "Miki Yashiro", characters: "Akiko Soma"],
    [actor: "Hideyo Amamoto", characters: "Mushroom Man"],
    [actor: "Takuzo Kumagai", characters: "Doctor"],
    [actor: "Akio Kusama", characters: "Official"],
    [actor: "Yutaka Oka", characters: "Doctor"],
    [actor: "Keisuke Yamada", characters: "Doctor"],
    [actor: "Kazuo Hinata", characters: "Official"],
    [actor: "Katsumi Tezuka", characters: "Official"],
    [actor: "Haruo Nakajima", characters: "Mushroom Man"],
    [actor: "Masaki Shinohara", characters: "Mushroom Man"]
  ],
  credits: true,
  synopsis: true
]
