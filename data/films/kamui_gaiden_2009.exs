[
  title: "Kamui Gaiden",
  path: "kamui-gaiden-2009",
  original_title: "カムイ外伝",
  transliteration: "Kamui Gaiden",
  translation: "Kamui Story",
  aliases: "Kamui: The Lone Ninja",
  release_date: ~D[2009-09-19],
  runtime: 120,
  produced_by: [
    "Shochiku",
    "Total",
    "Shogakukan",
    "Eisei Gekijo",
    "Horipro",
    "Kinoshita Corporation",
    "Shogakukan Shueisha Productions",
    "Yahoo! JAPAN",
    "Optrom"
  ],
  staff: [
    [roles: "Director", people: "Yoichi Sai"],
    [roles: "Original Story", people: "Sanpei Shirato"],
    [roles: "Screenplay", people: ["Kankuro Kudo", "Yoichi Sai"]],
    [roles: "Music", people: "Taro Iwashiro"],
    [roles: "Photography", people: ["Tomo Ezaki", "Junichi Fujisawa"]],
    [roles: "Art", people: "Tsutomu Imamura"],
    [roles: "Lighting", people: "Koichi Watanabe"],
    [roles: "Sound", people: "Mitsugu Shiratori"],
    [roles: "Editor", people: "Isao Kawase"],
    [roles: "Screenplay Cooperation", people: "Tadahiko Tsukuda"]
  ],
  top_billed_cast: [
    [actor: "Kenichi Matsuyama", characters: "Kamui"],
    [actor: "Koyuki", characters: "Sugaru (Oshika)"],
    [actor: "Suzuka Ogo", characters: "Sayaka"],
    [actor: "Yuta Kanai", characters: "Yoshito"],
    [actor: "Anna Tsuchiya", characters: "Ayu"],
    [actor: "PANTA", characters: "Painter"],
    [actor: "Sei Ashina", characters: "Mikuma"],
    [actor: "Koichi Sato", characters: "Gunbei Mizutani"],
    [actor: "Ekin Cheng", characters: "Oto"],
    [actor: "Hideaki Ito", characters: "Fudo"],
    [actor: "Kaoru Kobayashi", characters: "Hanbei"]
  ],
  other_cast: [
    [actor: "Jiro Dan", characters: "Fishing Village Chief"],
    [actor: "Daisuke Ryu", characters: "Gunbei's Advisor"],
    [actor: "Taro Suwa", characters: "Fisherman"],
    [actor: "Goro Tamiya", characters: "Fisherman"],
    [actor: "Kunito Watanabe", characters: "Shinobi"],
  ],
  synopsis: true
]
