[
  title: "Ring 2",
  path: "ring-2-1999",
  original_title: "リング2",
  transliteration: "Ringu 2",
  translation: "Ring 2",
  release_date: ~D[1999-01-23],
  runtime: 95,
  preceded_by: {"Rasen", 1998},
  followed_by: {"Ring 0: Birthday", 2000},
  produced_by: [
    "Kadokawa Shoten",
    "Asmik Ace",
    "Toho",
    "Sumitomo Corporation",
    "IMAGICA",
    "Nippon Shuppan Hanbai",
    "Omega Project"
  ],
  staff: [
    [roles: "Director", people: "Hideo Nakata"],
    [roles: "Original Story", people: "Koji Suzuki"],
    [roles: "Screenplay", people: "Hiroshi Takahashi"],
    [roles: "Photography", people: "Hideo Yamamoto"],
    [roles: "Art", people: "Iwao Saito"],
    [roles: "Lighting", people: "Akira Ono"],
    [roles: "Music", people: "Kenji Kawai"],
    [roles: "Editor", people: "Nobuyuki Takahashi"],
    [roles: "Sound", people: "Kiyoshi Kakizawa"]
  ],
  top_billed_cast: [
    [actor: "Miki Nakatani", characters: "Mai Takano"],
    [actor: "Hitomi Sato", characters: "Masami Kurahashi"],
    [actor: "Kyoko Fukada", characters: "Kanae Sawaguchi"],
    [actor: "Nanako Matsushima", characters: "Reiko Asakawa"],
    [actor: "Hiroyuki Sanada", characters: "Ryuji Takayama"]
  ],
  other_cast: [
    [actor: "Kenjiro Ishimaru", characters: "Omuta"],
    [actor: "Miwako Kaji", characters: "Kazue Yamamura"],
    [actor: "Fumiyo Kohinata", characters: "Kawajiri"],
    [actor: "Masako", characters: "Shizuko Yamamura"],
    [actor: "Katsumi Muramatsu", characters: "Koichi Asakawa"],
    [actor: "Shiro Namiki", characters: "Forensic Sculptor"],
    [actor: "Takashi Nishina", characters: "Forensic Photographer"],
    [actor: "Yoichi Numata", characters: "Takashi Yamamura"],
    [actor: "Rikiya Otaka", characters: "Yoichi Asakawa"],
    [actor: "Taro Suwa", characters: "Kakukura"],
    [actor: "Yurei Yanagi", characters: "Okazaki"]
  ],
  synopsis: true
]
