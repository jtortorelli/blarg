[
  title: "Three Outlaw Samurai",
  path: "three-outlaw-samurai-1964",
  original_title: "三匹の侍",
  transliteration: "Sanbiki No Samurai",
  translation: "Three Samurai",
  release_date: ~D[1964-05-13],
  runtime: 94,
  produced_by: ["Shochiku", "Samurai Productions"],
  staff: [
    [roles: "Director", people: "Hideo Gosha"],
    [roles: "Producer", people: ["Ginichi Kishimoto", "Tetsuro Tamba"]],
    [
      roles: "Screenplay",
      people: ["Keiichi Abe", "Eizaburo Shiba", "Ginichi Kishimoto", "Hideo Gosha"]
    ],
    [roles: "Photography", people: "Tadashi Sakai"],
    [roles: "Art", people: "Junichi Osumi"],
    [roles: "Sound", people: "Kenyo Takayasu"],
    [roles: "Music", people: "Toshiaki Tsushima"],
    [roles: "Lighting", people: "Hiroyoshi Somekawa"],
    [roles: "Editor", people: "Kazuo Ota"]
  ],
  top_billed_cast: [
    [actor: "Tetsuro Tamba", characters: "Sakon Shiba"],
    [actor: "Isamu Nagato", characters: "Kyojuro Sakura"],
    [actor: "Mikijiro Hira", characters: "Einosuke Kikyo"],
    [actor: "Miyuki Kuwano", characters: "Aya"],
    [actor: "Yoshiko Kayama", characters: "Oyasu"],
    [actor: "Kyoko Aoi", characters: "Omitsu"],
    [actor: "Kamatari Fujiwara", characters: "Jinbei"],
    [actor: "Tatsuya Ishiguro", characters: "Uzaemon Matsushita (The Magistrate)"],
    [actor: "Hisashi Igawa", characters: "Mosuke"],
    [actor: "Ichiro Izawa", characters: "Toranoshin Tanabe"],
    [actor: "Jun Tatara", characters: "Yasugoro"],
    [actor: "Toshie Kimura", characters: "Oine"],
    [actor: "Yoko Mihara", characters: "Omaki"],
    [actor: "Kichijiro Ueda", characters: "Ishigaki"],
    [actor: "Nakajiro Tomita", characters: "Onda"],
    [actor: "Mitsuo Nagata", characters: "Kaneko"],
    [actor: "Hisashi Imahashi", characters: "Gosaku"],
    [actor: "Sakae Umezu", characters: "Yohachi"],
    [actor: "Junkichi Orimoto", characters: "Kurahashi"],
    [actor: "Yoshiro Aoki", characters: "Genma Ouchi"],
    [actor: "Kyoichi Sato", characters: "Ronin"]
  ],
  synopsis: true
]
