[
  title: "The Invisible Man",
  path: "invisible-man-1954",
  original_title: "透明人間",
  transliteration: "Tōmei Ningen",
  translation: "Invisible Man",
  release_date: ~D[1954-12-29],
  runtime: 70,
  produced_by: "Toho",
  staff: [
    [roles: "Director", people: "Motoyoshi Oda"],
    [roles: ["Photography", "Special Effects Supervisor"], people: "Eiji Tsuburaya"],
    [roles: "Producer", people: "Takeo Kita"],
    [roles: "Original Story", people: "Kei Beppu"],
    [roles: "Screenplay", people: "Shigeaki Hidaka"],
    [roles: "Art", people: "Teruaki Abe"],
    [roles: "Sound", people: "Shoichi Fujinawa"],
    [roles: "Lighting", people: "Kuichiro Kishida"],
    [roles: "Music", people: "Kyosuke Kami"],
    [roles: "Editor", people: "Shuichi Ihara"]
  ],
  top_billed_cast: [
    [actor: "Seizaburo Kawazu", characters: "Nanjo"],
    [actor: "Miki Sanjo", characters: "Michiyo"],
    [actor: "Minoru Takada", characters: "Yajima"],
    [actor: "Yoshio Tsuchiya", characters: "Komatsu"],
    [actor: "Kenjiro Uemura", characters: "Ken"],
    [actor: "Kamatari Fujiwara", characters: "Mari's Grandfather"],
    [actor: "Fuyuki Murakami", characters: "Newspaper Editor"],
    [actor: "Yo Shiomi", characters: "Scientist"],
    [actor: "Sonosuke Sawamura", characters: "Parliamentarian"],
    [actor: "Seijiro Onda", characters: "Commissioner"],
    [actor: "Shin Otomo", characters: "Police Chief"],
    [actor: "Noriko Shigeyama", characters: "Nightclub Dancer"],
    [actor: "Keiko Kondo", characters: "Mari"],
    [actor: "Fuminto Matsuo", characters: "Yajima's Henchman"],
    [actor: "Yutaka Nakayama", characters: "Yajima's Henchman"],
    [actor: "Haruo Suzuki", characters: "Yajima's Henchman"],
    [actor: "Shiro Tsuchiya", characters: "Parliamentarian"],
    [actor: "Yutaka Sada", characters: ["Nightclub Mascot", "Bus Passenger"]],
    [actor: "Haruo Nakajima", characters: "Suicidal Invisible Man"],
    [actor: "Akira Sera", characters: "Food Stand Chef"],
    [actor: "Mitsuo Tsuda", characters: "Nightclub Patron"],
    [actor: "Yutaka Oka", characters: "Newsreader"],
    [actor: "Shoichi Hirose", characters: "Policeman"],
    [actor: "Yasuhisa Tsutsumi", characters: "Jewelry Store Clerk"],
    [actor: "Takuzo Kumagai", characters: ["Parliamentarian", "Crooked Businessman"]],
    [actor: "Minoru Ito", characters: "Driver"],
    [actor: "Keiji Sakakida", characters: "Security Guard"],
    [actor: "Shizuko Azuma", characters: "Waitress"]
  ],
  other_cast: [
    [actor: "Ken Echigo", characters: "Waiter"],
    [actor: "Kazuo Hinata", characters: ["Reporter", "Advertising Company Employee"]],
    [actor: "Junichiro Mukai", characters: ["Bus Passenger", "Detective"]],
    [actor: "Junpei Natsuki", characters: "Street Merchant"],
    [actor: "Tadashi Okabe", characters: "Detective"],
    [actor: "Haruya Sakamoto", characters: ["Bus Passenger", "Detective"]],
    [actor: "Hideo Shibuya", characters: ["Nightclub Patron", "Bus Passenger"]],
    [actor: "Jiro Suzukawa", characters: ["Bus Passenger", "Detective"]],
    [actor: "Kamayuki Tsubono", characters: "Bus Passenger"],
    [actor: "Koji Uno", characters: ["Bus Driver", "Detective"]]
  ],
  credits: true,
  synopsis: true
]
