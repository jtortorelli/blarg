[
  title: "Zatoichi's Flashing Sword",
  path: "zatoichis-flashing-sword-1964",
  original_title: "座頭市あばれ凧",
  transliteration: "Zatōichi Abaredako",
  translation: "Zatoichi Wild Kite",
  release_date: ~D[1964-07-11],
  runtime: 82,
  preceded_by: {"Zatoichi and the Chest of Gold", 1964},
  followed_by: {"Fight, Zatoichi, Fight", 1964},
  produced_by: "Daiei",
  staff: [
    [roles: "Director", people: "Kazuo Ikehiro"],
    [roles: "Original Story", people: "Kan Shimozawa"],
    [roles: "Screenplay", people: ["Minoru Inuzaka", "Shozaburo Asai"]],
    [roles: "Photography", people: "Yasukazu Takemura"],
    [roles: "Sound", people: "Masao Osumi"],
    [roles: "Lighting", people: "Hiroya Kato"],
    [roles: "Art", people: "Yoshinobu Nishioka"],
    [roles: "Music", people: "Sei Ikeno"],
    [roles: "Editor", people: "Takashi Taniguchi"]
  ],
  top_billed_cast: [
    [actor: "Shintaro Katsu", characters: "Zatoichi"],
    [actor: "Naoko Kubo", characters: "Okuni"],
    [actor: "Mayumi Nagisa", characters: "Oshizu"],
    [actor: "Ryutaro Gomi", characters: "Shibayama"],
    [actor: "Tatsuo Endo", characters: "Boss Yasugoro"],
    [actor: "Yasushi Sugita", characters: "Matsuji"],
    [actor: "Yutaka Nakamura", characters: "Mekichi"],
    [actor: "Bokuzen Hidari", characters: "Kyubei"],
    [actor: "Ryosuke Kagawa", characters: "Boss Bunkichi"],
    [actor: "Koichi Mizuhara", characters: "Monju"],
    [actor: "Takashi Edajima", characters: "Seiroku"],
    [actor: "Shintaro Nanjo", characters: "Senzo"],
    [actor: "Hachiro Misumi", characters: "Gonpei"],
    [actor: "Yoichi Funaki", characters: "Tappei"],
    [actor: "Ikuko Mori", characters: "Osen"],
    [actor: "Teruko Omi", characters: "Omine"],
    [actor: "Yuji Hamada", characters: "Takematsu"],
    [actor: "Kazue Tamaki", characters: "Genzo Soeda"],
    [actor: "Ichi Koshikawa", characters: "Heita"],
    [actor: "Tokio Oki", characters: "Yasugoro Yakuza"],
    [actor: "Kanae Kobayashi", characters: "Old Woman"]
  ],
  other_cast: [
    [actor: "Jun Katsumura", characters: "Yasugoro Yakuza"],
    [actor: "Gen Kuroki", characters: "Bunkichi Yakuza"]
  ],
  credits: true,
  synopsis: true
]
