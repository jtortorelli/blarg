[
  title: "Versus",
  path: "versus-2000",
  release_date: ~D[2000-09-08],
  runtime: 119,
  produced_by: ["Wevco", "Napalm Films", "Suplex", "KSS"],
  staff: [
    [roles: "Director", people: "Ryuhei Kitamura"],
    [roles: "Screenplay", people: ["Ryuhei Kitamura", "Yudai Yamaguchi"]],
    [roles: "Photography", people: "Takumi Furuya"],
    [roles: "Editor", people: "Shuichi Kakesu"],
    [roles: "Action Director", people: "Yuji Shimomura"],
    [roles: "Music", people: "Nobuhiko Morino"],
    [roles: "2nd Unit Director", people: "Yudai Yamaguchi"],
    [roles: "Lighting", people: ["Fumihiko Tamura", "Takashi Konno"]],
    [roles: "Stunts", people: "Yuji Shimomura"],
    [roles: "Screenplay Advisor", people: "Isao Kiriyama"]
  ],
  top_billed_cast: [
    [actor: "Tak Sakaguchi", characters: "Prisoner KSC2-303"],
    [actor: "Hideo Sakaki", characters: "The Man"],
    [actor: "Kenji Matsuda", characters: "Butterfly Knife Yakuza"],
    [actor: "Minoru Matsumoto", characters: "Amulet Yakuza"],
    [actor: "Yuichiro Arai", characters: "Revolver Yakuza"],
    [actor: "Chieko Misaka", characters: "The Girl"],
    [actor: "Kazuhito Oba", characters: "Glasses Yakuza"],
    [actor: "Takehiro Katayama", characters: "Red-haired Assassin"],
    [actor: "Ayumi Yoshihara", characters: "Pistol Assassin"],
    [actor: "Shoichiro Masumoto", characters: "One-handed Cop"],
    [actor: "Toshiro Kamiaka", characters: "Samurai"],
    [actor: "Yukihito Tanikado", characters: "Beretta Cop"],
    [actor: "Hoshimi Asai", characters: "Karate Assassin"],
    [actor: "Ryosuke Watabe", characters: "Leather Jacket Yakuza"],
    [actor: "Motonari Komiya", characters: "Prisoner"]
  ],
  synopsis: true
]
