[
  title: "Ponyo",
  path: "ponyo-2008",
  original_title: "崖の上のポニョ",
  transliteration: "Gake No Ue No Ponyo",
  translation: "Ponyo on the Cliff by the Sea",
  release_date: ~D[2008-07-19],
  runtime: 101,
  produced_by: [
    "Studio Ghibli",
    "Nippon TV",
    "Dentsu",
    "Hakuhodo DYMP",
    "Disney",
    "Mitsubishi Corporation",
    "Toho"
  ],
  staff: [
    [roles: ["Original Story", "Screenplay", "Director"], people: "Hayao Miyazaki"],
    [roles: "Producer", people: "Toshio Suzuki"],
    [roles: "Art", people: "Noboru Yoshida"],
    [roles: "Photography", people: "Atsushi Okui"],
    [roles: "Music", people: "Joe Hisaishi"],
    [roles: "Sound", people: "Eriko Kimura"],
    [roles: "Editor", people: "Takeshi Seyama"]
  ],
  top_billed_cast: [
    [actor: "Tomoko Yamaguchi", characters: "Lisa"],
    [actor: "Kazushige Nagashima", characters: "Koichi"],
    [actor: "Yuki Amami", characters: "Granmamare"],
    [actor: "George Tokoro", characters: "Fujimoto"],
    [actor: "Yuria Kotsuki", characters: "Ponyo"],
    [actor: "Hiroki Doi", characters: "Sosuke"],
    [actor: "Rumi Hiiragi", characters: "Young Mother"],
    [actor: "Akiko Yano", characters: "Ponyo's Sisters"],
    [actor: "Kazuko Yoshiyuki", characters: "Toki"],
    [actor: "Tomoko Naraoka", characters: "Yoshie"]
  ],
  synopsis: true
]
