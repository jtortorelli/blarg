[
  title: "Oblivion Island",
  path: "oblivion-island-2009",
  original_title: "ホッタラケの島 遥と魔法の鏡",
  transliteration: "Hottarake No Shima Haruka To Mahō No Kagame",
  translation: "Hottarake Island: Haruka and the Magic Mirror",
  release_date: ~D[2009-08-22],
  runtime: 93,
  produced_by: ["Fuji Television", "Production I.G", "Dentsu", "Pony Canyon"],
  staff: [
    [roles: "Director", people: "Shinsuke Sato"],
    [roles: "Screenplay", people: ["Hirotaka Adachi", "Shinsuke Sato"]],
    [roles: "Music", people: "Tadashi Ueda"],
    [roles: "Art Director", people: "Masanobu Nomura"],
    [roles: "Editor", people: ["Luna-parc", "Tsuyoshi Imai", "Hitomi Kato", "Kazumi Wakimoto"]],
    [roles: "Sound", people: "Ryoichi Fukumoto"]
  ],
  synopsis: true,
  top_billed_cast: [
    [actor: "Haruka Ayase", characters: "Haruka"],
    [actor: "Miyuki Sawashiro", characters: "Teo"],
    [actor: "Naho Toda", characters: "Haruka's Mother"],
    [actor: "Nao Omori", characters: "Haruka's Father"],
    [actor: "Mitsuki Tanimura", characters: "Miho"],
    [actor: "Iemasa Kayumi", characters: "The Baron"],
    [actor: "Tamaki Matsumoto", characters: "Cotton"],
    [actor: "Yuji Ueda", characters: "Picanta"],
    [actor: "Yuko Kaida", characters: "Vikki"],
    [actor: "Hidenari Ugaki", characters: "Decargot"]
  ],
  other_cast: [
    [actor: "Akihiko Ishizumi", characters: "Puppeteer"],
    [actor: "Hiroshi Iwasaki", characters: "Younger Soldier Brother"],
    [actor: "Masaaki Okabe", characters: "Mabarowa"],
    [actor: "Kozo Shioya", characters: "Elder Soldier Brother"],
    [actor: "Yoshiko Takemura", characters: "Puppeteer's Wife"]
  ]
]
