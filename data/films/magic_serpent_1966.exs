[
  title: "The Magic Serpent",
  path: "magic-serpent-1966",
  original_title: "怪竜大決戦",
  transliteration: "Kairyū Daikessen",
  translation: "Dragon Great Battle",
  release_date: ~D[1966-12-21],
  runtime: 94,
  produced_by: "Toei",
  staff: [
    [roles: "Director", people: "Tetsuya Yamanouchi"],
    [roles: "Screenplay", people: "Masaru Igami"],
    [roles: "Photography", people: "Motoya Washio"],
    [roles: "Lighting", people: "Takeo Hasegawa"],
    [roles: "Sound", people: "Teruhiko Arakawa"],
    [roles: "Art", people: "Shoji Yada"],
    [roles: "Music", people: "Toshiaki Tsushima"],
    [roles: "Editor", people: "Tadao Kanda"]
  ],
  top_billed_cast: [
    [actor: "Hiroki Matsukata", characters: "Ikazuchi"],
    [actor: "Tomoko Kagawa", characters: "Sunate"],
    [actor: "Yumi Suzumura", characters: "Osaki"],
    [actor: "Nobuo Kaneko", characters: "Master Hiki"],
    [actor: "Shinichiro Hayashi", characters: "Samanosuke Ogata"],
    [actor: "Bin Amatsu", characters: "Daijo"],
    [actor: "Kenji Kusumoto", characters: "Jihei"],
    [actor: "Michimaro Otabe", characters: "Honai"],
    [actor: "Izumi Hara", characters: "Spider Woman"],
    [actor: "Kensaku Hara", characters: "Zenbei"],
    [actor: "Toshio Chiba", characters: "Morobe"],
    [actor: "Chiyo Okada", characters: "Kureya"],
    [actor: "Masataka Iwao", characters: "Kido"],
    [actor: "Daizen Shishido", characters: "Kansai"],
    [actor: "Kuniomi Kiya", characters: "Donin"],
    [actor: "Ryutaro Otomo", characters: "Orochi"]
  ],
  other_cast: [
    [actor: "Takao Iwamura", characters: "Kushirota"]
  ],
  credits: true,
  synopsis: true
]
