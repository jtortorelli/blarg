[
  title: "Zatoichi's Pilgrimage",
  path: "zatoichis-pilgrimage-1966",
  original_title: "座頭市海を渡る",
  transliteration: "Zatōichi Umi O Wataru",
  translation: "Zatoichi Cross the Ocean",
  release_date: ~D[1966-08-13],
  runtime: 82,
  preceded_by: {"Zatoichi's Vengeance", 1966},
  followed_by: {"Zatoichi's Cane Sword", 1967},
  produced_by: "Daiei",
  staff: [
    [roles: "Director", people: "Kazuo Ikehiro"],
    [roles: "Original Story", people: "Kan Shimozawa"],
    [roles: "Screenplay", people: "Kaneto Shindo"],
    [roles: "Photography", people: "Senkichiro Takeda"],
    [roles: "Sound", people: "Iwao Otani"],
    [roles: "Lighting", people: "Reijiro Yamashita"],
    [roles: "Art", people: "Yoshinobu Nishioka"],
    [roles: "Music", people: "Ichiro Saito"],
    [roles: "Editor", people: "Toshio Taniguchi"]
  ],
  top_billed_cast: [
    [actor: "Shintaro Katsu", characters: "Zatoichi"],
    [actor: "Michiyo Masuda", characters: "Okichi"],
    [actor: "Takahiko Tono", characters: "Yasuzo"],
    [actor: "Isao Yamagata", characters: "Boss Tohachi"],
    [actor: "Ryutaro Gomi", characters: "Jonenbo"],
    [actor: "Jotaro Senba", characters: "Violent Pickpocket"],
    [actor: "Hisashi Igawa", characters: "Eigoro"],
    [actor: "Kunie Tanaka", characters: "Storyteller"],
    [actor: "Masao Mishima", characters: "Gonbei"],
    [actor: "Gakuya Morita", characters: "Shinzo"],
    [actor: "Saburo Date", characters: "Kagimatsu"],
    [actor: "Yukio Horikita", characters: "Heita"],
    [actor: "Shosaku Sugiyama", characters: "Gorobei"],
    [actor: "Yusaku Terashima", characters: "Passenger"],
    [actor: "Shintaro Nanjo", characters: "Passenger"],
    [actor: "Jun Katsumura", characters: "Torasame"],
    [actor: "Yoichi Funaki", characters: "Passenger"],
    [actor: "Tokio Oki", characters: "Passenger"],
    [actor: "Kanae Kobayashi", characters: "Yasuzo's Mother"]
  ],
  other_cast: [
    [actor: "Ichi Koshikawa", characters: "Passenger"]
  ],
  credits: true,
  synopsis: true
]
