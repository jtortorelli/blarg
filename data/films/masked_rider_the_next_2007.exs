[
  title: "Masked Rider: The Next",
  path: "masked-rider-the-next-2007",
  original_title: "仮面ライダー THE NEXT",
  transliteration: "Kamen Raidā The Next",
  translation: "Masked Rider: The Next",
  release_date: ~D[2007-10-27],
  runtime: 113,
  preceded_by: {"Masked Rider: The First", 2005},
  produced_by: ["Toei Video", "Toei", "Toei Channel", "Toei Agency"],
  staff: [
    [roles: "Director", people: "Ryuta Tasaki"],
    [roles: "Original Story", people: "Shotaro Ishinomori"],
    [roles: "Screenplay", people: "Toshio Inoue"],
    [roles: "Music", people: "Goro Yasukawa"],
    [roles: "Photography", people: "Kazushige Tanaka"],
    [roles: "Art", people: "Hiroshi Wada"],
    [roles: "Editor", people: "Hideaki Otani"],
    [roles: "Lighting", people: "Seiichiro Mieno"],
    [roles: "Sound", people: "Takeshi Murosone"]
  ],
  top_billed_cast: [
    [actor: "Masaya Kikawada", characters: "Takeshi Hongo / Masked Rider"],
    [actor: "Hassei Takano", characters: "Hayato Ichimonji / Masked Rider No. 2"],
    [actor: "Kazuki Kato", characters: "Shiro Kazami / Masked Rider V3"],
    [actor: "Miku Ishida", characters: "Kotomi Kikuma"],
    [actor: "Erika Mori", characters: "Chiharu Kazami"],
    [actor: "Kyusaku Shimada", characters: "Shindo"],
    [actor: "Tomorowo Taguchi", characters: "Scissors Jaguar"]
  ],
  other_cast: [
    [actor: "Yuki Kazamatsuri", characters: "Teruo's Mother"],
    [actor: "Rie Mashiko", characters: "Chainsaw Lizard"],
    [actor: "Takako Miki", characters: "Club Hostess"],
    [actor: "Goro Naya", characters: "Shocker Chief (Voice)"],
    [actor: "Shinji Rokkaku", characters: "Yamazaki"],
    [actor: "Yosuke Saito", characters: "Vice Principal"]
  ],
  synopsis: true
]
