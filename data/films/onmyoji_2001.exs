[
  title: "Onmyoji",
  path: "onmyoji-2001",
  original_title: "陰陽師",
  transliteration: "Onmyōji",
  translation: "Yin Yang Master",
  release_date: ~D[2001-10-06],
  runtime: 116,
  followed_by: {"Onmyoji II", 2003},
  produced_by: ["Tohokushinsha", "TBS", "Dentsu", "Kadokawa Shoten", "Toho"],
  staff: [
    [roles: "Director", people: "Yojiro Takita"],
    [roles: "Original Story", people: "Baku Yumemakura"],
    [roles: "Screenplay", people: ["Yasushi Fukuda", "Baku Yumemakura", "Itaru Eda"]],
    [roles: "Music", people: "Shigeru Umebayashi"],
    [roles: "Photography", people: "Naoki Kayano"],
    [roles: "Lighting", people: "Tatsuya Osada"],
    [roles: "Art", people: "Kyoko Heya"],
    [roles: "Sound", people: "Osamu Onodera"],
    [roles: "Editor", people: ["Isao Tomita", "Nobuko Tomita"]],
    [roles: "Makeup Effects", people: "Tomo Haraguchi"],
    [roles: "Special Effects Director", people: "Katsuhiro Onoe"]
  ],
  top_billed_cast: [
    [actor: "Mansai Nomura", characters: "Abe No Seimei"],
    [actor: "Hideaki Ito", characters: "Minamoto No Hiromasa"],
    [actor: "Eriko Imai", characters: "Mitsumushi"],
    [actor: "Kyoko Koizumi", characters: "Aone"],
    [actor: "Hiroyuki Sanada", characters: "Doson"]
  ],
  other_cast: [
    [actor: "Akira Emoto", characters: "Right Minister Fujiwara No Mototaka"],
    [actor: "Masato Hagiwara", characters: "Prince Sawara"],
    [actor: "Yukijiro Hotaru", characters: "Tadamasa Gen"],
    [actor: "Kenichi Ishii", characters: "Fujiwara No Kaneie"],
    [actor: "Kenjiro Ishimaru", characters: "Kanmu's Head Onmyoji"],
    [actor: "Hoka Kinoshita", characters: "Emperor Kanmu"],
    [actor: "Ittoku Kishibe", characters: "The Mikado"],
    [actor: "Sachiko Kokobu", characters: "Toko"],
    [actor: "Yui Natsukawa", characters: "Sukehime"],
    [actor: "Shiro Shimamoto", characters: "Kiyomaro Ono"],
    [actor: "Hitomi Tachihara", characters: "Ayako"],
    [actor: "Kenichi Yajima", characters: "Left Minister Fujiwara No Morosuke"],
    [actor: "Kenji Yamaki", characters: "Tachibana No Ukon"]
  ],
  synopsis: true
]
