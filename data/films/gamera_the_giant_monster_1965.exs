[
  title: "Gamera, the Giant Monster",
  path: "gamera-the-giant-monster-1965",
  original_title: "大怪獣ガメラ",
  transliteration: "Daikaijū Gamera",
  translation: "Giant Monster Gamera",
  aliases: ["Gamera", "Gammera the Invincible"],
  release_date: ~D[1965-11-27],
  runtime: 78,
  followed_by: {"Gamera vs. Barugon", 1966},
  produced_by: "Daiei",
  staff: [
    [roles: "Director", people: "Noriaki Yuasa"],
    [roles: "Screenplay", people: "Nisan Takahashi"],
    [roles: "Photography", people: "Nobuo Munekawa"],
    [roles: "Sound", people: "Toshikazu Watanabe"],
    [roles: "Lighting", people: "Yukio Ito"],
    [roles: "Art", people: "Akira Inoue"],
    [roles: "Music", people: "Tadashi Yamanouchi"],
    [roles: "Editor", people: "Tatsuji Nakashizuka"]
  ],
  top_billed_cast: [
    [actor: "Eiji Funakoshi", characters: "Dr. Hidaka"],
    [actor: "Junichiro Yamashita", characters: "Aoyagi"],
    [actor: "Michiko Sugata", characters: "Nobuyo Sakurai"],
    [actor: "Harumi Kiritachi", characters: "Kyoko Yamamoto"],
    [actor: "Yoshiro Kitahara", characters: "Mr. Sakurai"],
    [actor: "Bokuzen Hidari", characters: "Drunk Old Man"],
    [actor: "Jun Hamamura", characters: "Dr. Murase"],
    [actor: "Jutaro Hojo", characters: "SDF Commander"],
    [actor: "Yoshio Yoshida", characters: "Eskimo Chief"],
    [actor: "Kenji Oyama", characters: "Defense Commissioner"],
    [actor: "Jun Koyamauchi", characters: "Chidori-maru Captain"],
    [actor: "Koji Fujiyama", characters: "Oil Plant Worker"],
    [actor: "Munehiko Takada", characters: "Soviet Emissary"],
    [actor: "Kenichi Tani", characters: "Police Officer"],
    [actor: "Yuji Moriya", characters: "Announcer"],
    [actor: "Yoshiro Uchida", characters: "Toshio Sakurai"],
    [actor: "Kazuo Mori", characters: "Chidori-maru Correspondent"],
    [actor: "Daihachi Kita", characters: "Chidori-maru Helmsman"],
    [actor: "Keiichiro Yamane", characters: "Geothermal Plant Worker"],
    [actor: "M. Apanai", characters: "US Emissary"],
    [actor: "Hank Brown", characters: "US Arctic Pilot"]
  ],
  other_cast: [
    [actor: "Teruo Aragaki", characters: "Gamera"]
  ],
  credits: true,
  synopsis: true
]
