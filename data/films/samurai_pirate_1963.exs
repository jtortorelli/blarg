[
  title: "Samurai Pirate",
  path: "samurai-pirate-1963",
  original_title: "大盗賊",
  transliteration: "Daitōzoku",
  translation: "Great Bandit",
  aliases: "The Lost World of Sinbad",
  release_date: ~D[1963-10-26],
  runtime: 97,
  produced_by: "Toho",
  staff: [
    [roles: "Director", people: "Senkichi Taniguchi"],
    [roles: "Special Effects Director", people: "Eiji Tsuburaya"],
    [roles: "Producer", people: ["Tomoyuki Tanaka", "Kenichiro Tsunoda"]],
    [roles: "Story Coordinator", people: "Toshio Yasumi"],
    [roles: "Screenplay", people: ["Takeshi Kimura", "Shinichi Sekizawa"]],
    [roles: "Photography", people: "Takao Saito"],
    [roles: "Art", people: "Takeo Kita"],
    [roles: "Sound", people: "Shin Watarai"],
    [roles: "Lighting", people: "Norikazu Onda"],
    [roles: "Music", people: "Masaru Sato"],
    [roles: "Editor", people: "Yoshitami Kuroiwa"],
    [roles: "Special Effects Photography", people: "Sadamasa Arikawa"],
    [roles: "Special Effects Assistant Director", people: "Teruyoshi Nakano"],
    [roles: "Action Director", people: "Ryu Kuze"]
  ],
  top_billed_cast: [
    [actor: "Toshiro Mifune", characters: "Sukeza"],
    [actor: "Makoto Sato", characters: "The Black Pirate"],
    [actor: "Ichiro Arishima", characters: "The Wizard of Kume"],
    [actor: "Mitsuko Kusabue", characters: "Sobei"],
    [actor: "Mie Hama", characters: "Princess Yaya"],
    [actor: "Akiko Wakabayashi", characters: "Yaya's Handmaiden"],
    [actor: "Kumi Mizuno", characters: "Miwa"],
    [actor: "Tadao Nakamaru", characters: "The Chancellor"],
    [actor: "Jun Tazaki", characters: "Slim"],
    [actor: "Jun Funato", characters: "The Prince of Ming"],
    [actor: "Hideyo Amamoto", characters: "Granny the Witch"],
    [actor: "Takashi Shimura", characters: "King Rasetsu"],
    [actor: "Hideo Sunazuka", characters: "Mustachioed Rebel"],
    [actor: "Masanari Nihei", characters: "Tall Rebel"],
    [actor: "Shoji Oki", characters: "Turbaned Rebel"],
    [actor: "Yutaka Nakayama", characters: "Samurai Ichizo"],
    [actor: "Yoshio Kosugi", characters: "Ming Advisor"],
    [actor: "Nakajiro Tomita", characters: "Samurai Tokubei"],
    [actor: "Tetsu Nakamura", characters: "Archer"],
    [actor: "Nadao Kirino", characters: "Samurai"],
    [actor: "Junichiro Mukai", characters: "Jail Keeper"],
    [actor: "Yasuhisa Tsutsumi", characters: "Samurai"],
    [actor: "Hiroshi Hasegawa", characters: "Palace Guard"],
    [actor: "Eishu Kin", characters: "Giant Bodyguard"],
    [actor: "Haruo Suzuki", characters: "Samurai"]
  ],
  other_cast: [
    [actor: "Yoshio Katsube", characters: "Villager"],
    [actor: "Seishiro Kuno", characters: "Prison Guard"],
    [actor: "Akio Kusama", characters: "Villager"],
    [actor: "Little Man Machan", characters: "Dwarf Servant"],
    [actor: "Yutaka Oka", characters: "Pirate"],
    [actor: "Keiji Sakakida", characters: "Villager"],
    [actor: "Haruya Sakamoto", characters: "Palace Guard"],
    [actor: "Masaki Shinohara", characters: "Rebel"]
  ],
  credits: true,
  synopsis: true
]
