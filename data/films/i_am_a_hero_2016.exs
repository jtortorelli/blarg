[
  title: "I Am A Hero",
  path: "i-am-a-hero-2016",
  original_title: "アイアムアヒーロー",
  transliteration: "Ai Amu A Hīrō",
  translation: "I Am A Hero",
  release_date: ~D[2016-04-23],
  runtime: 127,
  produced_by: [
    "Toho",
    "Avex Pictures",
    "Shogakukan",
    "Dentsu",
    "WOWOW",
    "Hakuhodo DY Media Partners",
    "JR East Japan",
    "KDDI",
    "TOKYO FM",
    "Nippon Shuppan Hanbai",
    "Shogakukan Shueisha Productions",
    "Hikari TV",
    "GYAO"
  ],
  staff: [
    [roles: "Director", people: "Shinsuke Sato"],
    [roles: "Story", people: "Kengo Hanazawa"],
    [roles: "Screenplay", people: "Akiko Nogi"],
    [roles: "Photography", people: "Taro Kawazu"],
    [roles: "Art", people: "Iwao Saito"],
    [roles: "Special Effects", people: "Makoto Kamiya"],
    [roles: "Sound", people: "Kazujiko Yokono"],
    [roles: "Editor", people: "Tsuyoshi Imai"],
    [roles: "Action Coordinator", people: "Yuji Shimomura"],
    [roles: "Music", people: "Nima Fakhrara"]
  ],
  top_billed_cast: [
    [actor: "Yo Oizumi", characters: "Hideo Suzuki"],
    [actor: "Kasumi Arimura", characters: "Hiromi Hayakari"],
    [actor: "Hisashi Yoshizawa", characters: "Iura"],
    [actor: "Yoshinori Okada", characters: "Sango"],
    [actor: "Nana Katase", characters: "Tetsuko Kurokawa"],
    [actor: "Jin Katagiri", characters: "Korori Nakata"],
    [actor: "Makita Sports", characters: "Matsuo"],
    [actor: "Muga Tsukaji", characters: "Mitani"],
    [actor: "Lee Yonghoon", characters: "Athletic ZQN"],
    [actor: "Toshifumi Muramatsu", characters: "Taxi Driver"],
    [actor: "Toru Kazama", characters: "Chikura"],
    [actor: "Yu Tokui", characters: "Abe-san"],
    [actor: "Masami Nagasawa", characters: "Tsugumi Oda"]
  ],
  other_cast: [
    [actor: "Emi Kurita", characters: "Emi"],
    [actor: "Hirota Otsuka", characters: "Kato"]
  ],
  synopsis: true
]
