[
  title: "Mechanical Violator Hakaider",
  path: "mechanical-violator-hakaider-1995",
  original_title: "人造人間ハカイダー",
  transliteration: "Shinzō Ningen Hakaidā",
  translation: "Android Hakaider",
  release_date: ~D[1995-04-15],
  runtime: 52,
  produced_by: ["Toei", "Toei Video", "Ishinomori Productions"],
  staff: [
    [roles: "Director", people: "Keita Amemiya"],
    [roles: "Original Story", people: "Shotaro Ishinomori"],
    [roles: "Screenplay", people: "Toshio Inoue"],
    [roles: "Photography", people: "Fumio Matsumura"],
    [roles: "Lighting", people: "Masaru Saiki"],
    [roles: "Art", people: "Akihiko Iguchi"],
    [roles: "Editor", people: "Junkichi Sugano"],
    [roles: "Sound", people: "Katsumi Ota"],
    [roles: "Music", people: ["Koichi Ota", "Shinji Kinoshita"]],
    [roles: "Character Design", people: "Keita Amemiya"]
  ],
  top_billed_cast: [
    [actor: "Yuji Kishimoto", characters: "Ryo"],
    [actor: "Mai Hosho", characters: "Kaoru"],
    [actor: "Jiro Okamoto", characters: "Hakaider"],
    [actor: "Toshiyuki Kikuchi", characters: "Michael"],
    [actor: "Dai Matsumoto", characters: "Hakaider (Voice)"],
    [actor: "Kazuhiko Inoue", characters: "Michael (Voice)"],
    [actor: "Yasuaki Honda", characters: "Gurjev"]
  ],
  other_cast: [
    [actor: "Rauf Ahmed", characters: "Rauf"],
    [actor: "Shigeru Chiba", characters: "Andy (Voice)"],
    [actor: "Yukijiro Hotaru", characters: "Man in Brain Lab"],
    [actor: "Kiyohiko Inoue", characters: "Kiyo"],
    [actor: "Ami Kawai", characters: "Ami"],
    [actor: "Satoshi Kurihara", characters: "Cap"],
    [actor: "Eddie Lawrence", characters: "Eddie"],
    [actor: "Ed Sardy", characters: "Checkpoint Guard"],
    [actor: "Shohei Shibata", characters: "Boy in Brain Lab"],
    [actor: "Andy Smith", characters: "Andy"],
    [actor: "Toshimichi Takahashi", characters: "Fugitive"]
  ],
  synopsis: true
]
