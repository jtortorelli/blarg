[
  title: "Rebirth of Mothra 3",
  path: "rebirth-of-mothra-3-1998",
  original_title: "モスラ3 キングギドラ来襲",
  transliteration: "Mosura 3 Kingugidora Raishū",
  translation: "Mothra 3: King Ghidorah Appears",
  release_date: ~D[1998-12-12],
  runtime: 100,
  preceded_by: {"Rebirth of Mothra 2", 1997},
  produced_by: "Toho",
  staff: [
    [roles: "Director", people: "Okihiro Yoneda"],
    [roles: "Special Effects Director", people: "Kenji Suzuki"],
    [roles: "Producer", people: "Shogo Tomiyama"],
    [roles: "Screenplay", people: "Masuo Suetani"],
    [roles: "Music", people: "Toshiyuki Watanabe"],
    [roles: "Photography", people: "Yoshinori Sekiguchi"],
    [roles: "Art", people: "Akira Sakuraki"],
    [roles: "Sound", people: "Teiichi Sato"],
    [roles: "Lighting", people: "Tsuyoshi Awakihara"],
    [roles: "Editor", people: "Nobuo Ogawa"],
    [roles: "Assistant Director", people: "Masaaki Tezuka"]
  ],
  top_billed_cast: [
    [actor: "Megumi Kobayashi", characters: "Moll"],
    [actor: "Misato Date", characters: "Lora"],
    [actor: "Atsushi Onita", characters: "Yusuke Sonoda"],
    [actor: "Miyuki Matsuda", characters: "Yukie Sonoda"],
    [actor: "Takuma Yoshizawa", characters: "Shota Shinoda"],
    [actor: "Kyohei Shinozaki", characters: "Junpei Shinoda"],
    [actor: "Ayano Suzuki", characters: "Tamako Shinoda"],
    [actor: "Aki Hano", characters: "Belvera"]
  ],
  other_cast: [
    [actor: "Tsutomu Kitagawa", characters: "King Ghidorah"],
    [actor: "Koichi Ueda", characters: "Principal"]
  ],
  synopsis: true
]
