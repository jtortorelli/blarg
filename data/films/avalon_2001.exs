[
  title: "Avalon",
  path: "avalon-2001",
  release_date: ~D[2001-01-20],
  runtime: 106,
  produced_by: [
    "Bandai Visual",
    "Media Factory",
    "Dentsu",
    "Nippon Herald Films",
    "Deiz"
  ],
  staff: [
    [roles: "Director", people: "Mamoru Oshii"],
    [roles: "Screenplay", people: "Kazunori Ito"],
    [roles: "Music", people: "Kenji Kawai"],
    [roles: "Photography", people: "Grzegorz Kędzierski"],
    [roles: "Art Director", people: "Barbara Nowak"],
    [roles: "Visual Effects Supervisor", people: "Nobuaki Koga"],
    [roles: "Special Assistant Director", people: "Makoto Kamiya"],
    [roles: "Sound", people: "Kazuhiro Wakabayashi"],
    [roles: "Editor", people: "Hiroshi Okuda"]
  ],
  top_billed_cast: [
    [actor: "Małgorzata Foremniak", characters: "Ash"],
    [actor: "Jerzy Gudejko", characters: "Murphy"],
    [actor: "Dariusz Biskupski", characters: "Bishop"],
    [actor: "Bartek Świderski", characters: "Stunner"],
    [actor: "Władysław Kowalski", characters: "Game Master"]
  ],
  other_cast: [
    [actor: "Katarzyna Bargielowska", characters: "Receptionist"],
    [actor: "Michał Breitenwald", characters: "Murphy (Nine Sisters)"],
    [actor: "Zuzanna Kasz", characters: "Ghost"],
    [actor: "Alicja Sapryk", characters: "Gill"],
    [actor: "Elżbieta Towarnicka", characters: "Soprano"]
  ],
  synopsis: true
]
