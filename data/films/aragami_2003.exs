[
  title: "Aragami",
  path: "aragami-2003",
  original_title: "荒神",
  transliteration: "Aragami",
  translation: "Aragami",
  release_date: ~D[2003-03-27],
  runtime: 78,
  produced_by: [
    "Napalm Films",
    "Studio 3"
  ],
  staff: [
    [roles: "Director", people: "Ryuhei Kitamura"],
    [roles: "Screenplay", people: ["Ryuhei Kitamura", "Ryuichi Takatsu"]],
    [roles: "Music", people: "Nobuhiko Morino"],
    [roles: "Photography", people: "Takumi Furuya"],
    [roles: "Art", people: ["Yuji Hayashida", "Norifumi Ataka"]],
    [roles: "Lighting", people: "Yasuhiro Nomura"],
    [roles: "Sound", people: "Masayuki Iwakura"],
    [roles: "Editor", people: "Shuichi Kakesu"],
    [roles: "Original Story", people: ["Shoichiro Masumoto", "Isao Kiriyama"]],
    [roles: "Music Arrangement", people: "Daisuke Yano"],
    [roles: "Guitar Performer", people: "Paul Gilbert"],
    [roles: "Theme Song Performer", people: "Paul Gilbert"]
  ],
  top_billed_cast: [
    [actor: "Takao Osawa", characters: "The Samurai"],
    [actor: "Masaya Kato", characters: "Aragami"],
    [actor: "Kanae Uotani", characters: "The Woman"],
    [actor: "Tak Sakaguchi", characters: "The Future Challenger"],
    [actor: "Hideo Sakaki", characters: "The Samurai's Friend"]
  ],
  synopsis: true
]
