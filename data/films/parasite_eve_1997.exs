[
  title: "Parasite Eve",
  path: "parasite-eve-1997",
  original_title: "パラサイト・イヴ",
  transliteration: "Parasaito Ibu",
  translation: "Parasite Eve",
  release_date: ~D[1997-02-01],
  runtime: 120,
  produced_by: ["Fuji Television Network", "Kadokawa Shoten Publishing"],
  staff: [
    [roles: "Director", people: "Masayuki Ochiai"],
    [roles: "Original Story", people: "Hideaki Sena"],
    [roles: "Screenplay", people: "Ryuichi Kimitsuka"],
    [roles: "Music", people: "Joe Hisaishi"],
    [roles: "Photography", people: "Kozo Shibasaki"],
    [roles: "Lighting", people: "Shosuke Yoshikazu"],
    [roles: "Sound", people: "Hiroshi Yamakata"],
    [roles: "Art", people: "Wada Yanagawa"],
    [roles: "Editor", people: "Yoshifumi Fukazawa"]
  ],
  top_billed_cast: [
    [actor: "Hiroshi Mikami", characters: "Toshiaki Nagashima"],
    [actor: "Riona Hazuki", characters: ["Kiyomi Nagashima", "Eve"]],
    [actor: "Tomoko Nakajima", characters: "Sawako Asakura"],
    [actor: "Ayako Omura", characters: "Mariko Anzai"],
    [actor: "Goro Inagaki", characters: "Tatsuro Ono"],
    [actor: "Hisako Manda", characters: "Etsuko Odagiri"],
    [actor: "Tetsuya Bessho", characters: "Takatsugu Yoshizumi"]
  ],
  other_cast: [
    [actor: "Kenzo Kawarasaki", characters: "Shigeru Kataoka"],
    [actor: "Ren Osugi", characters: "Moderator"],
    [actor: "Sansho Shinsui", characters: "Shigenori Anzai"],
    [actor: "Ikkei Watanabe", characters: "Manabu Shimizu"]
  ],
  synopsis: true
]
