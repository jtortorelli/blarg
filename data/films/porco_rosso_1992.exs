[
  title: "Porco Rosso",
  path: "porco-rosso-1992",
  original_title: "紅の豚",
  transliteration: "Kurenai No Buta",
  translation: "Crimson Pig",
  release_date: ~D[1992-07-18],
  runtime: 93,
  produced_by: ["Tokuma Shoten", "Japan Airlines", "Nippon TV Network", "Studio Ghibli"],
  staff: [
    [roles: "Director", people: "Hayao Miyazaki"],
    [
      roles: "Producer",
      people: ["Toshio Suzuki", "Yasuyoshi Tokuma", "Matsuo Toshimiko", "Yoshio Sasaki"]
    ],
    [roles: ["Original Story", "Screenplay"], people: "Hayao Miyazaki"],
    [roles: "Music", people: "Joe Hisaishi"],
    [roles: "Art Director", people: "Katsu Hisamura"],
    [roles: "Photography", people: "Atsushi Okui"],
    [roles: "Editor", people: "Takeshi Seyama"],
    [roles: "Sound", people: "Naoko Asari"]
  ],
  top_billed_cast: [
    [actor: "Shuichiro Moriyama", characters: "Porco Rosso"],
    [actor: "Tokiko Kato", characters: "Madame Gina"],
    [actor: "Sanshi Katsura", characters: "Mr. Piccolo"],
    [actor: "Tsunehiko Kamijo", characters: "Mamma Aiuto Boss"],
    [actor: "Akemi Okamura", characters: "Fio Piccolo"],
    [actor: "Akio Otsuka", characters: "Donald Curtis"],
    [actor: "Hiroko Seki", characters: "Ba-chan"]
  ],
  synopsis: true
]
