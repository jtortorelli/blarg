[
  title: "Zatoichi",
  path: "zatoichi-1989",
  original_title: "座頭市",
  transliteration: "Zatōichi",
  aliases: ["Zatoichi: The Blind Swordsman", "Zatoichi: Darkness is His Ally"],
  release_date: ~D[1989-02-04],
  runtime: 116,
  preceded_by: {"Zatoichi's Conspiracy", 1973},
  produced_by: ["Miku", "Katsu Productions"],
  staff: [
    [roles: "Director", people: "Shintaro Katsu"],
    [roles: "Producer", people: ["Shintaro Katsu", "Tsukamoto Adams"]],
    [roles: "Original Story", people: "Kan Shimozawa"],
    [roles: "Screenplay", people: ["Shintaro Katsu", "Tsutomu Nakamura", "Tatsumi Ichiyama"]],
    [roles: "Adaptation", people: "Kyohei Nakaoka"],
    [roles: "Photography", people: "Mutsuo Naganuma"],
    [roles: "Lighting", people: "Hideo Kumagai"],
    [roles: "Sound", people: "Senchi Horiuchi"],
    [roles: "Art", people: "Chiyo Umeda"],
    [roles: "Editor", people: "Toshio Taniguchi"],
    [roles: "Music", people: "Edison"]
  ],
  top_billed_cast: [
    [actor: "Shintaro Katsu", characters: "Zatoichi"],
    [actor: "Kanako Higuchi", characters: "Bosatsu"],
    [actor: "Takanori Jinai", characters: "Hanshu"],
    [actor: "Ryutaro Kataoka", characters: "Tsuru"],
    [actor: "Takehiro Okumura", characters: "Goemon"],
    [actor: "Toyomi Kusano", characters: "Oume"],
    [actor: "Joe Yamanaka", characters: "Sukezaemon"],
    [actor: "Kazuko Matsumura", characters: "Singer"],
    [actor: "Norihei Miki", characters: "Giabara"],
    [actor: "Jun Tatara", characters: "Shoya"],
    [actor: "Kenzo Tabu", characters: "Big Boss"],
    [actor: "Takashi Ebata", characters: "Genta"],
    [actor: "Sakae Umezu", characters: "Okappiki"],
    [actor: "Keizo Kanie", characters: "Jin"],
    [actor: "Yuya Uchida", characters: "Akabei"],
    [actor: "Ken Ogata", characters: "Ronin"]
  ],
  other_cast: [
    [actor: "Senkichi Omura", characters: "Prisoner"],
    [actor: "Sachio Sakai", characters: "Yakuza"],
    [actor: "Koichi Ueda", characters: "Uchida Yakuza"]
  ],
  synopsis: true
]
