[
  title: "Gatchaman",
  path: "gatchaman-2013",
  release_date: ~D[2013-08-24],
  runtime: 113,
  produced_by: [
    "Nippon TV Network",
    "Nikkatsu",
    "Toho",
    "Yomiuri TV Broadcasting",
    "Vap",
    "Takara Tomy",
    "Tatsunoko Pro",
    "STV",
    "MMT",
    "SDT",
    "CTV",
    "HTV",
    "FBS"
  ],
  staff: [
    [roles: "Director", people: "Toya Sato"],
    [roles: "Original Story", people: "Tatsunoko Productions"],
    [roles: "Screenplay", people: "Yusuke Watanabe"],
    [roles: "Music", people: "Nima Fakhrara"],
    [roles: "Photography", people: "Takahiro Tsutai"],
    [roles: "Art", people: "Yasuaki Harada"],
    [roles: "Lighting", people: "Yoshitake Hikita"],
    [roles: "Sound", people: "Hiroshi Yamakata"],
    [roles: "Co-Director", people: "Ryo Nishimura"],
    [roles: "Editor", people: "Yoichi Shibuya"]
  ],
  top_billed_cast: [
    [actor: "Tori Matsuzaka", characters: "Ken Washio"],
    [actor: "Go Ayano", characters: "George Asakura"],
    [actor: "Ayame Goriki", characters: "Jun Otsuki"],
    [actor: "Tatsuomi Hamada", characters: "Jinpei Otsuki"],
    [actor: "Ryohei Suzuki", characters: "Ryu Nakanishi"],
    [actor: "Eriko Hatsune", characters: "Naomi / Berge Katze"],
    [actor: "Ken Mitsuishi", characters: "Dr. Kirkland"],
    [actor: "Shido Nakamura", characters: "Iriya"],
    [actor: "Goro Kishitani", characters: "Dr. Nanbu"]
  ],
  other_cast: [
    [actor: "Koji Kawamoto", characters: "Mike Miller"],
    [actor: "Nagato Okui", characters: "Ken Washio (Young)"],
    [actor: "Kisho Omoto", characters: "George Asakura (Young)"],
    [actor: "Koumi Raimu", characters: "Naomi (Young)"],
    [actor: "Hiromi Shinjo", characters: "Gillman"],
    [actor: "Yumiko", characters: "Mei"]
  ],
  synopsis: true
]
