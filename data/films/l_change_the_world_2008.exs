[
  title: "L: Change the World",
  path: "l-change-the-world-2008",
  release_date: ~D[2008-02-09],
  runtime: 129,
  preceded_by: {"Death Note: The Last Name", 2006},
  followed_by: {"Death Note: Light Up the New World", 2016},
  produced_by: [
    "Nippon TV Network",
    "Sapporo TV",
    "Miyagi TV",
    "Shizuoka Daiichi TV",
    "Chukyo TV",
    "Hiroshima TV",
    "Fukuoka Broadcasting",
    "Shueisha",
    "Horipro",
    "Yomiuri TV Broadcasting",
    "Vap",
    "Shochiku",
    "Warner Brothers Films",
    "Nikkatsu"
  ],
  staff: [
    [roles: "Director", people: "Hideo Nakata"],
    [roles: "Original Story", people: ["Tsugumi Oba", "Takeshi Obata"]],
    [roles: "Screenplay", people: "Hirotoshi Kobayashi"],
    [roles: "Music", people: "Kenji Kawai"],
    [roles: "Photography", people: "Tokusho Kikumura"],
    [roles: "Lighting", people: "Yuki Nakamura"],
    [roles: "Art", people: "Kyoko Yauchi"],
    [roles: "Sound", people: "Masato Komatsu"],
    [roles: "Editor", people: "Nobuyuki Takahashi"],
    [roles: "Special Effects Director", people: "Makoto Kamiya"]
  ],
  top_billed_cast: [
    [actor: "Kenichi Matsuyama", characters: "L"],
    [actor: "Yuki Kudo", characters: "K / Kimiko Kujo"],
    [actor: "Mayuko Fukada", characters: "Maki Nikaido"],
    [actor: "Sei Hiraizumi", characters: "Koichi Matsuto"],
    [actor: "Narushi Fukuda", characters: "Boy / Near"],
    [actor: "Bokuzo Masana", characters: "Akio Konishi"],
    [actor: "Yuta Kanai", characters: "Tamotsu Yoshizawa"],
    [actor: "Megumi Sato", characters: "Hatsuna Misawa"],
    [actor: "Kazuki Namioka", characters: "F"],
    [actor: "Renji Ishibashi", characters: "Shin Kakami"],
    [actor: "Kiyotaka Nanbara", characters: "Hideaki Suruga"],
    [actor: "Shunji Fujimura", characters: "Watari"],
    [actor: "Shingo Tsurumi", characters: "Kimihiko Nikaido"],
    [actor: "Masanobu Takashima", characters: "Daisuke Matoba"]
  ],
  other_cast: [
    [actor: "Tatsuya Fujiwara", characters: "Light Yagami"],
    [actor: "Shigeki Hosokawa", characters: "Raye Iwamatsu (Voice)"],
    [actor: "Shido Nakamura", characters: "Ryuk (Voice)"],
    [actor: "Asaka Seto", characters: "Naomi Misora"],
    [actor: "Yoji Tanaka", characters: "Taxi Driver"],
    [actor: "Erika Toda", characters: "Misa Amane"],
    [actor: "Yurei Yanagi", characters: "Security Guard"]
  ],
  synopsis: true
]
