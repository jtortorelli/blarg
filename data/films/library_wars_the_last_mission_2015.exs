[
  title: "Library Wars: The Last Mission",
  path: "library-wars-the-last-mission-2015",
  original_title: "図書館戦争 THE LAST MISSION",
  transliteration: "Toshōkan Sensō The Last Mission",
  translation: "Library War: The Last Mission",
  release_date: ~D[2015-10-10],
  runtime: 120,
  preceded_by: {"Library Wars", 2013},
  produced_by: [
    "TBS TV",
    "KADOKAWA",
    "Toho",
    "J Storm",
    "CBC TV",
    "MBS",
    "WOWOW",
    "Mainichi Shimbun",
    "HBC"
  ],
  staff: [
    [roles: "Director", people: "Shinsuke Sato"],
    [roles: "Original Story", people: "Hiro Arikawa"],
    [roles: "Screenplay", people: "Akiko Nogi"],
    [roles: "Music", people: "Yu Takami"],
    [roles: "Photography", people: "Taro Kawazu"],
    [roles: "Art", people: "Iwao Saito"],
    [roles: "Sound", people: "Kazujiko Yokono"],
    [roles: "Editor", people: "Tsuyoshi Imai"],
    [roles: "VFX Supervisor", people: ["Makoto Kamiya", "Minami Tsujino"]],
    [roles: "Action Director", people: "Yuji Shimomura"]
  ],
  top_billed_cast: [
    [actor: "Junichi Okada", characters: "Atsushi Dojo"],
    [actor: "Nana Eikura", characters: "Iku Kasahara"],
    [actor: "Kei Tanaka", characters: "Mikihisa Komaki"],
    [actor: "Sota Fukushi", characters: "Hikaru Tezuka"],
    [actor: "Naomi Nishida", characters: "Maki Origuchi"],
    [actor: "Jun Hashimoto", characters: "Ryusuke Genda"],
    [actor: "Tao Tsuchiya", characters: "Marie Nakazawa"],
    [actor: "Tori Matsuzaka", characters: "Kei Tezuka"],
    [actor: "Chiaki Kuriyama", characters: "Asako Shibasaki"],
    [actor: "Koji Ishizaka", characters: "Iwao Nishina"]
  ],
  other_cast: [
    [actor: "Joji Abe", characters: "Shinsuke Itagaki"],
    [actor: "Shohei Abe", characters: "Kei Tezuka's Assistant"],
    [actor: "Kazuyuki Aijima", characters: "Bitani"],
    [actor: "Yukijiro Hotaru", characters: "Sugawara"],
    [actor: "David Ito", characters: "Interrogator"],
    [actor: "Hirota Otsuka", characters: "Nomura"],
    [actor: "Kazuki Namioka", characters: "Shindo"],
    [actor: "Satoshi Nikaido", characters: "Ibaraki Prefecture Governor"],
    [actor: "Ryushin Tei", characters: "Akiya Ogata"],
    [actor: "Toru Tezuka", characters: "Interrogator"]
  ],
  synopsis: true
]
