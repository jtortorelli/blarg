[
  title: "Zeiram",
  path: "zeiram-1991",
  original_title: "ゼイラム",
  transliteration: "Zeiramu",
  translation: "Zeiram",
  release_date: ~D[1991-12-21],
  runtime: 97,
  followed_by: {"Zeiram 2", 1994},
  produced_by: ["Gaga Communications", "Crowd"],
  staff: [
    [roles: "Director", people: "Keita Amemiya"],
    [roles: "Screenplay", people: ["Hajime Matsumoto", "Keita Amemiya"]],
    [roles: "Photography", people: "Hiroshi Honjo"],
    [roles: "Lighting", people: "Yoshimi Hosaka"],
    [roles: "Art", people: ["Toshio Miike", "Akihiko Takahashi"]],
    [roles: "Character Design", people: "Keita Amemiya"],
    [roles: "Music", people: ["Koichi Ota", "Shinji Kinoshita"]],
    [roles: "Sound", people: "Katsumi Ito"],
    [roles: "Editor", people: "Koichi Sugisawa"]
  ],
  top_billed_cast: [
    [actor: "Yuko Moriyama", characters: "Iria"],
    [actor: "Kunihiko Ida", characters: "Teppei"],
    [actor: "Mizuho Yoshida", characters: "Zeiram"],
    [actor: "Yukitomo Tochino", characters: "Murata"],
    [actor: "Riko Kurenai", characters: "Momonga Hostess"],
    [actor: "Yukijiro Hotaru", characters: "Kamiya"]
  ],
  other_cast: [
    [actor: "Naomi Enami", characters: "Electronics Shop Owner"],
    [actor: "Masakazu Honda", characters: "Bob (Voice)"]
  ],
  synopsis: true
]
