[
  title: "The Adventures of Taklamakan",
  path: "adventures-of-taklamakan-1966",
  original_title: "奇巌城の冒険",
  transliteration: "Kiganjō No Bōken",
  translation: "Adventure of Stone Castle",
  release_date: ~D[1966-04-28],
  runtime: 100,
  produced_by: [
    "Toho",
    "Mifune Productions"
  ],
  staff: [
    [roles: "Director", people: "Senkichi Taniguchi"],
    [roles: "Producer", people: "Tomoyuki Tanaka"],
    [roles: "Assistant Producer", people: "Hiroshi Nezu"],
    [roles: "Screenplay", people: "Takeshi Kimura"],
    [roles: "Original Story", people: "Osamu Dazai"],
    [roles: "Photography", people: "Kazuo Yamada"],
    [roles: "Art", people: "Hiroshi Ueda"],
    [roles: "Sound", people: "Yoshio Nishikawa"],
    [roles: "Lighting", people: "Hiromitsu Mori"],
    [roles: "Music", people: "Akira Ifukube"],
    [roles: "Editor", people: "Yoshitami Kuroiwa"],
    [roles: "Action Director", people: "Ryu Kuze"]
  ],
  top_billed_cast: [
    [actor: "Toshiro Mifune", characters: "Osami"],
    [actor: "Tatsuya Mihashi", characters: "The King"],
    [actor: "Tadao Nakamaru", characters: "Ensai"],
    [actor: "Makoto Sato", characters: "Gorjaka the Bandit"],
    [actor: "Mie Hama", characters: "Kureya"],
    [actor: "Akiko Wakabayashi", characters: "Spriya"],
    [actor: "Yumi Shirakawa", characters: "The Queen"],
    [actor: "Ichiro Arishima", characters: "The Wizard Hermit"],
    [actor: "Jun Tazaki", characters: "The Innkeeper"],
    [actor: "Akihiko Hirata", characters: "The Chamberlain"],
    [actor: "Susumu Kurobe", characters: "Palace Guard"],
    [actor: "Hideyo Amamoto", characters: "Granny the Witch"],
    [actor: "Toshio Kurosawa", characters: "Osami's Brother"],
    [actor: "Hiroko Sakurai", characters: "The Queen's Handmaiden"],
    [actor: "Shoji Oki", characters: "Sundara"],
    [actor: "Sachio Sakai", characters: "Caravan Leader"],
    [actor: "Shigeki Ishida", characters: "Royal Advisor"],
    [actor: "Shunji Kasuga", characters: "Royal Advisor"],
    [actor: "Naoya Kusakawa", characters: "Palace Guard"],
    [actor: "Ren Yamamoto", characters: "Jail Keeper"],
    [actor: "Ikio Sawamura", characters: "Slave Auctioneer"],
    [actor: "Minoru Takada", characters: "Buddhist Priest"],
    [actor: "Hiroshi Hasegawa", characters: "Palace Guard"],
    [actor: "Shiro Tsuchiya", characters: "Merchant"]
  ],
  other_cast: [
    [actor: "Yukihiko Gondo", characters: "Palace Guard"],
    [actor: "Minoru Ito", characters: "Palace Guard"],
    [actor: "Yoshio Katsube", characters: "Pesil Villager"],
    [actor: "Seishiro Kuno", characters: "Palace Guard"],
    [actor: "Akio Kusama", characters: "Pesil Villager"],
    [actor: "Junpei Natsuki", characters: ["Merchant", "Pesil Villager"]],
    [actor: "Keiji Sakakida", characters: "Pesil Villager"],
    [actor: "Haruya Sakamoto", characters: "Merchant"],
    [actor: "Masaki Shinohara", characters: ["Merchant", "Palace Guard"]],
    [actor: "Jiro Suzukawa", characters: ["Merchant", "Pesil Villager"]],
    [actor: "Masaaki Tachibana", characters: "Pesil Villager"]
  ],
  credits: true,
  synopsis: true
]
