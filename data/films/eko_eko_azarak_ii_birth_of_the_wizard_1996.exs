[
  title: "Eko Eko Azarak II: Birth of the Wizard",
  path: "eko-eko-azarak-ii-birth-of-the-wizard-1996",
  original_title: "エコエコアザラクII BIRTH OF THE WIZARD",
  transliteration: "Eko Eko Azaraku II Birth of the Wizard",
  translation: "Eko Eko Azarak II: Birth of the Wizard",
  release_date: ~D[1996-04-10],
  runtime: 83,
  preceded_by: {"Eko Eko Azarak: Wizard of Darkness", 1995},
  followed_by: {"Eko Eko Azarak III: Misa the Dark Angel", 1998},
  produced_by: ["Gaga Communications", "Tsuburaya Films"],
  staff: [
    [roles: ["Screenplay", "Director"], people: "Shimako Sato"],
    [roles: "Original Story", people: "Shinichi Koga"],
    [roles: "Music", people: "Mikiya Katakura"],
    [roles: "Photography", people: "Shoei Sudo"],
    [roles: "Lighting", people: "Mitsuhiro Yoshimura"],
    [roles: "Sound", people: "Hideaki Yoneyama"],
    [roles: "Art", people: "Kyodai Sakamoto"],
    [roles: "Editor", people: "Hiroshi Kawahara"],
    [roles: "SFX Supervisor", people: "Takashi Yamazaki"]
  ],
  top_billed_cast: [
    [actor: "Kimika Yoshino", characters: "Misa Kuroi"],
    [actor: "Chieko Shiratori", characters: "Shoko Takahashi"],
    [actor: "Akira Otani", characters: "Shoko's Father"],
    [actor: "Satoru Saito", characters: "Doctor"],
    [actor: "Amina Tominaga", characters: "Kirie"],
    [actor: "Miho Fukuya", characters: "Assistant Archaeologist"],
    [actor: "Tamio Ishikura", characters: "Archaeologist"],
    [actor: "Hideyo Amamoto", characters: "Elder"],
    [actor: "Wataru Hosodo", characters: "Saiga"]
  ],
  synopsis: true
]
