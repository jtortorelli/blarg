[
  title: "Hara-Kiri: Death of a Samurai",
  path: "hara-kiri-death-of-a-samurai-2011",
  original_title: "一命",
  transliteration: "Ichimei",
  translation: "Life",
  release_date: ~D[2011-10-15],
  runtime: 126,
  produced_by: [
    "Sedic International",
    "Dentsu",
    "Shochiku",
    "Kodansha",
    "Recorded Picture Company",
    "OLM",
    "Yamanashi Nichinichi Shimbun",
    "Yamanashi Broadcasting",
    "Amuse Soft Entertainment",
    "Asahi Shimbun",
    "Yahoo"
  ],
  staff: [
    [roles: "Director", people: "Takashi Miike"],
    [roles: "Original Story", people: "Yasuhiko Takiguchi"],
    [roles: "Screenplay", people: "Kikumi Yamaguchi"],
    [roles: "Music", people: "Ryuichi Sakamoto"],
    [roles: "Photography", people: "Nobuyasu Kita"],
    [roles: "Lighting", people: "Yoshimi Watanabe"],
    [roles: "Sound", people: "Jun Nakamura"],
    [roles: "Art", people: "Yuji Hayashida"],
    [roles: "Editor", people: "Kenji Yamashita"]
  ],
  synopsis: true,
  top_billed_cast: [
    [actor: "Ebizo Ichikawa", characters: "Hanshiro Tsukumo"],
    [actor: "Eita", characters: "Motome Chijiwa"],
    [actor: "Hikari Mitsushima", characters: "Miho"],
    [actor: "Naoto Takenaka", characters: "Tajiri"],
    [actor: "Munetaka Aoki", characters: "Omodaka Hikokuro"],
    [actor: "Hirofumi Arai", characters: "Matsuzaki Hayatonosho"],
    [actor: "Kazuki Namioka", characters: "Kawabe Umanosuke"],
    [actor: "Yoshihisa Amano", characters: "Sasaki"],
    [actor: "Goro Daimon", characters: "Priest"],
    [actor: "Takehiro Hira", characters: "Naotaka Ii"],
    [actor: "Takashi Sasano", characters: "Sosuke"],
    [actor: "Baijaku Nakamura", characters: "Jinnai Chijiwa"],
    [actor: "Koji Yakusho", characters: "Kageyu Saito"]
  ]
]
