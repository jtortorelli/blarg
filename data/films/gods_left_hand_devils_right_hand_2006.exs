[
  title: "God's Left Hand, Devil's Right Hand",
  path: "gods-left-hand-devils-right-hand-2006",
  original_title: "神の左手悪魔の右手",
  transliteration: "Kami No Hidarite Akuma No Migite",
  translation: "God's Left Hand Devil's Right Hand",
  release_date: ~D[2006-07-22],
  runtime: 95,
  produced_by: [
    "Toshiba Entertainment",
    "Shochiku",
    "Eisei Gekijo",
    "Panorama Communications",
    "Arcimboldo"
  ],
  staff: [
    [roles: "Director", people: "Shusuke Kaneko"],
    [roles: "Original Story", people: "Kazuo Umezu"],
    [roles: "Screenplay", people: "Yoshinori Matsugae"],
    [roles: "Music", people: "Wataru Hokoyama"],
    [roles: "Photography", people: "Kenji Takama"],
    [roles: "Lighting", people: "Masamichi Joho"],
    [roles: "Sound", people: "Henji Iwakumi"],
    [roles: "Art", people: "Ichi Oikawa"],
    [roles: "Editor", people: "Yosuke Yafune"]
  ],
  top_billed_cast: [
    [actor: "Asuka Shibuya", characters: "Izumi"],
    [actor: "Tsubasa Kobayashi", characters: "Sou"],
    [actor: "Ai Maeda", characters: "Yoshiko"],
    [actor: "Momoko Shimizu", characters: "Momo"],
    [actor: "Daikichi Sugawara", characters: "Goro"],
    [actor: "Natsuko Yamamoto", characters: "Kaoru"],
    [actor: "Toshie Negishi", characters: "Miwako"],
    [actor: "Yoneko Matsukane", characters: "Kiyomi"],
    [actor: "Toshiyuki Watarai", characters: "Security Guard"],
    [actor: "Saaya", characters: "Ayu"],
    [actor: "Reon Kadena", characters: "Kie"],
    [actor: "Haruna Imai", characters: "Yoko"],
    [actor: "Taro Nogi", characters: "Fujimoto"],
    [actor: "Kazuo Umezu", characters: "Picture Book Writer"],
    [actor: "Shigemitsu Ogi", characters: "Furukawa"],
    [actor: "Tomorowo Taguchi", characters: "Koichiro"]
  ],
  synopsis: true
]
