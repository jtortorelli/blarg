[
  title: "Eko Eko Azarak III: Misa the Dark Angel",
  path: "eko-eko-azarak-iii-misa-the-dark-angel-1998",
  original_title: "エコエコアザラクIII MISA THE DARK ANGEL",
  transliteration: "Eko Eko Azaraku III Misa the Dark Angel",
  translation: "Eko Eko Azarak III: Misa the Dark Angel",
  release_date: ~D[1998-01-15],
  runtime: 95,
  preceded_by: {"Eko Eko Azarak II: Birth of the Wizard", 1996},
  produced_by: ["Gaga Communications", "Tsuburaya Films"],
  staff: [
    [roles: "Director", people: "Katsuhito Ueno"],
    [roles: "Original Story", people: "Shinichi Koga"],
    [roles: "Screenplay", people: ["Kyoichi Nanatsuki", "Sotaro Hayashi"]],
    [roles: "Music", people: "Daisuke Suzuki"],
    [roles: "Photography", people: "Masahiro Nishikubo"],
    [roles: "Lighting", people: "Haruo Hara"],
    [roles: "Sound", people: "Takenori Misawa"],
    [roles: "Art", people: "Akira Ishige"],
    [roles: "Special Makeup", people: "Tomo Haraguchi"],
    [roles: "Editor", people: "Yosuke Yafune"]
  ],
  top_billed_cast: [
    [actor: "Hinako Saeki", characters: "Misa Kuroi"],
    [actor: "Ayaka Nanami", characters: "Aya Kinoshita"],
    [actor: "Ayumi Takahashi", characters: "Kahori Dokite"],
    [actor: "Hitomi Miwa", characters: "Mami Mizushima"],
    [actor: "Chika Fujimura", characters: "Yoko Hino"],
    [actor: "Elena Yamamoto", characters: "Hitomi Kamata"],
    [actor: "Yuko Takimura", characters: "Yuki Kawai"],
    [actor: "Yuki Hagiwara", characters: "Hikaru Kazami"],
    [actor: "Sakae Umezu", characters: "Janitor"],
    [actor: "Bang-ho Cho", characters: "Satoru Kuroi"]
  ],
  synopsis: true
]
