[
  title: "Nausicaä of the Valley of the Wind",
  path: "nausicaa-of-the-valley-of-the-wind-1984",
  original_title: "風の谷のナウシカ",
  transliteration: "Kaze no Tani no Naushika",
  translation: "Nausicaä of the Valley of the Wind",
  release_date: ~D[1984-03-11],
  runtime: 116,
  produced_by: ["Tokuma Shoten", "Hakuhodo", "Top Craft"],
  staff: [
    [roles: "Director", people: "Hayao Miyazaki"],
    [roles: "Producer", people: ["Isao Takahata", "Yasuyoshi Tokuma", "Michitaka Kondo"]],
    [roles: ["Original Story", "Screenplay"], people: "Hayao Miyazaki"],
    [roles: "Art Director", people: "Mitsuki Nakamura"],
    [roles: "Music", people: "Joe Hisaishi"],
    [roles: "Sound", people: "Shigeharu Shiba"],
    [roles: "Key Animation", people: "Hideaki Anno"],
    [roles: "Photography", people: "Koji Shiragami"],
    [roles: "Editor", people: ["Tomoko Kida", "Naoki Kaneko", "Seiji Sakai"]]
  ],
  top_billed_cast: [
    [actor: "Sumi Shimamoto", characters: "Nausicaä"],
    [actor: "Mahito Tsujimura", characters: "Jihl"],
    [actor: "Hisako Kyoda", characters: "Obaba"],
    [actor: "Goro Naya", characters: "Yupa"],
    [actor: "Ichiro Nagai", characters: "Mito"],
    [actor: "Kohei Miyuchi", characters: "Gol"],
    [actor: "Joji Yanami", characters: "Gikkuri"],
    [actor: "Rihoko Yoshida", characters: "Teto"],
    [actor: "Yoji Matsuda", characters: "Asbel"],
    [actor: "Miina Tominaga", characters: "Lastelle"],
    [actor: "Makoto Terada", characters: "Major of Pejite"],
    [actor: "Akiko Tsuboi", characters: "Asbel and Lastelle's Mother"],
    [actor: "Yoshiko Sakakibara", characters: "Kushana"],
    [actor: "Iemasa Kayumi", characters: "Kurotowa"]
  ],
  synopsis: true
]
