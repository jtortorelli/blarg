[
  title: "Castle in the Sky",
  path: "castle-in-the-sky-1986",
  original_title: "空の城ラピュタ",
  transliteration: "Tenkū No Shiro Rapūta",
  translation: "Castle of Sky Laputa",
  release_date: ~D[1986-08-02],
  runtime: 124,
  produced_by: ["Tokuma Shoten", "Studio Ghibli"],
  staff: [
    [roles: ["Original Story", "Screenplay", "Director"], people: "Hayao Miyazaki"],
    [roles: "Producer", people: ["Isao Takahata", "Yasuyoshi Tokuma"]],
    [roles: "Art Director", people: ["Toshiro Nozaki", "Nizo Yamamoto"]],
    [roles: "Music", people: "Joe Hisaishi"],
    [roles: "Photography", people: "Hirofumi Takahashi"],
    [roles: "Editor", people: "Takeshi Seyama"],
    [roles: "Sound", people: "Shigeharu Shiba"]
  ],
  top_billed_cast: [
    [actor: "Mayumi Tanaka", characters: "Pazu"],
    [actor: "Keiko Yokozawa", characters: "Sheeta"],
    [actor: "Kotoe Hatsui", characters: "Captain Dola"],
    [actor: "Fujio Tokita", characters: "Uncle Pom"],
    [actor: "Minori Terada", characters: "Col. Muska"]
  ],
  other_cast: [
    [actor: "Hiroshi Ito", characters: "Pazu's Boss"],
    [actor: "Sukekiyo Kameyama", characters: "Henri"],
    [actor: "Takuzo Kamiyama", characters: "Charles"],
    [actor: "Ichiro Nagai", characters: "Gen. Moro"],
    [actor: "Ryuji Saikachi", characters: "Motro"],
    [actor: "Tarako", characters: "Madge"],
    [actor: "Machiko Washio", characters: "Okami"],
    [actor: "Yoshio Yasuhara", characters: "Louie"]
  ],
  synopsis: true
]
