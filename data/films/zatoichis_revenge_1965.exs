[
  title: "Zatoichi's Revenge",
  path: "zatoichis-revenge-1965",
  original_title: "座頭市二段斬り",
  transliteration: "Zatōichi Nidankiri",
  translation: "Zatoichi Two-Step Slash",
  release_date: ~D[1965-04-03],
  runtime: 84,
  preceded_by: {"Adventures of Zatoichi", 1964},
  followed_by: {"Zatoichi and the Doomed Man", 1965},
  produced_by: "Daiei",
  staff: [
    [roles: "Director", people: "Akira Inoue"],
    [roles: "Original Story", people: "Kan Shimozawa"],
    [roles: "Screenplay", people: "Minoru Inuzuka"],
    [roles: "Photography", people: "Fujio Morita"],
    [roles: "Sound", people: "Yukio Kaibara"],
    [roles: "Lighting", people: "Hiroshi Mima"],
    [roles: "Art", people: "Yoshinobu Nishioka"],
    [roles: "Music", people: "Akira Ifukube"],
    [roles: "Editor", people: "Hiroshi Yamada"]
  ],
  top_billed_cast: [
    [actor: "Shintaro Katsu", characters: "Zatoichi"],
    [actor: "Mikiko Tsubouchi", characters: "Osayo"],
    [actor: "Norihei Miki", characters: "Denroku"],
    [actor: "Takeshi Kato", characters: "Koheita Kadokura"],
    [actor: "Fujio Harumoto", characters: "Isoda"],
    [actor: "Mayumi Kurata", characters: "Oshika"],
    [actor: "Sachiko Kobayashi", characters: "Tsuru"],
    [actor: "Shinpaira Taira", characters: "Brothel Customer"],
    [actor: "Saburo Date", characters: "Daigashi"],
    [actor: "Sonosuke Sawamura", characters: "Boss Tatsugoro"],
    [actor: "Koji Fujiyama", characters: "Sakubei"],
    [actor: "Yusaku Terashima", characters: "Yasaku"],
    [actor: "Tamiemon Arashi", characters: "Hikonoichi"],
    [actor: "Gen Kimura", characters: "Jingo Odate"],
    [actor: "Yukio Horikita", characters: "Toruyoshi"],
    [actor: "Kazuo Moriuchi", characters: "Dice Thrower"]
  ],
  other_cast: [
    [actor: "Jun Katsumura", characters: "Yakuza"],
    [actor: "Kayo Mikimoto", characters: "Harlot"]
  ],
  credits: true,
  synopsis: true
]
