[
  title: "Assault Girls",
  path: "assault-girls-2009",
  release_date: ~D[2009-12-19],
  runtime: 70,
  produced_by: ["Deiz", "Geneon Universal Entertainment"],
  staff: [
    [roles: ["Screenplay", "Director"], people: "Mamoru Oshii"],
    [roles: "Music", people: "Kenji Kawai"],
    [roles: "Photography", people: "Hiroaki Yuasa"],
    [roles: ["Photography", "Editor", "VFX Supervisor"], people: "Atsuki Sato"],
    [roles: "Lighting", people: "Teruhisa Seki"],
    [roles: "Art", people: "Michitoshi Kurokawa"],
    [roles: "Sound", people: "Kazuhiro Wakabayashi"],
    [roles: "Key Art Design", people: "Shinji Higuchi"]
  ],
  top_billed_cast: [
    [actor: "Meisa Kuroki", characters: "Gray"],
    [actor: "Rinko Kikuchi", characters: "Lucifer"],
    [actor: "Hinako Saeki", characters: "Colonel"]
  ],
  other_cast: [
    [actor: "Yoshikatsu Fujiki", characters: "Jäger"],
    [actor: "Ian Moore", characters: "Gamemaster"]
  ],
  synopsis: true
]
