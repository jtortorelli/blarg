[
  title: "Jin-Roh: The Wolf Brigade",
  path: "jin-roh-the-wolf-brigade-2000",
  original_title: "人狼",
  transliteration: "Jinrō",
  translation: "Man Wolf",
  release_date: ~D[2000-06-03],
  runtime: 98,
  preceded_by: {"Stray Dog: Kerberos Panzer Cops", 1991},
  produced_by: ["Bandai Visual", "Production I.G"],
  staff: [
    [roles: ["Original Story", "Screenplay"], people: "Mamoru Oshii"],
    [roles: ["Director", "Storyboarding"], people: "Hiroyuki Okiura"],
    [roles: "Production", people: "Kenji Kamiyama"],
    [roles: "Art Director", people: "Hiromasa Ogura"],
    [roles: "Photography", people: "Hisao Shirai"],
    [roles: "Editor", people: "Shuichi Kakesu"],
    [roles: "Music", people: "Hajime Mizoguchi"],
    [roles: "Sound", people: "Kazuhiro Wakabayashi"]
  ],
  synopsis: true,
  top_billed_cast: [
    [actor: "Yoshikatsu Fujiki", characters: "Kazuki Fuse"],
    [actor: "Sumi Muto", characters: "Kei Amemiya"],
    [actor: "Hiroyuki Kinoshita", characters: "Atsushi Henmi"],
    [actor: "Kosei Hirota", characters: "Bunmei Muroto"],
    [actor: "Yukihiro Yoshida", characters: "Gen Handa"],
    [actor: "Ryuichi Horibe", characters: "Tatsushiro"],
    [actor: "Eri Sendai", characters: "Nanami Agawa"],
    [actor: "Kenji Nakagawa", characters: "Isao Aniya"],
    [actor: "Tamio Oki", characters: "Autonomous Police Officer"],
    [actor: "Yoshisada Sakaguchi", characters: "Hachiro Tobe"]
  ]
]
