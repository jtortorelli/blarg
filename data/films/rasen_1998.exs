[
  title: "Rasen",
  path: "rasen-1998",
  original_title: "らせん",
  transliteration: "Rasen",
  translation: "Spiral",
  release_date: ~D[1998-01-31],
  runtime: 97,
  preceded_by: {"Ring", 1998},
  followed_by: {"Ring 2", 1999},
  produced_by: ["Kadokawa Shoten", "Pony Canyon", "Toho", "IMAGICA", "Asmik", "Omega Project"],
  staff: [
    [roles: ["Screenplay", "Director"], people: "Joji Iida"],
    [roles: "Original Story", people: "Koji Suzuki"],
    [roles: "Photography", people: "Makoto Watanabe"],
    [roles: "Art", people: "Iwao Saito"],
    [roles: "Lighting", people: "Shoji Hosawa"],
    [roles: "Music", people: "La Finca"],
    [roles: "Sound", people: "Seiji Hosoi"],
    [roles: "Editor", people: "Hirohide Abe"]
  ],
  top_billed_cast: [
    [actor: "Koichi Sato", characters: "Mitsuo Ando"],
    [actor: "Miki Nakatani", characters: "Mai Takano"],
    [actor: "Hiroyuki Sanada", characters: "Ryuji Takayama"]
  ],
  other_cast: [
    [actor: "Daisuke Ban", characters: "Heihachiro Ikuma"],
    [actor: "Nanako Matsushima", characters: "Reiko Asakawa"],
    [actor: "Shigemitsu Ogi", characters: "Maekawa"],
    [actor: "Hinako Saeki", characters: "Sadako Yamamura"],
    [actor: "Shingo Tsurumi", characters: "Miyashita"],
    [actor: "Yutaka Matsushige", characters: "Kenzo Yoshino"]
  ],
  synopsis: true
]
