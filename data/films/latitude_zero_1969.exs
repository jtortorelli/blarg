[
  title: "Latitude Zero",
  path: "latitude-zero-1969",
  original_title: "緯度0大作戦",
  transliteration: "Ido Zero Daisakusen",
  translation: "Latitude Zero Great Strategy",
  release_date: ~D[1969-07-26],
  runtime: 105,
  produced_by: "Toho",
  staff: [
    [roles: "Director", people: "Ishiro Honda"],
    [roles: "Special Effects Director", people: "Eiji Tsuburaya"],
    [roles: "Producer", people: "Tomoyuki Tanaka"],
    [roles: "Screenplay", people: ["Shinichi Sekizawa", "Ted Sherdeman"]],
    [roles: "Photography", people: "Taiichi Kankura"],
    [roles: "Art", people: "Takeo Kita"],
    [roles: "Sound", people: "Masao Fujiyoshi"],
    [roles: "Lighting", people: "Kiichi Onda"],
    [roles: "Editor", people: "Ume Takeda"],
    [roles: "Special Effects Art", people: "Yasuyuki Inoue"],
    [roles: "Special Effects Assistant Director", people: "Teruyoshi Nakano"],
    [roles: "Music", people: "Akira Ifukube"]
  ],
  top_billed_cast: [
    [actor: "Joseph Cotten", characters: ["Craig McKenzie", "Col. John McKenzie"]],
    [actor: "Cesar Romero", characters: ["Malic", "Captain Hastings"]],
    [actor: "Akira Takarada", characters: ["Ken Tashiro", "Naval Officer"]],
    [actor: "Masumi Okada", characters: "Jules Masson"],
    [actor: "Richard Jaeckel", characters: "Perry Lawton"],
    [actor: "Patricia Medina", characters: "Lucretia"],
    [actor: "Mari Nakayama", characters: "Tsuruko Okada"],
    [actor: "Akihiko Hirata", characters: "Dr. Sugata"],
    [actor: "Tetsu Nakamura", characters: "Dr. Okada"],
    [actor: "Kin Omae", characters: "Kobo"],
    [actor: "Hikaru Kuroki", characters: "Kuroiga"],
    [actor: "Linda Haynes", characters: "Dr. Anne Barton"],
    [actor: "Susumu Kurobe", characters: "Chen"],
    [actor: "Yasuhiko Saijo", characters: "Naval Crew"],
    [actor: "Haruo Nakajima", characters: ["Giant Bat", "Giant Rat", "Griffon"]],
    [actor: "Hiroshi Sekita", characters: "Giant Bat"]
  ],
  other_cast: [
    [actor: "Ken Echigo", characters: "Observer"],
    [actor: "Kathy Horan", characters: "Latitude Zero Staff"],
    [actor: "Andrew Hughes", characters: "Sir Maurice Pauli"],
    [actor: "Minoru Ito", characters: "Research Ship Captain"],
    [actor: "Shigeo Kato", characters: "Observer"],
    [actor: "Yoshio Katsube", characters: "Observer"],
    [actor: "Akio Kusama", characters: "Observer"],
    [actor: "Rinsaku Ogata", characters: "Black Shark Crew"],
    [actor: "Yutaka Oka", characters: "Reporter"],
    [actor: "Tadashi Okabe", characters: "Reporter"],
    [actor: "Masaki Shinohara", characters: "Black Shark Crew"],
    [actor: "Masaaki Tachibana", characters: "Reporter"],
    [actor: "Osman Yusef", characters: "Latitude Zero Staff"]
  ],
  credits: true,
  synopsis: true
]
