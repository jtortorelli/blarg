[
  title: "Yamato Takeru",
  path: "yamato-takeru-1994",
  original_title: "ヤマトタケル",
  transliteration: "Yamato Takeru",
  translation: "The Strength of Yamato",
  aliases: "Orochi, the Eight-Headed Dragon",
  release_date: ~D[1994-07-09],
  runtime: 103,
  produced_by: "Toho",
  staff: [
    [roles: "Director", people: "Takao Okawara"],
    [roles: "Special Effects Director", people: "Koichi Kawakita"],
    [roles: "Producer", people: "Shogo Tomiyama"],
    [roles: "Screenplay", people: "Wataru Mimura"],
    [roles: "Photography", people: "Yoshinori Sekiguchi"],
    [roles: "Art", people: "Fumio Ogawa"],
    [roles: "Sound", people: "Nobuo Ikeda"],
    [roles: "Lighting", people: "Hideki Mochitsuki"],
    [roles: "Editor", people: "Nobuo Ogawa"],
    [roles: "Music", people: "Kiyoko Ogino"],
    [roles: "Special Effects Assistant Director", people: "Kenji Suzuki"]
  ],
  top_billed_cast: [
    [actor: "Masahiro Takashima", characters: "Ousu"],
    [actor: "Yasuko Sawaguchi", characters: "Oto Tachibana"],
    [actor: "Akaji Maru", characters: "Tsukinowa"],
    [actor: "Saburo Shinoda", characters: "Emperor Keiko"],
    [actor: "Keaki Mori", characters: "Princess Inahi"],
    [actor: "Bengal", characters: "Genbu"],
    [actor: "Masashi Ishibashi", characters: "Seiryu"],
    [actor: "Hiroshi Fujioka", characters: "Kumaso Takeru"],
    [actor: "Yuki Meguru", characters: "Susano-o"],
    [actor: "Hiroshi Abe", characters: "Tsukuyomi"],
    [actor: "Nobuko Miyamoto", characters: "Princess Yamato"]
  ],
  other_cast: [
    [actor: "Miho Akishino", characters: "Eta Tachibana"],
    [actor: "Wataru Fukuda", characters: "Uchu Ikusagami"],
    [actor: "Hurricane Ryu", characters: "Kumasogami"],
    [actor: "Akira Koieyama", characters: "O-ousu"],
    [actor: "Yuji Saeki", characters: "Wadatsumi Muba"],
    [actor: "Kenpachiro Satsuma", characters: "Yamata No Orochi"],
    [actor: "Koichi Ueda", characters: "Kumaso Villager"]
  ],
  synopsis: true
]
