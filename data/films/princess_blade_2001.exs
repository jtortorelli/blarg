[
  title: "The Princess Blade",
  path: "princess-blade-2001",
  original_title: "修羅雪姫",
  transliteration: "Shurayuki Hime",
  translation: "Lady Snowblood",
  release_date: ~D[2001-12-15],
  runtime: 93,
  produced_by: ["Nikkatsu", "Pioneer LDC", "Tokyo Theatres", "Oz", "Eisei Gekijo", "Sony PCL"],
  staff: [
    [roles: "Director", people: "Shinsuke Sato"],
    [roles: "Original Story", people: ["Kazuo Koike", "Kazuo Kamimura"]],
    [roles: "Screenplay", people: ["Shinsuke Sato", "Kei Kunii"]],
    [roles: "Action Director", people: "Donnie Yen"],
    [roles: "Stunt Coordinator", people: ["Kenji Tanigaki", "Yuji Shimomura"]],
    [roles: "Special Effects Director", people: "Shinji Higuchi"],
    [roles: "Music", people: "Kenji Kawai"],
    [roles: "Photography", people: "Taro Kawazu"],
    [roles: "Art", people: "Tomoyuki Maruo"],
    [roles: "Lighting", people: "Daisuke Nakagawa"],
    [roles: "Editor", people: "Hirohide Abe"],
    [roles: "Sound", people: "Kiyoshi Kakizawa"]
  ],
  top_billed_cast: [
    [actor: "Hideaki Ito", characters: "Takashi"],
    [actor: "Yumiko Shaku", characters: "Yuki"],
    [actor: "Shiro Sano", characters: "Kidokoro"],
    [actor: "Yoko Maki", characters: "Aya"],
    [actor: "Yoko Chosokabe", characters: "Soma"],
    [actor: "Naomasa Musaka", characters: "Kiri"],
    [actor: "Yutaka Matsushige", characters: "Renki"],
    [actor: "Shintaro Sonooka", characters: "Anka"],
    [actor: "Takashi Tsukamoto", characters: "Sen"],
    [actor: "Yoichi Numata", characters: "Kuka"],
    [actor: "Kyusaku Shimada", characters: "Byakurai"]
  ],
  other_cast: [
    [actor: "Shunta Fuchino", characters: "Targeted Man"],
    [actor: "Masako", characters: "Azora"],
    [actor: "Yuji Kido", characters: "Matsuo"]
  ],
  synopsis: true
]
