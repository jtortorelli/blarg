[
  title: "The Wind Rises",
  path: "wind-rises-2013",
  original_title: "風立ちぬ",
  transliteration: "Kaze Tachinu",
  translation: "Wind Rising",
  release_date: ~D[2013-07-20],
  runtime: 126,
  produced_by: [
    "Studio Ghibli",
    "Nippon TV",
    "Dentsu",
    "Hakuhodo DYMP",
    "Disney",
    "Mitsubishi Corporation",
    "Toho",
    "KDDI"
  ],
  staff: [
    [roles: ["Original Story", "Screenplay", "Director"], people: "Hayao Miyazaki"],
    [roles: "Producer", people: "Toshio Suzuki"],
    [roles: "Music", people: "Joe Hisaishi"],
    [roles: "Art Director", people: "Yoji Takeshige"],
    [roles: "Photography", people: "Atsushi Okui"],
    [roles: "Sound", people: "So Takagi"],
    [roles: "Editor", people: "Takeshi Seyama"]
  ],
  top_billed_cast: [
    [actor: "Hideaki Anno", characters: "Jiro Horikoshi"],
    [actor: "Miori Takimoto", characters: "Naoko Satomi"],
    [actor: "Hidetoshi Nishijima", characters: "Kiro Honjo"],
    [actor: "Masahiko Nishimura", characters: "Kurokawa"],
    [actor: "Stephen Albert", characters: "Castorp"],
    [actor: "Morio Kazama", characters: "Satomi"],
    [actor: "Keiko Takeshita", characters: "Jiro's Mother"],
    [actor: "Mirai Shida", characters: "Kayo Horikoshi"],
    [actor: "Jun Kunimura", characters: "Hattori"],
    [actor: "Shinobu Otake", characters: "Mrs. Kurokawa"],
    [actor: "Mansai Nomura", characters: "Giovanni Battista Caproni"]
  ],
  synopsis: true
]
