[
  title: "Rashomon",
  path: "rashomon-1950",
  original_title: "羅生門",
  transliteration: "Rashōmon",
  release_date: ~D[1950-08-26],
  runtime: 88,
  produced_by: "Daiei",
  staff: [
    [roles: "Director", people: "Akira Kurosawa"],
    [roles: "Original Story", people: "Ryunosuke Akutagawa"],
    [roles: "Screenplay", people: ["Akira Kurosawa", "Shinobu Hashimoto"]],
    [roles: "Photography", people: "Kazuo Miyagawa"],
    [roles: "Sound", people: "Iwao Otani"],
    [roles: "Art", people: "Takashi Matsuyama"],
    [roles: "Music", people: "Fumio Hayasaka"],
    [roles: "Lighting", people: "Kenichi Okamoto"],
    [roles: "Editor", people: "Shigeo Nishida"]
  ],
  top_billed_cast: [
    [actor: "Toshiro Mifune", characters: "Tajomaru the Bandit"],
    [actor: "Machiko Kyo", characters: "The Wife"],
    [actor: "Takashi Shimura", characters: "The Woodcutter"],
    [actor: "Masayuki Mori", characters: "The Husband"],
    [actor: "Minoru Chiaki", characters: "The Priest"],
    [actor: "Kichijiro Ueda", characters: "The Peasant"],
    [actor: "Noriko Honma", characters: "The Medium"],
    [actor: "Daisuke Kato", characters: "The Bailiff"]
  ],
  credits: true,
  synopsis: true
]
