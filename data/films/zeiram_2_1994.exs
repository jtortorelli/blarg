[
  title: "Zeiram 2",
  path: "zeiram-2-1994",
  original_title: "ゼイラム2",
  transliteration: "Zeiramu 2",
  translation: "Zeiram 2",
  release_date: ~D[1994-12-17],
  runtime: 100,
  preceded_by: {"Zeiram", 1991},
  produced_by: ["Embodiment Films", "Crowd"],
  staff: [
    [roles: "Director", people: "Keita Amemiya"],
    [roles: "Screenplay", people: ["Keita Amemiya", "Hajime Matsumoto"]],
    [roles: "Original Story", people: "Keita Amemiya"],
    [roles: "Photography", people: "Hiroshi Kidokoro"],
    [roles: "Art", people: "Akihiko Iguchi"],
    [roles: "Lighting", people: "Yoshimi Hosaka"],
    [roles: "Sound", people: "Ohta Fact"],
    [roles: "Music", people: "Koichi Ota"],
    [roles: "Editor", people: "Haruhito Konno"]
  ],
  top_billed_cast: [
    [actor: "Yuko Moriyama", characters: "Iria"],
    [actor: "Yukijiro Hotaru", characters: "Kamiya"],
    [actor: "Kunihiko Ida", characters: "Teppei"]
  ],
  other_cast: [
    [actor: "Mitsuo Abe", characters: "Hagi"],
    [actor: "Makio Hiraiwa", characters: "Electronics Store Manager"],
    [actor: "Kazuhiko Inoue", characters: "Bob"],
    [actor: "Satoshi Kurihara", characters: "Kanuto"],
    [actor: "Sabu", characters: "Fujikuro"],
    [actor: "Yukitomo Tochino", characters: "Murata"],
    [actor: "Tetsu Watanabe", characters: "Taguchi"],
    [actor: "Mizuho Yoshida", characters: ["Yanuki", "Zeiram"]]
  ],
  synopsis: true
]
