[
  title: "My Neighbor Totoro",
  path: "my-neighbor-totoro-1988",
  original_title: "となりのトトロ",
  transliteration: "Tonari No Totoro",
  translation: "Neighbor Totoro",
  release_date: ~D[1988-04-16],
  produced_by: ["Tokuma Shoten", "Studio Ghibli"],
  staff: [
    [roles: ["Original Story", "Screenplay", "Director"], people: "Hayao Miyazaki"],
    [roles: "Producer", people: ["Toru Hara", "Yasuyoshi Tokuma"]],
    [roles: "Art", people: "Kazuo Oga"],
    [roles: "Music", people: "Joe Hisaishi"],
    [roles: "Photography", people: "Hisao Shirai"],
    [roles: "Editor", people: "Takeshi Seyama"],
    [roles: "Sound", people: "Shigeharu Shiba"]
  ],
  top_billed_cast: [
    [actor: "Noriko Hidaka", characters: "Satsuki Kusakabe"],
    [actor: "Chika Sakamoto", characters: "Mei Kusakabe"],
    [actor: "Shigesato Itoi", characters: "Tatsuo Kusakabe"],
    [actor: "Sumi Shimamoto", characters: "Yasuko Kusakabe"],
    [actor: "Tanie Kitabayashi", characters: "Granny"],
    [actor: "Hitoshi Takagi", characters: "Totoro"],
    [actor: "Michiko Washio", characters: "Miss Hara"],
    [actor: "Masashi Hirose", characters: "Mr. Ogaki"],
    [actor: "Shigeru Chiba", characters: "Moving Man"],
    [actor: "Hiroko Maruyama", characters: "Mrs. Ogaki"],
    [actor: "Reiko Suzuki", characters: "Kanta's Aunt"],
    [actor: "Toshiyuki Amagaki", characters: "Kanta Ogaki"],
    [actor: "Naoki Tatsuta", characters: "Cat Bus"],
    [actor: "Tomohiro Nishimura", characters: "Mailman"],
    [actor: "Chie Kojiro", characters: "Michiko"],
    [actor: "Yuko Mizutani", characters: "Woman on Farm Vehicle"],
    [actor: "Daichi Nakamura", characters: "Man on Farm Vehicle"],
    [actor: "Akiko Hiramatsu", characters: "Bus Conductor"]
  ],
  synopsis: true
]
