[
  title: "Gamera the Brave",
  path: "gamera-the-brave-2006",
  original_title: "小さき勇者たち ガメラ",
  transliteration: "Chisaki Yūshatachi Gamera",
  translation: "The Little Braves: Gamera",
  release_date: ~D[2006-04-29],
  runtime: 96,
  preceded_by: {"Gamera 3: Revenge of Iris", 1999},
  produced_by: ["Kadokawa Herald Films", "Japan Movie Fund", "Nippon TV", "Yahoo! JAPAN"],
  staff: [
    [roles: "Director", people: "Ryuta Tasaki"],
    [roles: "Screenplay", people: "Yukari Tatsui"],
    [roles: "Special Effects", people: "Isao Kaneko"],
    [roles: "Music", people: "Yoko Ueno"],
    [roles: "Photography", people: "Kazuhiro Suzuki"],
    [roles: "Art", people: "Yuji Hayashida"],
    [roles: "Lighting", people: "Toshitaka Kamikuma"],
    [roles: "Sound", people: "Masato Yano"],
    [roles: "Editor", people: "Shingo Hirasawa"],
    [roles: "Monster Modeling", people: "Tomo Haraguchi"]
  ],
  top_billed_cast: [
    [actor: "Ryo Tomioka", characters: "Toru Aizawa"],
    [actor: "Kaho", characters: "Mai Nishio"],
    [actor: "Kanji Tsuda", characters: "Kosuke Aizawa"],
    [actor: "Susumu Terajima", characters: "Osamu Nishio"],
    [actor: "Kaoru Okunuki", characters: "Harumi Nishio"],
    [actor: "Shingo Ishikawa", characters: "Masaru Ishida"],
    [actor: "Shogo Narita", characters: "Katsuya Ishida"],
    [actor: "Kenjiro Ishimaru", characters: "Dr. Soichiro Amamiya"],
    [actor: "Tomorowo Taguchi", characters: "Minister Yoshimitsu Hitotsugi"]
  ],
  other_cast: [
    [actor: "Megumi Kobayashi", characters: "Miyuki Aizawa"],
    [actor: "Bokuzo Masana", characters: "Yuji Tobata"],
    [actor: "Taro Suwa", characters: "Restaurant Patron"],
    [actor: "Tetsu Watanabe", characters: "Shipwrecked Fisherman"]
  ],
  synopsis: true
]
