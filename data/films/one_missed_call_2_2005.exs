[
  title: "One Missed Call 2",
  path: "one-missed-call-2-2005",
  original_title: "着信アリ2",
  transliteration: "Chakushin Ari 2",
  translation: "Incoming Call 2",
  release_date: ~D[2005-02-05],
  runtime: 106,
  preceded_by: {"One Missed Call", 2003},
  followed_by: {"One Missed Call: Final", 2006},
  produced_by: ["Kadokawa Films", "Nippon TV Network", "SDP", "Toho"],
  staff: [
    [roles: "Director", people: "Renpei Tsugamoto"],
    [roles: "Original Story", people: "Yasushi Akimoto"],
    [roles: "Screenplay", people: "Miwako Daira"],
    [roles: "Photography", people: "Tokusho Sakumura"],
    [roles: "Lighting", people: "Masaru Saiki"],
    [roles: "Art", people: "Takayuki Nitta"],
    [roles: "Sound", people: "Osamu Takizawa"],
    [roles: "Editor", people: "Soichi Ueno"],
    [roles: "Music", people: "Koji Endo"]
  ],
  top_billed_cast: [
    [actor: "Mimura", characters: "Kyoko Okudera"],
    [actor: "Hiroshi Yoshizawa", characters: "Naoto Sakurai"],
    [actor: "Renji Ishibashi", characters: "Yusaku Motomiya"],
    [actor: "Haruko Wanibuchi", characters: "Sachie Mizunuma"],
    [actor: "Peter Ho", characters: "Yuting Chen"],
    [actor: "Asaka Seto", characters: "Takako Nozoe"]
  ],
  other_cast: [
    [actor: "Chisun", characters: "Madoka Uchiyama"],
    [actor: "Toshie Kobayashi", characters: "Shumei Gao"],
    [actor: "Nana Koizumi", characters: "Li Li"],
    [actor: "Yujiro Komura", characters: "Taiwanese Detective"],
    [actor: "Yasuto Kosuda", characters: "Taiwanese Detective"],
    [actor: "Shadow Liu", characters: "Meifeng Wang"],
    [actor: "Hidekazu Mashima", characters: "Katsuhiko Konno"],
    [actor: "Hakobu Okubo", characters: "Jianfeng Wang"],
    [actor: "Nagisa Okubo", characters: "Mariko Nozoe (9 Years Old)"],
    [actor: "Shiori Okubo", characters: "Takako Nozoe (9 Years Old)"],
    [actor: "Karen Oshima", characters: "Mimiko Mizunuma"],
    [actor: "Hajime Tanimoto", characters: "Detective"]
  ],
  synopsis: true
]
