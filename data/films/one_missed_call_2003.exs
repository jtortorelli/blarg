[
  title: "One Missed Call",
  path: "one-missed-call-2003",
  original_title: "着信アリ",
  transliteration: "Chakushin Ari",
  translation: "Incoming Call",
  release_date: ~D[2003-11-03],
  runtime: 112,
  followed_by: {"One Missed Call 2", 2005},
  produced_by: ["Kadokawa Daiei Films", "Nippon TV Network", "Dentsu", "SDP"],
  staff: [
    [roles: "Director", people: "Takashi Miike"],
    [roles: "Screenplay", people: "Miwako Daira"],
    [roles: "Photography", people: "Hideo Yamamoto"],
    [roles: "Art", people: "Hisao Inagaki"],
    [roles: "Music", people: "Koji Endo"],
    [roles: "Lighting", people: "Shinichi Matsukuma"],
    [roles: "Sound", people: "Jun Nakamura"],
    [roles: "Editor", people: "Yasushi Shimamura"]
  ],
  top_billed_cast: [
    [actor: "Ko Shibasaki", characters: "Yumi Nakamura"],
    [actor: "Shinichi Tsutsumi", characters: "Hiroshi Yamashita"],
    [actor: "Kazue Fukiishi", characters: "Natsumi Konishi"],
    [actor: "Yutaka Matsushige", characters: "Ichiro Fujieda"],
    [actor: "Goro Kishitani", characters: "Oka"],
    [actor: "Renji Ishibashi", characters: "Yusaku Motomiya"]
  ],
  other_cast: [
    [actor: "Kaoru Hanaki", characters: "TV Medium"],
    [actor: "Atsushi Ida", characters: "Kenji Kawai"],
    [actor: "Anna Nagata", characters: "Yoko Okazaki"],
    [actor: "Karen Oshima", characters: "Mimiko Mizunuma"],
    [actor: "Seinami Shimizu", characters: "Nanako Mizunuma"],
    [actor: "Azusa Takehana", characters: "Ritsuko Yamashita"],
    [actor: "Keiko Tomita", characters: "Child Care Worker"],
    [actor: "Mariko Tsutsui", characters: "Marie Mizunuma"]
  ],
  synopsis: true
]
