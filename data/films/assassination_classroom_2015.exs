[
  title: "Assassination Classroom",
  path: "assassination-classroom-2015",
  original_title: "暗殺教室",
  transliteration: "Ansatsu Kyōshitsu",
  translation: "Assassination Classroom",
  release_date: ~D[2015-03-21],
  runtime: 110,
  followed_by: {"Assassination Classroom: Graduation", 2016},
  produced_by: [
    "Fuji Television",
    "Shueisha",
    "J Storm",
    "Toho",
    "ROBOT"
  ],
  staff: [
    [roles: "Director", people: "Eiichiro Hasumi"],
    [roles: "Original Story", people: "Yusei Matsui"],
    [roles: "Screenplay", people: "Tatsuya Kanazawa"],
    [roles: "Music", people: "Naoki Sato"],
    [roles: "Photography", people: "Tomo Ezaki"],
    [roles: "Lighting", people: "Akiyo Miyoshi"],
    [roles: "Sound", people: "Fumihiko Yanagiya"],
    [roles: "Art", people: "Yoji Sakaki"],
    [roles: "Editor", people: "Hiroshi Matsuo"]
  ],
  top_billed_cast: [
    [actor: "Ryosuke Yamada", characters: "Nagisa Shiota"],
    [actor: "Masaki Suga", characters: "Karma Akabane"],
    [actor: "Maika Yamamoto", characters: "Kaede Kayano"],
    [actor: "Seika Taketomi", characters: "Rika Nakamura"],
    [actor: "Mio Yuki", characters: "Yukiko Kanzaki"],
    [actor: "Miku Uehara", characters: "Manami Okuda"],
    [actor: "Kanna Hashimoto", characters: "Ritsu"],
    [actor: "Seishiro Kato", characters: "Itona Horiba"],
    [actor: "Mirei Kiritani", characters: "Aguri Yukimura"],
    [actor: "Kazunari Ninomiya", characters: "Korosensei (Voice)"],
    [actor: "Kotaro Yoshida", characters: "Kensaku Ono"],
    [actor: "Takeo Nakahara", characters: "Goki Onaga"],
    [actor: "Jiyoung", characters: "Irina Jelavic"],
    [actor: "Masanobu Takashima", characters: "Akira Takaoka"],
    [actor: "Kippei Shiina", characters: "Tadaomi Karasuma"]
  ],
  other_cast: [
    [actor: "Wakana Aoi", characters: "Ayaka Saito"],
    [actor: "Shota Arai", characters: "Masayoshi Kimura"],
    [actor: "Tity Hasegawa", characters: "Taisei Yoshida"],
    [actor: "Riku Ichikawa", characters: "Tomohito Sugino"],
    [actor: "Mion Kaneko", characters: "Sumire Hara"],
    [actor: "Yuhi Kato", characters: "Yuma Isogai"],
    [actor: "Yugo Mikawa", characters: "Koki Mimura"],
    [actor: "Kanon Miyahara", characters: "Meg Kataoka"],
    [actor: "Hiroki Narimiya", characters: "Shiro"],
    [actor: "Takumi Oka", characters: "Ryunosuke Chiba"],
    [actor: "Ryunosuke Okada", characters: "Hiroto Maehara"],
    [actor: "Ami Okuma", characters: "Kirara Hazama"],
    [actor: "Koki Osamura", characters: "Taiga Okajima"],
    [actor: "Mitsugu Ozawa", characters: "Sosuke Sugaya"],
    [actor: "Rena Shimura", characters: "Hinano Kurahashi"],
    [actor: "Ken Sugawara", characters: "Ryuma Teraoka"],
    [actor: "Saki Takahashi", characters: "Hinata Okano"],
    [actor: "Yuji Takao", characters: "Takuya Muramatsu"],
    [actor: "Rena Takeda", characters: "Yuzuki Fuwa"],
    [actor: "Hinano Tanaka", characters: "Rinka Hayami"],
    [actor: "Takuya Yoshihara", characters: "Kotaro Takebayashi"]
  ],
  synopsis: true
]
