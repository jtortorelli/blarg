[
  title: "Daimajin Strikes Again",
  path: "daimajin-strikes-again-1966",
  original_title: "大魔神逆襲",
  transliteration: "Daimajin Gyakushū",
  translation: "Great Demon Counterattack",
  aliases: "Return of Daimajin",
  release_date: ~D[1966-12-10],
  runtime: 87,
  preceded_by: {"Return of Daimajin", 1966},
  produced_by: "Daiei",
  staff: [
    [roles: "Director", people: "Kazuo Mori"],
    [roles: "Special Effects Director", people: "Yoshiyuki Kuroda"],
    [roles: "Producer", people: "Masaichi Nagata"],
    [roles: "Screenplay", people: "Tetsuro Yoshida"],
    [roles: "Photography", people: ["Hiroshi Imai", "Fujio Morita"]],
    [roles: "Sound", people: "Iwao Otani"],
    [roles: "Lighting", people: ["Teiichi Ito", "Hiroshi Mima"]],
    [roles: "Art", people: ["Yoshinobu Nishioka", "Shigeru Kato"]],
    [roles: "Music", people: "Akira Ifukube"],
    [roles: "Editor", people: "Toshio Taniguchi"]
  ],
  top_billed_cast: [
    [actor: "Hideki Ninomiya", characters: "Tsurukichi"],
    [actor: "Makihide Iizuka", characters: "Kinta"],
    [actor: "Shinji Horii", characters: "Daisaku"],
    [actor: "Masayuki Nagatomo", characters: "Sugimatsu"],
    [actor: "Toru Abe", characters: "Hidanokami Arakawa"],
    [actor: "Takashi Nakamura", characters: "Sanpei"],
    [actor: "Hiroshi Nawa", characters: "Daizen Matsunaga"],
    [actor: "Tanie Kitabayashi", characters: "Kane"],
    [actor: "Junichiro Yamashita", characters: "Shohachi"],
    [actor: "Chikara Hashimoto", characters: "Daimajin"],
    [actor: "Gakuya Morita", characters: "Toma Kuroki"],
    [actor: "Kazue Tamaki", characters: "Samurai"],
    [actor: "Yuzo Hayakawa", characters: "Yoshibei"],
    [actor: "Yuji Hamada", characters: "Koroku"],
    [actor: "Yukio Horikita", characters: "Scout"],
    [actor: "Sumao Ishihara", characters: "Goju"],
    [actor: "Shozo Nanbu", characters: "Yoshimiemon"],
    [actor: "Yutaro Ban", characters: "Enslaved Peasant"]
  ],
  other_cast: [
    [actor: "Kayo Mikimoto", characters: "Peasant"]
  ],
  credits: true,
  synopsis: true
]
