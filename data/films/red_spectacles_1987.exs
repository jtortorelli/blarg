[
  title: "The Red Spectacles",
  path: "red-spectacles-1987",
  original_title: "紅い眼鏡",
  transliteration: "Akai Megane",
  translation: "Red Glasses",
  release_date: ~D[1987-02-07],
  runtime: 116,
  followed_by: {"Stray Dog: Kerberos Panzer Cops", 1991},
  produced_by: "Omnibus Promotion",
  staff: [
    [roles: "Director", people: "Mamoru Oshii"],
    [roles: "Screenplay", people: ["Kazunori Ito", "Mamoru Oshii"]],
    [roles: "Photography", people: "Yosuke Mamiya"],
    [roles: "Lighting", people: "Yoshimi Hosaka"],
    [roles: "Assistant Director", people: "Kazunori Ito"],
    [roles: "Art", people: ["Hiroaki Kamino", "Tetsuji Mikami"]],
    [roles: "Music", people: "Kenji Kawai"],
    [roles: "Editor", people: "Seiji Morita"]
  ],
  top_billed_cast: [
    [actor: "Shigeru Chiba", characters: "Koichi Todome"],
    [actor: "Machiko Washio", characters: "Midori Washio"],
    [actor: "Hideyuki Tanaka", characters: "Soichiro Toribe"],
    [actor: "Tessho Genda", characters: "Bunmei Muroto"],
    [actor: "Mako Hyodo", characters: "Young Lady"],
    [actor: "Yasuo Otsuka", characters: "Taxi Driver"],
    [actor: "Hideyo Amamoto", characters: "Moongaze Ginji"]
  ],
  other_cast: [
    [actor: "Yoshitada Mitsui", characters: "Bunmei Subordinate"],
    [actor: "Hiroo Oikawa", characters: "Hotel Receptionist"],
    [actor: "Fumihiko Tachiki", characters: "Bunmei Subordinate"]
  ],
  synopsis: true
]
