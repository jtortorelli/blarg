[
  title: "Gamera, the Space Monster",
  path: "gamera-the-space-monster-1980",
  original_title: "宇宙怪獣ガメラ",
  transliteration: "Uchū Kaijū Gamera",
  translation: "Space Monster Gamera",
  aliases: "Super Monster Gamera",
  release_date: ~D[1980-03-20],
  runtime: 109,
  preceded_by: {"Gamera vs. Zigra", 1972},
  followed_by: {"Gamera: The Guardian of the Universe", 1995},
  produced_by: "Daiei",
  staff: [
    [roles: "Director", people: "Noriaki Yuasa"],
    [roles: "Producer", people: "Hiroichi Oka"],
    [roles: "Screenplay", people: "Nisan Takahashi"],
    [roles: "Music", people: "Shunsuke Kikuchi"],
    [roles: "Photography", people: "Akira Kitazaki"],
    [roles: "Sound", people: "Kimio Tobita"],
    [roles: "Lighting", people: "Tadaaki Shimada"],
    [roles: "Art", people: "Tsuneo Yokoshima"],
    [roles: "Editor", people: "Kei Taga"]
  ],
  top_billed_cast: [
    [actor: "Mach Fumiake", characters: "Kilara"],
    [actor: "Yaeko Kojima", characters: "Marsha"],
    [actor: "Yoko Komatsu", characters: "Mitan"],
    [actor: "Keiko Kudo", characters: "Giruge"],
    [actor: "Koichi Maeda", characters: "Keiichi"],
    [actor: "Toshie Takada", characters: "Keiichi's Mother"]
  ],
  synopsis: true
]
