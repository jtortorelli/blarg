[
  title: "Genocide",
  path: "genocide-1968",
  original_title: "昆虫大戦争",
  transliteration: "Konchū Daisensō",
  translation: "Insect Great War",
  release_date: ~D[1968-11-09],
  runtime: 84,
  produced_by: "Shochiku",
  staff: [
    [roles: "Director", people: "Kazui Nihonmatsu"],
    [roles: "Story", people: "Kunimoto Amita"],
    [roles: "Screenplay", people: "Susumu Takaku"],
    [roles: "Photography", people: ["Shizuo Hirase", "Sozaburo Shinomura"]],
    [roles: "Art", people: "Yunota Yoshino"],
    [roles: "Music", people: "Shunsuke Kikuchi"],
    [roles: "Lighting", people: "Tatsuo Aomoto"],
    [roles: "Editor", people: "Akimatsu Terada"],
    [roles: "Sound", people: "Hiroshi Nakamura"]
  ],
  top_billed_cast: [
    [actor: "Keisuke Sonoi", characters: "Dr. Nagumo"],
    [actor: "Yusuke Kawazu", characters: "Joji"],
    [actor: "Emi Shindo", characters: "Yukari"],
    [actor: "Reiko Hitomi", characters: "Komuro"],
    [actor: "Yoko Hirayama", characters: "Laboratory Courier"],
    [actor: "Kathy Horan", characters: "Annabelle"],
    [actor: "Chico Roland", characters: "Charlie"],
    [actor: "Ralph Jesser", characters: "Gordon"],
    [actor: "Toshiyuki Ichimura", characters: "Kudo"],
    [actor: "Saburo Aonuma", characters: "Detective"],
    [actor: "Mike Daneen", characters: "Bomber Pilot"],
    [actor: "Franz Gruber", characters: "USAF Doctor"],
    [actor: "Harold S. Conway", characters: "USAF Officer"]
  ],
  credits: true,
  synopsis: true
]
