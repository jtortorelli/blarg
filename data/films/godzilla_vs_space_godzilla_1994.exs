[
  title: "Godzilla VS Space Godzilla",
  path: "godzilla-vs-space-godzilla-1994",
  original_title: "ゴジラvsスペースゴジラ",
  transliteration: "Gojira VS Supēsugojira",
  translation: "Godzilla VS Space Godzilla",
  release_date: ~D[1994-12-10],
  runtime: 108,
  preceded_by: {"Godzilla VS Mechagodzilla", 1993},
  followed_by: {"Godzilla VS Destroyer", 1995},
  produced_by: "Toho",
  staff: [
    [roles: "Director", people: "Kensho Yamashita"],
    [roles: "Special Effects Director", people: "Koichi Kawakita"],
    [roles: "Producer", people: "Tomoyuki Tanaka"],
    [roles: "Screenplay", people: "Hiroshi Kashiwabara"],
    [roles: "Music", people: "Takayuki Hattori"],
    [roles: "Co-Producer", people: "Shogo Tomiyama"],
    [roles: "Photography", people: "Masahisa Kishimoto"],
    [roles: "Art", people: "Ken Sakai"],
    [roles: "Sound", people: "Kazuo Miyauchi"],
    [roles: "Lighting", people: "Hideki Mochitsuki"],
    [roles: "Editor", people: "Miho Yoneda"],
    [roles: "Special Effects Assistant Director", people: "Kenji Suzuki"]
  ],
  top_billed_cast: [
    [actor: "Jun Hashizume", characters: "Koji Shinjo"],
    [actor: "Megumi Odaka", characters: "Miki Saegusa"],
    [actor: "Zenkichi Yoneyama", characters: "Kiyoshi Sato"],
    [actor: "Towako Yoshikawa", characters: "Chinatsu Gondo"],
    [actor: "Akira Nakao", characters: "G-Force Chief Tadao Aso"],
    [actor: "Akira Emoto", characters: "Akira Yuki"]
  ],
  other_cast: [
    [actor: "Wataru Fukuda", characters: "Mogera"],
    [actor: "Ryo Hariya", characters: "Space Godzilla"],
    [actor: "Ronald Hoerr", characters: "Alexander Mamilov"],
    [actor: "Little Frankie", characters: "Little Godzilla"],
    [actor: "Keiko Imamura", characters: "Cosmos"],
    [actor: "Sayaka Osawa", characters: "Cosmos"],
    [actor: "Eddie Quinlan", characters: "Frank Reynolds"],
    [actor: "Kenji Sahara", characters: "Secretary Takayuki Segawa"],
    [actor: "Yosuke Saito", characters: "Shin Okubo"],
    [actor: "Ed Sardy", characters: "Eric Gould"],
    [actor: "Kenpachiro Satsuma", characters: "Godzilla"],
    [actor: "Koichi Ueda", characters: "Hyodo"]
  ],
  synopsis: true
]
