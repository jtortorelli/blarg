[
  title: "The H-Man",
  path: "h-man-1958",
  original_title: "美女と液体人間",
  transliteration: "Bijo To Ekitainingen",
  translation: "Beauty and the Liquid Man",
  release_date: ~D[1958-06-24],
  runtime: 87,
  produced_by: "Toho",
  staff: [
    [roles: "Director", people: "Ishiro Honda"],
    [roles: "Special Effects Director", people: "Eiji Tsuburaya"],
    [roles: "Producer", people: "Tomoyuki Tanaka"],
    [roles: "Original Story", people: "Hideo Unagami"],
    [roles: "Screenplay", people: "Shinichi Sekizawa"],
    [roles: "Photography", people: "Hajime Koizumi"],
    [roles: "Art", people: "Takeo Kita"],
    [roles: "Sound", people: ["Choshichiro Mikami", "Masanobu Miyazaki"]],
    [roles: "Lighting", people: "Tsuruzo Nishikawa"],
    [roles: "Music", people: "Masaru Sato"],
    [roles: "Special Effects Photography", people: "Sadamasa Arikawa"],
    [roles: "Editor", people: "Kazuji Taira"]
  ],
  top_billed_cast: [
    [actor: "Yumi Shirakawa", characters: "Chikako Arai"],
    [actor: "Kenji Sahara", characters: "Dr. Masada"],
    [actor: "Akihiko Hirata", characters: "Detective Tominaga"],
    [actor: "Eitaro Ozawa", characters: "Detective Miyashita"],
    [actor: "Koreya Senda", characters: "Dr. Maki"],
    [actor: "Makoto Sato", characters: "Uchida"],
    [actor: "Hisaya Ito", characters: "Misaki"],
    [actor: "Yoshio Tsuchiya", characters: "Detective Taguchi"],
    [actor: "Ko Mishima", characters: "Gangster"],
    [actor: "Yoshibumi Tajima", characters: "Detective Sakata"],
    [actor: "Tetsu Nakamura", characters: "Mr. Chin"],
    [actor: "Haruya Kato", characters: "Fisherman"],
    [actor: "Senkichi Omura", characters: "Fisherman"],
    [actor: "Ayumi Sonoda", characters: "Nightclub Dancer"],
    [actor: "Kan Hayashi", characters: "Police Executive"],
    [actor: "Minosuke Yamada", characters: "Police Executive"],
    [actor: "Jun Fujio", characters: "Nishiyama"],
    [actor: "Ren Yamamoto", characters: "Gangster"],
    [actor: "Akira Sera", characters: "Fisherman"],
    [actor: "Tadao Nakamaru", characters: "Detective Seki"],
    [actor: "Yosuke Natsuki", characters: "Bystander"],
    [actor: "Nadao Kirino", characters: "Gangster Waiter"],
    [actor: "Yutaka Sada", characters: "Taxi Driver"],
    [actor: "Shin Otomo", characters: "Gangster"],
    [actor: "Soji Ubukata", characters: "Police Executive"],
    [actor: "Mitsuo Tsuda", characters: "Police Executive"],
    [actor: "Yutaka Nakayama", characters: "Informant Gangster"],
    [actor: "Kamayuki Tsubono", characters: "Detective Ogawa"],
    [actor: "Shigeo Kato", characters: "Fisherman"],
    [actor: "Yutaka Oka", characters: "Soldier"],
    [actor: "Shoichi Hirose", characters: "Fire Chief"],
    [actor: "Takuzo Kumagai", characters: "Soldier"],
    [actor: "Akio Kusama", characters: "Police Chemist"],
    [actor: "Shiro Tsuchiya", characters: "Police Executive"],
    [actor: "Katsumi Tezuka", characters: "Fishing Captain"],
    [actor: "Haruo Nakajima", characters: "Fisherman"]
  ],
  other_cast: [
    [actor: "Ken Echigo", characters: "Nightclub Patron"],
    [actor: "Yukihiko Gondo", characters: "Policeman"],
    [actor: "Kazuo Hinata", characters: ["Barfly", "Police Executive"]],
    [actor: "Yoshio Katsube", characters: "Reporter"],
    [actor: "Junichiro Mukai", characters: "Soldier"],
    [actor: "Keiji Sakakida", characters: "Policeman"],
    [actor: "Hideo Shibuya", characters: "Reporter"],
    [actor: "Haruo Suzuki", characters: "Policeman"],
    [actor: "Masaaki Tachibana", characters: "Waiter"]
  ],
  credits: true,
  synopsis: true
]
