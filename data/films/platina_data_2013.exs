[
  title: "Platina Data",
  path: "platina-data-2013",
  original_title: "プラチナデータ",
  transliteration: "Purachina Dēta",
  translation: "Platina Data",
  release_date: ~D[2013-03-16],
  runtime: 133,
  produced_by: [
    "Toho",
    "Dentsu",
    "J Storm",
    "Gentosha",
    "JR East Japan",
    "Nippon Shuppan Hanbai",
    "Yahoo! JAPAN Group"
  ],
  staff: [
    [roles: "Director", people: "Keishi Otomo"],
    [roles: "Original Story", people: "Keiko Higashino"],
    [roles: "Screenplay", people: "Hideya Hamada"],
    [roles: "Music", people: "Hiroyuki Sawano"],
    [roles: "Photography", people: "Akira Sako"],
    [roles: "Art", people: "Hajime Hashimoto"],
    [roles: "Sound", people: "Fusao Yuwaki"],
    [roles: "Lighting", people: "Yoshimi Watanabe"],
    [roles: "Editor", people: "Tsuyoshi Imai"],
    [roles: "Action Coordinator", people: "Yuji Shimomura"],
    [roles: "VFX Supervisor", people: "Minami Tsujino"]
  ],
  top_billed_cast: [
    [actor: "Kazunari Ninomiya", characters: "Ryuhei Kagura / Ryu"],
    [actor: "Honami Suzuki", characters: "Reiko Mizukami"],
    [actor: "Katsuhisa Namase", characters: "Takashi Shiga"],
    [actor: "Anne", characters: "Risa Shiratori"],
    [actor: "Kiko Mizuhara", characters: "Saki Tateshina"],
    [actor: "Kaname Endo", characters: "Minoru Tokura"],
    [actor: "Soko Wada", characters: "Kosaku Tateshina"],
    [actor: "Masato Hagiwara", characters: "Shogo Kagura"],
    [actor: "Ikuji Nakamura", characters: "Masayuki Nasu"],
    [actor: "Etsushi Toyokawa", characters: "Reiji Asama"]
  ],
  other_cast: [
    [actor: "Toshimasa Komatsu", characters: "Technician"],
    [actor: "Shigemitsu Ogi", characters: "Shogo's Neighbor"],
    [actor: "Asahi Yoshida", characters: "Security Guard"]
  ],
  synopsis: true
]
