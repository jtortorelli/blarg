[
  title: "Eko Eko Azarak: Wizard of Darkness",
  path: "eko-eko-azarak-wizard-of-darkness-1995",
  original_title: "エコエコアザラク WIZARD OF DARKNESS",
  transliteration: "Eko Eko Azaraku Wizard of Darkness",
  translation: "Eko Eko Azarak: Wizard of Darkness",
  release_date: ~D[1995-04-08],
  runtime: 80,
  followed_by: {"Eko Eko Azarak II: Birth of the Wizard", 1996},
  produced_by: ["Gaga Communications", "Tsuburaya Films"],
  staff: [
    [roles: "Director", people: "Shimako Sato"],
    [roles: "Original Story", people: "Shinichi Koga"],
    [roles: "Screenplay", people: "Junki Takegami"],
    [roles: "Story Draft", people: "Shimako Sato"],
    [roles: "Music", people: "Mikiya Katakura"],
    [roles: "Photography", people: "Shoei Sudo"],
    [roles: "Lighting", people: "Mitsuhiro Yoshimura"],
    [roles: "Sound", people: "Koji Inoue"],
    [roles: "Art", people: "Kyodai Sakamoto"],
    [roles: "Editor", people: "Hiroshi Kawahara"],
    [roles: "Digital Visual Effects", people: "Takashi Yamazaki"]
  ],
  top_billed_cast: [
    [actor: "Kimika Yoshino", characters: "Misa Kuroi"],
    [actor: "Miho Kanno", characters: "Mizuki Kurahashi"],
    [actor: "Shuma", characters: "Kenichi Shindo"],
    [actor: "Naozumi Takahashi", characters: "Takayuki Mizuno"],
    [actor: "Kanori Kadomatsu", characters: "Kazumi Tanaka"],
    [actor: "Mio Takaki", characters: "Kyoko Shirai"]
  ],
  other_cast: [
    [actor: "Yoichi Okamura", characters: "Hideki Numata"]
  ],
  synopsis: true
]
