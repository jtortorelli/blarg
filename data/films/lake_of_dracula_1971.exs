[
  title: "Lake of Dracula",
  path: "lake-of-dracula-1971",
  original_title: "呪いの館 血を吸う眼",
  transliteration: "Noroi No Yakata Chiwosū Me",
  translation: "House of Curses: Bloodsucking Eyes",
  release_date: ~D[1971-06-16],
  runtime: 82,
  preceded_by: {"Vampire Doll", 1970},
  followed_by: {"Evil of Dracula", 1974},
  produced_by: "Toho",
  staff: [
    [roles: "Director", people: "Michio Yamamoto"],
    [roles: "Producer", people: "Fumio Tanaka"],
    [roles: "Screenplay", people: ["Ei Ogawa", "Sho Takemitsu"]],
    [roles: "Photography", people: "Rokuro Nishigaki"],
    [roles: "Art", people: "Shigekazu Ikuno"],
    [roles: "Sound", people: "Shin Watarai"],
    [roles: "Lighting", people: "Kojiro Sato"],
    [roles: "Music", people: "Riichiro Manabe"],
    [roles: "Editor", people: "Hisashi Kondo"]
  ],
  top_billed_cast: [
    [actor: "Choei Takahashi", characters: "Saeki"],
    [actor: "Tsuzuru Nakasato", characters: "Natsuko Kashiwagi"],
    [actor: "Midori Fujita", characters: "Akiko Kashiwagi"],
    [actor: "Shin Kishida", characters: "The Vampire"],
    [actor: "Kaku Takashina", characters: "Kyusaku"],
    [actor: "Hideji Otaki", characters: "The Vampire's Father"],
    [actor: "Tatsuo Matsushita", characters: "Doctor"],
    [actor: "Yasuzo Ogawa", characters: "Fisherman"],
    [actor: "Wataru Omae", characters: "Fisherman"],
    [actor: "Mika Katsuragi", characters: "Woman Attacked by Vampire"],
    [actor: "Tadao Futami", characters: "Delivery Truck Driver"],
    [actor: "Fusako Tachibana", characters: "Corpse at Piano"],
    [actor: "Setsuko Kawaguchi", characters: "Nurse"],
    [actor: "Haruo Suzuki", characters: "Policeman"],
    [actor: "Yoshie Kihara", characters: "Nurse"],
    [actor: "Sachiko Mori", characters: "Nurse"],
    [actor: "Michio Yamazoe", characters: "Young Akiko Kashiwagi"]
  ],
  other_cast: [
    [actor: "Shigeo Kato", characters: "Coffee Shop Patron"],
    [actor: "Yoshio Katsube", characters: "Coffee Shop Patron"]
  ],
  credits: true,
  synopsis: true
]
