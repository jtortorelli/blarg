[
  title: "Godzilla, King of the Monsters",
  path: "godzilla-king-of-the-monsters-1954",
  original_title: "ゴジラ",
  transliteration: "Gojira",
  translation: "Godzilla",
  aliases: "Godzilla",
  release_date: ~D[1954-11-03],
  runtime: 97,
  followed_by: {"Godzilla Raids Again", 1955},
  produced_by: "Toho",
  staff: [
    [roles: "Director", people: "Ishiro Honda"],
    [roles: "Producer", people: "Tomoyuki Tanaka"],
    [roles: "Original Story", people: "Shigeru Kayama"],
    [roles: "Screenplay", people: ["Takeo Murata", "Ishiro Honda"]],
    [roles: "Photography", people: "Masao Tamai"],
    [roles: "Art Director", people: "Takeo Kita"],
    [roles: "Sound", people: "Hisashi Shimonaga"],
    [roles: "Lighting", people: "Choshiro Ishii"],
    [roles: "Music", people: "Akira Ifukube"],
    [roles: "Special Effects", people: "Eiji Tsuburaya"],
    [roles: "Editor", people: "Kazuji Taira"]
  ],
  top_billed_cast: [
    [actor: "Akira Takarada", characters: "Hideto Ogata"],
    [actor: "Momoko Kochi", characters: "Emiko Yamane"],
    [actor: "Akihiko Hirata", characters: "Dr. Daisuke Serizawa"],
    [actor: "Takashi Shimura", characters: "Dr. Kyohei Yamane"],
    [actor: "Fuyuki Murakami", characters: "Dr. Tanabe"],
    [actor: "Sachio Sakai", characters: "Hagiwara"],
    [actor: "Toranosuke Ogawa", characters: "Fishing Company President"],
    [actor: "Ren Yamamoto", characters: "Masaji"],
    [actor: "Kan Hayashi", characters: "Diet Chairman"],
    [actor: "Seijiro Onda", characters: "Parliamentarian"],
    [actor: "Takeo Oikawa", characters: "Defense Secretary"],
    [actor: "Keiji Sakakida", characters: "Mayor Inada"],
    [actor: "Toyoaki Suzuki", characters: "Shinkichi"],
    [actor: "Kuninori Kodo", characters: "Izuma"],
    [actor: "Kin Sugai", characters: "Parliamentarian"],
    [actor: "Shizuko Azuma", characters: "Cruise Passenger"],
    [actor: "Tsuruko Mano", characters: "Shinkichi's and Masaji's Mother"],
    [actor: "Tadashi Okabe", characters: "Yamane's Assistant"],
    [actor: "Kiyoshi Kimata", characters: "Cruise Passenger"],
    [actor: "Ren Imaizumi", characters: "Coast Guard"],
    [actor: "Masaaki Tachibana", characters: "Doomed Reporter"],
    [actor: "Yasuhisa Tsutsumi", characters: "Islander"],
    [actor: "Jiro Suzukawa", characters: "Islander"],
    [actor: "Saburo Iketani", characters: "Reporter"],
    [actor: "Katsumi Tezuka", characters: ["Godzilla", "Newspaper Editor"]],
    [actor: "Haruo Nakajima", characters: ["Godzilla", "Newspaper Reporter"]]
  ],
  other_cast: [
    [actor: "Ken Echigo", characters: "Sailor"],
    [actor: "Yu Fujiki", characters: "Ship's Radio Operator"],
    [actor: "Kazuo Hinata", characters: ["Defense Official", "Radio Operator"]],
    [actor: "Shoichi Hirose", characters: "Parliamentarian"],
    [actor: "Takuzo Kumagai", characters: "Defense Official"],
    [actor: "Sokichi Maki", characters: "Coast Guard"],
    [actor: "Junichiro Mukai", characters: ["Reporter", "Defense Official"]],
    [actor: "Junpei Natsuki", characters: "Substation Operator"],
    [actor: "Rinsaku Ogata", characters: "Radio Operator"],
    [actor: "Yutaka Sada", characters: "Defense Official"],
    [actor: "Kenji Sahara", characters: ["Reporter", "Cruise Passenger"]],
    [actor: "Akira Sera", characters: "Parliamentarian"],
    [actor: "Hideo Shibuya", characters: "Reporter"],
    [actor: "Kamayuki Tsubono", characters: "Coast Guard"],
    [actor: "Mitsuo Tsuda", characters: "Policeman"],
    [actor: "Koji Uno", characters: "Correspondent"]
  ],
  credits: true,
  synopsis: true
]
