[
  title: "Skyhigh",
  path: "skyhigh-2003",
  original_title: "スカイ ハイ skyhigh",
  transliteration: "Sukai Hai Skyhigh",
  translation: "Sky High Skyhigh",
  release_date: ~D[2003-11-08],
  runtime: 123,
  produced_by: ["Amuse", "Toei", "TV Asahi", "Asahi Broadcasting"],
  staff: [
    [roles: "Director", people: "Ryuhei Kitamura"],
    [roles: "Original Story", people: "Tsutomu Takahashi"],
    [roles: "Screenplay", people: "Isao Kiriyama"],
    [roles: "Photography", people: "Takumi Furuya"],
    [roles: "Lighting", people: "Toshihide Takasaka"],
    [roles: "Art", people: "Hidefumi Hanaya"],
    [roles: "Sound", people: "Yasuo Takano"],
    [roles: "Editor", people: "Shuichi Kakesu"],
    [roles: "Music", people: ["Nobuhiko Morino", "Daisuke Yano"]]
  ],
  top_billed_cast: [
    [actor: "Yumiko Shaku", characters: "Mina Saeki"],
    [actor: "Shosuke Tanihara", characters: "Kohei Kanzaki"],
    [actor: "Naho Toda", characters: "Kyoko Aoyama"],
    [actor: "Hiromasa Taguchi", characters: "Kazuo Kishi"],
    [actor: "Toshiyuki Kitami", characters: "Toshio Onda"],
    [actor: "Maiko Yamada", characters: "Maya Ito"],
    [actor: "Eihi Shiina", characters: "Izuko"],
    [actor: "Yumi Kikuchi", characters: "Kamina"],
    [actor: "Yue", characters: "Eri Kudo"],
    [actor: "Kanae Uotani", characters: "Rei Miwa"],
    [actor: "Terumi Yoshida", characters: "Sayuri's Editor"],
    [actor: "Kazuki Kitamura", characters: "Informant"],
    [actor: "Aya Okamoto", characters: "Sayuri Toyama"],
    [actor: "Takao Osawa", characters: "Tatsuya Kudo"]
  ],
  synopsis: true
]
