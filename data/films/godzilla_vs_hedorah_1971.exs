[
  title: "Godzilla vs. Hedorah",
  path: "godzilla-vs-hedorah-1971",
  original_title: "ゴジラ対ヘドラ",
  transliteration: "Gojira Tai Hedora",
  translation: "Godzilla Against Hedorah",
  aliases: "Godzilla vs. the Smog Monster",
  release_date: ~D[1971-07-24],
  runtime: 85,
  preceded_by: {"Godzilla's Revenge", 1969},
  followed_by: {"Godzilla vs. Gigan", 1972},
  produced_by: "Toho",
  staff: [
    [roles: "Director", people: "Yoshimitsu Banno"],
    [roles: "Producer", people: "Tomoyuki Tanaka"],
    [roles: "Screenplay", people: ["Takeshi Kimura", "Yoshimitsu Banno"]],
    [roles: "Photography", people: "Yoichi Manoda"],
    [roles: "Art", people: "Yasuyuki Inoue"],
    [roles: "Sound", people: "Masao Fujiyoshi"],
    [roles: "Lighting", people: "Funyoshi Hara"],
    [roles: "Music", people: "Riichiro Manabe"],
    [roles: "Special Effects", people: "Teruyoshi Nakano"],
    [roles: "Editor", people: "Yoshitami Kuroiwa"]
  ],
  top_billed_cast: [
    [actor: "Akira Yamauchi", characters: "Toru Yano"],
    [actor: "Hiroyuki Kawase", characters: "Ken Yano"],
    [actor: "Toshie Kimura", characters: "Toshie Yano"],
    [actor: "Keiko Mari", characters: "Miki Fujinomiya"],
    [actor: "Toshio Shibamoto", characters: "Yukio Mochi"],
    [actor: "Yoshio Yoshida", characters: "Gohei the Old Fisherman"],
    [actor: "Kenpachiro Satsuma", characters: "Hedorah"],
    [actor: "Haruo Nakajima", characters: ["Godzilla", "Talking Head"]],
    [actor: "Haruo Suzuki", characters: "Military Officer"],
    [actor: "Yoshio Katsube", characters: "Soldier"],
    [actor: "Tadashi Okabe", characters: "TV Scientist"],
    [actor: "Wataru Omae", characters: "Policeman"],
    [actor: "Yukihiko Gondo", characters: "Helicopter Pilot"],
    [actor: "Kentaro Watanabe", characters: "TV Interviewer"]
  ],
  other_cast: [
    [actor: "Ken Echigo", characters: "Talking Head"],
    [actor: "Shigeo Kato", characters: "Construction Worker"],
    [actor: "Akio Kusama", characters: "Talking Head"],
    [actor: "Yutaka Oka", characters: "Soldier"],
    [actor: "Haruya Sakamoto", characters: "Talking Head"],
    [actor: "Masaki Shinohara", characters: ["Mahjong Player", "Soldier"]],
    [actor: "Soji Ubukata", characters: "Talking Head"]
  ],
  credits: true,
  synopsis: true
]
