[
  title: "Stray Dog: Kerberos Panzer Cops",
  path: "stray-dog-kerberos-panzer-cops-1991",
  original_title: "ケルベロス 地獄の番犬",
  transliteration: "Keruberosu Jigoku No Banken",
  translation: "Kerberos: Guard Dog of Hell",
  release_date: ~D[1991-03-23],
  runtime: 99,
  preceded_by: {"The Red Spectacles", 1987},
  followed_by: {"Jin-Roh: The Wolf Brigade", 2000},
  produced_by: ["Omnibus Promotion", "Bandai", "Fuji Television"],
  staff: [
    [roles: ["Screenplay", "Director"], people: "Mamoru Oshii"],
    [roles: "Original Story", people: "Mamoru Oshii"],
    [roles: "Photography", people: "Yosuke Mamiya"],
    [roles: "Lighting", people: "Yoshimi Hosaka"],
    [roles: "Music", people: "Kenji Kawai"],
    [roles: "Sound", people: "Naoko Asanashi"],
    [roles: "Editor", people: "Seiji Morita"],
    [roles: "Art", people: "Nikko Tezuka"]
  ],
  top_billed_cast: [
    [actor: "Yoshikatsu Fujiki", characters: "Inui"],
    [actor: "Sue Eaching", characters: "Tang Mie"],
    [actor: "Takashi Matsuyama", characters: "Hayashi"],
    [actor: "Shigeru Chiba", characters: "Koichi Todome"]
  ],
  other_cast: [
    [actor: "Fumihiko Tachiki", characters: "Assassin"]
  ],
  synopsis: true
]
