[
  title: "Ring 0: Birthday",
  path: "ring-0-birthday-2000",
  original_title: "リング0 バースデイ",
  transliteration: "Ringu 0 Bāsudei",
  translation: "Ring 0: Birthday",
  release_date: ~D[2000-01-22],
  runtime: 99,
  preceded_by: {"Ring 2", 1999},
  produced_by: [
    "Kadokawa Shoten",
    "Asmik Ace Entertainment",
    "Toho",
    "Imagica",
    "Nippon Shuppan Hanbai",
    "Sumitomo Corporation"
  ],
  staff: [
    [roles: "Director", people: "Norio Tsuruta"],
    [roles: "Original Story", people: "Koji Suzuki"],
    [roles: "Screenplay", people: "Hiroshi Takahashi"],
    [roles: "Photography", people: "Takahide Shibanushi"],
    [roles: "Art", people: "Osamu Yamaguchi"],
    [roles: "Lighting", people: "Yoshimi Watanabe"],
    [roles: "Music", people: "Shinichiro Ogata"],
    [roles: "Sound", people: "Tetsuo Segawa"],
    [roles: "Editor", people: "Hiroshi Sunaga"],
    [roles: "Special Molding", people: "Tomo Haraguchi"]
  ],
  top_billed_cast: [
    [actor: "Yukie Nakama", characters: "Sadako Yamamura"],
    [actor: "Seiichi Tanabe", characters: "Hiroshi Toyama"],
    [actor: "Kumiko Aso", characters: "Etsuko Tachihara"],
    [actor: "Yoshiko Tanaka", characters: "Akiko Miyaji"]
  ],
  other_cast: [
    [actor: "Daisuke Ban", characters: "Heihachiro Ikuma"],
    [actor: "Mami Hashimoto", characters: "Kiyomi"],
    [actor: "Shuichiro Idemitsu", characters: "Mamoru Ujie"],
    [actor: "Yasushi Kimura", characters: "Togashi"],
    [actor: "Masako", characters: "Shizuko Yamamura"],
    [actor: "Ryushi Mizukami", characters: "Wataru Kuno"],
    [actor: "Yoshiyuki Morishita", characters: "Okubo"],
    [actor: "Norio Murata", characters: "Shuji Kodama"],
    [actor: "Kaoru Okunuki", characters: "Aiko Hazuki"],
    [actor: "Tsuyoshi Shimada", characters: "Photographer"],
    [actor: "Junko Takahata", characters: "Kaoru Arima"],
    [actor: "Yoji Tanaka", characters: "Teacher"],
    [actor: "Kazue Tsunogae", characters: "Sudo"],
    [actor: "Takeshi Wakamatsu", characters: "Yusaku Shigemori"],
    [actor: "Kanji Watanabe", characters: "Yuki"],
    [actor: "Koji Yano", characters: "Kasuhiko Tamba"]
  ],
  synopsis: true
]
