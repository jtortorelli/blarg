[
  title: "The X from Outer Space",
  path: "x-from-outer-space-1967",
  original_title: "宇宙大怪獣ギララ",
  transliteration: "Uchū Daikaijū Girara",
  translation: "Giant Space Monster Guirara",
  release_date: ~D[1967-03-25],
  runtime: 88,
  produced_by: "Shochiku",
  staff: [
    [roles: "Director", people: "Kazui Nihonmatsu"],
    [roles: "Special Effects Director", people: "Hiroshi Ikeda"],
    [roles: "Screenplay", people: ["Eibi Motomochi", "Moriyoshi Ishida", "Kazui Nihonmatsu"]],
    [roles: "Photography", people: ["Shizuo Hirase", "Chita Okoshi"]],
    [roles: "Art", people: "Shigemori Shigeta"],
    [roles: "Music", people: "Taku Izumi"],
    [roles: "Lighting", people: ["Masa Tsubuki", "Toshifumi Takahashi"]],
    [roles: "Editor", people: "Yoshi Sugihara"]
  ],
  top_billed_cast: [
    [actor: "Shunya Wazaki", characters: "Sano"],
    [actor: "Itoko Harada", characters: "Michiko"],
    [actor: "Shinichi Yanagisawa", characters: "Miyamoto"],
    [actor: "Keisuke Sonoi", characters: "Shioda"],
    [actor: "Hiroshi Fujioka", characters: "Moon Base Staff"],
    [actor: "Eiji Okada", characters: "Dr. Kato"],
    [actor: "Peggy Neal", characters: "Lisa"],
    [actor: "Franz Gruber", characters: "Dr. Berman"],
    [actor: "Mike Daneen", characters: "Stein"],
    [actor: "Ryuji Kita", characters: "SDF Chief"],
    [actor: "Torahiko Hamada", characters: "Kimura"],
    [actor: "Kathy Horan", characters: "Moon Base Nurse"]
  ],
  credits: true,
  synopsis: true
]
