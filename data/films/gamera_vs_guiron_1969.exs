[
  title: "Gamera vs. Guiron",
  path: "gamera-vs-guiron-1969",
  original_title: "ガメラ対大悪獣ギロン",
  transliteration: "Gamera Tai Daiakujū Giron",
  translation: "Gamera Against Great Villain Beast Guiron",
  aliases: "Attack of the Monsters",
  release_date: ~D[1969-03-21],
  runtime: 82,
  preceded_by: {"Gamera vs. Viras", 1968},
  followed_by: {"Gamera vs. Jiger", 1970},
  produced_by: "Daiei",
  staff: [
    [roles: "Director", people: "Noriaki Yuasa"],
    [roles: "Hidemasa Nagata", people: "Producer"],
    [roles: "Screenplay", people: "Nisan Takahashi"],
    [roles: "Photography", people: "Akira Kitazaki"],
    [roles: "Sound", people: "Kimio Tobita"],
    [roles: "Lighting", people: "Shoichi Uehara"],
    [roles: "Art", people: "Akira Inoue"],
    [roles: "Music", people: "Shunsuke Kikuchi"],
    [roles: "Editor", people: "Yoshiyuki Miyazaki"]
  ],
  top_billed_cast: [
    [actor: "Nobuhiro Kashima", characters: "Akio"],
    [actor: "Christopher Murphy", characters: "Tom"],
    [actor: "Miyuki Akiyama", characters: "Tomoko"],
    [actor: "Eiji Funakoshi", characters: "Astronomer"],
    [actor: "Kon Omura", characters: "Officer Kondo"],
    [actor: "Yuko Hamada", characters: "Akio's Mother"],
    [actor: "Edith Hanson", characters: "Tom's Mother"],
    [actor: "Reiko Kasahara", characters: "Floravera"],
    [actor: "Hiroko Kai", characters: "Barbera"],
    [actor: "Sho Natsuki", characters: "Reporter"],
    [actor: "Teppei Endo", characters: "Reporter"],
    [actor: "Tsutomu Nakata", characters: "Reporter"],
    [actor: "Masaki Minamido", characters: "Assistant Astronomer"],
    [actor: "Daihachi Kita", characters: "Policeman"]
  ],
  other_cast: [
    [actor: "Umenosuke Izumi", characters: "Gamera"],
    [actor: "Keiichiro Yamane", characters: "Reporter"]
  ],
  credits: true,
  synopsis: true
]
