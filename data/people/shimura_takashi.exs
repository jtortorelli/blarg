[
  given_name: "Takashi",
  family_name: "Shimura",
  path: "shimura-takashi",
  original_name: "志村 喬",
  birth_name: "Shoji Shimazaki (島崎 捷爾)",
  dob: [year: 1905, month: 3, day: 12],
  birth_place: "Ikuno, Asago, Hyogo, Japan",
  dod: [year: 1982, month: 2, day: 11],
  death_place: "Shinjuku, Tokyo, Japan",
  selected_filmography: [
    roles: [
      [
        role: "Actor",
        films: [
          [film: {"Rashomon", 1950}, characters: "The Woodcutter"],
          [film: {"Seven Samurai", 1954}, characters: "Kanbei Shimada"],
          [film: {"Godzilla, King of the Monsters", 1954}, characters: "Dr. Kyohei Yamane"],
          [film: {"Godzilla Raids Again", 1955}, characters: "Dr. Kyohei Yamane"],
          [film: {"Samurai III: Duel at Ganryu Island", 1956}, characters: "Okinaga Matsui"],
          [film: {"Throne of Blood", 1957}, characters: "Noriyasu Odagura"],
          [film: {"The Mysterians", 1957}, characters: "Dr. Tanjiro Adachi"],
          [film: {"The Hidden Fortress", 1958}, characters: "Izumi Nagakura"],
          [film: {"The Birth of Japan", 1959}, characters: "Elder Kumaso"],
          [film: {"Daredevil in the Castle", 1961}, characters: "Katsumoto Katagiri"],
          [film: {"Yojimbo", 1961}, characters: "Sake Merchant Tokuemon"],
          [film: {"Mothra", 1961}, characters: "Newspaper Editor"],
          [film: {"Sanjuro", 1962}, characters: "Kurofuji"],
          [film: {"Gorath", 1962}, characters: "Dr. Keisuke Sonoda"],
          [film: {"High and Low", 1963}, characters: "Police Executive"],
          [film: {"Samurai Pirate", 1963}, characters: "King Rasetsu"],
          [film: {"Ghidorah, the Three-Headed Monster", 1964}, characters: "Dr. Tsukamoto"],
          [film: {"Frankenstein Conquers the World", 1965}, characters: "Axis Scientist"],
          [film: {"Zatoichi and the Fugitives", 1968}, characters: "Junan"],
          [film: {"Zatoichi's Conspiracy", 1973}, characters: "Sakubei"],
          [film: {"Prophecies of Nostradamus", 1974}, characters: "Hospital Director"],
          [film: {"The Bullet Train", 1975}, characters: "JNR President"],
          [film: {"Kagemusha", 1980}, characters: "Gyobu Taguchi"]
        ]
      ]
    ]
  ],
  bio: true
]
