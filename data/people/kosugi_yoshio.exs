[
  given_name: "Yoshio",
  family_name: "Kosugi",
  path: "kosugi-yoshio",
  original_name: "小杉 義男",
  dob: [year: 1903, month: 9, day: 15],
  birth_place: "Nikko, Kamitsuga, Tochigi, Japan",
  dod: [year: 1968, month: 3, day: 12],
  selected_filmography: [
    roles: [
      [
        role: "Actor",
        films: [
          [film: {"Seven Samurai", 1954}, characters: "Mosuke"],
          [film: {"Samurai I: Musashi Miyamoto", 1954}, characters: "Tanzaemon Aoki"],
          [film: {"The Mysterians", 1957}, characters: "Officer Sugimoto"],
          [film: {"The Hidden Fortress", 1958}, characters: "Akizuki Soldier"],
          [film: {"The Birth of Japan", 1959}, characters: "Inaba"],
          [film: {"The Human Vapor", 1960}, characters: "Detective Inao"],
          [film: {"Daredevil in the Castle", 1961}, characters: "Gitao Morimoto"],
          [film: {"Mothra", 1961}, characters: "Ship's Captain"],
          [film: {"King Kong vs. Godzilla", 1962}, characters: "Faro Island Chief"],
          [film: {"Samurai Pirate", 1963}, characters: "Ming Advisor"],
          [film: {"Whirlwind", 1964}, characters: "Iyo Nonomura"],
          [film: {"Mothra vs. Godzilla", 1964}, characters: "Infant Island Chief"],
          [film: {"Ghidorah, the Three-Headed Monster", 1964}, characters: "Infant Island Chief"],
          [film: {"Frankenstein Conquers the World", 1965}, characters: "Soldier"]
        ]
      ]
    ]
  ],
  bio: true
]
