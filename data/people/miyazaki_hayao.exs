[
  given_name: "Hayao",
  family_name: "Miyazaki",
  path: "miyazaki-hayao",
  original_name: "宮崎 駿",
  dob: [year: 1941, month: 1, day: 15],
  birth_place: "Tokyo, Japan",
  selected_filmography: [
    roles: [
      [
        role: "Director",
        films: [
          [film: {"Lupin the Third: The Castle of Cagliostro", 1979}],
          [film: {"Nausicaä of the Valley of the Wind", 1984}],
          [film: {"Castle in the Sky", 1986}],
          [film: {"My Neighbor Totoro", 1988}],
          [film: {"Kiki's Delivery Service", 1989}],
          [film: {"Porco Rosso", 1992}],
          [film: {"Princess Mononoke", 1997}],
          [film: {"Spirited Away", 2001}],
          [film: {"Howl's Moving Castle", 2004}],
          [film: {"Ponyo", 2008}],
          [film: {"The Wind Rises", 2013}]
        ]
      ],
      [
        role: "Producer",
        films: [
          [film: {"Kiki's Delivery Service", 1989}]
        ]
      ],
      [
        role: "Screenwriter",
        films: [
          [film: {"Lupin the Third: The Castle of Cagliostro", 1979}],
          [film: {"Nausicaä of the Valley of the Wind", 1984}],
          [film: {"Castle in the Sky", 1986}],
          [film: {"My Neighbor Totoro", 1988}],
          [film: {"Kiki's Delivery Service", 1989}],
          [film: {"Porco Rosso", 1992}],
          [film: {"Princess Mononoke", 1997}],
          [film: {"Spirited Away", 2001}],
          [film: {"Howl's Moving Castle", 2004}],
          [film: {"Ponyo", 2008}],
          [film: {"The Wind Rises", 2013}]
        ]
      ]
    ]
  ],
  bio: true
]
