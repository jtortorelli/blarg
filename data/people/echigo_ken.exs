[
  given_name: "Ken",
  family_name: "Echigo",
  path: "echigo-ken",
  original_name: "越後 憲",
  aliases: "Kenzo Echigo (越後 憲三)",
  dob: [year: 1929, month: 12, day: 2],
  birth_place: "Akita, Japan",
  dod: [unknown: true],
  selected_filmography: [
    roles: [
      [
        role: "Actor",
        films: [
          [film: {"Samurai I: Musashi Miyamoto", 1954}, characters: "Villager"],
          [film: {"Godzilla, King of the Monsters", 1954}, characters: "Sailor"],
          [film: {"The Invisible Man", 1954}, characters: "Waiter"],
          [film: {"Rodan", 1956}, characters: "Policeman"],
          [film: {"The Mysterians", 1957}, characters: "Pilot"],
          [film: {"The H-Man", 1958}, characters: "Nightclub Patron"],
          [film: {"The Birth of Japan", 1959}, characters: "Yamato Soldier"],
          [film: {"Battle in Outer Space", 1959}, characters: "Soldier"],
          [film: {"The Secret of the Telegian", 1960}, characters: "Policeman"],
          [film: {"The Human Vapor", 1960}, characters: "Scientist"],
          [film: {"Daredevil in the Castle", 1961}, characters: "Warrior Priest"],
          [film: {"The Last War", 1961}, characters: "TV Singer"],
          [film: {"Sanjuro", 1962}, characters: "Kikui Samurai"],
          [film: {"Gorath", 1962}, characters: "Satellite Crew"],
          [film: {"High and Low", 1963}, characters: "Train Crew"],
          [film: {"Mothra vs. Godzilla", 1964}, characters: "Reporter"],
          [film: {"Dogora, the Space Monster", 1964}, characters: "Soldier"],
          [
            film: {"Ghidorah, the Three-Headed Monster", 1964},
            characters: ["Bystander", "Cafe Patron"]
          ],
          [film: {"Frankenstein Conquers the World", 1965}, characters: "Man Fleeing Resort"],
          [film: {"Godzilla vs. the Sea Monster", 1966}, characters: "Red Bamboo Soldier"],
          [film: {"Destroy All Monsters", 1968}, characters: "SY-3 Pilot"],
          [film: {"Latitude Zero", 1969}, characters: "Observer"],
          [film: {"Godzilla's Revenge", 1969}, characters: "Policeman"],
          [film: {"Space Amoeba", 1970}, characters: "Journalist"],
          [film: {"Godzilla vs. Hedorah", 1971}, characters: "Talking Head"]
        ]
      ]
    ]
  ],
  bio: true
]
