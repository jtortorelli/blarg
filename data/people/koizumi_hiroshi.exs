[
  given_name: "Hiroshi",
  family_name: "Koizumi",
  path: "koizumi-hiroshi",
  original_name: "小泉 博",
  dob: [year: 1926, month: 8, day: 12],
  birth_place: "Kamakura, Kanagawa, Japan",
  dod: [year: 2015, month: 5, day: 31],
  selected_filmography: [
    roles: [
      [
        role: "Actor",
        films: [
          [film: {"Godzilla Raids Again", 1955}, characters: "Tsukioka"],
          [film: {"Mothra", 1961}, characters: "Dr. Shinichi Chujo"],
          [film: {"Matango", 1963}, characters: "Naoyuki Sakuta"],
          [film: {"Atragon", 1963}, characters: "Detective Ito"],
          [film: {"Mothra vs. Godzilla", 1964}, characters: "Dr. Miura"],
          [film: {"Dogora, the Space Monster", 1964}, characters: "Dr. Kirino"],
          [film: {"Ghidorah, the Three-Headed Monster", 1964}, characters: "Dr. Murai"],
          [film: {"Godzilla vs. the Cosmic Monster", 1974}, characters: "Dr. Wagura"],
          [film: {"Prophecies of Nostradamus", 1974}, characters: "Environmental Scientist"],
          [film: {"The Return of Godzilla", 1984}, characters: "Dr. Minami"],
          [
            film: {"Godzilla X Mothra X Mechagodzilla: Tokyo SOS", 2003},
            characters: "Shinichi Chujo"
          ]
        ]
      ]
    ]
  ],
  bio: true
]
