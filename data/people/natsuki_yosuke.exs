[
  given_name: "Yosuke",
  family_name: "Natsuki",
  path: "natsuki-yosuke",
  original_name: "夏木 陽介",
  birth_name: "Tamotsu Akuzawa (阿久沢 有)",
  dob: [year: 1936, month: 2, day: 27],
  birth_place: "Hachioji, Tokyo, Japan",
  dod: [year: 2018, month: 1, day: 14],
  selected_filmography: [
    roles: [
      [
        role: "Actor",
        films: [
          [film: {"The H-Man", 1958}, characters: "Bystander"],
          [film: {"Daredevil in the Castle", 1961}, characters: "Nagato Kimura"],
          [film: {"Yojimbo", 1961}, characters: "Farmer's Son"],
          [film: {"Whirlwind", 1964}, characters: "Kyunosuke Okuno"],
          [film: {"Dogora, the Space Monster", 1964}, characters: "Detective Komai"],
          [film: {"Ghidorah, the Three-Headed Monster", 1964}, characters: "Detective Shindo"],
          [film: {"The Return of Godzilla", 1984}, characters: "Shin Hayashida"]
        ]
      ]
    ]
  ],
  bio: true
]
