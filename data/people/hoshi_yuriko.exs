[
  given_name: "Yuriko",
  family_name: "Hoshi",
  path: "hoshi-yuriko",
  original_name: "星 由里子",
  dob: [year: 1943, month: 12, day: 6],
  birth_place: "Kajicho, Chiyoda, Tokyo, Japan",
  dod: [year: 2018, month: 5, day: 16],
  death_place: "Kyoto, Japan",
  selected_filmography: [
    roles: [
      [
        role: "Actress",
        films: [
          [film: {"Daredevil in the Castle", 1961}, characters: "Princess Sen"],
          [film: {"The Last War", 1961}, characters: "Saeko Tamura"],
          [film: {"Whirlwind", 1964}, characters: "Kozato"],
          [film: {"Mothra vs. Godzilla", 1964}, characters: "Junko Nakanishi"],
          [film: {"Ghidorah, the Three-Headed Monster", 1964}, characters: "Naoko Shindo"],
          [film: {"Godzilla X Megaguirus", 2000}, characters: "Yoshino Yoshizawa"]
        ]
      ]
    ]
  ],
  bio: true
]
