[
  given_name: "Harold",
  family_name: "Conway",
  path: "conway-harold-s",
  middle_name: "S.",
  japanese_name: "ハロルド・S・コンウェイ",
  dob: [year: 1911, month: 5, day: 24],
  birth_place: "Pennsylvania, United States",
  dod: [year: 1996],
  death_place: "Japan",
  selected_filmography: [
    roles: [
      [
        role: "Actor",
        films: [
          [film: {"The Mysterians", 1957}, characters: "Dr. DeGracia"],
          [film: {"Battle in Outer Space", 1959}, characters: "Dr. Immelman"],
          [film: {"Mothra", 1961}, characters: "Rolisican Ambassador"],
          [film: {"The Last War", 1961}, characters: "Federation Missile Commander"],
          [film: {"King Kong vs. Godzilla", 1962}, characters: "Scientist"],
          [
            film: {"Mothra vs. Godzilla", 1964},
            characters: "Military Advisor (American Version)"
          ],
          [film: {"Goke, Body Snatcher from Hell", 1968}, characters: "UK Ambassador"],
          [film: {"Genocide", 1968}, characters: "USAF Officer"]
        ]
      ]
    ]
  ],
  bio: true
]
