[
  given_name: "Minosuke",
  family_name: "Yamada",
  path: "yamada-minosuke",
  original_name: "山田 巳之助",
  dob: [year: 1893, month: 11, day: 13],
  birth_place: "Tokyo, Japan",
  dod: [year: 1968, month: 8, day: 3],
  selected_filmography: [
    roles: [
      [
        role: "Actor",
        films: [
          [film: {"Godzilla Raids Again", 1955}, characters: "Defense Secretary"],
          [film: {"Samurai III: Duel at Ganryu Island", 1956}, characters: "Old Man Villager"],
          [film: {"Rodan", 1956}, characters: "Mining Chief Osaki"],
          [film: {"The Mysterians", 1957}, characters: "Defense Secretary"],
          [film: {"The H-Man", 1958}, characters: "Police Executive"],
          [film: {"Varan the Unbelievable", 1958}, characters: "Defense Secretary"],
          [film: {"The Birth of Japan", 1959}, characters: "Okuri"],
          [film: {"The Human Vapor", 1960}, characters: "Newspaper Executive"]
        ]
      ]
    ]
  ],
  bio: true
]
