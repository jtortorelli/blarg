[
  given_name: "Toru",
  family_name: "Ibuki",
  path: "ibuki-toru",
  original_name: "伊吹 徹",
  dob: [year: 1940, month: 1, day: 28],
  birth_place: "Fukuoka, Japan",
  selected_filmography: [
    roles: [
      [
        role: "Actor",
        films: [
          [film: {"Ghidorah, the Three-Headed Monster", 1964}, characters: "Assassin"],
          [film: {"Monster Zero", 1965}, characters: "Xian"],
          [film: {"Godzilla vs. the Sea Monster", 1966}, characters: "Yata"],
          [film: {"King Kong Escapes", 1967}, characters: "Who Henchman"],
          [film: {"Destroy All Monsters", 1968}, characters: "Possessed Monster Island Tech"],
          [film: {"Terror of Mechagodzilla", 1975}, characters: "Tsuda"]
        ]
      ]
    ]
  ],
  bio: true
]
