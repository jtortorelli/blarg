[
  given_name: "Tetsu",
  family_name: "Nakamura",
  path: "nakamura-tetsu",
  original_name: "中村 哲",
  birth_name: "Satoshi Nakamura (中村 哲)",
  dob: [year: 1908, month: 9, day: 19],
  birth_place: "Vancouver, British Columbia, Canada",
  dod: [year: 1992, month: 8, day: 3],
  selected_filmography: [
    roles: [
      [
        role: "Actor",
        films: [
          [film: {"The Mysterians", 1957}, characters: "Dr. Koda"],
          [film: {"The H-Man", 1958}, characters: "Mr. Chin"],
          [film: {"The Human Vapor", 1960}, characters: "Newspaper Executive"],
          [film: {"Mothra", 1961}, characters: "Nelson's Henchman"],
          [film: {"Samurai Pirate", 1963}, characters: "Archer"],
          [film: {"Atragon", 1963}, characters: "Cargo Ship Captain"],
          [film: {"Latitude Zero", 1969}, characters: "Dr. Okada"],
          [film: {"Space Amoeba", 1970}, characters: "Ombo"],
          [film: {"The Submersion of Japan", 1973}, characters: "UN Ambassador"]
        ]
      ]
    ]
  ],
  bio: true
]
