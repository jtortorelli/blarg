[
  given_name: "Jun",
  family_name: "Funato",
  path: "funato-jun",
  original_name: "船戸 順",
  birth_name: "Tsunetaka Nishina (仁科 常隆)",
  dob: [year: 1938, month: 11, day: 26],
  birth_place: "Wakayama, Japan",
  selected_filmography: [
    roles: [
      [
        role: "Actor",
        films: [
          [film: {"Samurai Pirate", 1963}, characters: "The Prince of Ming"],
          [film: {"Dogora, the Space Monster", 1964}, characters: "Detective Nitta"]
        ]
      ]
    ]
  ],
  bio: true
]
