[
  given_name: "Tadashi",
  family_name: "Okabe",
  path: "okabe-tadashi",
  original_name: "岡部 正",
  dob: [year: 1923, month: 5, day: 11],
  dod: [unknown: true],
  birth_place: "Saitama, Japan",
  selected_filmography: [
    roles: [
      [
        role: "Actor",
        films: [
          [film: {"Godzilla, King of the Monsters", 1954}, characters: "Yamane's Assistant"],
          [film: {"The Invisible Man", 1954}, characters: "Detective"],
          [film: {"Samurai III: Duel at Ganryu Island", 1956}, characters: "Spectator at Duel"],
          [film: {"Rodan", 1956}, characters: "Reporter"],
          [film: {"The Mysterians", 1957}, characters: "Pilot"],
          [film: {"Battle in Outer Space", 1959}, characters: "Military Officer"],
          [film: {"The Secret of the Telegian", 1960}, characters: "Checkpoint Policeman"],
          [film: {"Daredevil in the Castle", 1961}, characters: "Villager"],
          [film: {"Mothra", 1961}, characters: "Expedition Member"],
          [film: {"Gorath", 1962}, characters: "Spaceship Crew"],
          [film: {"King Kong vs. Godzilla", 1962}, characters: "Soldier"],
          [film: {"Atragon", 1963}, characters: "Policeman"],
          [film: {"Mothra vs. Godzilla", 1964}, characters: "Soldier"],
          [film: {"Dogora, the Space Monster", 1964}, characters: "Policeman"],
          [
            film: {"Ghidorah, the Three-Headed Monster", 1964},
            characters: "Tsukamoto's Assistant"
          ],
          [film: {"Frankenstein Conquers the World", 1965}, characters: "Reporter"],
          [film: {"Monster Zero", 1965}, characters: "Reporter"],
          [film: {"War of the Gargantuas", 1966}, characters: "Reporter"],
          [film: {"Godzilla vs. the Sea Monster", 1966}, characters: "Red Bamboo Scientist"],
          [film: {"King Kong Escapes", 1967}, characters: ["Submarine Crew", "Soldier"]],
          [film: {"Destroy All Monsters", 1968}, characters: "Mt. Fuji Reporter"],
          [film: {"Latitude Zero", 1969}, characters: "Reporter"],
          [film: {"Space Amoeba", 1970}, characters: "Journalist"],
          [film: {"Godzilla vs. Hedorah", 1971}, characters: "TV Scientist"],
          [film: {"Godzilla vs. Gigan", 1972}, characters: "Policeman"]
        ]
      ]
    ]
  ],
  bio: true
]
