[
  given_name: "Chotaro",
  family_name: "Togin",
  path: "togin-chotaro",
  original_name: "当銀 長太郎",
  dob: [year: 1941, month: 11, day: 18],
  birth_place: "Arakawa, Tokyo, Japan",
  selected_filmography: [
    roles: [
      [
        role: "Actor",
        films: [
          [film: {"Dogora, the Space Monster", 1964}, characters: "Truck Driver"],
          [film: {"Godzilla vs. the Sea Monster", 1966}, characters: "Ichino"],
          [film: {"Son of Godzilla", 1967}, characters: "Pilot"],
          [film: {"Destroy All Monsters", 1968}, characters: "SY-3 Pilot Ogata"],
          [film: {"Godzilla's Revenge", 1969}, characters: "Detective"],
          [film: {"Space Amoeba", 1970}, characters: "Yokoyama"]
        ]
      ]
    ]
  ],
  bio: true
]
