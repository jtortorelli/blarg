[
  given_name: "Soji",
  family_name: "Ubukata",
  path: "ubukata-soji",
  original_name: "生方 壮児",
  dob: [year: 1908, month: 3, day: 29],
  birth_place: "Tokyo, Japan",
  dod: [unknown: true],
  selected_filmography: [
    roles: [
      [
        role: "Actor",
        films: [
          [film: {"The Mysterians", 1957}, characters: "Dr. Noda"],
          [film: {"The H-Man", 1958}, characters: "Police Executive"],
          [film: {"Varan the Unbelievable", 1958}, characters: "Policeman"],
          [film: {"Daredevil in the Castle", 1961}, characters: "Court Samurai"],
          [film: {"The Last War", 1961}, characters: "Minister"],
          [film: {"High and Low", 1963}, characters: "Detective"],
          [film: {"Mothra vs. Godzilla", 1964}, characters: "Reporter"],
          [film: {"Ghidorah, the Three-Headed Monster", 1964}, characters: "Parliamentarian"],
          [film: {"Frankenstein Conquers the World", 1965}, characters: "Reporter"],
          [film: {"Monster Zero", 1965}, characters: "Minister"],
          [film: {"Destroy All Monsters", 1968}, characters: "Reporter"],
          [film: {"Godzilla vs. Hedorah", 1971}, characters: "Talking Head"]
        ]
      ]
    ]
  ],
  bio: true
]
