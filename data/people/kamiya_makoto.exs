[
  given_name: "Makoto",
  family_name: "Kamiya",
  path: "kamiya-makoto",
  original_name: "神谷 誠",
  dob: [year: 1965, month: 10, day: 6],
  birth_place: "Tokyo, Japan",
  selected_filmography: [
    roles: [
      [
        role: "Actor",
        films: [
          [film: {"Returner", 2002}, characters: "Wounded Future Soldier"]
        ]
      ],
      [
        role: "SFX Assistant Director",
        films: [
          [film: {"Godzilla VS Biollante", 1989}],
          [film: {"Godzilla VS King Ghidorah", 1991}],
          [film: {"Godzilla VS Mothra", 1992}],
          [film: {"Gamera: The Guardian of the Universe", 1995}],
          [film: {"Gamera 2: Advent of Legion", 1996}],
          [film: {"Gamera 3: Revenge of Iris", 1999}],
          [film: {"Avalon", 2001}]
        ]
      ],
      [
        role: "SFX Director",
        films: [
          [film: {"GMK", 2001}],
          [film: {"The Sinking of Japan", 2006}],
          [film: {"L: Change the World", 2008}],
          [film: {"Gantz", 2011}],
          [film: {"Gantz: Perfect Answer", 2011}],
          [film: {"I Am A Hero", 2016}],
          [film: {"Death Note: Light Up the New World", 2016}]
        ]
      ],
      [
        role: "VFX Supervisor",
        films: [
          [film: {"Library Wars", 2013}],
          [film: {"Library Wars: The Last Mission", 2015}]
        ]
      ]
    ]
  ],
  bio: true
]
