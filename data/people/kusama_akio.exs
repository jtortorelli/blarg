[
  given_name: "Akio",
  family_name: "Kusama",
  path: "kusama-akio",
  original_name: "草間 璋夫",
  dob: [year: 1913, month: 10, day: 7],
  dod: [unknown: true],
  selected_filmography: [
    roles: [
      [
        role: "Actor",
        films: [
          [film: {"Seven Samurai", 1954}, characters: "Bandit"],
          [film: {"Samurai III: Duel at Ganryu Island", 1956}, characters: "Villager"],
          [film: {"Rodan", 1956}, characters: "Tsuda"],
          [film: {"The Mysterians", 1957}, characters: "Policeman"],
          [film: {"The H-Man", 1958}, characters: "Police Chemist"],
          [film: {"Varan the Unbelievable", 1958}, characters: "Soldier"],
          [film: {"The Hidden Fortress", 1958}, characters: "Yamana Peasant"],
          [film: {"The Birth of Japan", 1959}, characters: ["Yamato Villager", "Deity"]],
          [film: {"Battle in Outer Space", 1959}, characters: "Photographer"],
          [film: {"The Secret of the Telegian", 1960}, characters: "Police Executive"],
          [film: {"The Human Vapor", 1960}, characters: "Police Executive"],
          [film: {"Daredevil in the Castle", 1961}, characters: "Villager"],
          [film: {"Yojimbo", 1961}, characters: "Ushitora Underling"],
          [film: {"Mothra", 1961}, characters: ["Military Officer", "Soldier"]],
          [film: {"The Last War", 1961}, characters: "Press Club Chauffeur"],
          [film: {"Sanjuro", 1962}, characters: "Kikui Samurai"],
          [film: {"Gorath", 1962}, characters: "Minister"],
          [film: {"King Kong vs. Godzilla", 1962}, characters: "Military Official"],
          [film: {"High and Low", 1963}, characters: "Detective"],
          [film: {"Matango", 1963}, characters: "Official"],
          [film: {"Samurai Pirate", 1963}, characters: "Villager"],
          [film: {"Mothra vs. Godzilla", 1964}, characters: ["Military Official", "Islander"]],
          [film: {"Dogora, the Space Monster", 1964}, characters: "Soldier"],
          [film: {"Ghidorah, the Three-Headed Monster", 1964}, characters: "Parliamentarian"],
          [film: {"Frankenstein Conquers the World", 1965}, characters: "Policeman"],
          [film: {"Monster Zero", 1965}, characters: "Military Advisor"],
          [film: {"The Adventures of Taklamakan", 1966}, characters: "Pesil Villager"],
          [film: {"War of the Gargantuas", 1966}, characters: "Military Advisor"],
          [film: {"King Kong Escapes", 1967}, characters: "Military Advisor"],
          [
            film: {"Destroy All Monsters", 1968},
            characters: ["Monster Island Tech", "Military Advisor"]
          ],
          [film: {"Latitude Zero", 1969}, characters: "Observer"],
          [film: {"Godzilla vs. Hedorah", 1971}, characters: "Talking Head"],
          [film: {"Godzilla vs. the Cosmic Monster", 1974}, characters: "Construction Worker"],
          [film: {"Prophecies of Nostradamus", 1974}, characters: ["Face in Crowd", "Minister"]],
          [film: {"Terror of Mechagodzilla", 1975}, characters: "Skeptical Scientist"]
        ]
      ]
    ]
  ],
  bio: true
]
