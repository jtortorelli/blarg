[
  given_name: "Sonosuke",
  family_name: "Sawamura",
  path: "sawamura-sonosuke",
  original_name: "澤村 宗之助",
  dob: [year: 1918, month: 7, day: 1],
  birth_place: "Akakusa, Tokyo, Japan",
  dod: [year: 1978, month: 11, day: 3],
  selected_filmography: [
    roles: [
      [
        role: "Actor",
        films: [
          [film: {"The Invisible Man", 1954}, characters: "Parliamentarian"],
          [film: {"Godzilla Raids Again", 1955}, characters: "Branch Manager Shibeki"],
          [
            film: {"Samurai III: Duel at Ganryu Island", 1956},
            characters: "Tarozaemon Kobayashi"
          ],
          [film: {"The Tale of Zatoichi Continues", 1962}, characters: "Boss Kanbei"],
          [film: {"Zatoichi on the Road", 1963}, characters: "Boss Tobei"],
          [film: {"Zatoichi's Revenge", 1965}, characters: "Boss Tatsugoro"]
        ]
      ]
    ]
  ],
  bio: true
]
