[
  given_name: "Teruyoshi",
  family_name: "Nakano",
  path: "nakano-teruyoshi",
  original_name: "中野 昭慶",
  dob: [year: 1935, month: 10, day: 9],
  birth_place: "Dandong, China",
  selected_filmography: [
    roles: [
      [
        role: "Assistant Director",
        films: [
          [film: {"Godzilla's Revenge", 1969}]
        ]
      ],
      [
        role: "SFX Assistant Director",
        films: [
          [film: {"Matango", 1963}],
          [film: {"Samurai Pirate", 1963}],
          [film: {"Atragon", 1963}],
          [film: {"Whirlwind", 1964}],
          [film: {"Mothra vs. Godzilla", 1964}],
          [film: {"Dogora, the Space Monster", 1964}],
          [film: {"Ghidorah, the Three-Headed Monster", 1964}],
          [film: {"Frankenstein Conquers the World", 1965}],
          [film: {"Monster Zero", 1965}],
          [film: {"War of the Gargantuas", 1966}],
          [film: {"Godzilla vs. the Sea Monster", 1966}],
          [film: {"King Kong Escapes", 1967}],
          [film: {"Son of Godzilla", 1967}],
          [film: {"Destroy All Monsters", 1968}],
          [film: {"Latitude Zero", 1969}],
          [film: {"Space Amoeba", 1970}]
        ]
      ],
      [
        role: "SFX Director",
        films: [
          [film: {"Godzilla vs. Hedorah", 1971}],
          [film: {"Godzilla vs. Gigan", 1972}],
          [film: {"Godzilla vs. Megalon", 1973}],
          [film: {"The Submersion of Japan", 1973}],
          [film: {"Godzilla vs. the Cosmic Monster", 1974}],
          [film: {"Prophecies of Nostradamus", 1974}],
          [film: {"Espy", 1974}],
          [film: {"Terror of Mechagodzilla", 1975}],
          [film: {"The Explosion", 1975}],
          [film: {"The War in Space", 1977}],
          [film: {"Earthquake Archipelago", 1980}],
          [film: {"The Return of Godzilla", 1984}],
          [film: {"Tokyo Blackout", 1987}]
        ]
      ]
    ]
  ],
  bio: true
]
