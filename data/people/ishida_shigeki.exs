[
  given_name: "Shigeki",
  family_name: "Ishida",
  path: "ishida-shigeki",
  original_name: "石田 茂樹",
  dob: [year: 1924, month: 3, day: 17],
  birth_place: "Kanazawa, Ishikawa, Japan",
  dod: [year: 1997],
  selected_filmography: [
    roles: [
      [
        role: "Actor",
        films: [
          [film: {"The Last War", 1961}, characters: "Press Club Chauffeur"],
          [film: {"Ghidorah, the Three-Headed Monster", 1964}, characters: "Parliamentarian"],
          [film: {"Frankenstein Conquers the World", 1965}, characters: "Skeptical Scientist"],
          [film: {"The Adventures of Taklamakan", 1966}, characters: "Royal Advisor"],
          [film: {"Godzilla vs. the Sea Monster", 1966}, characters: "Newspaper Editor"],
          [film: {"Godzilla's Revenge", 1969}, characters: "Apartment Manager"]
        ]
      ]
    ]
  ],
  bio: true
]
