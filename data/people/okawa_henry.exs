[
  given_name: "Henry",
  family_name: "Okawa",
  path: "okawa-henry",
  original_name: "ヘンリー大川",
  birth_name: "Heihachiro Okawa (大川 平八郎)",
  dob: [year: 1905, month: 9, day: 9],
  birth_place: "Saitama, Japan",
  dod: [year: 1971, month: 5, day: 27],
  selected_filmography: [
    roles: [
      [
        role: "Actor",
        films: [
          [film: {"The Mysterians", 1957}, characters: "Interpreter"],
          [film: {"Ghidorah, the Three-Headed Monster", 1964}, characters: "UFO Club Member"],
          [film: {"War of the Gargantuas", 1966}, characters: "Doctor"],
          [film: {"Destroy All Monsters", 1968}, characters: "UNSC Scientist"]
        ]
      ]
    ]
  ],
  bio: true
]
