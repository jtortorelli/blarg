[
  given_name: "Eiji",
  family_name: "Tsuburaya",
  path: "tsuburaya-eiji",
  original_name: "円谷 英二",
  birth_name: "Eiichi Tsuburaya (円谷 英一)",
  dob: [year: 1901, month: 7, day: 7],
  birth_place: "Sukagawa, Fukushima, Japan",
  dod: [year: 1970, month: 1, day: 25],
  death_place: "Ito, Shizuoka, Japan",
  selected_filmography: [
    roles: [
      [
        role: "Cinematographer",
        films: [
          [film: {"The Invisible Man", 1954}]
        ]
      ],
      [
        role: "SFX Director",
        films: [
          [film: {"Godzilla, King of the Monsters", 1954}],
          [film: {"Godzilla Raids Again", 1955}],
          [film: {"Rodan", 1956}],
          [film: {"The Mysterians", 1957}],
          [film: {"The H-Man", 1958}],
          [film: {"Varan the Unbelievable", 1958}],
          [film: {"The Birth of Japan", 1959}],
          [film: {"Battle in Outer Space", 1959}],
          [film: {"The Secret of the Telegian", 1960}],
          [film: {"The Human Vapor", 1960}],
          [film: {"Daredevil in the Castle", 1961}],
          [film: {"Mothra", 1961}],
          [film: {"The Last War", 1961}],
          [film: {"Gorath", 1962}],
          [film: {"King Kong vs. Godzilla", 1962}],
          [film: {"Matango", 1963}],
          [film: {"Samurai Pirate", 1963}],
          [film: {"Atragon", 1963}],
          [film: {"Whirlwind", 1964}],
          [film: {"Mothra vs. Godzilla", 1964}],
          [film: {"Dogora, the Space Monster", 1964}],
          [film: {"Ghidorah, the Three-Headed Monster", 1964}],
          [film: {"Frankenstein Conquers the World", 1965}],
          [film: {"Monster Zero", 1965}],
          [film: {"War of the Gargantuas", 1966}],
          [film: {"Godzilla vs. the Sea Monster", 1966}],
          [film: {"King Kong Escapes", 1967}],
          [film: {"Latitude Zero", 1969}]
        ]
      ],
      [
        role: "SFX Supervisor",
        films: [
          [film: {"The Invisible Man", 1954}],
          [film: {"Son of Godzilla", 1967}],
          [film: {"Destroy All Monsters", 1968}],
          [film: {"Godzilla's Revenge", 1969}]
        ]
      ]
    ]
  ],
  bio: true
]
