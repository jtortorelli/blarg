[
  given_name: "Nakajiro",
  family_name: "Tomita",
  path: "tomita-nakajiro",
  original_name: "富田 仲次郎",
  dob: [year: 1911, month: 11, day: 1],
  birth_place: "Yotsuya, Tokyo, Japan",
  dod: [year: 1990, month: 11, day: 15],
  selected_filmography: [
    roles: [
      [
        role: "Actor",
        films: [
          [film: {"Samurai III: Duel at Ganryu Island", 1956}, characters: "Kohei Tsujikaze"],
          [film: {"Throne of Blood", 1957}, characters: "Warlord"],
          [film: {"The Hidden Fortress", 1958}, characters: "Yamana Border Guard"],
          [film: {"Samurai Pirate", 1963}, characters: "Samurai Tokubei"],
          [film: {"Three Outlaw Samurai", 1964}, characters: "Onda"],
          [film: {"Ghidorah, the Three-Headed Monster", 1964}, characters: "General"],
          [film: {"Adventures of Zatoichi", 1964}, characters: "Kanjuro"]
        ]
      ]
    ]
  ],
  bio: true
]
