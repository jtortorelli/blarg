[
  given_name: "Keiji",
  family_name: "Sakakida",
  path: "sakakida-keiji",
  original_name: "榊田 敬二",
  dob: [year: 1900, month: 1, day: 15],
  birth_place: "Omagari, Senboku, Akita, Japan",
  dod: [unknown: true],
  selected_filmography: [
    roles: [
      [
        role: "Actor",
        films: [
          [film: {"Seven Samurai", 1954}, characters: "Gosaku"],
          [film: {"Godzilla, King of the Monsters", 1954}, characters: "Mayor Inada"],
          [film: {"The Invisible Man", 1954}, characters: "Security Guard"],
          [film: {"Godzilla Raids Again", 1955}, characters: ["Military Officer", "Policeman"]],
          [
            film: {"Samurai II: Duel at Ichijoji Temple", 1955},
            characters: "Yoshioka School Manservant"
          ],
          [film: {"Rodan", 1956}, characters: "Miner"],
          [film: {"The Mysterians", 1957}, characters: "Military Officer"],
          [film: {"The H-Man", 1958}, characters: "Policeman"],
          [film: {"Varan the Unbelievable", 1958}, characters: "Truck Driver"],
          [film: {"The Hidden Fortress", 1958}, characters: "Yamana Peasant"],
          [film: {"Battle in Outer Space", 1959}, characters: "UN Official"],
          [film: {"The Human Vapor", 1960}, characters: "Jail Officer"],
          [film: {"Daredevil in the Castle", 1961}, characters: "Court Samurai"],
          [film: {"Mothra", 1961}, characters: "Doctor"],
          [film: {"Sanjuro", 1962}, characters: "Samurai Reading Notice"],
          [film: {"Gorath", 1962}, characters: "Minister"],
          [film: {"Samurai Pirate", 1963}, characters: "Villager"],
          [film: {"Atragon", 1963}, characters: "Mihara Tourist"],
          [film: {"Mothra vs. Godzilla", 1964}, characters: ["Passerby", "Islander"]],
          [film: {"Ghidorah, the Three-Headed Monster", 1964}, characters: "Serginan Official"],
          [film: {"Frankenstein Conquers the World", 1965}, characters: "Scientist"],
          [film: {"Monster Zero", 1965}, characters: "Minister"],
          [film: {"The Adventures of Taklamakan", 1966}, characters: "Pesil Villager"],
          [film: {"Destroy All Monsters", 1968}, characters: "Reporter"],
          [film: {"Prophecies of Nostradamus", 1974}, characters: "Audience Member"],
          [film: {"Terror of Mechagodzilla", 1975}, characters: "Skeptical Scientist"]
        ]
      ]
    ]
  ],
  bio: true
]
