[
  given_name: "Kenzo",
  family_name: "Tabu",
  path: "tabu-kenzo",
  original_name: "田武 謙三",
  birth_name: "Yasutaro Tabu (田武 安太郎)",
  dob: [year: 1914, month: 8, day: 13],
  birth_place: "Kyoto, Japan",
  dod: [year: 1993, month: 11, day: 19],
  selected_filmography: [
    roles: [
      [
        role: "Actor",
        films: [
          [film: {"Seven Samurai", 1954}, characters: "Peasant"],
          [film: {"Samurai I: Musashi Miyamoto", 1954}, characters: "Soldier"],
          [film: {"Samurai II: Duel at Ichijoji Temple", 1955}, characters: "Kensuke Kenshi"],
          [film: {"King Kong vs. Godzilla", 1962}, characters: "TV Host"],
          [film: {"Mothra vs. Godzilla", 1964}, characters: "Industrial Park Developer"],
          [film: {"Frankenstein Conquers the World", 1965}, characters: "Skeptical Reporter"],
          [film: {"Monster Zero", 1965}, characters: "Xian Commander"],
          [film: {"Zatoichi Challenged", 1967}, characters: "Manzo"],
          [film: {"Zatoichi Goes to the Fire Festival", 1970}, characters: "Lecherous Bidder"],
          [film: {"Zatoichi's Conspiracy", 1973}, characters: "Traveler"],
          [film: {"Zatoichi", 1989}, characters: "Big Boss"]
        ]
      ]
    ]
  ],
  bio: true
]
