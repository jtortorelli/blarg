[
  given_name: "Yasuhisa",
  family_name: "Tsutsumi",
  path: "tsutsumi-yasuhisa",
  original_name: "堤 康久",
  dob: [year: 1922, month: 3, day: 30],
  birth_place: "Tokyo, Japan",
  dod: [unknown: true],
  selected_filmography: [
    roles: [
      [
        role: "Actor",
        films: [
          [film: {"Seven Samurai", 1954}, characters: "Villager"],
          [film: {"Samurai I: Musashi Miyamoto", 1954}, characters: "Villager"],
          [film: {"Godzilla, King of the Monsters", 1954}, characters: "Islander"],
          [film: {"The Invisible Man", 1954}, characters: "Jewelry Store Clerk"],
          [
            film: {"Samurai II: Duel at Ichijoji Temple", 1955},
            characters: "Yoshioka School Samurai"
          ],
          [film: {"Rodan", 1956}, characters: "Pilot"],
          [film: {"The Birth of Japan", 1959}, characters: "Yamato Soldier"],
          [film: {"Battle in Outer Space", 1959}, characters: "Train Conductor"],
          [film: {"The Secret of the Telegian", 1960}, characters: "Reporter"],
          [film: {"The Human Vapor", 1960}, characters: "Policeman"],
          [film: {"Daredevil in the Castle", 1961}, characters: "Zenbei's Apprentice"],
          [film: {"Mothra", 1961}, characters: "Expedition Member"],
          [film: {"King Kong vs. Godzilla", 1962}, characters: "Soldier"],
          [film: {"High and Low", 1963}, characters: "Detective"],
          [film: {"Samurai Pirate", 1963}, characters: "Samurai"],
          [film: {"Mothra vs. Godzilla", 1964}, characters: "Village Policeman"],
          [film: {"Dogora, the Space Monster", 1964}, characters: "Policeman"],
          [film: {"Monster Zero", 1965}, characters: "Soldier"],
          [film: {"War of the Gargantuas", 1966}, characters: "Soldier"]
        ]
      ]
    ]
  ],
  bio: true
]
