[
  given_name: "Nobuo",
  family_name: "Nakamura",
  path: "nakamura-nobuo",
  original_name: "中村 伸郎",
  dob: [year: 1908, month: 9, day: 14],
  birth_place: "Otaru, Hokkaido, Japan",
  dod: [year: 1991, month: 7, day: 5],
  death_place: "Tokyo, Japan",
  selected_filmography: [
    roles: [
      [
        role: "Actor",
        films: [
          [film: {"Throne of Blood", 1957}, characters: "Phantom Warrior"],
          [film: {"The Last War", 1961}, characters: "Cabinet Secretary"],
          [film: {"High and Low", 1963}, characters: "Ishimaru"],
          [film: {"Dogora, the Space Monster", 1964}, characters: "Dr. Munakata"],
          [
            film: {"Frankenstein Conquers the World", 1965},
            characters: "Skeptical Museum Curator"
          ],
          [film: {"War of the Gargantuas", 1966}, characters: "Dr. Kida"],
          [film: {"The Submersion of Japan", 1973}, characters: "Envoy Nozaki"]
        ]
      ]
    ]
  ],
  bio: true
]
