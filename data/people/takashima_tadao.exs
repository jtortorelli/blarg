[
  given_name: "Tadao",
  family_name: "Takashima",
  path: "takashima-tadao",
  original_name: "高島 忠夫",
  dob: [year: 1930, month: 7, day: 27],
  dod: [year: 2019, month: 6, day: 26],
  birth_place: "Muko, Hyogo, Japan",
  death_place: "Tokyo, Japan",
  selected_filmography: [
    roles: [
      [
        role: "Actor",
        films: [
          [film: {"King Kong vs. Godzilla", 1962}, characters: "Osamu Sakurai"],
          [film: {"Atragon", 1963}, characters: "Susumu Hatanaka"],
          [film: {"Frankenstein Conquers the World", 1965}, characters: "Dr. Yuzo Kawaji"],
          [film: {"Son of Godzilla", 1967}, characters: "Dr. Kusumi"],
          [film: {"Godzilla VS Mechagodzilla", 1993}, characters: "Psychic Researcher Hosono"]
        ]
      ]
    ]
  ],
  bio: true
]
