[
  given_name: "Katsumi",
  family_name: "Tezuka",
  path: "tezuka-katsumi",
  original_name: "手塚 勝巳",
  dob: [year: 1912, month: 8, day: 21],
  dod: [unknown: true],
  selected_filmography: [
    roles: [
      [
        role: "Actor",
        films: [
          [
            film: {"Godzilla, King of the Monsters", 1954},
            characters: ["Godzilla", "Newspaper Editor"]
          ],
          [film: {"Godzilla Raids Again", 1955}, characters: "Anguirus"],
          [film: {"Rodan", 1956}, characters: ["Meganulon", "Hotel Manager"]],
          [film: {"The Mysterians", 1957}, characters: "Mogera"],
          [film: {"The H-Man", 1958}, characters: "Fishing Captain"],
          [film: {"Varan the Unbelievable", 1958}, characters: "Varan"],
          [film: {"The Birth of Japan", 1959}, characters: "Utte Soldier"],
          [film: {"Battle in Outer Space", 1959}, characters: "Military Officer"],
          [film: {"Mothra", 1961}, characters: "Mothra"],
          [film: {"Gorath", 1962}, characters: "Magma"],
          [film: {"King Kong vs. Godzilla", 1962}, characters: "Godzilla"],
          [film: {"Matango", 1963}, characters: "Official"],
          [film: {"Atragon", 1963}, characters: "Military Officer"],
          [film: {"Mothra vs. Godzilla", 1964}, characters: "Godzilla"],
          [film: {"Ghidorah, the Three-Headed Monster", 1964}, characters: "Godzilla"]
        ]
      ]
    ]
  ],
  bio: true
]
