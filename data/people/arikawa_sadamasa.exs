[
  given_name: "Sadamasa",
  family_name: "Arikawa",
  path: "arikawa-sadamasa",
  original_name: "有川 貞昌",
  dob: [year: 1925, month: 6, day: 17],
  birth_place: "Tokyo, Japan",
  dod: [year: 2005, month: 9, day: 22],
  death_place: "Higashiizu, Kamo, Shizuoka, Japan",
  selected_filmography: [
    roles: [
      [
        role: "SFX Cinematographer",
        films: [
          [film: {"The Mysterians", 1957}],
          [film: {"The H-Man", 1958}],
          [film: {"Varan the Unbelievable", 1958}],
          [film: {"The Birth of Japan", 1959}],
          [film: {"Battle in Outer Space", 1959}],
          [film: {"The Secret of the Telegian", 1960}],
          [film: {"The Human Vapor", 1960}],
          [film: {"Daredevil in the Castle", 1961}],
          [film: {"Mothra", 1961}],
          [film: {"The Last War", 1961}],
          [film: {"Gorath", 1962}],
          [film: {"King Kong vs. Godzilla", 1962}],
          [film: {"Matango", 1963}],
          [film: {"Samurai Pirate", 1963}],
          [film: {"Atragon", 1963}],
          [film: {"Whirlwind", 1964}],
          [film: {"Mothra vs. Godzilla", 1964}],
          [film: {"Dogora, the Space Monster", 1964}],
          [film: {"Ghidorah, the Three-Headed Monster", 1964}],
          [film: {"Frankenstein Conquers the World", 1965}],
          [film: {"Monster Zero", 1965}],
          [film: {"War of the Gargantuas", 1966}]
        ]
      ],
      [
        role: "SFX Co-Director",
        films: [
          [film: {"Godzilla vs. the Sea Monster", 1966}]
        ]
      ],
      [
        role: "SFX Director",
        films: [
          [film: {"Son of Godzilla", 1967}],
          [film: {"Destroy All Monsters", 1968}],
          [film: {"Space Amoeba", 1970}]
        ]
      ]
    ]
  ],
  bio: true
]
