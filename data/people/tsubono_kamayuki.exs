[
  given_name: "Kamayuki",
  family_name: "Tsubono",
  path: "tsubono-kamayuki",
  original_name: "坪野 鎌之",
  dob: [year: 1919, month: 7, day: 26],
  dod: [unknown: true],
  selected_filmography: [
    roles: [
      [
        role: "Actor",
        films: [
          [film: {"Seven Samurai", 1954}, characters: "Bandit"],
          [film: {"Godzilla, King of the Monsters", 1954}, characters: "Coast Guard"],
          [film: {"The Invisible Man", 1954}, characters: "Bus Passenger"],
          [film: {"Samurai II: Duel at Ichijoji Temple", 1955}, characters: "Samurai on Bridge"],
          [film: {"Rodan", 1956}, characters: ["Police Chemist", "Air Force Officer"]],
          [film: {"The Mysterians", 1957}, characters: "Policeman"],
          [film: {"The H-Man", 1958}, characters: "Detective Ogawa"],
          [film: {"The Birth of Japan", 1959}, characters: "Otomo Soldier"],
          [film: {"Battle in Outer Space", 1959}, characters: "Reporter"],
          [film: {"The Human Vapor", 1960}, characters: "Detective Osaki"],
          [film: {"Daredevil in the Castle", 1961}, characters: "Mohei's Crew"],
          [film: {"Yojimbo", 1961}, characters: "Ushitora Underling"],
          [film: {"Mothra", 1961}, characters: ["Military Officer", "Soldier"]],
          [film: {"The Last War", 1961}, characters: "Press Club Chauffeur"],
          [film: {"Gorath", 1962}, characters: "Minister"],
          [film: {"King Kong vs. Godzilla", 1962}, characters: "Sailor"],
          [film: {"Mothra vs. Godzilla", 1964}, characters: "Islander"],
          [film: {"Dogora, the Space Monster", 1964}, characters: "Detective"],
          [film: {"Ghidorah, the Three-Headed Monster", 1964}, characters: "Sailor"],
          [film: {"Frankenstein Conquers the World", 1965}, characters: "Policeman"],
          [film: {"Monster Zero", 1965}, characters: "Reporter"],
          [film: {"War of the Gargantuas", 1966}, characters: "Soldier"],
          [film: {"Godzilla vs. the Sea Monster", 1966}, characters: "Captive Islander"],
          [film: {"King Kong Escapes", 1967}, characters: "Military Advisor"],
          [film: {"Destroy All Monsters", 1968}, characters: "Special Police"]
        ]
      ]
    ]
  ],
  bio: true
]
