[
  given_name: "Koji",
  family_name: "Tsuruta",
  path: "tsuruta-koji",
  original_name: "鶴田 浩二",
  birth_name: "Eiichi Ono (小野 榮一)",
  dob: [year: 1924, month: 12, day: 6],
  birth_place: "Nishinomiya, Hyogo, Japan",
  dod: [year: 1987, month: 6, day: 16],
  selected_filmography: [
    roles: [
      [
        role: "Actor",
        films: [
          [film: {"Samurai II: Duel at Ichijoji Temple", 1955}, characters: "Kojiro Sasaki"],
          [film: {"Samurai III: Duel at Ganryu Island", 1956}, characters: "Kojiro Sasaki"],
          [film: {"The Birth of Japan", 1959}, characters: "Younger Kumaso"],
          [film: {"The Secret of the Telegian", 1960}, characters: "Kirioka"]
        ]
      ]
    ]
  ],
  bio: true
]
