[
  given_name: "Kuninori",
  family_name: "Kodo",
  path: "kodo-kuninori",
  original_name: "高堂 国典",
  birth_name: "Saichiro Tanikawa (谷川 佐市郎)",
  dob: [year: 1887, month: 1, day: 29],
  birth_place: "Takasago, Hyogo, Japan",
  dod: [year: 1960, month: 1, day: 22],
  selected_filmography: [
    roles: [
      [
        role: "Actor",
        films: [
          [film: {"Seven Samurai", 1954}, characters: "Gisaku the Village Chief"],
          [film: {"Godzilla, King of the Monsters", 1954}, characters: "Izuma"],
          [film: {"Samurai II: Duel at Ichijoji Temple", 1955}, characters: "Nitsukan the Monk"],
          [film: {"Samurai III: Duel at Ganryu Island", 1956}, characters: "Nitsukan the Monk"],
          [film: {"Throne of Blood", 1957}, characters: "Warlord"],
          [film: {"The Hidden Fortress", 1958}, characters: "Priest in Crowd"]
        ]
      ]
    ]
  ],
  bio: true
]
