[
  given_name: "Shigeo",
  family_name: "Kato",
  path: "kato-shigeo",
  original_name: "加藤 茂雄",
  dob: [year: 1925, month: 6, day: 16],
  dod: [year: 2020, month: 6, day: 14],
  birth_place: "Kamakura, Kanagawa, Japan",
  death_place: "Kamakura, Kanagawa, Japan",
  selected_filmography: [
    roles: [
      [
        role: "Actor",
        films: [
          [film: {"Seven Samurai", 1954}, characters: "Peasant"],
          [
            film: {"Samurai III: Duel at Ganryu Island", 1956},
            characters: "Bearer in Procession"
          ],
          [film: {"The H-Man", 1958}, characters: "Fisherman"],
          [film: {"The Hidden Fortress", 1958}, characters: "Yamana Peasant"],
          [film: {"Battle in Outer Space", 1959}, characters: "Train Conductor"],
          [film: {"Mothra", 1961}, characters: "Dam Worker"],
          [film: {"Sanjuro", 1962}, characters: "Kikui Samurai"],
          [film: {"Mothra vs. Godzilla", 1964}, characters: "Islander"],
          [film: {"Dogora, the Space Monster", 1964}, characters: "Soldier"],
          [film: {"Ghidorah, the Three-Headed Monster", 1964}, characters: "Mt. Aso Tourist"],
          [film: {"Frankenstein Conquers the World", 1965}, characters: "Village Policeman"],
          [film: {"War of the Gargantuas", 1966}, characters: "Soldier"],
          [film: {"King Kong Escapes", 1967}, characters: "Who Henchman"],
          [
            film: {"Destroy All Monsters", 1968},
            characters: ["Mt. Fuji Soldier", "Military Advisor", "Mt. Fuji Reporter"]
          ],
          [film: {"Latitude Zero", 1969}, characters: "Observer"],
          [film: {"Space Amoeba", 1970}, characters: "Islander"],
          [film: {"Lake of Dracula", 1971}, characters: "Coffee Shop Patron"],
          [film: {"Godzilla vs. Hedorah", 1971}, characters: "Construction Worker"],
          [film: {"Prophecies of Nostradamus", 1974}, characters: "Burned Man"],
          [film: {"Terror of Mechagodzilla", 1975}, characters: "Submarine Tech"],
          [film: {"The Explosion", 1975}, characters: "Misuga HQ Staff"],
          [film: {"The Return of Godzilla", 1984}, characters: "Ship's Radio Operator"],
          [film: {"Godzilla X Megaguirus", 2000}, characters: "Janitor"]
        ]
      ]
    ]
  ],
  bio: true
]
