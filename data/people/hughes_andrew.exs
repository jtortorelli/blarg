[
  given_name: "Andrew",
  family_name: "Hughes",
  path: "hughes-andrew",
  japanese_name: "アンドリュー・ヒューズ",
  dob: [year: 1908],
  birth_place: "Turkey",
  dod: [unknown: true],
  selected_filmography: [
    roles: [
      [
        role: "Actor",
        films: [
          [film: {"Battle in Outer Space", 1959}, characters: "UN Official"],
          [film: {"The Golden Bat", 1966}, characters: "Dr. Pearl"],
          [film: {"King Kong Escapes", 1967}, characters: "UN Reporter"],
          [film: {"Destroy All Monsters", 1968}, characters: "Dr. Stevenson"],
          [film: {"Latitude Zero", 1969}, characters: "Sir Maurice Pauli"],
          [film: {"The Submersion of Japan", 1973}, characters: "Australian Prime Minister"],
          [film: {"Espy", 1974}, characters: "Espy Chief"],
          [film: {"Sayonara, Jupiter", 1984}, characters: "Senator Shadillic"]
        ]
      ]
    ]
  ],
  bio: true
]
