[
  given_name: "Shiro",
  family_name: "Tsuchiya",
  path: "tsuchiya-shiro",
  original_name: "土屋 詩朗",
  aliases: "Hirotoshi Tsuchiya (土屋 博敏)",
  dob: [year: 1901, month: 1, day: 5],
  birth_place: "Akira Japan",
  dod: [unknown: true],
  selected_filmography: [
    roles: [
      [
        role: "Actor",
        films: [
          [film: {"Seven Samurai", 1954}, characters: "Samurai"],
          [film: {"The Invisible Man", 1954}, characters: "Parliamentarian"],
          [film: {"Godzilla Raids Again", 1955}, characters: "Fishing Company Employee"],
          [film: {"Throne of Blood", 1957}, characters: "Warlord"],
          [film: {"The H-Man", 1958}, characters: "Police Executive"],
          [film: {"The Hidden Fortress", 1958}, characters: "Yamana Samurai"],
          [film: {"The Birth of Japan", 1959}, characters: "Yamato Soldier"],
          [film: {"The Secret of the Telegian", 1960}, characters: "Police Executive"],
          [film: {"Daredevil in the Castle", 1961}, characters: "Tosho Hotta"],
          [film: {"The Last War", 1961}, characters: "Minister"],
          [film: {"King Kong vs. Godzilla", 1962}, characters: "Evacuee"],
          [film: {"Atragon", 1963}, characters: "Atragon Crew"],
          [film: {"Mothra vs. Godzilla", 1964}, characters: "Fisherman"],
          [film: {"Dogora, the Space Monster", 1964}, characters: "Coal Miner"],
          [film: {"The Adventures of Taklamakan", 1966}, characters: "Merchant"],
          [film: {"War of the Gargantuas", 1966}, characters: "Military Advisor"]
        ]
      ]
    ]
  ],
  bio: true
]
