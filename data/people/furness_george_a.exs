[
  given_name: "George",
  family_name: "Furness",
  path: "furness-george-a",
  middle_name: "A.",
  japanese_name: "ジョージ・A・ファーネス",
  dob: [year: 1896],
  birth_place: "New Jersey, United States",
  dod: [year: 1985, month: 4, day: 2],
  selected_filmography: [
    roles: [
      [
        role: "Actor",
        films: [
          [film: {"The Mysterians", 1957}, characters: "Dr. Svenson"],
          [film: {"Gorath", 1962}, characters: "UN Ambassador"]
        ]
      ]
    ]
  ],
  bio: true
]
