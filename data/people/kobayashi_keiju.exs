[
  given_name: "Keiju",
  family_name: "Kobayashi",
  path: "kobayashi-keiju",
  original_name: "小林 桂樹",
  dob: [year: 1923, month: 11, day: 23],
  birth_place: "Murota, Gunma, Japan",
  dod: [year: 2010, month: 9, day: 16],
  death_place: "Minato, Tokyo, Japan",
  selected_filmography: [
    roles: [
      [
        role: "Actor",
        films: [
          [film: {"The Birth of Japan", 1959}, characters: "Amatsumara"],
          [film: {"Sanjuro", 1962}, characters: "Kimura the Captured Samurai"],
          [film: {"The Submersion of Japan", 1973}, characters: "Dr. Yusuke Tadokoro"],
          [film: {"The Return of Godzilla", 1984}, characters: "Prime Minister Mitamura"]
        ]
      ]
    ]
  ],
  bio: true
]
