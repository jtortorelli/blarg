[
  given_name: "Chishu",
  family_name: "Ryu",
  path: "ryu-chishu",
  original_name: "笠 智衆",
  dob: [year: 1904, month: 5, day: 13],
  birth_place: "Tachibana, Tamamizu, Tamana, Kumamoto, Japan",
  dod: [year: 1993, month: 3, day: 16],
  selected_filmography: [
    roles: [
      [
        role: "Actor",
        films: [
          [film: {"The Last War", 1961}, characters: "Ebara"]
        ]
      ]
    ]
  ],
  bio: true
]
