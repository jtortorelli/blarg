[
  given_name: "Minoru",
  family_name: "Takada",
  path: "takada-minoru",
  original_name: "高田 稔 ",
  birth_name: "Noboru Takada (高田 昇)",
  dob: [year: 1899, month: 12, day: 20],
  birth_place: "Higashinaruse, Ogachi, Akita, Japan",
  dod: [year: 1977, month: 12, day: 27],
  selected_filmography: [
    roles: [
      [
        role: "Actor",
        films: [
          [film: {"The Invisible Man", 1954}, characters: "Yajima"],
          [film: {"Battle in Outer Space", 1959}, characters: "Defense Commander"],
          [film: {"The Last War", 1961}, characters: "Missile Defense Commander"],
          [film: {"Atragon", 1963}, characters: "Military Officer"],
          [film: {"Ghidorah, the Three-Headed Monster", 1964}, characters: "Defense Minister"],
          [film: {"The Adventures of Taklamakan", 1966}, characters: "Buddhist Priest"]
        ]
      ]
    ]
  ],
  bio: true
]
