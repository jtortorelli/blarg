[
  given_name: "Keiko",
  family_name: "Sawai",
  path: "sawai-keiko",
  original_name: "沢井 桂子",
  dob: [year: 1945, month: 1, day: 2],
  birth_place: "Osaka, Japan",
  selected_filmography: [
    roles: [
      [
        role: "Actress",
        films: [
          [film: {"Frankenstein Conquers the World", 1965}, characters: "Kazuko"],
          [film: {"Monster Zero", 1965}, characters: "Haruno Fuji"]
        ]
      ]
    ]
  ],
  bio: true
]
