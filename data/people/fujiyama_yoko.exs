[
  given_name: "Yoko",
  family_name: "Fujiyama",
  path: "fujiyama-yoko",
  original_name: "藤山 陽子",
  dob: [year: 1941, month: 12, day: 17],
  birth_place: "Hakkei, Kanazawa, Yokohama, Kanagawa, Japan",
  selected_filmography: [
    roles: [
      [
        role: "Actress",
        films: [
          [film: {"Atragon", 1963}, characters: "Makoto Jinguji"],
          [film: {"Dogora, the Space Monster", 1964}, characters: "Masayo Kirino"]
        ]
      ]
    ]
  ],
  bio: true
]
