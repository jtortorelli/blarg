[
  given_name: "Yukihiko",
  family_name: "Gondo",
  path: "gondo-yukihiko",
  original_name: "権藤 幸彦",
  selected_filmography: [
    roles: [
      [
        role: "Actor",
        films: [
          [film: {"The H-Man", 1958}, characters: "Policeman"],
          [film: {"The Hidden Fortress", 1958}, characters: "Yamana Soldier"],
          [film: {"The Birth of Japan", 1959}, characters: "Yamato Villager"],
          [film: {"Battle in Outer Space", 1959}, characters: "Space Station Crew"],
          [film: {"The Secret of the Telegian", 1960}, characters: "Island Policeman"],
          [film: {"The Human Vapor", 1960}, characters: "Detective Hotta"],
          [film: {"Yojimbo", 1961}, characters: "Ushitora Underling"],
          [film: {"Mothra", 1961}, characters: "Dam Worker"],
          [film: {"Sanjuro", 1962}, characters: "Kikui Samurai"],
          [film: {"Gorath", 1962}, characters: "Spaceship Crew"],
          [film: {"King Kong vs. Godzilla", 1962}, characters: "Helicopter Pilot"],
          [film: {"High and Low", 1963}, characters: "Train Crew"],
          [film: {"Atragon", 1963}, characters: "Mihara Tourist"],
          [film: {"Whirlwind", 1964}, characters: "Toyotomi Loyalist"],
          [film: {"Mothra vs. Godzilla", 1964}, characters: "Kumayama's Henchman"],
          [film: {"Dogora, the Space Monster", 1964}, characters: "Train Attendant"],
          [film: {"Frankenstein Conquers the World", 1965}, characters: "TV Cameraman"],
          [film: {"The Adventures of Taklamakan", 1966}, characters: "Palace Guard"],
          [film: {"Godzilla vs. the Sea Monster", 1966}, characters: "Red Bamboo Soldier"],
          [film: {"Destroy All Monsters", 1968}, characters: "Soldier"],
          [film: {"Space Amoeba", 1970}, characters: "Islander"],
          [film: {"Godzilla vs. Hedorah", 1971}, characters: "Helicopter Pilot"]
        ]
      ]
    ]
  ],
  bio: true
]
