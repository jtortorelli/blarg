[
  given_name: "Frankie",
  family_name: "Sakai",
  path: "sakai-frankie",
  original_name: "フランキー 堺",
  birth_name: "Masatoshi Sakai (堺 正俊)",
  dob: [year: 1929, month: 2, day: 13],
  birth_place: "Kagoshima, Japan",
  dod: [year: 1996, month: 6, day: 10],
  death_place: "Minato, Tokyo, Japan",
  selected_filmography: [
    roles: [
      [
        role: "Actor",
        films: [
          [film: {"Mothra", 1961}, characters: "Senichiro Fukuda"],
          [film: {"The Last War", 1961}, characters: "Mokichi Tamura"]
        ]
      ]
    ]
  ],
  bio: true
]
