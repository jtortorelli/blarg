[
  given_name: "Susumu",
  family_name: "Fujita",
  path: "fujita-susumu",
  original_name: "藤田 進",
  dob: [year: 1912, month: 1, day: 8],
  birth_place: "Kurume, Fukuoka, Japan",
  dod: [year: 1990, month: 3, day: 23],
  death_place: "Shibuya, Tokyo, Japan",
  selected_filmography: [
    roles: [
      [
        role: "Actor",
        films: [
          [film: {"The Mysterians", 1957}, characters: "General Morita"],
          [film: {"The Hidden Fortress", 1958}, characters: "Hyoe Tadokoro"],
          [film: {"Daredevil in the Castle", 1961}, characters: "Katsuyasu Sakakibara"],
          [film: {"Yojimbo", 1961}, characters: "Honma"],
          [film: {"High and Low", 1963}, characters: "Police Executive"],
          [film: {"Atragon", 1963}, characters: "General"],
          [film: {"Mothra vs. Godzilla", 1964}, characters: "General"],
          [film: {"Dogora, the Space Monster", 1964}, characters: "General Iwata"],
          [film: {"Frankenstein Conquers the World", 1965}, characters: "Osaka Police Chief"]
        ]
      ]
    ]
  ],
  bio: true
]
