[
  given_name: "Tadao",
  family_name: "Nakamaru",
  path: "nakamaru-tadao",
  original_name: "中丸 忠雄",
  dob: [year: 1933, month: 3, day: 31],
  birth_place: "Adachi, Tokyo, Japan",
  dod: [year: 2009, month: 4, day: 23],
  death_place: "Hongo, Bunkyo, Tokyo, Japan",
  selected_filmography: [
    roles: [
      [
        role: "Actor",
        films: [
          [film: {"Godzilla Raids Again", 1955}, characters: "Policeman"],
          [
            film: {"Samurai III: Duel at Ganryu Island", 1956},
            characters: "Bearer in Procession"
          ],
          [film: {"The Mysterians", 1957}, characters: "Soldier"],
          [film: {"The H-Man", 1958}, characters: "Detective Seki"],
          [film: {"The Hidden Fortress", 1958}, characters: "Akizuki Guard"],
          [film: {"The Secret of the Telegian", 1960}, characters: "Tsudo"],
          [film: {"Daredevil in the Castle", 1961}, characters: "Hyogo Ujiya"],
          [film: {"Samurai Pirate", 1963}, characters: "The Chancellor"],
          [film: {"The Adventures of Taklamakan", 1966}, characters: "Ensai"],
          [film: {"The Submersion of Japan", 1973}, characters: "Kunieda"],
          [film: {"Terror of Mechagodzilla", 1975}, characters: "Chief Tagawa"]
        ]
      ]
    ]
  ],
  bio: true
]
