[
  given_name: "Obel",
  family_name: "Wyatt",
  path: "wyatt-obel",
  japanese_name: "オーベル・ワイアット",
  selected_filmography: [
    roles: [
      [
        role: "Actor",
        films: [
          [film: {"Battle in Outer Space", 1959}, characters: "Military Officer"],
          [film: {"Mothra", 1961}, characters: "Rolisican Mayor"],
          [film: {"The Last War", 1961}, characters: "Alliance Officer"],
          [film: {"Gorath", 1962}, characters: "UN Ambassador"]
        ]
      ]
    ]
  ],
  bio: true
]
