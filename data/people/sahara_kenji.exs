[
  given_name: "Kenji",
  family_name: "Sahara",
  path: "sahara-kenji",
  original_name: "佐原 健二",
  birth_name: "Masayoshi Kato (加藤 正好)",
  dob: [year: 1932, month: 5, day: 14],
  birth_place: "Kawasaki, Kanagawa, Japan",
  selected_filmography: [
    roles: [
      [
        role: "Actor",
        films: [
          [
            film: {"Godzilla, King of the Monsters", 1954},
            characters: ["Reporter", "Cruise Passenger"]
          ],
          [film: {"Rodan", 1956}, characters: "Shigeru Kawamura"],
          [film: {"The Mysterians", 1957}, characters: "Dr. Joji Atsumi"],
          [film: {"The H-Man", 1958}, characters: "Dr. Masada"],
          [film: {"Mothra", 1961}, characters: "Helicopter Pilot"],
          [film: {"Gorath", 1962}, characters: "Lt. Saiki"],
          [film: {"King Kong vs. Godzilla", 1962}, characters: "Kazuo Fujita"],
          [film: {"Matango", 1963}, characters: "Senzo Koyama"],
          [film: {"Atragon", 1963}, characters: "Unno"],
          [film: {"Mothra vs. Godzilla", 1964}, characters: "Torahata"],
          [film: {"Ghidorah, the Three-Headed Monster", 1964}, characters: "Newspaper Editor"],
          [film: {"Frankenstein Conquers the World", 1965}, characters: "Policeman"],
          [film: {"War of the Gargantuas", 1966}, characters: "Dr. Yuzo Majida"],
          [film: {"Son of Godzilla", 1967}, characters: "Morio"],
          [film: {"Destroy All Monsters", 1968}, characters: "Moon Base Commander Nishikawa"],
          [film: {"Godzilla's Revenge", 1969}, characters: "Kenichi Mitsuki"],
          [film: {"Space Amoeba", 1970}, characters: "Makoto Obata"],
          [film: {"Godzilla vs. the Cosmic Monster", 1974}, characters: "Cruise Ship Captain"],
          [film: {"Terror of Mechagodzilla", 1975}, characters: "Defense Commander"],
          [film: {"The Explosion", 1975}, characters: "Kumamoto Shinpo Reporter"],
          [film: {"Godzilla VS King Ghidorah", 1991}, characters: "Defense Secretary"],
          [film: {"Godzilla VS Mechagodzilla", 1993}, characters: "Secretary Takayuki Segawa"],
          [film: {"Godzilla VS Space Godzilla", 1994}, characters: "Secretary Takayuki Segawa"],
          [film: {"Godzilla: Final Wars", 2004}, characters: "Hachiro Jinguji"]
        ]
      ]
    ]
  ],
  bio: true
]
