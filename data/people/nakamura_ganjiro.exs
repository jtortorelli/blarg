[
  given_name: "Ganjiro",
  family_name: "Nakamura",
  path: "nakamura-ganjiro",
  original_name: "中村 鴈治郎",
  birth_name: "Yoshio Hayashi (林 好雄)",
  dob: [year: 1902, month: 2, day: 17],
  birth_place: "Osaka, Japan",
  dod: [year: 1983, month: 4, day: 13],
  selected_filmography: [
    roles: [
      [
        role: "Actor",
        films: [
          [film: {"The Birth of Japan", 1959}, characters: "Emperor Keiko"]
        ]
      ]
    ]
  ],
  bio: true
]
