[
  given_name: "Takuzo",
  family_name: "Kumagai",
  path: "kumagai-takuzo",
  original_name: "熊谷 卓三",
  aliases: "Jiro Kumagai (熊谷 二良)",
  dob: [year: 1906, month: 11, day: 3],
  birth_place: "Nagano, Japan",
  dod: [unknown: true],
  selected_filmography: [
    roles: [
      [
        role: "Actor",
        films: [
          [film: {"Seven Samurai", 1954}, characters: "Village Chief's Son"],
          [film: {"Samurai I: Musashi Miyamoto", 1954}, characters: "Villager"],
          [film: {"Godzilla, King of the Monsters", 1954}, characters: "Defense Official"],
          [
            film: {"The Invisible Man", 1954},
            characters: ["Parliamentarian", "Crooked Businessman"]
          ],
          [film: {"Godzilla Raids Again", 1955}, characters: "Fishing Company Employee"],
          [
            film: {"Samurai II: Duel at Ichijoji Temple", 1955},
            characters: "Yoshioka School Samurai"
          ],
          [film: {"Samurai III: Duel at Ganryu Island", 1956}, characters: "Okinaga Samurai"],
          [film: {"Rodan", 1956}, characters: "Policeman"],
          [film: {"The Mysterians", 1957}, characters: "Soldier"],
          [film: {"The H-Man", 1958}, characters: "Soldier"],
          [film: {"Varan the Unbelievable", 1958}, characters: "Soldier"],
          [film: {"The Hidden Fortress", 1958}, characters: "Yamana Soldier"],
          [film: {"The Birth of Japan", 1959}, characters: "Yamato Villager"],
          [film: {"Battle in Outer Space", 1959}, characters: "Military Officer"],
          [film: {"The Secret of the Telegian", 1960}, characters: "Tourist"],
          [film: {"The Human Vapor", 1960}, characters: "Newspaper Executive"],
          [film: {"Yojimbo", 1961}, characters: "Seibei Underling"],
          [film: {"The Last War", 1961}, characters: "Minister"],
          [film: {"Sanjuro", 1962}, characters: ["Kikui Samurai", "Samurai Reading Notice"]],
          [film: {"Gorath", 1962}, characters: "Minister"],
          [film: {"King Kong vs. Godzilla", 1962}, characters: "Military Official"],
          [film: {"High and Low", 1963}, characters: "Detective"],
          [film: {"Matango", 1963}, characters: "Doctor"],
          [film: {"Atragon", 1963}, characters: "Military Officer"],
          [film: {"Mothra vs. Godzilla", 1964}, characters: "Fisherman"],
          [film: {"Dogora, the Space Monster", 1964}, characters: "Military Advisor"],
          [film: {"Ghidorah, the Three-Headed Monster", 1964}, characters: "Parliamentarian"],
          [film: {"Monster Zero", 1965}, characters: "Soldier"],
          [film: {"War of the Gargantuas", 1966}, characters: "Military Advisor"],
          [film: {"Destroy All Monsters", 1968}, characters: "Military Advisor"],
          [film: {"Space Amoeba", 1970}, characters: "Islander"],
          [
            film: {"The Submersion of Japan", 1973},
            characters: "Disaster Prevention Center Director"
          ],
          [film: {"Prophecies of Nostradamus", 1974}, characters: "Minister"],
          [film: {"Terror of Mechagodzilla", 1975}, characters: "Military Officer"]
        ]
      ]
    ]
  ],
  bio: true
]
