[
  given_name: "Tatsuya",
  family_name: "Mihashi",
  path: "mihashi-tatsuya",
  original_name: "三橋 達也",
  dob: [year: 1923, month: 11, day: 2],
  birth_place: "Chuo, Tokyo, Japan",
  dod: [year: 2004, month: 5, day: 15],
  selected_filmography: [
    roles: [
      [
        role: "Actor",
        films: [
          [film: {"The Human Vapor", 1960}, characters: "Detective Okamoto"],
          [film: {"High and Low", 1963}, characters: "Kawanishi"],
          [film: {"The Adventures of Taklamakan", 1966}, characters: "The King"],
          [film: {"Casshern", 2004}, characters: "Dr. Furoi"]
        ]
      ]
    ]
  ],
  bio: true
]
