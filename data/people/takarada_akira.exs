[
  given_name: "Akira",
  family_name: "Takarada",
  path: "takarada-akira",
  original_name: "宝田 明",
  dob: [year: 1934, month: 4, day: 29],
  birth_place: "Hamgyeongbuk-do, Korea",
  selected_filmography: [
    roles: [
      [
        role: "Actor",
        films: [
          [film: {"Godzilla, King of the Monsters", 1954}, characters: "Hideto Ogata"],
          [film: {"The Birth of Japan", 1959}, characters: "Prince Wakatarashi"],
          [film: {"The Last War", 1961}, characters: "Takano"],
          [film: {"Mothra vs. Godzilla", 1964}, characters: "Ichiro Sakai"],
          [film: {"Monster Zero", 1965}, characters: "Astronaut Fuji"],
          [film: {"Godzilla vs. the Sea Monster", 1966}, characters: "Yoshimura"],
          [film: {"King Kong Escapes", 1967}, characters: "Lt. Jiro Nomura"],
          [film: {"Latitude Zero", 1969}, characters: ["Ken Tashiro", "Naval Officer"]],
          [film: {"Godzilla VS Mothra", 1992}, characters: "Joji Minamino"],
          [film: {"Godzilla: Final Wars", 2004}, characters: "SG Naotaro Daigo"]
        ]
      ]
    ]
  ],
  bio: true
]
