[
  given_name: "Osman",
  family_name: "Yusef",
  path: "yusef-osman",
  japanese_name: "オスマン・ユセフ",
  aliases: "Johnny Yusef",
  dob: [year: 1920, month: 5, day: 23],
  birth_place: "Ottoman Empire",
  dod: [year: 1982, month: 8, day: 29],
  selected_filmography: [
    roles: [
      [
        role: "Actor",
        films: [
          [film: {"Battle in Outer Space", 1959}, characters: "Astronaut"],
          [film: {"Mothra", 1961}, characters: "Nelson's Henchman"],
          [film: {"The Last War", 1961}, characters: "Alliance Pilot"],
          [film: {"Gorath", 1962}, characters: "South Pole Crew"],
          [film: {"King Kong vs. Godzilla", 1962}, characters: "Scientist"],
          [film: {"Atragon", 1963}, characters: "Mu Soldier"],
          [film: {"Mothra vs. Godzilla", 1964}, characters: "Reporter (American Version)"],
          [film: {"King Kong Escapes", 1967}, characters: "Submarine Crew"],
          [film: {"Son of Godzilla", 1967}, characters: "Submarine Officer"],
          [film: {"Latitude Zero", 1969}, characters: "Latitude Zero Staff"],
          [film: {"Godzilla's Revenge", 1969}, characters: "Plane Passenger"],
          [film: {"Prophecies of Nostradamus", 1974}, characters: "New Guinea Researcher"],
          [film: {"The Explosion", 1975}, characters: "Reporter"]
        ]
      ]
    ]
  ],
  bio: true
]
