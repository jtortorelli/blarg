[
  given_name: "Minoru",
  family_name: "Ito",
  path: "ito-minoru",
  original_name: "伊藤 実",
  dob: [year: 1928, month: 3, day: 13],
  birth_place: "Chiba, Japan",
  selected_filmography: [
    roles: [
      [
        role: "Actor",
        films: [
          [film: {"Seven Samurai", 1954}, characters: "Bandit"],
          [film: {"The Invisible Man", 1954}, characters: "Driver"],
          [film: {"Samurai III: Duel at Ganryu Island", 1956}, characters: "Spectator"],
          [film: {"The Mysterians", 1957}, characters: "Reporter"],
          [film: {"The Hidden Fortress", 1958}, characters: "Yamana Cavalry"],
          [film: {"Battle in Outer Space", 1959}, characters: "Speaker"],
          [film: {"The Secret of the Telegian", 1960}, characters: "Thriller Show Employee"],
          [film: {"The Human Vapor", 1960}, characters: "Reporter"],
          [film: {"Daredevil in the Castle", 1961}, characters: "Warrior Priest"],
          [film: {"Yojimbo", 1961}, characters: "Ushitora Underling"],
          [film: {"Mothra", 1961}, characters: "Soldier"],
          [film: {"Sanjuro", 1962}, characters: "Kikui Samurai"],
          [film: {"High and Low", 1963}, characters: "Detective"],
          [film: {"Atragon", 1963}, characters: "Atragon Crew"],
          [film: {"Dogora, the Space Monster", 1964}, characters: "Soldier"],
          [film: {"Ghidorah, the Three-Headed Monster", 1964}, characters: "Reporter"],
          [film: {"Frankenstein Conquers the World", 1965}, characters: "Scientist"],
          [film: {"Monster Zero", 1965}, characters: "Reporter"],
          [film: {"The Adventures of Taklamakan", 1966}, characters: "Palace Guard"],
          [film: {"War of the Gargantuas", 1966}, characters: "Reporter"],
          [film: {"King Kong Escapes", 1967}, characters: "Who Henchman"],
          [film: {"Destroy All Monsters", 1968}, characters: "Possessed Monster Island Tech"],
          [film: {"Latitude Zero", 1969}, characters: "Research Ship Captain"]
        ]
      ]
    ]
  ],
  bio: true
]
