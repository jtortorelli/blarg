[
  given_name: "Kazuo",
  family_name: "Suzuki",
  path: "suzuki-kazuo",
  original_name: "鈴木 和夫",
  dob: [year: 1937, month: 1, day: 18],
  dod: [unknown: true],
  birth_place: "Tokyo, Japan",
  selected_filmography: [
    roles: [
      [
        role: "Actor",
        films: [
          [film: {"King Kong vs. Godzilla", 1962}, characters: "Bystander"],
          [film: {"High and Low", 1963}, characters: "Drug Addict"],
          [film: {"Ghidorah, the Three-Headed Monster", 1964}, characters: "Assassin"],
          [film: {"Monster Zero", 1965}, characters: "Xian"],
          [film: {"Godzilla vs. the Sea Monster", 1966}, characters: "Escaped Islander"],
          [film: {"King Kong Escapes", 1967}, characters: "Who Henchman"],
          [film: {"Son of Godzilla", 1967}, characters: "Pilot"],
          [film: {"Destroy All Monsters", 1968}, characters: "Possessed Monster Island Tech"],
          [film: {"Godzilla's Revenge", 1969}, characters: "Okuda"],
          [film: {"Terror of Mechagodzilla", 1975}, characters: "Alien Henchman"]
        ]
      ]
    ]
  ],
  bio: true
]
