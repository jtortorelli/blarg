[
  given_name: "Yutaka",
  family_name: "Oka",
  path: "oka-yutaka",
  original_name: "岡 豊",
  dob: [year: 1925, month: 7, day: 11],
  birth_place: "Kyoto, Japan",
  dod: [year: 2000, month: 7, day: 27],
  selected_filmography: [
    roles: [
      [
        role: "Actor",
        films: [
          [film: {"Seven Samurai", 1954}, characters: "Villager"],
          [film: {"The Invisible Man", 1954}, characters: "Newsreader"],
          [film: {"Samurai III: Duel at Ganryu Island", 1956}, characters: "Tadatoshi Hosokawa"],
          [film: {"Rodan", 1956}, characters: "Pilot Kitahara"],
          [film: {"The Mysterians", 1957}, characters: "Soldier"],
          [film: {"The H-Man", 1958}, characters: "Soldier"],
          [film: {"Varan the Unbelievable", 1958}, characters: "Bomber Pilot"],
          [film: {"The Birth of Japan", 1959}, characters: "Deity"],
          [film: {"Battle in Outer Space", 1959}, characters: "Astronaut"],
          [film: {"The Secret of the Telegian", 1960}, characters: "Reporter"],
          [film: {"The Human Vapor", 1960}, characters: "Man in Audience"],
          [film: {"Daredevil in the Castle", 1961}, characters: "Toyotomi Soldier"],
          [film: {"Mothra", 1961}, characters: "Pilot"],
          [film: {"The Last War", 1961}, characters: "Defense Crew"],
          [film: {"Sanjuro", 1962}, characters: "Samurai Reading Notice"],
          [film: {"Gorath", 1962}, characters: "South Pole Crew"],
          [film: {"Matango", 1963}, characters: "Doctor"],
          [film: {"Samurai Pirate", 1963}, characters: "Pirate"],
          [film: {"Atragon", 1963}, characters: "Mihara Tourist"],
          [film: {"Mothra vs. Godzilla", 1964}, characters: ["Hotel Clerk", "Fisherman"]],
          [film: {"Dogora, the Space Monster", 1964}, characters: "Transportation Chief"],
          [film: {"Ghidorah, the Three-Headed Monster", 1964}, characters: "Dam Worker"],
          [film: {"Monster Zero", 1965}, characters: "Reporter"],
          [film: {"War of the Gargantuas", 1966}, characters: "Reporter"],
          [film: {"King Kong Escapes", 1967}, characters: "Submarine Crew"],
          [film: {"Destroy All Monsters", 1968}, characters: "Reporter"],
          [film: {"Latitude Zero", 1969}, characters: "Reporter"],
          [film: {"Godzilla vs. Hedorah", 1971}, characters: "Soldier"]
        ]
      ]
    ]
  ],
  bio: true
]
