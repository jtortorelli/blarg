[
  given_name: "Takamaru",
  family_name: "Sasaki",
  path: "sasaki-takamaru",
  original_name: "佐々木 孝丸",
  dob: [year: 1898, month: 1, day: 30],
  birth_place: "Kawakami, Hokkaido, Japan",
  dod: [year: 1986, month: 12, day: 28],
  death_place: "Setagaya, Tokyo, Japan",
  selected_filmography: [
    roles: [
      [
        role: "Actor",
        films: [
          [film: {"Samurai III: Duel at Ganryu Island", 1956}, characters: "Kakubei Iwama"],
          [film: {"Throne of Blood", 1957}, characters: "Kuniharu Tsuzuki"],
          [film: {"The Secret of the Telegian", 1960}, characters: "Dr. Nikki"],
          [film: {"The Human Vapor", 1960}, characters: "Police Executive"],
          [film: {"Gorath", 1962}, characters: "Prime Minister Seki"],
          [film: {"Monster Zero", 1965}, characters: "Prime Minister"],
          [film: {"Zatoichi Meets the One-Armed Swordsman", 1971}, characters: "Monk"]
        ]
      ]
    ]
  ],
  bio: true
]
