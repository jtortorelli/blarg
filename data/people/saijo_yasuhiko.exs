[
  given_name: "Yasuhiko",
  family_name: "Saijo",
  path: "saijo-yasuhiko",
  original_name: "西條 康彦",
  dob: [year: 1939, month: 2, day: 20],
  birth_place: "Kagurazaka, Shinjuku, Tokyo, Japan",
  selected_filmography: [
    roles: [
      [
        role: "Actor",
        films: [
          [film: {"The Secret of the Telegian", 1960}, characters: "Reporter"],
          [film: {"Gorath", 1962}, characters: "Spaceship Crew"],
          [film: {"Frankenstein Conquers the World", 1965}, characters: "TV Cameraman"],
          [film: {"War of the Gargantuas", 1966}, characters: "Man in Convertible"],
          [film: {"Son of Godzilla", 1967}, characters: "Suzuki"],
          [film: {"Destroy All Monsters", 1968}, characters: "SY-3 Pilot"],
          [film: {"Latitude Zero", 1969}, characters: "Naval Crew"]
        ]
      ]
    ]
  ],
  bio: true
]
