[
  type: :group,
  group_name: "The Peanuts",
  path: "peanuts",
  original_name: "ザ・ピーナッツ",
  active_period: "1959 - 1975",
  members: [
    "Emi Ito",
    "Yumi Ito"
  ],
  selected_filmography: [
    roles: [
      [
        role: "Actresses",
        films: [
          [film: {"Mothra", 1961}, characters: "The Shobijin"],
          [film: {"Mothra vs. Godzilla", 1964}, characters: "The Shobijin"],
          [film: {"Ghidorah, the Three-Headed Monster", 1964}, characters: "The Shobijin"]
        ]
      ]
    ]
  ],
  bio: true
]
