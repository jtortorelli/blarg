[
  given_name: "Masaaki",
  family_name: "Tachibana",
  path: "tachibana-masaaki",
  original_name: "橘 正晃",
  dob: [year: 1925, month: 3, day: 18],
  dod: [unknown: true],
  selected_filmography: [
    roles: [
      [
        role: "Actor",
        films: [
          [film: {"Seven Samurai", 1954}, characters: "Bandit"],
          [film: {"Samurai I: Musashi Miyamoto", 1954}, characters: "Villager"],
          [film: {"Godzilla, King of the Monsters", 1954}, characters: "Doomed Reporter"],
          [film: {"Godzilla Raids Again", 1955}, characters: "Policeman"],
          [film: {"Samurai III: Duel at Ganryu Island", 1956}, characters: "Okinaga Samurai"],
          [film: {"Rodan", 1956}, characters: "Policeman"],
          [film: {"The Mysterians", 1957}, characters: ["Policeman", "Soldier"]],
          [film: {"The H-Man", 1958}, characters: "Waiter"],
          [film: {"The Secret of the Telegian", 1960}, characters: "Crime Scene Investigator"],
          [film: {"The Human Vapor", 1960}, characters: "Reporter"],
          [film: {"Mothra", 1961}, characters: "Reporter"],
          [film: {"The Last War", 1961}, characters: ["Reporter", "Security Guard"]],
          [film: {"Sanjuro", 1962}, characters: "Samurai Reading Notice"],
          [film: {"King Kong vs. Godzilla", 1962}, characters: "Reporter"],
          [film: {"High and Low", 1963}, characters: "Train Crew"],
          [film: {"Atragon", 1963}, characters: "Atragon Crew"],
          [film: {"Mothra vs. Godzilla", 1964}, characters: "Soldier"],
          [film: {"Dogora, the Space Monster", 1964}, characters: "Soldier"],
          [film: {"Frankenstein Conquers the World", 1965}, characters: "Reporter"],
          [film: {"Monster Zero", 1965}, characters: "Scientist"],
          [film: {"The Adventures of Taklamakan", 1966}, characters: "Pesil Villager"],
          [film: {"War of the Gargantuas", 1966}, characters: "Reporter"],
          [film: {"King Kong Escapes", 1967}, characters: "Soldier"],
          [film: {"Destroy All Monsters", 1968}, characters: "Military Advisor"],
          [film: {"Latitude Zero", 1969}, characters: "Reporter"]
        ]
      ]
    ]
  ],
  bio: true
]
