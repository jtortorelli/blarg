[
  given_name: "Goro",
  family_name: "Mutsumi",
  path: "mutsumi-goro",
  original_name: "睦 五朗",
  birth_name: "Seiji Nakanishi (中西 清二)",
  dob: [year: 1934, month: 9, day: 11],
  birth_place: "Higashi-Nada, Kobe, Hyogo, Japan",
  selected_filmography: [
    roles: [
      [
        role: "Actor",
        films: [
          [film: {"War of the Gargantuas", 1966}, characters: "Dr. Paul Stewart (Voice)"],
          [film: {"Godzilla vs. the Cosmic Monster", 1974}, characters: "Commander Kuronuma"],
          [film: {"Espy", 1974}, characters: "Teraoka"],
          [film: {"Terror of Mechagodzilla", 1975}, characters: "Commander Mugal"],
          [film: {"The War in Space", 1977}, characters: "Commander Hell"]
        ]
      ]
    ]
  ],
  bio: true
]
