[
  given_name: "Somesho",
  family_name: "Matsumoto",
  path: "matsumoto-somesho",
  original_name: "松本 染升",
  birth_name: "Hachiro Nomura (野村 八郎)",
  dob: [year: 1903, month: 3, day: 20],
  birth_place: "Ushigome, Tokyo, Japan",
  dod: [year: 1985, month: 8, day: 12],
  death_place: "Nishi, Shinjuku, Tokyo, Japan",
  selected_filmography: [
    roles: [
      [
        role: "Actor",
        films: [
          [film: {"The Human Vapor", 1960}, characters: "Kasuga's Instructor"],
          [film: {"Gorath", 1962}, characters: "Minister"],
          [film: {"King Kong vs. Godzilla", 1962}, characters: "Dr. Onuki"],
          [film: {"Ghidorah, the Three-Headed Monster", 1964}, characters: "UFO Club President"],
          [film: {"Monster Zero", 1965}, characters: "Priest"]
        ]
      ]
    ]
  ],
  bio: true
]
