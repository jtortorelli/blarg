[
  given_name: "Yukiko",
  family_name: "Kobayashi",
  path: "kobayashi-yukiko",
  original_name: "小林 夕岐子",
  dob: [year: 1946, month: 10, day: 6],
  birth_place: "Tokyo, Japan",
  selected_filmography: [
    roles: [
      [
        role: "Actress",
        films: [
          [film: {"Destroy All Monsters", 1968}, characters: "Kyoko Manabe"],
          [film: {"Vampire Doll", 1970}, characters: "Yuko Nonomura"],
          [film: {"Space Amoeba", 1970}, characters: "Saki"]
        ]
      ]
    ]
  ],
  bio: true
]
