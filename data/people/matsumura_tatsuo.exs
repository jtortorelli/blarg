[
  given_name: "Tatsuo",
  family_name: "Matsumura",
  path: "matsumura-tatsuo",
  original_name: "松村 達雄",
  dob: [year: 1914, month: 12, day: 18],
  birth_place: "Yokohama, Kanagawa, Japan",
  dod: [year: 2005, month: 6, day: 18],
  death_place: "Shinjuku, Tokyo, Japan",
  selected_filmography: [
    roles: [
      [
        role: "Actor",
        films: [
          [film: {"The Secret of the Telegian", 1960}, characters: "Newspaper Editor"],
          [film: {"The Human Vapor", 1960}, characters: "Newspaper Editor"],
          [film: {"King Kong vs. Godzilla", 1962}, characters: "Dr. Makino"],
          [film: {"Zatoichi Challenged", 1967}, characters: "Taibei"],
          [film: {"Earthquake Archipelago", 1980}, characters: "Kozo Ashida"]
        ]
      ]
    ]
  ],
  bio: true
]
