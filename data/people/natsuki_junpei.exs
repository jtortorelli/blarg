[
  given_name: "Junpei",
  family_name: "Natsuki",
  path: "natsuki-junpei",
  original_name: "夏木 順平",
  dob: [year: 1918, month: 6, day: 15],
  birth_place: "Kitakyushu, Fukuoka, Japan",
  dod: [year: 2010, month: 2, day: 21],
  selected_filmography: [
    roles: [
      [
        role: "Actor",
        films: [
          [film: {"Seven Samurai", 1954}, characters: "Peasant"],
          [film: {"Samurai I: Musashi Miyamoto", 1954}, characters: "Villager"],
          [film: {"Godzilla, King of the Monsters", 1954}, characters: "Substation Operator"],
          [film: {"The Invisible Man", 1954}, characters: "Street Merchant"],
          [film: {"Godzilla Raids Again", 1955}, characters: "Escaped Convict"],
          [film: {"Samurai III: Duel at Ganryu Island", 1956}, characters: "Spectator"],
          [film: {"Rodan", 1956}, characters: "Mine Staff"],
          [film: {"The Birth of Japan", 1959}, characters: "Owari Villager"],
          [film: {"The Secret of the Telegian", 1960}, characters: "Policeman"],
          [film: {"The Human Vapor", 1960}, characters: "Reporter"],
          [film: {"Daredevil in the Castle", 1961}, characters: "Villager"],
          [film: {"Yojimbo", 1961}, characters: "Seibei Underling"],
          [film: {"Mothra", 1961}, characters: "Evacuee"],
          [film: {"The Last War", 1961}, characters: "Evacuating Parent"],
          [film: {"Sanjuro", 1962}, characters: "Samurai Reading Notice"],
          [film: {"Gorath", 1962}, characters: "Funeral Attendant"],
          [film: {"King Kong vs. Godzilla", 1962}, characters: "Islander"],
          [film: {"Mothra vs. Godzilla", 1964}, characters: "Reporter"],
          [film: {"Dogora, the Space Monster", 1964}, characters: "Witness at Kyushu"],
          [film: {"Ghidorah, the Three-Headed Monster", 1964}, characters: "UFO Club Member"],
          [film: {"Frankenstein Conquers the World", 1965}, characters: "Villager"],
          [film: {"Monster Zero", 1965}, characters: "Scientist"],
          [
            film: {"The Adventures of Taklamakan", 1966},
            characters: ["Merchant", "Pesil Villager"]
          ],
          [film: {"War of the Gargantuas", 1966}, characters: ["Fisherman", "Reporter"]],
          [film: {"Godzilla vs. the Sea Monster", 1966}, characters: "Captive Islander"],
          [film: {"Destroy All Monsters", 1968}, characters: "Military Advisor"],
          [film: {"The Submersion of Japan", 1973}, characters: "Refugee on Train"],
          [film: {"Prophecies of Nostradamus", 1974}, characters: "Face in Crowd"],
          [film: {"Terror of Mechagodzilla", 1975}, characters: "Skeptical Scientist"]
        ]
      ]
    ]
  ],
  bio: true
]
