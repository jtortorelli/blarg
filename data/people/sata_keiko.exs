[
  given_name: "Keiko",
  family_name: "Sata",
  path: "sata-keiko",
  original_name: "佐多 契子",
  dob: [year: 1941, month: 6, day: 9],
  selected_filmography: [
    roles: [
      [
        role: "Actress",
        films: [
          [film: {"The Human Vapor", 1960}, characters: "Kyoko Kono"],
          [film: {"Gorath", 1962}, characters: "Secretary"]
        ]
      ]
    ]
  ],
  bio: true
]
