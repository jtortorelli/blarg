[
  given_name: "Jun",
  family_name: "Tazaki",
  path: "tazaki-jun",
  original_name: "田崎 潤",
  birth_name: "Minoru Tanaka (田中実)",
  dob: [year: 1913, month: 8, day: 28],
  birth_place: "Aomori, Japan",
  dod: [year: 1985, month: 10, day: 18],
  death_place: "Tokyo, Japan",
  selected_filmography: [
    roles: [
      [
        role: "Actor",
        films: [
          [film: {"The Birth of Japan", 1959}, characters: "Kurohiko"],
          [film: {"Daredevil in the Castle", 1961}, characters: "Shikaibo Tsutsui"],
          [film: {"Gorath", 1962}, characters: "Captain Raizo Sonoda"],
          [film: {"King Kong vs. Godzilla", 1962}, characters: "General Shinzo"],
          [film: {"High and Low", 1963}, characters: "Kamiya"],
          [film: {"Samurai Pirate", 1963}, characters: "Slim"],
          [film: {"Atragon", 1963}, characters: "Captain Hachiro Jinguji"],
          [film: {"Mothra vs. Godzilla", 1964}, characters: "Newspaper Editor"],
          [film: {"Dogora, the Space Monster", 1964}, characters: "Police Chief"],
          [film: {"Frankenstein Conquers the World", 1965}, characters: "Officer Nishi"],
          [film: {"Monster Zero", 1965}, characters: "Dr. Sakurai"],
          [film: {"The Adventures of Taklamakan", 1966}, characters: "The Innkeeper"],
          [film: {"War of the Gargantuas", 1966}, characters: "General Hashimoto"],
          [film: {"Godzilla vs. the Sea Monster", 1966}, characters: "Red Bamboo Commander"],
          [film: {"Destroy All Monsters", 1968}, characters: "Dr. Yoshido"]
        ]
      ]
    ]
  ],
  bio: true
]
