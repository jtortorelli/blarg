[
  given_name: "Hideo",
  family_name: "Sunazuka",
  path: "sunazuka-hideo",
  original_name: "砂塚 秀夫",
  dob: [year: 1932, month: 8, day: 7],
  birth_place: "Atami, Shizuoka, Japan",
  selected_filmography: [
    roles: [
      [
        role: "Actor",
        films: [
          [film: {"Samurai Pirate", 1963}, characters: "Mustachioed Rebel"],
          [film: {"Godzilla vs. the Sea Monster", 1966}, characters: "Nita"],
          [film: {"Zatoichi Meets Yojimbo", 1970}, characters: "Yunosuke"],
          [film: {"Daigoro vs. Goliath", 1972}, characters: "Salaryman"]
        ]
      ]
    ]
  ],
  bio: true
]
