[
  given_name: "Koji",
  family_name: "Uno",
  path: "uno-koji",
  original_name: "宇野 晃司",
  dob: [year: 1924, month: 4, day: 21],
  birth_place: "Omuta, Fukuoka, Japan",
  dod: [year: 2003, month: 7, day: 28],
  selected_filmography: [
    roles: [
      [
        role: "Actor",
        films: [
          [film: {"Seven Samurai", 1954}, characters: "Bandit"],
          [film: {"Godzilla, King of the Monsters", 1954}, characters: "Correspondent"],
          [film: {"The Invisible Man", 1954}, characters: ["Bus Driver", "Detective"]],
          [film: {"Godzilla Raids Again", 1955}, characters: "Assistant"],
          [film: {"Samurai III: Duel at Ganryu Island", 1956}, characters: "Fallen Samurai"],
          [film: {"Rodan", 1956}, characters: "Reporter"],
          [film: {"The Secret of the Telegian", 1960}, characters: "Delivery Truck Driver"],
          [film: {"Mothra", 1961}, characters: "Policeman"],
          [film: {"The Last War", 1961}, characters: "Defense Officer"],
          [film: {"Gorath", 1962}, characters: "Reporter"],
          [film: {"Atragon", 1963}, characters: "Policeman"],
          [film: {"Mothra vs. Godzilla", 1964}, characters: "Fisherman"],
          [film: {"Dogora, the Space Monster", 1964}, characters: "Coal Miner"],
          [film: {"Ghidorah, the Three-Headed Monster", 1964}, characters: "Hotel Clerk"],
          [film: {"Monster Zero", 1965}, characters: "Xian"]
        ]
      ]
    ]
  ],
  bio: true
]
