[
  given_name: "Seijiro",
  family_name: "Onda",
  path: "onda-seijiro",
  original_name: "恩田 清二郎",
  selected_filmography: [
    roles: [
      [
        role: "Actor",
        films: [
          [film: {"Samurai I: Musashi Miyamoto", 1954}, characters: "Official"],
          [film: {"Godzilla, King of the Monsters", 1954}, characters: "Parliamentarian"],
          [film: {"The Invisible Man", 1954}, characters: "Commissioner"],
          [film: {"Godzilla Raids Again", 1955}, characters: "Captain Terasawa"],
          [film: {"Throne of Blood", 1957}, characters: "Miki Servant"]
        ]
      ]
    ]
  ],
  bio: true
]
