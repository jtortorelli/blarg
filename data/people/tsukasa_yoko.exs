[
  given_name: "Yoko",
  family_name: "Tsukasa",
  path: "tsukasa-yoko",
  original_name: "司 葉子",
  birth_name: "Yoko Shoji (庄司 葉子)",
  dob: [year: 1934, month: 8, day: 20],
  birth_place: "Saihaku, Tottori, Japan",
  selected_filmography: [
    roles: [
      [
        role: "Actress",
        films: [
          [film: {"The Birth of Japan", 1959}, characters: "Oto Tachibana"],
          [film: {"Yojimbo", 1961}, characters: "Kodaira's Wife"],
          [film: {"Prophecies of Nostradamus", 1974}, characters: "Nobue Nishiyama"]
        ]
      ]
    ]
  ],
  bio: true
]
