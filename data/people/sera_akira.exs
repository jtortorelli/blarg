[
  given_name: "Akira",
  family_name: "Sera",
  path: "sera-akira",
  original_name: "瀬良 明",
  birth_name: "Akira Watanabe (渡辺 章)",
  dob: [year: 1912, month: 10, day: 14],
  birth_place: "Azino, Kojima, Okayama, Japan",
  dod: [unknown: true],
  selected_filmography: [
    roles: [
      [
        role: "Actor",
        films: [
          [film: {"Samurai I: Musashi Miyamoto", 1954}, characters: "Villager"],
          [film: {"Godzilla, King of the Monsters", 1954}, characters: "Parliamentarian"],
          [film: {"The Invisible Man", 1954}, characters: "Food Stand Chef"],
          [film: {"Godzilla Raids Again", 1955}, characters: "Fishing Company Employee"],
          [film: {"The H-Man", 1958}, characters: "Fisherman"],
          [film: {"Varan the Unbelievable", 1958}, characters: "Village Priest"],
          [film: {"The Birth of Japan", 1959}, characters: "Anazuchi"],
          [film: {"The Secret of the Telegian", 1960}, characters: "Caretaker"]
        ]
      ]
    ]
  ],
  bio: true
]
