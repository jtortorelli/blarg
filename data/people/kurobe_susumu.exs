[
  given_name: "Susumu",
  family_name: "Kurobe",
  path: "kurobe-susumu",
  original_name: "黒部 進",
  birth_name: "Takashi Yoshimoto (吉本 隆志)",
  dob: [year: 1939, month: 10, day: 22],
  birth_place: "Kurobe, Toyama, Japan",
  selected_filmography: [
    roles: [
      [
        role: "Actor",
        films: [
          [film: {"Ghidorah, the Three-Headed Monster", 1964}, characters: "Assassin"],
          [film: {"The Adventures of Taklamakan", 1966}, characters: "Palace Guard"],
          [film: {"King Kong Escapes", 1967}, characters: "Who Henchman"],
          [film: {"Son of Godzilla", 1967}, characters: "Pilot"],
          [film: {"Destroy All Monsters", 1968}, characters: "Possessed Monster Island Tech"],
          [film: {"Latitude Zero", 1969}, characters: "Chen"],
          [film: {"The Bullet Train", 1975}, characters: "Detective Goto"],
          [film: {"Godzilla VS King Ghidorah", 1991}, characters: "Chief of Air Staff"],
          [film: {"Godzilla VS Mothra", 1992}, characters: "Chief of Air Staff"],
          [film: {"Godzilla X Megaguirus", 2000}, characters: "SDF Official"]
        ]
      ]
    ]
  ],
  bio: true
]
