[
  given_name: "Shinichi",
  family_name: "Sekizawa",
  path: "sekizawa-shinichi",
  original_name: "関沢 新一",
  dob: [year: 1920, month: 6, day: 20],
  birth_place: "Kyoto, Japan",
  dod: [year: 1992, month: 11, day: 19],
  selected_filmography: [
    roles: [
      [
        role: "Screenwriter",
        films: [
          [film: {"The H-Man", 1958}],
          [film: {"Varan the Unbelievable", 1958}],
          [film: {"Battle in Outer Space", 1959}],
          [film: {"The Secret of the Telegian", 1960}],
          [film: {"Mothra", 1961}],
          [film: {"King Kong vs. Godzilla", 1962}],
          [film: {"Samurai Pirate", 1963}],
          [film: {"Atragon", 1963}],
          [film: {"Mothra vs. Godzilla", 1964}],
          [film: {"Dogora, the Space Monster", 1964}],
          [film: {"Ghidorah, the Three-Headed Monster", 1964}],
          [film: {"Monster Zero", 1965}],
          [film: {"Godzilla vs. the Sea Monster", 1966}],
          [film: {"Son of Godzilla", 1967}],
          [film: {"Latitude Zero", 1969}],
          [film: {"Godzilla's Revenge", 1969}],
          [film: {"Godzilla vs. Gigan", 1972}]
        ]
      ],
      [
        role: "Writer",
        films: [
          [film: {"Godzilla vs. Megalon", 1973}],
          [film: {"Godzilla vs. the Cosmic Monster", 1974}]
        ]
      ]
    ]
  ],
  bio: true
]
