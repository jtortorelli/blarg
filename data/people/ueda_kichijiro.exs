[
  given_name: "Kichijiro",
  family_name: "Ueda",
  path: "ueda-kichijiro",
  original_name: "上田 吉二郎",
  birth_name: "Sadao Ueda (上田 定雄)",
  dob: [year: 1904, month: 3, day: 30],
  birth_place: "Sannomiya, Kobe, Hyogo, Japan",
  dod: [year: 1972, month: 11, day: 3],
  death_place: "Kojimacho, Chofu, Tokyo, Japan",
  selected_filmography: [
    roles: [
      [
        role: "Actor",
        films: [
          [film: {"Rashomon", 1950}, characters: "The Peasant"],
          [film: {"Seven Samurai", 1954}, characters: "Bandit Scout"],
          [film: {"Samurai III: Duel at Ganryu Island", 1956}, characters: "Ogon the Priest"],
          [film: {"Throne of Blood", 1957}, characters: "Warlord"],
          [film: {"The Hidden Fortress", 1958}, characters: "Slave Dealer"],
          [film: {"The Birth of Japan", 1959}, characters: "Hachihara"],
          [film: {"Daredevil in the Castle", 1961}, characters: "Tool Merchant Zenbei"],
          [film: {"Three Outlaw Samurai", 1964}, characters: "Ishigaki"],
          [film: {"Adventures of Zatoichi", 1964}, characters: "Boss Jinbei"],
          [film: {"Gamera vs. Gyaos", 1967}, characters: "Tanaemon Kanamaru"]
        ]
      ]
    ]
  ],
  bio: true
]
