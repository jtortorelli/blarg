[
  given_name: "Hisaya",
  family_name: "Ito",
  path: "ito-hisaya",
  original_name: "伊藤 久哉",
  birth_name: "Naoya Ito (伊藤 尚也)",
  dob: [year: 1924, month: 8, day: 7],
  birth_place: "Kobe, Hyogo, Japan",
  dod: [year: 2005],
  selected_filmography: [
    roles: [
      [
        role: "Actor",
        films: [
          [film: {"Seven Samurai", 1954}, characters: "Samurai"],
          [film: {"The Mysterians", 1957}, characters: "Officer Seki"],
          [film: {"The H-Man", 1958}, characters: "Misaki"],
          [film: {"Varan the Unbelievable", 1958}, characters: "Ichiro Shinjo"],
          [film: {"The Birth of Japan", 1959}, characters: "Kodate Otomo"],
          [film: {"Battle in Outer Space", 1959}, characters: "Astronaut Kogure"],
          [film: {"The Human Vapor", 1960}, characters: "Dr. Tamiya"],
          [film: {"Atragon", 1963}, characters: "Kidnapped Scientist"],
          [film: {"Ghidorah, the Three-Headed Monster", 1964}, characters: "Malmess"],
          [film: {"Frankenstein Conquers the World", 1965}, characters: "Policeman"],
          [film: {"War of the Gargantuas", 1966}, characters: "Coast Guard"],
          [film: {"Godzilla vs. the Sea Monster", 1966}, characters: "Red Bamboo Scientist"],
          [film: {"Destroy All Monsters", 1968}, characters: "Major Tada"]
        ]
      ]
    ]
  ],
  bio: true
]
