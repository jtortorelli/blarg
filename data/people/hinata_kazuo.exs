[
  given_name: "Kazuo",
  family_name: "Hinata",
  path: "hinata-kazuo",
  original_name: "日方 一夫",
  dob: [year: 1918, month: 7, day: 21],
  birth_place: "Nihonbashi, Chuo, Tokyo, Japan",
  dod: [unknown: true],
  selected_filmography: [
    roles: [
      [
        role: "Actor",
        films: [
          [film: {"Seven Samurai", 1954}, characters: "Samurai"],
          [
            film: {"Godzilla, King of the Monsters", 1954},
            characters: ["Defense Official", "Radio Operator"]
          ],
          [
            film: {"The Invisible Man", 1954},
            characters: ["Reporter", "Advertising Company Employee"]
          ],
          [film: {"The Mysterians", 1957}, characters: "Pilot"],
          [film: {"The H-Man", 1958}, characters: ["Barfly", "Police Executive"]],
          [film: {"The Hidden Fortress", 1958}, characters: "Yamana River Guard"],
          [film: {"The Birth of Japan", 1959}, characters: "Utte Villager"],
          [film: {"Battle in Outer Space", 1959}, characters: "UN Official"],
          [film: {"The Secret of the Telegian", 1960}, characters: "Waiter"],
          [film: {"The Human Vapor", 1960}, characters: "Bank Official"],
          [film: {"Mothra", 1961}, characters: ["Audience Member", "Military Officer"]],
          [film: {"The Last War", 1961}, characters: "Official"],
          [film: {"King Kong vs. Godzilla", 1962}, characters: "Scientist"],
          [film: {"High and Low", 1963}, characters: "Man at Crime Scene"],
          [film: {"Matango", 1963}, characters: "Official"],
          [film: {"Atragon", 1963}, characters: "Military Officer"],
          [film: {"Mothra vs. Godzilla", 1964}, characters: "Reporter"],
          [film: {"Dogora, the Space Monster", 1964}, characters: "Witness at Coal Plant"],
          [
            film: {"Ghidorah, the Three-Headed Monster", 1964},
            characters: ["Serginan on Plane", "Reporter", "Parliamentarian"]
          ],
          [film: {"Frankenstein Conquers the World", 1965}, characters: "Reporter"],
          [film: {"Monster Zero", 1965}, characters: "Scientist"],
          [film: {"King Kong Escapes", 1967}, characters: "Military Advisor"],
          [film: {"Destroy All Monsters", 1968}, characters: "UNSC Tech"]
        ]
      ]
    ]
  ],
  bio: true
]
