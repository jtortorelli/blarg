[
  given_name: "Ikio",
  family_name: "Sawamura",
  path: "sawamura-ikio",
  original_name: "沢村 いき雄",
  birth_name: "Shizuo Okabe (岡部 静雄)",
  dob: [year: 1905, month: 9, day: 4],
  birth_place: "Tochigi, Japan",
  dod: [year: 1975, month: 9, day: 20],
  selected_filmography: [
    roles: [
      [
        role: "Actor",
        films: [
          [film: {"Samurai III: Duel at Ganryu Island", 1956}, characters: "Innkeeper"],
          [film: {"Throne of Blood", 1957}, characters: "Washizu Soldier"],
          [film: {"The Hidden Fortress", 1958}, characters: "Yamana Peasant"],
          [film: {"The Birth of Japan", 1959}, characters: "Lecherous Deity"],
          [film: {"Battle in Outer Space", 1959}, characters: "Line Inspector"],
          [film: {"The Secret of the Telegian", 1960}, characters: "Thriller Show Announcer"],
          [film: {"Daredevil in the Castle", 1961}, characters: "Man Buying Rice"],
          [film: {"Yojimbo", 1961}, characters: "Corrupt Bailiff"],
          [film: {"Gorath", 1962}, characters: "Taxi Driver"],
          [film: {"King Kong vs. Godzilla", 1962}, characters: "Island Priest"],
          [film: {"High and Low", 1963}, characters: "Train Crew"],
          [film: {"Atragon", 1963}, characters: "Taxi Driver"],
          [film: {"Mothra vs. Godzilla", 1964}, characters: "Village Priest"],
          [film: {"Ghidorah, the Three-Headed Monster", 1964}, characters: "Fisherman"],
          [film: {"Frankenstein Conquers the World", 1965}, characters: "Man Walking Dog"],
          [film: {"The Adventures of Taklamakan", 1966}, characters: "Slave Auctioneer"],
          [film: {"War of the Gargantuas", 1966}, characters: "Fisherman"],
          [film: {"Godzilla vs. the Sea Monster", 1966}, characters: "Captive Islander"],
          [film: {"King Kong Escapes", 1967}, characters: "Islander"],
          [film: {"Destroy All Monsters", 1968}, characters: "Mountaineer"],
          [film: {"Godzilla's Revenge", 1969}, characters: "Bartender"],
          [film: {"Terror of Mechagodzilla", 1975}, characters: "Mafune's Manservant"]
        ]
      ]
    ]
  ],
  bio: true
]
