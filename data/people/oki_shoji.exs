[
  given_name: "Shoji",
  family_name: "Oki",
  path: "oki-shoji",
  original_name: "大木 正司",
  dob: [year: 1936, month: 9, day: 27],
  birth_place: "Numazu, Shizuoka, Japan",
  dod: [year: 2009, month: 11, day: 20],
  selected_filmography: [
    roles: [
      [
        role: "Actor",
        films: [
          [film: {"Yojimbo", 1961}, characters: "Seibei Underling"],
          [film: {"Samurai Pirate", 1963}, characters: "Turbaned Rebel"],
          [film: {"The Adventures of Taklamakan", 1966}, characters: "Sundara"]
        ]
      ]
    ]
  ],
  bio: true
]
