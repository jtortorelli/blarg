[
  given_name: "Yoshibumi",
  family_name: "Tajima",
  path: "tajima-yoshibumi",
  original_name: "田島 義文",
  dob: [year: 1918, month: 8, day: 4],
  birth_place: "Kobe, Hyogo, Japan",
  dod: [year: 2009, month: 9, day: 10],
  selected_filmography: [
    roles: [
      [
        role: "Actor",
        films: [
          [film: {"Samurai II: Duel at Ichijoji Temple", 1955}, characters: "Heisuke Otaguro"],
          [film: {"Rodan", 1956}, characters: "Izeki"],
          [film: {"The H-Man", 1958}, characters: "Detective Sakata"],
          [film: {"Varan the Unbelievable", 1958}, characters: "Naval Officer"],
          [film: {"The Hidden Fortress", 1958}, characters: "Man at Fire Festival"],
          [film: {"The Birth of Japan", 1959}, characters: "Deity"],
          [film: {"The Secret of the Telegian", 1960}, characters: "Takamasa"],
          [film: {"The Human Vapor", 1960}, characters: "Detective Tabata"],
          [film: {"Mothra", 1961}, characters: "Soldier"],
          [film: {"King Kong vs. Godzilla", 1962}, characters: "Research Ship Captain"],
          [film: {"High and Low", 1963}, characters: "Policeman"],
          [film: {"Atragon", 1963}, characters: "Lt. Amano"],
          [film: {"Mothra vs. Godzilla", 1964}, characters: "Kumayama"],
          [film: {"Dogora, the Space Monster", 1964}, characters: "Gangster"],
          [film: {"Ghidorah, the Three-Headed Monster", 1964}, characters: "Cruise Ship Captain"],
          [film: {"Frankenstein Conquers the World", 1965}, characters: "Axis Submarine Captain"],
          [film: {"Monster Zero", 1965}, characters: "Soldier"],
          [film: {"War of the Gargantuas", 1966}, characters: "Coast Guard"],
          [film: {"King Kong Escapes", 1967}, characters: "Who Henchman"],
          [film: {"Destroy All Monsters", 1968}, characters: "Defense Chief Sugiyama"],
          [film: {"Godzilla's Revenge", 1969}, characters: "Detective"],
          [film: {"The Bullet Train", 1975}, characters: "Detective Sasaki"],
          [film: {"The Return of Godzilla", 1984}, characters: "Minister Hidaka"]
        ]
      ]
    ]
  ],
  bio: true
]
