[
  given_name: "Masaru",
  family_name: "Sato",
  path: "sato-masaru",
  original_name: "佐藤 勝",
  dob: [year: 1928, month: 5, day: 29],
  birth_place: "Rumoi, Hokkaido, Japan",
  dod: [year: 1999, month: 12, day: 5],
  selected_filmography: [
    roles: [
      [
        role: "Composer",
        films: [
          [film: {"Godzilla Raids Again", 1955}],
          [film: {"Throne of Blood", 1957}],
          [film: {"The H-Man", 1958}],
          [film: {"The Hidden Fortress", 1958}],
          [film: {"Yojimbo", 1961}],
          [film: {"Sanjuro", 1962}],
          [film: {"High and Low", 1963}],
          [film: {"Samurai Pirate", 1963}],
          [film: {"Godzilla vs. the Sea Monster", 1966}],
          [film: {"Son of Godzilla", 1967}],
          [film: {"The Submersion of Japan", 1973}],
          [film: {"Godzilla vs. the Cosmic Monster", 1974}]
        ]
      ]
    ]
  ],
  bio: true
]
