[
  given_name: "Sanezumi",
  family_name: "Fujimoto",
  path: "fujimoto-sanezumi",
  original_name: "藤本 真澄",
  dob: [year: 1910, month: 7, day: 15],
  birth_place: "Ryojun, Manchuria",
  dod: [year: 1979, month: 5, day: 2],
  selected_filmography: [
    roles: [
      [
        role: "Producer",
        films: [
          [film: {"The Hidden Fortress", 1958}],
          [film: {"The Birth of Japan", 1959}],
          [film: {"The Last War", 1961}]
        ]
      ]
    ]
  ],
  bio: true
]
