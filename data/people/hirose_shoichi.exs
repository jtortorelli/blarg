[
  given_name: "Shoichi",
  family_name: "Hirose",
  path: "hirose-shoichi",
  original_name: "広瀬 正一",
  dob: [year: 1918, month: 6, day: 23],
  dod: [unknown: true],
  selected_filmography: [
    roles: [
      [
        role: "Actor",
        films: [
          [film: {"Seven Samurai", 1954}, characters: "Bandit"],
          [film: {"Godzilla, King of the Monsters", 1954}, characters: "Parliamentarian"],
          [film: {"The Invisible Man", 1954}, characters: "Policeman"],
          [film: {"Godzilla Raids Again", 1955}, characters: "Escaped Convict"],
          [film: {"Samurai III: Duel at Ganryu Island", 1956}, characters: "Gambler"],
          [film: {"Rodan", 1956}, characters: ["Meganulon", "Pilot"]],
          [film: {"The Mysterians", 1957}, characters: "Detective"],
          [film: {"The H-Man", 1958}, characters: "Fire Chief"],
          [film: {"Varan the Unbelievable", 1958}, characters: "Fisherman"],
          [film: {"The Hidden Fortress", 1958}, characters: "Yamana Prisoner Guard"],
          [film: {"The Birth of Japan", 1959}, characters: "Kumaso Soldier"],
          [film: {"The Secret of the Telegian", 1960}, characters: "Bodyguard"],
          [film: {"The Human Vapor", 1960}, characters: "Jail Officer"],
          [film: {"Yojimbo", 1961}, characters: "Ushitora Underling"],
          [film: {"Mothra", 1961}, characters: "Dam Worker"],
          [film: {"Sanjuro", 1962}, characters: "Kikui Guard"],
          [film: {"Gorath", 1962}, characters: "Policeman"],
          [film: {"King Kong vs. Godzilla", 1962}, characters: "King Kong"],
          [film: {"Atragon", 1963}, characters: "Mu Citizen"],
          [film: {"Dogora, the Space Monster", 1964}, characters: "Coal Miner"],
          [
            film: {"Ghidorah, the Three-Headed Monster", 1964},
            characters: ["King Ghidorah", "Villager"]
          ],
          [film: {"Frankenstein Conquers the World", 1965}, characters: "Miner"],
          [film: {"Monster Zero", 1965}, characters: "King Ghidorah"],
          [film: {"War of the Gargantuas", 1966}, characters: "Guide"],
          [film: {"Godzilla vs. the Sea Monster", 1966}, characters: "Escaped Islander"],
          [film: {"King Kong Escapes", 1967}, characters: "Submarine Crew"],
          [film: {"Terror of Mechagodzilla", 1975}, characters: "Slave Worker"]
        ]
      ]
    ]
  ],
  bio: true
]
