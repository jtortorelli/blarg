[
  given_name: "Naoki",
  family_name: "Sato",
  path: "sato-naoki",
  original_name: "佐藤 直紀",
  dob: [year: 1970, month: 5, day: 2],
  birth_place: "Chiba, Japan",
  selected_filmography: [
    roles: [
      [
        role: "Composer",
        films: [
          [film: {"Lorelei", 2005}],
          [film: {"Always", 2005}],
          [film: {"Always 2", 2007}],
          [film: {"The Glorious Team Batista", 2008}],
          [film: {"Hidden Fortress: The Last Princess", 2008}],
          [film: {"K-20", 2008}],
          [film: {"The Triumphant General Rouge", 2009}],
          [film: {"Ballad", 2009}],
          [film: {"Space Battleship Yamato", 2010}],
          [film: {"Friends", 2011}],
          [film: {"Always 3", 2012}],
          [film: {"Rurouni Kenshin", 2012}],
          [film: {"The Eternal Zero", 2013}],
          [film: {"Rurouni Kenshin: Kyoto Inferno", 2014}],
          [film: {"Stand By Me, Doraemon", 2014}],
          [film: {"Rurouni Kenshin: The Legend Ends", 2014}],
          [film: {"Parasyte", 2014}],
          [film: {"Assassination Classroom", 2015}],
          [film: {"Parasyte: Completion", 2015}],
          [film: {"Assassination Classroom: Graduation", 2016}],
          [film: {"The Top Secret", 2016}],
          [film: {"A Man Called Pirate", 2016}]
        ]
      ]
    ]
  ],
  bio: true
]
