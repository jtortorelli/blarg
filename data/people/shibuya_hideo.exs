[
  given_name: "Hideo",
  family_name: "Shibuya",
  path: "shibuya-hideo",
  original_name: "渋谷 英男",
  dob: [year: 1928, month: 2, day: 20],
  birth_place: "Tokyo, Japan",
  selected_filmography: [
    roles: [
      [
        role: "Actor",
        films: [
          [film: {"Seven Samurai", 1954}, characters: "Bandit"],
          [film: {"Godzilla, King of the Monsters", 1954}, characters: "Reporter"],
          [film: {"The Invisible Man", 1954}, characters: ["Nightclub Patron", "Bus Passenger"]],
          [
            film: {"Samurai II: Duel at Ichijoji Temple", 1955},
            characters: "Yoshioka School Samurai"
          ],
          [
            film: {"Samurai III: Duel at Ganryu Island", 1956},
            characters: "Guest at Geisha House"
          ],
          [film: {"Rodan", 1956}, characters: "Miner"],
          [film: {"The Mysterians", 1957}, characters: "Policeman"],
          [film: {"The H-Man", 1958}, characters: "Reporter"],
          [film: {"Varan the Unbelievable", 1958}, characters: "Reporter"],
          [film: {"The Hidden Fortress", 1958}, characters: "Yamana Peasant"],
          [film: {"The Secret of the Telegian", 1960}, characters: "Policeman"],
          [film: {"The Human Vapor", 1960}, characters: "Banker"],
          [film: {"Yojimbo", 1961}, characters: "Ushitora Underling"],
          [film: {"Mothra", 1961}, characters: "Reporter"],
          [film: {"The Last War", 1961}, characters: "Sailor"],
          [film: {"Sanjuro", 1962}, characters: "Kikui Samurai"],
          [film: {"Gorath", 1962}, characters: "Reporter"],
          [film: {"King Kong vs. Godzilla", 1962}, characters: "Reporter"],
          [film: {"High and Low", 1963}, characters: "Dining Car Guest"],
          [film: {"Atragon", 1963}, characters: "Soldier"],
          [film: {"Mothra vs. Godzilla", 1964}, characters: "Reporter"],
          [film: {"Dogora, the Space Monster", 1964}, characters: "Reporter"],
          [
            film: {"Ghidorah, the Three-Headed Monster", 1964},
            characters: ["Volcanologist", "Villager"]
          ],
          [film: {"Frankenstein Conquers the World", 1965}, characters: "Reporter"],
          [film: {"War of the Gargantuas", 1966}, characters: "Reporter"],
          [film: {"Godzilla vs. the Sea Monster", 1966}, characters: "Policeman"],
          [film: {"King Kong Escapes", 1967}, characters: ["Submarine Crew", "Soldier"]],
          [film: {"Destroy All Monsters", 1968}, characters: "Reporter"]
        ]
      ]
    ]
  ],
  bio: true
]
