[
  given_name: "Kaoru",
  family_name: "Yachigusa",
  path: "yachigusa-kaoru",
  original_name: "八千草 薫",
  birth_name: "Hitomi Matsuda (松田 瞳)",
  dob: [year: 1931, month: 1, day: 6],
  dod: [year: 2019, month: 10, day: 24],
  birth_place: "Osaka, Japan",
  selected_filmography: [
    roles: [
      [
        role: "Actress",
        films: [
          [film: {"Samurai I: Musashi Miyamoto", 1954}, characters: "Otsu"],
          [film: {"Samurai II: Duel at Ichijoji Temple", 1955}, characters: "Otsu"],
          [film: {"Samurai III: Duel at Ganryu Island", 1956}, characters: "Otsu"],
          [film: {"The Human Vapor", 1960}, characters: "Fujichiyo Kasuga"]
        ]
      ]
    ]
  ],
  bio: true
]
