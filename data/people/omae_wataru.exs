[
  given_name: "Wataru",
  family_name: "Omae",
  path: "omae-wataru",
  original_name: "大前 亘",
  dob: [year: 1934, month: 2, day: 14],
  dod: [year: 2017, month: 12, day: 24],
  birth_place: "Gunma, Japan",
  selected_filmography: [
    roles: [
      [
        role: "Actor",
        films: [
          [film: {"The Human Vapor", 1960}, characters: "Reporter"],
          [film: {"Mothra", 1961}, characters: "Coast Guard"],
          [film: {"The Last War", 1961}, characters: "Sailor"],
          [film: {"Sanjuro", 1962}, characters: "Kikui Samurai"],
          [film: {"Gorath", 1962}, characters: "Spaceship Crew"],
          [film: {"High and Low", 1963}, characters: "Toll Booth Worker"],
          [film: {"Atragon", 1963}, characters: "Radar Officer"],
          [film: {"Mothra vs. Godzilla", 1964}, characters: "Kumayama's Henchman"],
          [film: {"Dogora, the Space Monster", 1964}, characters: "Satellite Technician"],
          [film: {"War of the Gargantuas", 1966}, characters: "Air Traffic Controller"],
          [film: {"Godzilla vs. the Sea Monster", 1966}, characters: "Reporter"],
          [film: {"Son of Godzilla", 1967}, characters: "Pilot"],
          [film: {"Destroy All Monsters", 1968}, characters: "SY-3 Pilot"],
          [film: {"Space Amoeba", 1970}, characters: "Sakura"],
          [film: {"Lake of Dracula", 1971}, characters: "Fisherman"],
          [film: {"Godzilla vs. Hedorah", 1971}, characters: "Policeman"],
          [film: {"Godzilla vs. Gigan", 1972}, characters: "Alien Henchman"]
        ]
      ]
    ]
  ],
  bio: true
]
