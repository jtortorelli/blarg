[
  given_name: "Masanari",
  family_name: "Nihei",
  path: "nihei-masanari",
  original_name: "二瓶 正也",
  dob: [year: 1940, month: 12, day: 4],
  dod: [year: 2021, month: 8, day: 21],
  birth_place: "Nagatacho, Kojimachi, Tokyo, Japan",
  selected_filmography: [
    roles: [
      [
        role: "Actor",
        films: [
          [film: {"Mothra", 1961}, characters: "Policeman"],
          [film: {"The Last War", 1961}, characters: "TV Singer"],
          [film: {"Gorath", 1962}, characters: "Astronaut Ito"],
          [film: {"Samurai Pirate", 1963}, characters: "Tall Rebel"]
        ]
      ]
    ]
  ],
  bio: true
]
