[
  given_name: "Fuyuki",
  family_name: "Murakami",
  path: "murakami-fuyuki",
  original_name: "村上 冬樹",
  birth_name: "Saishu Murakami (村上 済州)",
  dob: [year: 1911, month: 12, day: 23],
  birth_place: "Tokuyama, Yamaguchi, Japan",
  dod: [year: 2007, month: 4, day: 5],
  death_place: "Meguro, Tokyo, Japan",
  selected_filmography: [
    roles: [
      [
        role: "Actor",
        films: [
          [film: {"Godzilla, King of the Monsters", 1954}, characters: "Dr. Tanabe"],
          [film: {"The Invisible Man", 1954}, characters: "Newspaper Editor"],
          [film: {"Rodan", 1956}, characters: "Dr. Minami"],
          [film: {"The Mysterians", 1957}, characters: "Dr. Kawanami"],
          [film: {"Varan the Unbelievable", 1958}, characters: "Dr. Majima"],
          [film: {"The Birth of Japan", 1959}, characters: "Deity"],
          [film: {"Battle in Outer Space", 1959}, characters: "Detective Iriake"],
          [film: {"The Secret of the Telegian", 1960}, characters: "Dr. Miura"],
          [film: {"The Human Vapor", 1960}, characters: "Dr. Sano"],
          [film: {"Monster Zero", 1965}, characters: "Minister"]
        ]
      ]
    ]
  ],
  bio: true
]
