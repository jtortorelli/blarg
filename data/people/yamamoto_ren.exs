[
  given_name: "Ren",
  family_name: "Yamamoto",
  path: "yamamoto-ren",
  original_name: "山本 廉",
  birth_name: "Kiyoshi Yamamoto (山本 廉)",
  dob: [year: 1930, month: 5, day: 12],
  birth_place: "Minamiashigara, Ashigarakami, Kanagawa, Japan",
  dod: [year: 2003, month: 6, day: 17],
  selected_filmography: [
    roles: [
      [
        role: "Actor",
        films: [
          [film: {"Seven Samurai", 1954}, characters: "Peasant"],
          [film: {"Godzilla, King of the Monsters", 1954}, characters: "Masaji"],
          [film: {"Godzilla Raids Again", 1955}, characters: "Ikeda"],
          [film: {"Samurai II: Duel at Ichijoji Temple", 1955}, characters: "Riichi Kawakami"],
          [film: {"Samurai III: Duel at Ganryu Island", 1956}, characters: "Villager"],
          [film: {"Rodan", 1956}, characters: "Soldier"],
          [film: {"The H-Man", 1958}, characters: "Gangster"],
          [film: {"The Birth of Japan", 1959}, characters: "Yamato Soldier"],
          [film: {"The Secret of the Telegian", 1960}, characters: "Detective Marune"],
          [film: {"The Human Vapor", 1960}, characters: "Nishiyama"],
          [film: {"Daredevil in the Castle", 1961}, characters: "Mohei's Crew"],
          [film: {"Mothra", 1961}, characters: "Marooned Sailor"],
          [film: {"King Kong vs. Godzilla", 1962}, characters: "Soldier"],
          [film: {"Whirlwind", 1964}, characters: "Mineo"],
          [film: {"Mothra vs. Godzilla", 1964}, characters: "Sailor"],
          [film: {"Frankenstein Conquers the World", 1965}, characters: "Kawai's Assistant"],
          [film: {"The Adventures of Taklamakan", 1966}, characters: "Jail Keeper"],
          [film: {"War of the Gargantuas", 1966}, characters: "Sailor"]
        ]
      ]
    ]
  ],
  bio: true
]
