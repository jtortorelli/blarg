[
  given_name: "Senkichi",
  family_name: "Taniguchi",
  path: "taniguchi-senkichi",
  original_name: "谷口 千吉",
  dob: [year: 1912, month: 2, day: 19],
  birth_place: "Tokyo, Japan",
  dod: [year: 2007, month: 10, day: 29],
  selected_filmography: [
    roles: [
      [
        role: "Director",
        films: [
          [film: {"Samurai Pirate", 1963}],
          [film: {"The Adventures of Taklamakan", 1966}]
        ]
      ]
    ]
  ],
  bio: true
]
