[
  given_name: "Kenji",
  family_name: "Kawai",
  path: "kawai-kenji",
  original_name: "川井 憲次",
  dob: [year: 1957, month: 4, day: 23],
  selected_filmography: [
    roles: [
      [
        role: "Composer",
        films: [
          [film: {"The Red Spectacles", 1987}],
          [film: {"Patlabor: The Movie", 1989}],
          [film: {"Stray Dog: Kerberos Panzer Cops", 1991}],
          [film: {"Talking Head", 1992}],
          [film: {"Patlabor 2: The Movie", 1993}],
          [film: {"Ghost in the Shell", 1995}],
          [film: {"Ring", 1998}],
          [film: {"Ring 2", 1999}],
          [film: {"Sakuya, Slayer of Demons", 2000}],
          [film: {"Avalon", 2001}],
          [film: {"The Princess Blade", 2001}],
          [film: {"Death Note", 2006}],
          [film: {"Death Note: The Last Name", 2006}],
          [film: {"L: Change the World", 2008}],
          [film: {"The Sky Crawlers", 2008}],
          [film: {"Assault Girls", 2009}],
          [film: {"Gantz", 2011}],
          [film: {"Gantz: Perfect Answer", 2011}]
        ]
      ]
    ]
  ],
  bio: true
]
