[
  given_name: "Yutaka",
  family_name: "Nakayama",
  path: "nakayama-yutaka",
  original_name: "中山 豊",
  dob: [year: 1939, month: 9, day: 23],
  birth_place: "Himeji, Hyogo, Japan",
  selected_filmography: [
    roles: [
      [
        role: "Actor",
        films: [
          [film: {"The Invisible Man", 1954}, characters: "Yajima's Henchman"],
          [film: {"The H-Man", 1958}, characters: "Informant Gangster"],
          [film: {"The Secret of the Telegian", 1960}, characters: "Waiter"],
          [film: {"Mothra", 1961}, characters: "Marooned Sailor"],
          [film: {"King Kong vs. Godzilla", 1962}, characters: "Sailor"],
          [film: {"Samurai Pirate", 1963}, characters: "Samurai Ichizo"],
          [film: {"Atragon", 1963}, characters: "Cargo Ship Crew"],
          [film: {"Mothra vs. Godzilla", 1964}, characters: "Fisherman"],
          [film: {"Dogora, the Space Monster", 1964}, characters: "Truck Driver"],
          [film: {"Ghidorah, the Three-Headed Monster", 1964}, characters: "Mt. Aso Tourist"],
          [film: {"Frankenstein Conquers the World", 1965}, characters: "TV Cameraman"],
          [film: {"Godzilla's Revenge", 1969}, characters: "Painter"]
        ]
      ]
    ]
  ],
  bio: true
]
