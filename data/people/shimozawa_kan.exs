[
  given_name: "Kan",
  family_name: "Shimozawa",
  path: "shimozawa-kan",
  original_name: "子母沢 寛",
  birth_name: "Matsutaro Umetani (梅谷 松太郎)",
  dob: [year: 1892, month: 2, day: 1],
  birth_place: "Atsuta, Hokkaido, Japan",
  dod: [year: 1968, month: 7, day: 19],
  death_place: "Tokyo, Japan",
  selected_filmography: [
    roles: [
      [
        role: "Writer",
        films: [
          [film: {"The Tale of Zatoichi", 1962}],
          [film: {"The Tale of Zatoichi Continues", 1962}],
          [film: {"New Tale of Zatoichi", 1963}],
          [film: {"Zatoichi the Fugitive", 1963}],
          [film: {"Zatoichi on the Road", 1963}],
          [film: {"Zatoichi and the Chest of Gold", 1964}],
          [film: {"Zatoichi's Flashing Sword", 1964}],
          [film: {"Fight, Zatoichi, Fight", 1964}],
          [film: {"Adventures of Zatoichi", 1964}],
          [film: {"Zatoichi's Revenge", 1965}],
          [film: {"Zatoichi and the Doomed Man", 1965}],
          [film: {"Zatoichi and the Chess Expert", 1965}],
          [film: {"Zatoichi's Vengeance", 1966}],
          [film: {"Zatoichi's Pilgrimage", 1966}],
          [film: {"Zatoichi's Cane Sword", 1967}],
          [film: {"Zatoichi the Outlaw", 1967}],
          [film: {"Zatoichi Challenged", 1967}],
          [film: {"Zatoichi and the Fugitives", 1968}],
          [film: {"Samaritan Zatoichi", 1968}],
          [film: {"Zatoichi Meets Yojimbo", 1970}],
          [film: {"Zatoichi Goes to the Fire Festival", 1970}],
          [film: {"Zatoichi Meets the One-Armed Swordsman", 1971}],
          [film: {"Zatoichi at Large", 1972}],
          [film: {"Zatoichi in Desperation", 1972}],
          [film: {"Zatoichi's Conspiracy", 1973}],
          [film: {"Zatoichi", 1989}]
        ]
      ]
    ]
  ],
  bio: true
]
