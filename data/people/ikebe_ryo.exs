[
  given_name: "Ryo",
  family_name: "Ikebe",
  path: "ikebe-ryo",
  original_name: "池部 良",
  dob: [year: 1918, month: 2, day: 11],
  birth_place: "Omori, Tokyo, Japan",
  dod: [year: 2010, month: 10, day: 8],
  death_place: "Tokyo, Japan",
  selected_filmography: [
    roles: [
      [
        role: "Actor",
        films: [
          [film: {"Battle in Outer Space", 1959}, characters: "Major Ichiro Katsumiya"],
          [film: {"Gorath", 1962}, characters: "Dr. Tazawa"],
          [film: {"The War in Space", 1977}, characters: "Masato Takigawa"]
        ]
      ]
    ]
  ],
  bio: true
]
