[
  given_name: "Jun",
  family_name: "Fukuda",
  path: "fukuda-jun",
  original_name: "福田 純",
  dob: [year: 1923, month: 2, day: 17],
  birth_place: "New Kyoto, Manchuria",
  dod: [year: 2000, month: 12, day: 3],
  selected_filmography: [
    roles: [
      [
        role: "Assistant Director",
        films: [
          [film: {"Samurai I: Musashi Miyamoto", 1954}],
          [film: {"Samurai II: Duel at Ichijoji Temple", 1955}],
          [film: {"Samurai III: Duel at Ganryu Island", 1956}],
          [film: {"Rodan", 1956}]
        ]
      ],
      [
        role: "Director",
        films: [
          [film: {"The Secret of the Telegian", 1960}],
          [film: {"Godzilla vs. the Sea Monster", 1966}],
          [film: {"Son of Godzilla", 1967}],
          [film: {"Godzilla vs. Gigan", 1972}],
          [film: {"Godzilla vs. Megalon", 1973}],
          [film: {"Godzilla vs. the Cosmic Monster", 1974}],
          [film: {"Espy", 1974}],
          [film: {"The War in Space", 1977}]
        ]
      ],
      [
        role: "Screenwriter",
        films: [
          [film: {"Godzilla vs. Megalon", 1973}],
          [film: {"Godzilla vs. the Cosmic Monster", 1974}]
        ]
      ]
    ]
  ],
  bio: true
]
