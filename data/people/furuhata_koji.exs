[
  given_name: "Koji",
  family_name: "Furuhata",
  path: "furuhata-koji",
  original_name: "古畑 弘二",
  selected_filmography: [
    roles: [
      [
        role: "Actor",
        films: [
          [film: {"Frankenstein Conquers the World", 1965}, characters: "Frankenstein"]
        ]
      ]
    ]
  ],
  bio: true
]
