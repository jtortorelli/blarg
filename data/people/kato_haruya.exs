[
  given_name: "Haruya",
  family_name: "Kato",
  path: "kato-haruya",
  original_name: "加藤 春哉",
  dob: [year: 1928, month: 6, day: 22],
  dod: [year: 2015],
  birth_place: "Minato, Tokyo, Japan",
  selected_filmography: [
    roles: [
      [
        role: "Actor",
        films: [
          [film: {"The Mysterians", 1957}, characters: "Villager"],
          [film: {"The H-Man", 1958}, characters: "Fisherman"],
          [film: {"Mothra", 1961}, characters: "Marooned Sailor"],
          [film: {"King Kong vs. Godzilla", 1962}, characters: "Obayashi's Assistant"],
          [film: {"Dogora, the Space Monster", 1964}, characters: "Gangster"],
          [film: {"Ghidorah, the Three-Headed Monster", 1964}, characters: "Reporter"],
          [film: {"Frankenstein Conquers the World", 1965}, characters: "TV Reporter"]
        ]
      ]
    ]
  ],
  bio: true
]
