[
  given_name: "Mitsuko",
  family_name: "Kusabue",
  path: "kusabue-mitsuko",
  original_name: "草笛 光子",
  dob: [year: 1933, month: 10, day: 22],
  birth_place: "Yokohama, Kanagawa, Japan",
  selected_filmography: [
    roles: [
      [
        role: "Actress",
        films: [
          [film: {"Samurai Pirate", 1963}, characters: "Sobei"],
          [film: {"Whirlwind", 1964}, characters: "Shuzuki"]
        ]
      ]
    ]
  ],
  bio: true
]
