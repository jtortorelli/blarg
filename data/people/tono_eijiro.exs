[
  given_name: "Eijiro",
  family_name: "Tono",
  path: "tono-eijiro",
  original_name: "東野 英治郎",
  dob: [year: 1907, month: 9, day: 17],
  birth_place: "Tomioka, Kanra, Gunma, Japan",
  dod: [year: 1994, month: 9, day: 8],
  death_place: "Kokubunji, Tokyo, Japan",
  selected_filmography: [
    roles: [
      [
        role: "Actor",
        films: [
          [film: {"Seven Samurai", 1954}, characters: "Kidnapper"],
          [film: {"Samurai II: Duel at Ichijoji Temple", 1955}, characters: "Baiken Shishido"],
          [film: {"The Birth of Japan", 1959}, characters: "Otomo"],
          [film: {"Yojimbo", 1961}, characters: "Gonji"],
          [film: {"The Last War", 1961}, characters: "Takano's Captain"],
          [film: {"High and Low", 1963}, characters: "Shoe Factory Worker"],
          [film: {"Zatoichi's Cane Sword", 1967}, characters: "Senzo"]
        ]
      ]
    ]
  ],
  bio: true
]
