[
  given_name: "Haruya",
  family_name: "Sakamoto",
  path: "sakamoto-haruya",
  original_name: "坂本 晴哉",
  dob: [year: 1928, month: 8, day: 23],
  selected_filmography: [
    roles: [
      [
        role: "Actor",
        films: [
          [film: {"Seven Samurai", 1954}, characters: "Bandit"],
          [film: {"Samurai I: Musashi Miyamoto", 1954}, characters: "Soldier"],
          [film: {"The Invisible Man", 1954}, characters: ["Bus Passenger", "Detective"]],
          [
            film: {"Samurai II: Duel at Ichijoji Temple", 1955},
            characters: "Yoshioka School Samurai"
          ],
          [film: {"Rodan", 1956}, characters: "Miner"],
          [film: {"Throne of Blood", 1957}, characters: "Washizu Soldier"],
          [film: {"Varan the Unbelievable", 1958}, characters: "Soldier"],
          [film: {"The Hidden Fortress", 1958}, characters: "Yamana Cavalry"],
          [film: {"The Birth of Japan", 1959}, characters: "Yamato Soldier"],
          [film: {"Battle in Outer Space", 1959}, characters: "Policeman"],
          [film: {"The Human Vapor", 1960}, characters: "Jailbreaker"],
          [film: {"Daredevil in the Castle", 1961}, characters: "Tokugawa Emissary"],
          [film: {"Yojimbo", 1961}, characters: "Ushitora Underling"],
          [film: {"Mothra", 1961}, characters: "Policeman"],
          [film: {"Sanjuro", 1962}, characters: ["Samurai Reading Notice", "Escort Samurai"]],
          [film: {"King Kong vs. Godzilla", 1962}, characters: "Soldier"],
          [film: {"Samurai Pirate", 1963}, characters: "Palace Guard"],
          [film: {"Atragon", 1963}, characters: "Atragon Crew"],
          [film: {"Mothra vs. Godzilla", 1964}, characters: "Soldier"],
          [film: {"Dogora, the Space Monster", 1964}, characters: "Truck Driver"],
          [film: {"Ghidorah, the Three-Headed Monster", 1964}, characters: "King Ghidorah"],
          [film: {"Frankenstein Conquers the World", 1965}, characters: "Axis Soldier"],
          [film: {"Monster Zero", 1965}, characters: "Soldier"],
          [film: {"The Adventures of Taklamakan", 1966}, characters: "Merchant"],
          [film: {"War of the Gargantuas", 1966}, characters: "Soldier"],
          [film: {"King Kong Escapes", 1967}, characters: "Soldier"],
          [film: {"Destroy All Monsters", 1968}, characters: "Military Advisor"],
          [film: {"Godzilla vs. Hedorah", 1971}, characters: "Talking Head"]
        ]
      ]
    ]
  ],
  bio: true
]
