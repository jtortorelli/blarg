[
  given_name: "Yasuyuki",
  family_name: "Inoue",
  path: "inoue-yasuyuki",
  original_name: "井上 泰幸",
  dob: [year: 1922, month: 11, day: 26],
  birth_place: "Fukuoka, Japan",
  dod: [year: 2012, month: 2, day: 19],
  selected_filmography: [
    roles: [
      [
        role: "Art Director",
        films: [
          [film: {"Godzilla vs. Hedorah", 1971}]
        ]
      ],
      [
        role: "SFX Art Director",
        films: [
          [film: {"War of the Gargantuas", 1966}],
          [film: {"Godzilla vs. the Sea Monster", 1966}],
          [film: {"King Kong Escapes", 1967}],
          [film: {"Son of Godzilla", 1967}],
          [film: {"Destroy All Monsters", 1968}],
          [film: {"Latitude Zero", 1969}],
          [film: {"Space Amoeba", 1970}],
          [film: {"The Submersion of Japan", 1973}],
          [film: {"Prophecies of Nostradamus", 1974}],
          [film: {"The Explosion", 1975}],
          [film: {"The War in Space", 1977}],
          [film: {"Earthquake Archipelago", 1980}],
          [film: {"The Return of Godzilla", 1984}],
          [film: {"Tokyo Blackout", 1987}]
        ]
      ]
    ]
  ],
  bio: true
]
