[
  given_name: "Kozo",
  family_name: "Nomura",
  path: "nomura-kozo",
  original_name: "野村 浩三",
  birth_name: "Kazuhiro Osao (尾棹 一浩)",
  aliases: "Akiji Nomura (野村明司)",
  dob: [year: 1931, month: 12, day: 22],
  birth_place: "Nerima, Toshima, Tokyo, Japan",
  selected_filmography: [
    roles: [
      [
        role: "Actor",
        films: [
          [film: {"Varan the Unbelievable", 1958}, characters: "Kenji Uozaki"],
          [film: {"The Birth of Japan", 1959}, characters: "Makeri Otomo"],
          [film: {"Battle in Outer Space", 1959}, characters: "Space Jet Pilot"],
          [film: {"The Human Vapor", 1960}, characters: "Reporter Kawasaki"],
          [film: {"The Last War", 1961}, characters: "Tamura's Stock Broker"],
          [film: {"Gorath", 1962}, characters: "Satellite Commander"],
          [film: {"Mothra vs. Godzilla", 1964}, characters: "Soldier"],
          [film: {"Ghidorah, the Three-Headed Monster", 1964}, characters: "Geologist"],
          [film: {"Frankenstein Conquers the World", 1965}, characters: "Reporter"],
          [film: {"War of the Gargantuas", 1966}, characters: "Soldier"]
        ]
      ]
    ]
  ],
  bio: true
]
