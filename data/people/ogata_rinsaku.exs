[
  given_name: "Rinsaku",
  family_name: "Ogata",
  path: "ogata-rinsaku",
  original_name: "緒方 燐作",
  dob: [year: 1925, month: 1, day: 6],
  selected_filmography: [
    roles: [
      [
        role: "Actor",
        films: [
          [film: {"Godzilla, King of the Monsters", 1954}, characters: "Radio Operator"],
          [film: {"Samurai III: Duel at Ganryu Island", 1956}, characters: "Samurai in Retinue"],
          [film: {"Rodan", 1956}, characters: "Goro"],
          [film: {"Throne of Blood", 1957}, characters: "Washizu Soldier"],
          [film: {"The Mysterians", 1957}, characters: "Policeman"],
          [film: {"Varan the Unbelievable", 1958}, characters: "Soldier"],
          [film: {"The Hidden Fortress", 1958}, characters: "Akizuki Guard"],
          [film: {"The Birth of Japan", 1959}, characters: "Yamato Soldier"],
          [film: {"Battle in Outer Space", 1959}, characters: "Astronaut"],
          [
            film: {"The Secret of the Telegian", 1960},
            characters: "Policeman Climbing Power Pole"
          ],
          [film: {"The Human Vapor", 1960}, characters: "Policeman"],
          [film: {"Daredevil in the Castle", 1961}, characters: "Court Samurai"],
          [film: {"Yojimbo", 1961}, characters: "Seibei Underling"],
          [film: {"Mothra", 1961}, characters: "Fighter Pilot"],
          [film: {"Sanjuro", 1962}, characters: "Kikui Samurai"],
          [film: {"Gorath", 1962}, characters: "Spaceship Crew"],
          [film: {"King Kong vs. Godzilla", 1962}, characters: "Soldier"],
          [film: {"Atragon", 1963}, characters: "Military Officer"],
          [film: {"Mothra vs. Godzilla", 1964}, characters: "Islander"],
          [film: {"Dogora, the Space Monster", 1964}, characters: "Soldier"],
          [film: {"Frankenstein Conquers the World", 1965}, characters: "Soldier"],
          [film: {"Monster Zero", 1965}, characters: "Soldier"],
          [film: {"War of the Gargantuas", 1966}, characters: "Soldier"],
          [film: {"Godzilla vs. the Sea Monster", 1966}, characters: "Red Bamboo Soldier"],
          [film: {"King Kong Escapes", 1967}, characters: "Submarine Crew"],
          [film: {"Destroy All Monsters", 1968}, characters: "Military Advisor"],
          [film: {"Latitude Zero", 1969}, characters: "Black Shark Crew"],
          [film: {"Space Amoeba", 1970}, characters: "Islander"]
        ]
      ]
    ]
  ],
  bio: true
]
