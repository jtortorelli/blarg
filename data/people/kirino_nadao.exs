[
  given_name: "Nadao",
  family_name: "Kirino",
  path: "kirino-nadao",
  original_name: "桐野 洋雄",
  dob: [year: 1932, month: 11, day: 24],
  birth_place: "Matsuyama, Ehime, Japan",
  selected_filmography: [
    roles: [
      [
        role: "Actor",
        films: [
          [film: {"The H-Man", 1958}, characters: "Gangster Waiter"],
          [film: {"Varan the Unbelievable", 1958}, characters: "Yutaka Wada"],
          [film: {"The Birth of Japan", 1959}, characters: "Yamato Soldier"],
          [film: {"Battle in Outer Space", 1959}, characters: "Astronaut Okada"],
          [film: {"The Secret of the Telegian", 1960}, characters: "Bodyguard"],
          [film: {"The Last War", 1961}, characters: "Missile Defense Officer"],
          [film: {"Gorath", 1962}, characters: "Lt. Manabe"],
          [film: {"King Kong vs. Godzilla", 1962}, characters: "Soldier"],
          [film: {"Samurai Pirate", 1963}, characters: "Samurai"],
          [film: {"Atragon", 1963}, characters: "Kidnapped Scientist"],
          [film: {"Dogora, the Space Monster", 1964}, characters: "Gangster"],
          [film: {"Frankenstein Conquers the World", 1965}, characters: "Policeman"],
          [film: {"Monster Zero", 1965}, characters: "Soldier"],
          [film: {"War of the Gargantuas", 1966}, characters: "Soldier"],
          [film: {"King Kong Escapes", 1967}, characters: "Who Henchman"],
          [film: {"Destroy All Monsters", 1968}, characters: "Special Police"]
        ]
      ]
    ]
  ],
  bio: true
]
