[
  given_name: "Ichiro",
  family_name: "Arishima",
  path: "arishima-ichiro",
  original_name: "有島 一郎",
  birth_name: "Tadao Oshima (大島 忠雄)",
  dob: [year: 1916, month: 3, day: 1],
  birth_place: "Nagoya, Aichi, Japan",
  dod: [year: 1987, month: 7, day: 20],
  selected_filmography: [
    roles: [
      [
        role: "Actor",
        films: [
          [film: {"The Birth of Japan", 1959}, characters: "Ridouri"],
          [film: {"King Kong vs. Godzilla", 1962}, characters: "Mr. Tako"],
          [film: {"Samurai Pirate", 1963}, characters: "The Wizard of Kume"],
          [film: {"The Adventures of Taklamakan", 1966}, characters: "The Wizard Hermit"]
        ]
      ]
    ]
  ],
  bio: true
]
