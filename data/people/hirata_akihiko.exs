[
  given_name: "Akihiko",
  family_name: "Hirata",
  path: "hirata-akihiko",
  original_name: "平田 昭彦",
  birth_name: "Akihiko Onoda (小野田 昭彦)",
  dob: [year: 1927, month: 12, day: 16],
  birth_place: "Gyeongseong, Korea",
  dod: [year: 1984, month: 7, day: 25],
  selected_filmography: [
    roles: [
      [
        role: "Actor",
        films: [
          [film: {"Samurai I: Musashi Miyamoto", 1954}, characters: "Seijuro Yoshioka"],
          [film: {"Godzilla, King of the Monsters", 1954}, characters: "Dr. Daisuke Serizawa"],
          [film: {"Samurai II: Duel at Ichijoji Temple", 1955}, characters: "Seijuro Yoshioka"],
          [film: {"Rodan", 1956}, characters: "Dr. Kashiwagi"],
          [film: {"The Mysterians", 1957}, characters: "Dr. Ryoichi Shiraishi"],
          [film: {"The H-Man", 1958}, characters: "Detective Tominaga"],
          [film: {"Varan the Unbelievable", 1958}, characters: "Dr. Fujimora"],
          [film: {"The Birth of Japan", 1959}, characters: "Takehiko"],
          [film: {"The Secret of the Telegian", 1960}, characters: "Detective Kobayashi"],
          [film: {"Daredevil in the Castle", 1961}, characters: "Kanesuke Susukida"],
          [film: {"Mothra", 1961}, characters: "Doctor"],
          [film: {"Sanjuro", 1962}, characters: "Samurai Bunji Terada"],
          [film: {"Gorath", 1962}, characters: "Captain Endo"],
          [film: {"King Kong vs. Godzilla", 1962}, characters: "Minister Shigezawa"],
          [film: {"Atragon", 1963}, characters: "Mu Agent No. 23"],
          [film: {"Whirlwind", 1964}, characters: "Matazaemon Soga"],
          [film: {"Ghidorah, the Three-Headed Monster", 1964}, characters: "Chief Okita"],
          [film: {"The Adventures of Taklamakan", 1966}, characters: "The Chamberlain"],
          [film: {"Godzilla vs. the Sea Monster", 1966}, characters: "Red Bamboo Captain Yamoto"],
          [film: {"Son of Godzilla", 1967}, characters: "Fujisaki"],
          [film: {"Latitude Zero", 1969}, characters: "Dr. Sugata"],
          [film: {"Godzilla vs. the Cosmic Monster", 1974}, characters: "Dr. Hideto Miyajima"],
          [film: {"Prophecies of Nostradamus", 1974}, characters: "Environmental Scientist"],
          [film: {"Terror of Mechagodzilla", 1975}, characters: "Dr. Shinzo Mafune"],
          [film: {"The War in Space", 1977}, characters: "Defense Force Commander"],
          [film: {"Sayonara, Jupiter", 1984}, characters: "Dr. Inoue"]
        ]
      ]
    ]
  ],
  bio: true
]
