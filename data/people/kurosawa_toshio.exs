[
  given_name: "Toshio",
  family_name: "Kurosawa",
  path: "kurosawa-toshio",
  original_name: "黒沢 年雄",
  dob: [year: 1944, month: 2, day: 4],
  birth_place: "Nishi, Yokohama, Kanagawa, Japan",
  selected_filmography: [
    roles: [
      [
        role: "Actor",
        films: [
          [film: {"The Adventures of Taklamakan", 1966}, characters: "Osami's Brother"],
          [film: {"Evil of Dracula", 1974}, characters: "Shiraki"],
          [film: {"Prophecies of Nostradamus", 1974}, characters: "Akira Nakagawa"]
        ]
      ]
    ]
  ],
  bio: true
]
