[
  given_name: "Saburo",
  family_name: "Iketani",
  path: "iketani-saburo",
  original_name: "池谷 三郎",
  dob: [year: 1923, month: 8, day: 19],
  birth_place: "Chuo, Tokyo, Japan",
  dod: [year: 2002, month: 10, day: 19],
  selected_filmography: [
    roles: [
      [
        role: "Actor",
        films: [
          [film: {"Godzilla, King of the Monsters", 1954}, characters: "Reporter"],
          [film: {"Rodan", 1956}, characters: "Newsreader"],
          [film: {"Battle in Outer Space", 1959}, characters: "Newsreader"],
          [film: {"Mothra", 1961}, characters: "Announcer"],
          [film: {"The Last War", 1961}, characters: "TV Announcer"],
          [film: {"Gorath", 1962}, characters: "TV Announcer"],
          [film: {"Monster Zero", 1965}, characters: "Radio Announcer"],
          [film: {"Destroy All Monsters", 1968}, characters: "Radio Announcer"]
        ]
      ]
    ]
  ],
  bio: true
]
