[
  given_name: "Keiko",
  family_name: "Kondo",
  path: "kondo-keiko",
  original_name: "近藤 圭子",
  dob: [year: 1943, month: 3, day: 18],
  selected_filmography: [
    roles: [
      [
        role: "Actress",
        films: [
          [film: {"The Invisible Man", 1954}, characters: "Mari"],
          [film: {"Samurai II: Duel at Ichijoji Temple", 1955}, characters: "Wataru Hagerin"]
        ]
      ]
    ]
  ],
  bio: true
]
