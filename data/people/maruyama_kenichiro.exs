[
  given_name: "Kenichiro",
  family_name: "Maruyama",
  path: "maruyama-kenichiro",
  original_name: "丸山 謙一郎",
  dob: [year: 1938, month: 7, day: 12],
  selected_filmography: [
    roles: [
      [
        role: "Actor",
        films: [
          [film: {"Gorath", 1962}, characters: "Spaceship Crew"],
          [film: {"High and Low", 1963}, characters: "Dining Car Waiter"],
          [film: {"Atragon", 1963}, characters: "Cargo Ship Crew"],
          [film: {"Mothra vs. Godzilla", 1964}, characters: "Islander"],
          [film: {"Godzilla vs. the Sea Monster", 1966}, characters: "Reporter"],
          [film: {"Son of Godzilla", 1967}, characters: "Ozawa"],
          [film: {"Destroy All Monsters", 1968}, characters: "Moon Base Tech"]
        ]
      ]
    ]
  ],
  bio: true
]
