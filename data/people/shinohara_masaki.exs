[
  given_name: "Masaki",
  family_name: "Shinohara",
  path: "shinohara-masaki",
  original_name: "篠原 正記",
  dob: [year: 1926, month: 12, day: 28],
  dod: [year: 2018, month: 11, day: 1],
  selected_filmography: [
    roles: [
      [
        role: "Actor",
        films: [
          [film: {"Seven Samurai", 1954}, characters: "Peasant"],
          [film: {"Varan the Unbelievable", 1958}, characters: "Fisherman"],
          [film: {"The Hidden Fortress", 1958}, characters: "Yamana Soldier"],
          [film: {"The Birth of Japan", 1959}, characters: "Otomo Soldier"],
          [film: {"The Secret of the Telegian", 1960}, characters: "Truck Driver"],
          [film: {"Daredevil in the Castle", 1961}, characters: "Villager"],
          [film: {"The Last War", 1961}, characters: "Defense Crew"],
          [film: {"Sanjuro", 1962}, characters: "Samurai Reading Notice"],
          [film: {"High and Low", 1963}, characters: "Farmer"],
          [film: {"Matango", 1963}, characters: "Mushroom Man"],
          [film: {"Samurai Pirate", 1963}, characters: "Rebel"],
          [film: {"Atragon", 1963}, characters: "Mihara Tourist"],
          [
            film: {"Mothra vs. Godzilla", 1964},
            characters: ["Reporter", "Islander", "Policeman"]
          ],
          [film: {"Dogora, the Space Monster", 1964}, characters: "Detective"],
          [film: {"Monster Zero", 1965}, characters: "Rodan"],
          [
            film: {"The Adventures of Taklamakan", 1966},
            characters: ["Merchant", "Palace Guard"]
          ],
          [film: {"King Kong Escapes", 1967}, characters: "Who Henchman"],
          [film: {"Destroy All Monsters", 1968}, characters: "Military Advisor"],
          [film: {"Latitude Zero", 1969}, characters: "Black Shark Crew"],
          [film: {"Godzilla's Revenge", 1969}, characters: "Policeman"],
          [film: {"Space Amoeba", 1970}, characters: "Islander"],
          [film: {"Godzilla vs. Hedorah", 1971}, characters: ["Mahjong Player", "Soldier"]]
        ]
      ]
    ]
  ],
  bio: true
]
