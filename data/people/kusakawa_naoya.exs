[
  given_name: "Naoya",
  family_name: "Kusakawa",
  path: "kusakawa-naoya",
  original_name: "草川 直也",
  dob: [year: 1929, month: 6, day: 11],
  birth_place: "Manchuria",
  selected_filmography: [
    roles: [
      [
        role: "Actor",
        films: [
          [film: {"Yojimbo", 1961}, characters: "Seibei Underling"],
          [film: {"The Last War", 1961}, characters: "Helicopter Crew"],
          [film: {"King Kong vs. Godzilla", 1962}, characters: "Reporter"],
          [film: {"The Adventures of Taklamakan", 1966}, characters: "Palace Guard"],
          [film: {"King Kong Escapes", 1967}, characters: "Who Henchman"],
          [film: {"Destroy All Monsters", 1968}, characters: "Special Police"],
          [film: {"Godzilla vs. Gigan", 1972}, characters: "Policeman"]
        ]
      ]
    ]
  ],
  bio: true
]
