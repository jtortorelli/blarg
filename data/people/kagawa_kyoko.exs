[
  given_name: "Kyoko",
  family_name: "Kagawa",
  path: "kagawa-kyoko",
  original_name: "香川 京子",
  birth_name: "Kyoko Makino (牧野 香子)",
  dob: [
    year: 1931,
    month: 12,
    day: 5
  ],
  birth_place: "Aso, Namekata, Ibaraki, Japan",
  selected_filmography: [
    roles: [
      [
        role: "Actress",
        films: [
          [film: {"The Birth of Japan", 1959}, characters: "Princess Miyazu"],
          [film: {"Daredevil in the Castle", 1961}, characters: "Ai"],
          [film: {"Mothra", 1961}, characters: "Michi Hanamura"],
          [film: {"High and Low", 1963}, characters: "Reiko Gondo"],
          [film: {"Ballad", 2009}, characters: "Yoshino"]
        ]
      ]
    ]
  ],
  bio: true
]
