[
  given_name: "Katsuhiro",
  family_name: "Onoe",
  path: "onoe-katsuhiro",
  original_name: "尾上 克郎",
  dob: [year: 1960, month: 6, day: 4],
  birth_place: "Kagoshima, Japan",
  selected_filmography: [
    roles: [
      [
        role: "Assistant Director",
        films: [
          [film: {"Lorelei", 2005}]
        ]
      ],
      [
        role: "Co-Director",
        films: [
          [film: {"The Sinking of Japan", 2006}],
          [film: {"Shin Godzilla", 2016}]
        ]
      ],
      [
        role: "Second Unit Director",
        films: [
          [film: {"Hidden Fortress: The Last Princess", 2008}]
        ]
      ],
      [
        role: "Physical FX",
        films: [
          [film: {"Gamera 3: Revenge of Iris", 1999}]
        ]
      ],
      [
        role: "Physical FX Assistant",
        films: [
          [film: {"Godzilla 2000", 1999}],
          [film: {"Godzilla X Megaguirus", 2000}]
        ]
      ],
      [
        role: "SFX Assistant Director",
        films: [
          [film: {"Lorelei", 2005}]
        ]
      ],
      [
        role: "SFX Director",
        films: [
          [film: {"Onmyoji", 2001}],
          [film: {"Returner", 2002}],
          [film: {"Onmyoji II", 2003}],
          [film: {"Samurai Commando: Mission 1549", 2005}],
          [film: {"Attack on Titan", 2015}],
          [film: {"Attack on Titan: End of the World", 2015}]
        ]
      ],
      [
        role: "SFX Supervisor",
        films: [
          [film: {"Sakuya, Slayer of Demons", 2000}],
          [film: {"The Sinking of Japan", 2006}],
          [film: {"Shin Godzilla", 2016}]
        ]
      ]
    ]
  ],
  bio: true
]
