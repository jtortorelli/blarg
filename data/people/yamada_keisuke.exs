[
  given_name: "Keisuke",
  family_name: "Yamada",
  path: "yamada-keisuke",
  original_name: "山田 圭介",
  selected_filmography: [
    roles: [
      [
        role: "Actor",
        films: [
          [film: {"Varan the Unbelievable", 1958}, characters: "Soldier"],
          [film: {"Battle in Outer Space", 1959}, characters: "UN Official"],
          [film: {"The Human Vapor", 1960}, characters: "Police Executive"],
          [film: {"Gorath", 1962}, characters: "Minister"],
          [film: {"King Kong vs. Godzilla", 1962}, characters: "Military Official"],
          [film: {"Matango", 1963}, characters: "Doctor"],
          [film: {"Atragon", 1963}, characters: "Military Officer"],
          [film: {"Mothra vs. Godzilla", 1964}, characters: "Policeman"],
          [film: {"Dogora, the Space Monster", 1964}, characters: "Soldier"],
          [film: {"Ghidorah, the Three-Headed Monster", 1964}, characters: "Prime Minister"],
          [film: {"Monster Zero", 1965}, characters: "Minister"],
          [film: {"War of the Gargantuas", 1966}, characters: "Military Advisor"]
        ]
      ]
    ]
  ],
  bio: true
]
