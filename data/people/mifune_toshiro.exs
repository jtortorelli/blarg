[
  given_name: "Toshiro",
  family_name: "Mifune",
  path: "mifune-toshiro",
  original_name: "三船 敏郎",
  dob: [year: 1920, month: 4, day: 1],
  birth_place: "Qingdao, China",
  dod: [year: 1997, month: 12, day: 24],
  death_place: "Mitaka, Tokyo, Japan",
  selected_filmography: [
    roles: [
      [
        role: "Actor",
        films: [
          [film: {"Rashomon", 1950}, characters: "Tajomaru the Bandit"],
          [film: {"Seven Samurai", 1954}, characters: "Kikuchiyo"],
          [film: {"Samurai I: Musashi Miyamoto", 1954}, characters: "Musashi Miyamoto"],
          [film: {"Samurai II: Duel at Ichijoji Temple", 1955}, characters: "Musashi Miyamoto"],
          [film: {"Samurai III: Duel at Ganryu Island", 1956}, characters: "Musashi Miyamoto"],
          [film: {"Throne of Blood", 1957}, characters: "Taketoki Washizu"],
          [film: {"The Hidden Fortress", 1958}, characters: "Rokurota Makabe"],
          [film: {"The Birth of Japan", 1959}, characters: ["Prince Ousu", "Susano-o"]],
          [film: {"Daredevil in the Castle", 1961}, characters: "Mohei"],
          [film: {"Yojimbo", 1961}, characters: "Sanjuro Kuwabatake"],
          [film: {"Sanjuro", 1962}, characters: "Sanjuro Tsubaki"],
          [film: {"High and Low", 1963}, characters: "Kingo Gondo"],
          [film: {"Samurai Pirate", 1963}, characters: "Sukeza"],
          [film: {"Whirlwind", 1964}, characters: "Morishige Akashi"],
          [film: {"The Adventures of Taklamakan", 1966}, characters: "Osami"],
          [film: {"Zatoichi Meets Yojimbo", 1970}, characters: "Sasa"]
        ]
      ]
    ]
  ],
  bio: true
]
