[
  given_name: "Jerry",
  family_name: "Ito",
  path: "ito-jerry",
  birth_name: "Gerald Takemichi Ito",
  japanese_name: "ジェリー伊藤",
  dob: [year: 1927, month: 7, day: 12],
  birth_place: "New York, United States",
  dod: [year: 2007, month: 7, day: 8],
  death_place: "Los Angeles, California, United States",
  selected_filmography: [
    roles: [
      [
        role: "Actor",
        films: [
          [film: {"Mothra", 1961}, characters: "Clark Nelson"],
          [film: {"The Last War", 1961}, characters: "Watkins"],
          [film: {"Message from Space", 1978}, characters: "Earth Federation Commander"]
        ]
      ]
    ]
  ],
  bio: true
]
