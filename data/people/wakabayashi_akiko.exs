[
  given_name: "Akiko",
  family_name: "Wakabayashi",
  path: "wakabayashi-akiko",
  original_name: "若林 映子",
  dob: [year: 1939, month: 12, day: 13],
  birth_place: "Kitasen, Ota, Tokyo, Japan",
  selected_filmography: [
    roles: [
      [
        role: "Actress",
        films: [
          [film: {"King Kong vs. Godzilla", 1962}, characters: "Tamiye"],
          [film: {"Samurai Pirate", 1963}, characters: "Yaya's Handmaiden"],
          [film: {"Dogora, the Space Monster", 1964}, characters: "Hamako"],
          [film: {"Ghidorah, the Three-Headed Monster", 1964}, characters: "Princess Salno"],
          [film: {"The Adventures of Taklamakan", 1966}, characters: "Spriya"]
        ]
      ]
    ]
  ],
  bio: true
]
