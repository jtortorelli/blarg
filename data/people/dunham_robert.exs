[
  given_name: "Robert",
  family_name: "Dunham",
  path: "dunham-robert",
  japanese_name: "ロバート・ダンハム",
  aliases: "Dan Yuma (ダン・ユマ)",
  dob: [year: 1931, month: 7, day: 6],
  birth_place: "Portland, Maine, United States",
  dod: [year: 2001, month: 8, day: 6],
  death_place: "Sarasota, Florida, United States",
  selected_filmography: [
    roles: [
      [
        role: "Actor",
        films: [
          [film: {"Mothra", 1961}, characters: "Rolisican Policeman"],
          [film: {"Dogora, the Space Monster", 1964}, characters: "Mark Jackson"],
          [film: {"Godzilla vs. Megalon", 1973}, characters: "Emperor Antonio"],
          [film: {"Espy", 1974}, characters: "Pilot"]
        ]
      ]
    ]
  ],
  bio: true
]
