[
  given_name: "Takeshi",
  family_name: "Kimura",
  path: "kimura-takeshi",
  original_name: "木村 武",
  aliases: "Kaoru Mabuchi (馬淵 薫)",
  dob: [year: 1911, month: 2, day: 4],
  birth_place: "Osaka, Japan",
  dod: [year: 1987, month: 5, day: 3],
  selected_filmography: [
    roles: [
      [
        role: "Screenwriter",
        films: [
          [film: {"Rodan", 1956}],
          [film: {"The Mysterians", 1957}],
          [film: {"The Human Vapor", 1960}],
          [film: {"Daredevil in the Castle", 1961}],
          [film: {"The Last War", 1961}],
          [film: {"Gorath", 1962}],
          [film: {"Matango", 1963}],
          [film: {"Samurai Pirate", 1963}],
          [film: {"Whirlwind", 1964}],
          [film: {"Frankenstein Conquers the World", 1965}],
          [film: {"The Adventures of Taklamakan", 1966}],
          [film: {"War of the Gargantuas", 1966}],
          [film: {"King Kong Escapes", 1967}],
          [film: {"Destroy All Monsters", 1968}],
          [film: {"Godzilla vs. Hedorah", 1971}]
        ]
      ]
    ]
  ],
  bio: true
]
