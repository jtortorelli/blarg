[
  given_name: "Senkichi",
  family_name: "Omura",
  path: "omura-senkichi",
  original_name: "大村 千吉",
  dob: [year: 1922, month: 4, day: 27],
  birth_place: "Fukagawa, Tokyo, Japan",
  dod: [year: 1991, month: 11, day: 24],
  selected_filmography: [
    roles: [
      [
        role: "Actor",
        films: [
          [film: {"Seven Samurai", 1954}, characters: "Fleeing Bandit"],
          [film: {"Godzilla Raids Again", 1955}, characters: "Escaped Convict"],
          [film: {"Samurai III: Duel at Ganryu Island", 1956}, characters: "Gambler"],
          [film: {"Throne of Blood", 1957}, characters: "Washizu Soldier"],
          [film: {"The Mysterians", 1957}, characters: "Villager"],
          [film: {"The H-Man", 1958}, characters: "Fisherman"],
          [film: {"The Hidden Fortress", 1958}, characters: "Akizuki Soldier"],
          [film: {"The Birth of Japan", 1959}, characters: "Yamato Villager"],
          [film: {"The Secret of the Telegian", 1960}, characters: "Islander"],
          [film: {"Yojimbo", 1961}, characters: "Corrupt Official"],
          [film: {"King Kong vs. Godzilla", 1962}, characters: "Guide"],
          [film: {"High and Low", 1963}, characters: "Hospital Outpatient"],
          [film: {"Mothra vs. Godzilla", 1964}, characters: "Fisherman"],
          [film: {"Ghidorah, the Three-Headed Monster", 1964}, characters: "Mt. Aso Tourist"],
          [film: {"Frankenstein Conquers the World", 1965}, characters: "TV Cameraman"],
          [film: {"Kagemusha", 1980}, characters: "Takeda Stableboy"],
          [film: {"Zatoichi", 1989}, characters: "Prisoner"]
        ]
      ]
    ]
  ],
  bio: true
]
