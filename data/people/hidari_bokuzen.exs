[
  given_name: "Bokuzen",
  family_name: "Hidari",
  path: "hidari-bokuzen",
  original_name: "左 卜全",
  birth_name: "Ichiro Mikashima (三ヶ島 一郎)",
  dob: [year: 1894, month: 2, day: 20],
  birth_place: "Kitano, Kotesashi, Iruma, Saitama, Japan",
  dod: [year: 1971, month: 5, day: 26],
  selected_filmography: [
    roles: [
      [
        role: "Actor",
        films: [
          [film: {"Seven Samurai", 1954}, characters: "Yohei"],
          [film: {"The Birth of Japan", 1959}, characters: "Deity"],
          [film: {"The Human Vapor", 1960}, characters: "Jiya"],
          [film: {"Zatoichi's Flashing Sword", 1964}, characters: "Kyubei"],
          [film: {"Gamera, the Giant Monster", 1965}, characters: "Drunk Old Man"]
        ]
      ]
    ]
  ],
  bio: true
]
