[
  given_name: "Mie",
  family_name: "Hama",
  path: "hama-mie",
  original_name: "浜 美枝",
  dob: [year: 1943, month: 11, day: 20],
  birth_place: "Tokyo, Japan",
  selected_filmography: [
    roles: [
      [
        role: "Actress",
        films: [
          [film: {"King Kong vs. Godzilla", 1962}, characters: "Fumiko Sakurai"],
          [film: {"Samurai Pirate", 1963}, characters: "Princess Yaya"],
          [film: {"The Adventures of Taklamakan", 1966}, characters: "Kureya"],
          [film: {"King Kong Escapes", 1967}, characters: "Madame Piranha"]
        ]
      ]
    ]
  ],
  bio: true
]
