[
  given_name: "Haruo",
  family_name: "Nakajima",
  path: "nakajima-haruo",
  original_name: "中島 春雄",
  dob: [year: 1929, month: 1, day: 1],
  birth_place: "Sakata, Yamagata, Japan",
  dod: [year: 2017, month: 8, day: 7],
  selected_filmography: [
    roles: [
      [
        role: "Actor",
        films: [
          [film: {"Seven Samurai", 1954}, characters: "Bandit Scout"],
          [
            film: {"Godzilla, King of the Monsters", 1954},
            characters: ["Godzilla", "Newspaper Reporter"]
          ],
          [film: {"The Invisible Man", 1954}, characters: "Suicidal Invisible Man"],
          [film: {"Godzilla Raids Again", 1955}, characters: "Godzilla"],
          [film: {"Samurai II: Duel at Ichijoji Temple", 1955}, characters: "Litter Bearer"],
          [film: {"Rodan", 1956}, characters: ["Rodan", "Soldier"]],
          [film: {"The Mysterians", 1957}, characters: ["Mogera", "Soldier"]],
          [film: {"The H-Man", 1958}, characters: "Fisherman"],
          [film: {"Varan the Unbelievable", 1958}, characters: "Varan"],
          [film: {"The Hidden Fortress", 1958}, characters: "Akizuki Soldier"],
          [film: {"The Secret of the Telegian", 1960}, characters: "Island Policeman"],
          [film: {"The Human Vapor", 1960}, characters: "Man at Bank"],
          [film: {"Mothra", 1961}, characters: "Mothra"],
          [film: {"Sanjuro", 1962}, characters: "Litter Bearer"],
          [film: {"Gorath", 1962}, characters: ["Policeman", "Magma"]],
          [film: {"King Kong vs. Godzilla", 1962}, characters: "Godzilla"],
          [film: {"Matango", 1963}, characters: "Mushroom Man"],
          [film: {"Atragon", 1963}, characters: "Military Officer"],
          [film: {"Mothra vs. Godzilla", 1964}, characters: "Godzilla"],
          [film: {"Dogora, the Space Monster", 1964}, characters: "Witness at Coal Plant"],
          [film: {"Ghidorah, the Three-Headed Monster", 1964}, characters: "Godzilla"],
          [film: {"Frankenstein Conquers the World", 1965}, characters: "Baragon"],
          [film: {"Monster Zero", 1965}, characters: "Godzilla"],
          [film: {"War of the Gargantuas", 1966}, characters: "Gaira"],
          [film: {"Godzilla vs. the Sea Monster", 1966}, characters: "Godzilla"],
          [film: {"King Kong Escapes", 1967}, characters: ["King Kong", "Bystander"]],
          [film: {"Son of Godzilla", 1967}, characters: "Godzilla"],
          [film: {"Destroy All Monsters", 1968}, characters: ["Godzilla", "Military Advisor"]],
          [film: {"Latitude Zero", 1969}, characters: ["Giant Bat", "Giant Rat", "Griffon"]],
          [film: {"Godzilla's Revenge", 1969}, characters: "Godzilla"],
          [film: {"Space Amoeba", 1970}, characters: ["Gezora", "Ganime"]],
          [film: {"Godzilla vs. Hedorah", 1971}, characters: ["Godzilla", "Talking Head"]],
          [film: {"Godzilla vs. Gigan", 1972}, characters: "Godzilla"]
        ]
      ]
    ]
  ],
  bio: true
]
