[
  given_name: "Koreya",
  family_name: "Senda",
  path: "senda-koreya",
  original_name: "千田 是也",
  birth_name: "Kunio Ito (伊藤 圀夫)",
  dob: [year: 1904, month: 7, day: 15],
  birth_place: "Tokyo, Japan",
  dod: [year: 1994, month: 12, day: 21],
  death_place: "Minato, Tokyo, Japan",
  selected_filmography: [
    roles: [
      [
        role: "Actor",
        films: [
          [film: {"The H-Man", 1958}, characters: "Dr. Maki"],
          [film: {"Varan the Unbelievable", 1958}, characters: "Dr. Sugimoto"],
          [film: {"Battle in Outer Space", 1959}, characters: "Dr. Adachi"]
        ]
      ]
    ]
  ],
  bio: true
]
