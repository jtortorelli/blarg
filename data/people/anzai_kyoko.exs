[
  given_name: "Kyoko",
  family_name: "Anzai",
  path: "anzai-kyoko",
  original_name: "安西 郷子",
  dob: [year: 1934, month: 9, day: 27],
  birth_place: "Osaka, Japan",
  dod: [year: 2002, month: 12, day: 28],
  death_place: "Shibuya, Tokyo, Japan",
  selected_filmography: [
    roles: [
      [
        role: "Actress",
        films: [
          [film: {"Battle in Outer Space", 1959}, characters: "Etsuko Shiraishi"]
        ]
      ]
    ]
  ],
  bio: true
]
