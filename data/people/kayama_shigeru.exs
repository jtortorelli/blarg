[
  given_name: "Shigeru",
  family_name: "Kayama",
  path: "kayama-shigeru",
  original_name: "香山 滋",
  birth_name: "Koji Yamada (山田 鉀治)",
  dob: [year: 1904, month: 7, day: 1],
  birth_place: "Tokyo, Japan",
  dod: [year: 1975, month: 2, day: 7],
  selected_filmography: [
    roles: [
      [
        role: "Writer",
        films: [
          [film: {"Godzilla, King of the Monsters", 1954}],
          [film: {"Godzilla Raids Again", 1955}]
        ]
      ],
      [
        role: "Screenwriter",
        films: [
          [film: {"The Mysterians", 1957}]
        ]
      ]
    ]
  ],
  bio: true
]
