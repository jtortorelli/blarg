[
  given_name: "Yoshio",
  family_name: "Katsube",
  path: "katsube-yoshio",
  original_name: "勝部 義夫",
  dob: [year: 1934, month: 3, day: 19],
  birth_place: "Ota, Shimane, Japan",
  selected_filmography: [
    roles: [
      [
        role: "Actor",
        films: [
          [film: {"Samurai II: Duel at Ichijoji Temple", 1955}, characters: "Passerby"],
          [film: {"The Mysterians", 1957}, characters: "Reporter"],
          [film: {"The H-Man", 1958}, characters: "Reporter"],
          [film: {"Varan the Unbelievable", 1958}, characters: "Reporter"],
          [film: {"The Hidden Fortress", 1958}, characters: "Yamana Soldier"],
          [film: {"Battle in Outer Space", 1959}, characters: "Attendant"],
          [film: {"The Secret of the Telegian", 1960}, characters: "Policeman"],
          [film: {"The Human Vapor", 1960}, characters: "Reporter"],
          [film: {"Daredevil in the Castle", 1961}, characters: "Toyotomi Soldier"],
          [film: {"Mothra", 1961}, characters: "Expedition Member"],
          [film: {"The Last War", 1961}, characters: "Reporter"],
          [film: {"Gorath", 1962}, characters: ["Reporter", "Satellite Crew"]],
          [film: {"Samurai Pirate", 1963}, characters: "Villager"],
          [film: {"Atragon", 1963}, characters: "Atragon Crew"],
          [film: {"Mothra vs. Godzilla", 1964}, characters: "Fisherman"],
          [film: {"Dogora, the Space Monster", 1964}, characters: "Satellite Technician"],
          [film: {"Ghidorah, the Three-Headed Monster", 1964}, characters: "Reporter"],
          [film: {"Frankenstein Conquers the World", 1965}, characters: "Bystander"],
          [film: {"Monster Zero", 1965}, characters: "Xian"],
          [film: {"The Adventures of Taklamakan", 1966}, characters: "Pesil Villager"],
          [film: {"War of the Gargantuas", 1966}, characters: "Reporter"],
          [
            film: {"Godzilla vs. the Sea Monster", 1966},
            characters: ["Captive Islander", "Red Bamboo Soldier"]
          ],
          [film: {"King Kong Escapes", 1967}, characters: "Who Henchman"],
          [film: {"Destroy All Monsters", 1968}, characters: "UNSC Tech"],
          [film: {"Latitude Zero", 1969}, characters: "Observer"],
          [film: {"Godzilla's Revenge", 1969}, characters: "Reporter"],
          [film: {"Lake of Dracula", 1971}, characters: "Coffee Shop Patron"],
          [film: {"Godzilla vs. Hedorah", 1971}, characters: "Soldier"],
          [film: {"Espy", 1974}, characters: "Man in Garage"],
          [film: {"The Explosion", 1975}, characters: "TV Location Staff"]
        ]
      ]
    ]
  ],
  bio: true
]
