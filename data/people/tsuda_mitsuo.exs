[
  given_name: "Mitsuo",
  family_name: "Tsuda",
  path: "tsuda-mitsuo",
  original_name: "津田 光男",
  dob: [year: 1910, month: 9, day: 13],
  birth_place: "Fukushima, Japan",
  dod: [unknown: true],
  selected_filmography: [
    roles: [
      [
        role: "Actor",
        films: [
          [film: {"Godzilla, King of the Monsters", 1954}, characters: "Policeman"],
          [film: {"The Invisible Man", 1954}, characters: "Nightclub Patron"],
          [
            film: {"Samurai II: Duel at Ichijoji Temple", 1955},
            characters: "Yoshioka School Samurai"
          ],
          [film: {"Samurai III: Duel at Ganryu Island", 1956}, characters: "Gambler"],
          [film: {"Rodan", 1956}, characters: "Air Force Officer"],
          [film: {"The Mysterians", 1957}, characters: "Parliamentarian"],
          [film: {"The H-Man", 1958}, characters: "Police Executive"],
          [film: {"Varan the Unbelievable", 1958}, characters: "Soldier"],
          [film: {"The Birth of Japan", 1959}, characters: "Yamato Soldier"],
          [film: {"Battle in Outer Space", 1959}, characters: "Military Officer"],
          [film: {"Yojimbo", 1961}, characters: "Ushitora Underling"],
          [film: {"Mothra", 1961}, characters: "Cruise Ship Captain"],
          [film: {"King Kong vs. Godzilla", 1962}, characters: "Military Official"],
          [film: {"Atragon", 1963}, characters: "Military Officer"],
          [film: {"Mothra vs. Godzilla", 1964}, characters: "Soldier"],
          [film: {"Dogora, the Space Monster", 1964}, characters: "Soldier"],
          [film: {"Ghidorah, the Three-Headed Monster", 1964}, characters: "Parliamentarian"],
          [film: {"Frankenstein Conquers the World", 1965}, characters: "Miner"],
          [film: {"Monster Zero", 1965}, characters: "Soldier"],
          [film: {"War of the Gargantuas", 1966}, characters: "Soldier"],
          [film: {"The Submersion of Japan", 1973}, characters: "Emergency HQ Official"]
        ]
      ]
    ]
  ],
  bio: true
]
