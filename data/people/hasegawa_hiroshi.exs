[
  given_name: "Hiroshi",
  family_name: "Hasegawa",
  path: "hasegawa-hiroshi",
  original_name: "長谷川 弘",
  dob: [year: 1928, month: 1, day: 3],
  birth_place: "Tokyo, Japan",
  selected_filmography: [
    roles: [
      [
        role: "Actor",
        films: [
          [film: {"Samurai Pirate", 1963}, characters: "Palace Guard"],
          [film: {"Atragon", 1963}, characters: "Atragon Crew"],
          [film: {"The Adventures of Taklamakan", 1966}, characters: "Palace Guard"]
        ]
      ]
    ]
  ],
  bio: true
]
