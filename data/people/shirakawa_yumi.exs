[
  given_name: "Yumi",
  family_name: "Shirakawa",
  path: "shirakawa-yumi",
  original_name: "白川 由美",
  birth_name: "Akiko Yamazaki (山崎 安基子)",
  dob: [year: 1936, month: 10, day: 21],
  birth_place: "Shinagawa, Tokyo, Japan",
  dod: [year: 2016, month: 6, day: 14],
  death_place: "Tokyo, Japan",
  selected_filmography: [
    roles: [
      [
        role: "Actress",
        films: [
          [film: {"Rodan", 1956}, characters: "Kiyo"],
          [film: {"The Mysterians", 1957}, characters: "Etsuko Shiraishi"],
          [film: {"The H-Man", 1958}, characters: "Chikako Arai"],
          [film: {"The Secret of the Telegian", 1960}, characters: "Akiko Chujo"],
          [film: {"The Last War", 1961}, characters: "Sanae Ebara"],
          [film: {"Gorath", 1962}, characters: "Tomoko Sonoda"],
          [film: {"The Adventures of Taklamakan", 1966}, characters: "The Queen"]
        ]
      ]
    ]
  ],
  bio: true
]
