[
  given_name: "Akira",
  family_name: "Tani",
  path: "tani-akira",
  original_name: "谷 晃",
  dob: [year: 1910, month: 9, day: 22],
  birth_place: "Osaka, Japan",
  dod: [year: 1966, month: 8, day: 11],
  death_place: "Satoshihigashi, Komae, Kitatama, Tokyo, Japan",
  selected_filmography: [
    roles: [
      [
        role: "Actor",
        films: [
          [film: {"Seven Samurai", 1954}, characters: "Bandit Scout"],
          [film: {"Samurai I: Musashi Miyamoto", 1954}, characters: "Gonroku Kawarano"],
          [film: {"Samurai II: Duel at Ichijoji Temple", 1955}, characters: "Gonroku Kawarano"],
          [film: {"Throne of Blood", 1957}, characters: "Washizu Soldier"],
          [film: {"The Hidden Fortress", 1958}, characters: "Kidnapped Yamana Soldier"],
          [film: {"The Birth of Japan", 1959}, characters: "Deity"],
          [film: {"Daredevil in the Castle", 1961}, characters: "Rice Merchant"],
          [film: {"Yojimbo", 1961}, characters: "Ushitora Underling"],
          [film: {"Whirlwind", 1964}, characters: "Shichibei"],
          [film: {"Mothra vs. Godzilla", 1964}, characters: "Chief Fisherman"]
        ]
      ]
    ]
  ],
  bio: true
]
