[
  given_name: "Minoru",
  family_name: "Chiaki",
  path: "chiaki-minoru",
  original_name: "千秋 実",
  birth_name: "Katsuji Sasaki (佐々木 勝治)",
  dob: [year: 1917, month: 4, day: 28],
  birth_place: "Onnenai, Nakagawa, Hokkaido, Japan",
  dod: [year: 1999, month: 11, day: 1],
  death_place: "Fuchu, Tokyo, Japan",
  selected_filmography: [
    roles: [
      [
        role: "Actor",
        films: [
          [film: {"Rashomon", 1950}, characters: "The Priest"],
          [film: {"Seven Samurai", 1954}, characters: "Heihachi Hayashida"],
          [film: {"Godzilla Raids Again", 1955}, characters: "Kobayashi"],
          [film: {"Samurai III: Duel at Ganryu Island", 1956}, characters: "Sasuke the Boatman"],
          [film: {"Throne of Blood", 1957}, characters: "Yoshiaki Miki"],
          [film: {"The Hidden Fortress", 1958}, characters: "Taihei"],
          [film: {"High and Low", 1963}, characters: "Reporter"]
        ]
      ]
    ]
  ],
  bio: true
]
