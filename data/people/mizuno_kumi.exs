[
  given_name: "Kumi",
  family_name: "Mizuno",
  path: "mizuno-kumi",
  original_name: "水野 久美",
  birth_name: "Maya Igarashi (五十嵐 麻耶)",
  dob: [year: 1937, month: 1, day: 1],
  birth_place: "Sanjo, Niigata, Japan",
  selected_filmography: [
    roles: [
      [
        role: "Actress",
        films: [
          [film: {"The Birth of Japan", 1959}, characters: "Azami"],
          [film: {"Gorath", 1962}, characters: "Takiko Nomura"],
          [film: {"Matango", 1963}, characters: "Mami Sekiguchi"],
          [film: {"Samurai Pirate", 1963}, characters: "Miwa"],
          [film: {"Whirlwind", 1964}, characters: "Orie"],
          [film: {"Frankenstein Conquers the World", 1965}, characters: "Dr. Sueko Togami"],
          [film: {"Monster Zero", 1965}, characters: "Namikawa"],
          [film: {"War of the Gargantuas", 1966}, characters: "Akemi"],
          [film: {"Godzilla vs. the Sea Monster", 1966}, characters: "Daiyo"],
          [film: {"Godzilla X Mechagodzilla", 2002}, characters: "Minister Machiko Tsuge"],
          [film: {"Godzilla: Final Wars", 2004}, characters: "EDF Commander Reiko Namikawa"]
        ]
      ]
    ]
  ],
  bio: true
]
