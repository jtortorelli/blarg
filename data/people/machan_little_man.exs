[
  stage_name: "Little Man Machan",
  path: "machan-little-man",
  original_name: "小人のマーチャン",
  birth_name: "Masao Fukasawa (深沢 政雄)",
  dob: [year: 1921],
  dod: [year: 2000],
  selected_filmography: [
    roles: [
      [
        role: "Actor",
        films: [
          [film: {"Samurai Pirate", 1963}, characters: "Dwarf Servant"],
          [film: {"Son of Godzilla", 1967}, characters: "Minya"],
          [film: {"Destroy All Monsters", 1968}, characters: "Minya"],
          [film: {"Godzilla's Revenge", 1969}, characters: "Minya"]
        ]
      ]
    ]
  ],
  bio: true
]
