[
  given_name: "Yoshio",
  family_name: "Tsuchiya",
  path: "tsuchiya-yoshio",
  original_name: "土屋 嘉男",
  dob: [year: 1927, month: 5, day: 18],
  birth_place: "Yamanashi, Japan",
  dod: [year: 2017, month: 2, day: 2],
  selected_filmography: [
    roles: [
      [
        role: "Actor",
        films: [
          [film: {"Seven Samurai", 1954}, characters: "Rikichi"],
          [film: {"The Invisible Man", 1954}, characters: "Komatsu"],
          [film: {"Godzilla Raids Again", 1955}, characters: "Tajima"],
          [film: {"Throne of Blood", 1957}, characters: "Washizu Servant"],
          [film: {"The Mysterians", 1957}, characters: "Grand Mysterian"],
          [film: {"The H-Man", 1958}, characters: "Detective Taguchi"],
          [film: {"Varan the Unbelievable", 1958}, characters: "Officer Katsumoto"],
          [film: {"The Hidden Fortress", 1958}, characters: "Hayakawa Cavalry"],
          [film: {"Battle in Outer Space", 1959}, characters: "Astronaut Iwamura"],
          [film: {"The Secret of the Telegian", 1960}, characters: "Detective Ozaki"],
          [film: {"The Human Vapor", 1960}, characters: "Mizuno"],
          [film: {"Yojimbo", 1961}, characters: "Kodaira"],
          [film: {"Sanjuro", 1962}, characters: "Samurai Shunpei Hirose"],
          [film: {"High and Low", 1963}, characters: "Detective Murata"],
          [film: {"Matango", 1963}, characters: "Masafumi Kasai"],
          [film: {"Frankenstein Conquers the World", 1965}, characters: "Kawai"],
          [film: {"Monster Zero", 1965}, characters: "Controller of Planet X"],
          [film: {"Son of Godzilla", 1967}, characters: "Furukawa"],
          [film: {"Destroy All Monsters", 1968}, characters: "Dr. Otani"],
          [film: {"Space Amoeba", 1970}, characters: "Dr. Kyoichi Miya"],
          [film: {"Zatoichi's Conspiracy", 1973}, characters: "Shobei"],
          [film: {"Godzilla VS King Ghidorah", 1991}, characters: "Yasuaki Shindo"]
        ]
      ]
    ]
  ],
  bio: true
]
