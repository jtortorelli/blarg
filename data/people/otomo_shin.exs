[
  given_name: "Shin",
  family_name: "Otomo",
  path: "otomo-shin",
  original_name: "大友 伸",
  dob: [year: 1919, month: 4, day: 13],
  birth_place: "Akita, Japan",
  dod: [unknown: true],
  selected_filmography: [
    roles: [
      [
        role: "Actor",
        films: [
          [film: {"Seven Samurai", 1954}, characters: "Bandit Second-in-Command"],
          [film: {"The Invisible Man", 1954}, characters: "Police Chief"],
          [film: {"Godzilla Raids Again", 1955}, characters: "Escaped Convict"],
          [film: {"Throne of Blood", 1957}, characters: "Warlord"],
          [film: {"The Mysterians", 1957}, characters: "Policeman"],
          [film: {"The H-Man", 1958}, characters: "Gangster"],
          [film: {"The Hidden Fortress", 1958}, characters: "Yamana Messenger"],
          [film: {"The Birth of Japan", 1959}, characters: "Otomo Soldier"],
          [film: {"The Secret of the Telegian", 1960}, characters: "Tsukamoto"],
          [film: {"Daredevil in the Castle", 1961}, characters: "Itamiya's Bodyguard"],
          [film: {"Yojimbo", 1961}, characters: "Horseback Messenger"],
          [film: {"Sanjuro", 1962}, characters: "Escort Samurai"],
          [film: {"King Kong vs. Godzilla", 1962}, characters: "Transport Ship Captain"],
          [film: {"Atragon", 1963}, characters: "Military Officer"],
          [film: {"Mothra vs. Godzilla", 1964}, characters: "Policeman"],
          [
            film: {"Ghidorah, the Three-Headed Monster", 1964},
            characters: "Sergina Opposition Leader"
          ],
          [film: {"Frankenstein Conquers the World", 1965}, characters: "Policeman"]
        ]
      ]
    ]
  ],
  bio: true
]
