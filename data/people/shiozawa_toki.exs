[
  given_name: "Toki",
  family_name: "Shiozawa",
  path: "shiozawa-toki",
  original_name: "塩沢 とき",
  dob: [year: 1928, month: 4, day: 1],
  birth_place: "Nakazato, Ushigome, Tokyo, Japan",
  dod: [year: 2007, month: 5, day: 17],
  death_place: "Meguro, Tokyo, Japan",
  selected_filmography: [
    roles: [
      [
        role: "Actress",
        films: [
          [film: {"The Human Vapor", 1960}, characters: "Instructor's Wife"],
          [film: {"Monster Zero", 1965}, characters: "Minister"]
        ]
      ]
    ]
  ],
  bio: true
]
