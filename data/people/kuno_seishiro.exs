[
  given_name: "Seishiro",
  family_name: "Kuno",
  path: "kuno-seishiro",
  original_name: "久野 征四郎",
  dob: [year: 1940, month: 4, day: 18],
  birth_place: "Shizuoka, Japan",
  selected_filmography: [
    roles: [
      [
        role: "Actor",
        films: [
          [film: {"Samurai Pirate", 1963}, characters: "Prison Guard"],
          [film: {"Mothra vs. Godzilla", 1964}, characters: "Soldier"],
          [film: {"Ghidorah, the Three-Headed Monster", 1964}, characters: "Waiter"],
          [film: {"The Adventures of Taklamakan", 1966}, characters: "Palace Guard"],
          [film: {"War of the Gargantuas", 1966}, characters: "Fisherman"],
          [film: {"Godzilla vs. the Sea Monster", 1966}, characters: "Youth at Dance Contest"],
          [film: {"King Kong Escapes", 1967}, characters: "Soldier"],
          [film: {"Son of Godzilla", 1967}, characters: "Tashiro"],
          [film: {"Destroy All Monsters", 1968}, characters: "SY-3 Pilot"]
        ]
      ]
    ]
  ],
  bio: true
]
