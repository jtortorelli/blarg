[
  given_name: "Chieko",
  family_name: "Nakakita",
  path: "nakakita-chieko",
  original_name: "中北 千枝子",
  dob: [year: 1926, month: 5, day: 21],
  birth_place: "Tokyo, Japan",
  dod: [year: 2005, month: 9, day: 13],
  death_place: "Tokyo, Japan",
  selected_filmography: [
    roles: [
      [
        role: "Actress",
        films: [
          [film: {"The Birth of Japan", 1959}, characters: "Tenazuchi"],
          [film: {"Daredevil in the Castle", 1961}, characters: "Yae No Kyoku"],
          [film: {"The Last War", 1961}, characters: "Ohara"],
          [film: {"Godzilla vs. the Sea Monster", 1966}, characters: "Yata's & Ryota's Mother"]
        ]
      ]
    ]
  ],
  bio: true
]
