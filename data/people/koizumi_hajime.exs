[
  given_name: "Hajime",
  family_name: "Koizumi",
  path: "koizumi-hajime",
  original_name: "小泉一",
  selected_filmography: [
    roles: [
      [
        role: "Cinematographer",
        films: [
          [film: {"The Mysterians", 1957}],
          [film: {"The H-Man", 1958}],
          [film: {"Varan the Unbelievable", 1958}],
          [film: {"Battle in Outer Space", 1959}],
          [film: {"The Human Vapor", 1960}],
          [film: {"Mothra", 1961}],
          [film: {"Gorath", 1962}],
          [film: {"King Kong vs. Godzilla", 1962}],
          [film: {"Matango", 1963}],
          [film: {"Atragon", 1963}],
          [film: {"Mothra vs. Godzilla", 1964}],
          [film: {"Dogora, the Space Monster", 1964}],
          [film: {"Ghidorah, the Three-Headed Monster", 1964}],
          [film: {"Frankenstein Conquers the World", 1965}],
          [film: {"Monster Zero", 1965}],
          [film: {"War of the Gargantuas", 1966}],
          [film: {"King Kong Escapes", 1967}]
        ]
      ]
    ]
  ],
  bio: true
]
