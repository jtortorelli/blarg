[
  given_name: "Rhodes",
  family_name: "Reason",
  path: "reason-rhodes",
  japanese_name: "ローズ・リーズン",
  dob: [year: 1930, month: 4, day: 19],
  birth_place: "Glendale, California, United States",
  dod: [year: 2014, month: 12, day: 26],
  death_place: "Palm Springs, California, United States",
  selected_filmography: [
    roles: [
      [
        role: "Actor",
        films: [
          [film: {"King Kong Escapes", 1967}, characters: "Carl Nelson"]
        ]
      ]
    ]
  ],
  bio: true
]
