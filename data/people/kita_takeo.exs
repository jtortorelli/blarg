[
  given_name: "Takeo",
  family_name: "Kita",
  path: "kita-takeo",
  original_name: "北 猛夫",
  dob: [year: 1901, month: 1, day: 9],
  birth_place: "Osaka, Japan",
  dod: [year: 1979, month: 9, day: 1],
  selected_filmography: [
    roles: [
      [
        role: "Art Director",
        films: [
          [film: {"Godzilla, King of the Monsters", 1954}],
          [film: {"Godzilla Raids Again", 1955}],
          [film: {"The H-Man", 1958}],
          [film: {"Mothra", 1961}],
          [film: {"The Last War", 1961}],
          [film: {"Gorath", 1962}],
          [film: {"King Kong vs. Godzilla", 1962}],
          [film: {"Samurai Pirate", 1963}],
          [film: {"Atragon", 1963}],
          [film: {"Mothra vs. Godzilla", 1964}],
          [film: {"Dogora, the Space Monster", 1964}],
          [film: {"Ghidorah, the Three-Headed Monster", 1964}],
          [film: {"Frankenstein Conquers the World", 1965}],
          [film: {"Monster Zero", 1965}],
          [film: {"War of the Gargantuas", 1966}],
          [film: {"Godzilla vs. the Sea Monster", 1966}],
          [film: {"King Kong Escapes", 1967}],
          [film: {"Son of Godzilla", 1967}],
          [film: {"Destroy All Monsters", 1968}],
          [film: {"Latitude Zero", 1969}],
          [film: {"Godzilla's Revenge", 1969}],
          [film: {"Space Amoeba", 1970}]
        ]
      ],
      [
        role: "Producer",
        films: [
          [film: {"The Invisible Man", 1954}]
        ]
      ]
    ]
  ],
  bio: true
]
