[
  given_name: "Akira",
  family_name: "Kubo",
  path: "kubo-akira",
  original_name: "久保 明",
  birth_name: "Yasuyoshi Yamauchi (山内 康儀)",
  dob: [year: 1936, month: 12, day: 1],
  birth_place: "Tokyo, Japan",
  selected_filmography: [
    roles: [
      [
        role: "Actor",
        films: [
          [film: {"Throne of Blood", 1957}, characters: "Yoshiteru Miki"],
          [film: {"The Birth of Japan", 1959}, characters: "Prince Ioki"],
          [film: {"Sanjuro", 1962}, characters: "Samurai Hayato Morishima"],
          [film: {"Gorath", 1962}, characters: "Tatsuma Kanai"],
          [film: {"Matango", 1963}, characters: "Kenji Murai"],
          [film: {"Whirlwind", 1964}, characters: "Daijiro"],
          [film: {"Monster Zero", 1965}, characters: "Tetsuo"],
          [film: {"Son of Godzilla", 1967}, characters: "Goro Maki"],
          [film: {"Destroy All Monsters", 1968}, characters: "Katsuo Yamabe"],
          [film: {"Space Amoeba", 1970}, characters: "Taro Kudo"],
          [
            film: {"Gamera: The Guardian of the Universe", 1995},
            characters: "Captain of the Kairyu-maru"
          ]
        ]
      ]
    ]
  ],
  bio: true
]
