[
  given_name: "Nick",
  family_name: "Adams",
  path: "adams-nick",
  birth_name: "Nicholas Aloysius Adamschock",
  japanese_name: "ニック・アダムス",
  dob: [year: 1931, month: 7, day: 10],
  birth_place: "Nanticoke, Pennsylvania, United States",
  dod: [year: 1968, month: 2, day: 7],
  death_place: "Beverly Hills, California, United States",
  selected_filmography: [
    roles: [
      [
        role: "Actor",
        films: [
          [film: {"Frankenstein Conquers the World", 1965}, characters: "Dr. James Bowen"],
          [film: {"Monster Zero", 1965}, characters: "Astronaut Glenn"]
        ]
      ]
    ]
  ],
  bio: true
]
