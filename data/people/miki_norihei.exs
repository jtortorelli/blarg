[
  given_name: "Norihei",
  family_name: "Miki",
  path: "miki-norihei",
  original_name: "三木 のり平",
  birth_name: "Tadashi Tanuma (田沼 則子)",
  dob: [year: 1924, month: 4, day: 11],
  birth_place: "Hama, Nihonbashi, Tokyo, Japan",
  dod: [year: 1999, month: 1, day: 25],
  selected_filmography: [
    roles: [
      [
        role: "Actor",
        films: [
          [film: {"The Birth of Japan", 1959}, characters: "Koyane"],
          [film: {"Zatoichi's Revenge", 1965}, characters: "Denroku"],
          [film: {"Earthquake Archipelago", 1980}, characters: "Shozo Sukegawa"],
          [film: {"Tokyo Blackout", 1987}, characters: "Matsukichi Kimura"],
          [film: {"Zatoichi", 1989}, characters: "Giabara"]
        ]
      ]
    ]
  ],
  bio: true
]
