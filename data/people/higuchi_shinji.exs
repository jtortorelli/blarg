[
  given_name: "Shinji",
  family_name: "Higuchi",
  path: "higuchi-shinji",
  original_name: "樋口 真嗣",
  dob: [year: 1965, month: 9, day: 22],
  birth_place: "Shinjuku, Tokyo, Japan",
  selected_filmography: [
    roles: [
      [
        role: "Actor",
        films: [
          [film: {"Returner", 2002}, characters: "UFO Researcher"]
        ]
      ],
      [
        role: "Concept Artist",
        films: [
          [film: {"Gamera 2: Advent of Legion", 1996}],
          [film: {"Gamera 3: Revenge of Iris", 1999}],
          [film: {"Casshern", 2004}],
          [film: {"Assault Girls", 2009}]
        ]
      ],
      [
        role: "Director",
        films: [
          [film: {"Lorelei", 2005}],
          [film: {"The Sinking of Japan", 2006}],
          [film: {"Hidden Fortress: The Last Princess", 2008}],
          [film: {"Attack on Titan", 2015}],
          [film: {"Attack on Titan: End of the World", 2015}],
          [film: {"Shin Godzilla", 2016}]
        ]
      ],
      [
        role: "Planning Coordinator",
        films: [
          [film: {"Samurai Commando: Mission 1549", 2005}]
        ]
      ],
      [
        role: "SFX Director",
        films: [
          [film: {"Gamera: The Guardian of the Universe", 1995}],
          [film: {"Gamera 2: Advent of Legion", 1996}],
          [film: {"Gamera 3: Revenge of Iris", 1999}],
          [film: {"Sakuya, Slayer of Demons", 2000}],
          [film: {"The Princess Blade", 2001}],
          [film: {"Lorelei", 2005}],
          [film: {"Shin Godzilla", 2016}]
        ]
      ]
    ]
  ],
  bio: true
]
