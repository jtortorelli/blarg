[
  given_name: "Ko",
  family_name: "Mishima",
  path: "mishima-ko",
  original_name: "三島 耕",
  birth_name: "Katsuhiro Hase (長谷 勝博)",
  dob: [year: 1927, month: 5, day: 20],
  birth_place: "Nippori, Toshima, Tokyo, Japan",
  selected_filmography: [
    roles: [
      [
        role: "Actor",
        films: [
          [film: {"The H-Man", 1958}, characters: "Gangster"],
          [film: {"The Birth of Japan", 1959}, characters: "Yakumo"],
          [film: {"The Human Vapor", 1960}, characters: "Detective Fujita"],
          [film: {"Gorath", 1962}, characters: "South Pole Engineer Sanada"],
          [film: {"King Kong vs. Godzilla", 1962}, characters: "Coast Guard"]
        ]
      ]
    ]
  ],
  bio: true
]
