[
  given_name: "Gen",
  family_name: "Shimizu",
  path: "shimizu-gen",
  original_name: "清水 元",
  dob: [year: 1907, month: 1, day: 1],
  birth_place: "Matsunaga, Kanda, Tokyo, Japan",
  dod: [year: 1972, month: 12, day: 20],
  selected_filmography: [
    roles: [
      [
        role: "Actor",
        films: [
          [film: {"Seven Samurai", 1954}, characters: "Offended Samurai"],
          [film: {"Throne of Blood", 1957}, characters: "Washizu Soldier"],
          [film: {"Yojimbo", 1961}, characters: "Seibei Underling"],
          [film: {"Sanjuro", 1962}, characters: "Kikui Deputy"],
          [film: {"High and Low", 1963}, characters: "Doctor"],
          [film: {"Monster Zero", 1965}, characters: "General"],
          [film: {"Godzilla vs. Gigan", 1972}, characters: "Defense Commander"]
        ]
      ]
    ]
  ],
  bio: true
]
