[
  given_name: "Akira",
  family_name: "Kurosawa",
  path: "kurosawa-akira",
  original_name: "黒澤 明",
  dob: [year: 1910, month: 3, day: 23],
  birth_place: "Oimachi, Ebara, Tokyo, Japan",
  dod: [year: 1998, month: 9, day: 6],
  death_place: "Seijo, Setagaya, Tokyo, Japan",
  selected_filmography: [
    roles: [
      [
        role: "Director",
        films: [
          [film: {"Rashomon", 1950}],
          [film: {"Seven Samurai", 1954}],
          [film: {"Throne of Blood", 1957}],
          [film: {"The Hidden Fortress", 1958}],
          [film: {"Yojimbo", 1961}],
          [film: {"Sanjuro", 1962}],
          [film: {"High and Low", 1963}],
          [film: {"Kagemusha", 1980}]
        ]
      ],
      [
        role: "Producer",
        films: [
          [film: {"Throne of Blood", 1957}],
          [film: {"The Hidden Fortress", 1958}],
          [film: {"Kagemusha", 1980}]
        ]
      ],
      [
        role: "Screenwriter",
        films: [
          [film: {"Rashomon", 1950}],
          [film: {"Seven Samurai", 1954}],
          [film: {"Throne of Blood", 1957}],
          [film: {"The Hidden Fortress", 1958}],
          [film: {"Yojimbo", 1961}],
          [film: {"Sanjuro", 1962}],
          [film: {"High and Low", 1963}],
          [film: {"Kagemusha", 1980}]
        ]
      ]
    ]
  ],
  bio: true
]
