[
  given_name: "Russ",
  family_name: "Tamblyn",
  path: "tamblyn-russ",
  birth_name: "Russel Irving Tamblyn",
  japanese_name: "ラス・タンブリン",
  dob: [year: 1934, month: 12, day: 30],
  birth_place: "Los Angeles, California, United States",
  selected_filmography: [
    roles: [
      [
        role: "Actor",
        films: [
          [film: {"War of the Gargantuas", 1966}, characters: "Dr. Paul Stewart"]
        ]
      ]
    ]
  ],
  bio: true
]
