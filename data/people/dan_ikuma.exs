[
  given_name: "Ikuma",
  family_name: "Dan",
  path: "dan-ikuma",
  original_name: "團 伊玖磨",
  dob: [year: 1924, month: 4, day: 17],
  birth_place: "Yotsuya, Tokyo, Japan",
  dod: [year: 2001, month: 5, day: 17],
  death_place: "Suzhou, Jiangsu, China",
  selected_filmography: [
    roles: [
      [
        role: "Composer",
        films: [
          [film: {"Samurai I: Musashi Miyamoto", 1954}],
          [film: {"Samurai II: Duel at Ichijoji Temple", 1955}],
          [film: {"Samurai III: Duel at Ganryu Island", 1956}],
          [film: {"The Last War", 1961}]
        ]
      ]
    ]
  ],
  bio: true
]
