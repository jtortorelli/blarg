[
  given_name: "Ren",
  family_name: "Imaizumi",
  path: "imaizumi-ren",
  original_name: "今泉廉",
  selected_filmography: [
    roles: [
      [
        role: "Actor",
        films: [
          [film: {"Godzilla, King of the Monsters", 1954}, characters: "Coast Guard"],
          [
            film: {"Samurai II: Duel at Ichijoji Temple", 1955},
            characters: "Yoshioka School Samurai"
          ],
          [film: {"Rodan", 1956}, characters: "Dr. Sunagawa"],
          [film: {"The Mysterians", 1957}, characters: "Adachi's Assistant"]
        ]
      ]
    ]
  ],
  bio: true
]
