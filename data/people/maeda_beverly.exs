[
  given_name: "Beverly",
  family_name: "Maeda",
  path: "maeda-beverly",
  original_name: "前田 美波里",
  dob: [year: 1948, month: 8, day: 8],
  birth_place: "Kamakura, Kanagawa, Japan",
  selected_filmography: [
    roles: [
      [
        role: "Actress",
        films: [
          [film: {"Son of Godzilla", 1967}, characters: "Saeko"]
        ]
      ]
    ]
  ],
  bio: true
]
