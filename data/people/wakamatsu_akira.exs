[
  given_name: "Akira",
  family_name: "Wakamatsu",
  path: "wakamatsu-akira",
  original_name: "若松 明",
  dob: [year: 1933, month: 6, day: 12],
  birth_place: "Fukushima, Japan",
  selected_filmography: [
    roles: [
      [
        role: "Actor",
        films: [
          [film: {"Mothra", 1961}, characters: "Nelson's Henchman"],
          [film: {"Dogora, the Space Monster", 1964}, characters: "Gangster"],
          [film: {"Monster Zero", 1965}, characters: "Xian"]
        ]
      ]
    ]
  ],
  bio: true
]
