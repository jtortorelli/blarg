[
  given_name: "Takashi",
  family_name: "Yamazaki",
  path: "yamazaki-takashi",
  original_name: "山崎 貴",
  dob: [year: 1964, month: 6, day: 12],
  birth_place: "Matsumoto, Nagano, Japan",
  selected_filmography: [
    roles: [
      [
        role: "Concept Artist",
        films: [
          [film: {"Juvenile", 2000}]
        ]
      ],
      [
        role: "Co-Screenwriter",
        films: [
          [film: {"K-20", 2008}]
        ]
      ],
      [
        role: "Director",
        films: [
          [film: {"Juvenile", 2000}],
          [film: {"Returner", 2002}],
          [film: {"Always", 2005}],
          [film: {"Always 2", 2007}],
          [film: {"Ballad", 2009}],
          [film: {"Space Battleship Yamato", 2010}],
          [film: {"Friends", 2011}],
          [film: {"Always 3", 2012}],
          [film: {"The Eternal Zero", 2013}],
          [film: {"Stand By Me, Doraemon", 2014}],
          [film: {"Parasyte", 2014}],
          [film: {"Parasyte: Completion", 2015}],
          [film: {"A Man Called Pirate", 2016}]
        ]
      ],
      [
        role: "Screenwriter",
        films: [
          [film: {"Juvenile", 2000}],
          [film: {"Returner", 2002}],
          [film: {"Always", 2005}],
          [film: {"Always 2", 2007}],
          [film: {"Ballad", 2009}],
          [film: {"Friends", 2011}],
          [film: {"Always 3", 2012}],
          [film: {"The Eternal Zero", 2013}],
          [film: {"Stand By Me, Doraemon", 2014}],
          [film: {"Parasyte", 2014}],
          [film: {"Parasyte: Completion", 2015}],
          [film: {"A Man Called Pirate", 2016}]
        ]
      ],
      [
        role: "VFX",
        films: [
          [film: {"Eko Eko Azarak: Wizard of Darkness", 1995}],
          [film: {"Eko Eko Azarak II: Birth of the Wizard", 1996}],
          [film: {"Juvenile", 2000}],
          [film: {"Returner", 2002}],
          [film: {"Always", 2005}],
          [film: {"Always 2", 2007}],
          [film: {"K-20", 2008}],
          [film: {"Ballad", 2009}],
          [film: {"Space Battleship Yamato", 2010}],
          [film: {"Always 3", 2012}],
          [film: {"The Eternal Zero", 2013}],
          [film: {"Parasyte", 2014}],
          [film: {"Parasyte: Completion", 2015}],
          [film: {"A Man Called Pirate", 2016}]
        ]
      ]
    ]
  ],
  bio: true
]
