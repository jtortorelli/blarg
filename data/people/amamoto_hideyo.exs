[
  given_name: "Hideyo",
  family_name: "Amamoto",
  path: "amamoto-hideyo",
  original_name: "天本 英世",
  aliases: "Eisei Amamoto (天本 英世)",
  dob: [
    year: 1926,
    month: 1,
    day: 2
  ],
  birth_place: "Wakamatsu, Fukuoka, Japan",
  dod: [
    year: 2003,
    month: 3,
    day: 23
  ],
  death_place: "Wakamatsu, Fukuoka, Japan",
  selected_filmography: [
    roles: [
      [
        role: "Actor",
        films: [
          [film: {"The Birth of Japan", 1959}, characters: "Deity"],
          [film: {"The Secret of the Telegian", 1960}, characters: "Bodyguard"],
          [film: {"Daredevil in the Castle", 1961}, characters: "Portuguese Interpreter"],
          [film: {"Yojimbo", 1961}, characters: "Seibei Underling"],
          [film: {"Gorath", 1962}, characters: "Barfly"],
          [film: {"Matango", 1963}, characters: "Mushroom Man"],
          [film: {"Samurai Pirate", 1963}, characters: "Granny the Witch"],
          [film: {"Atragon", 1963}, characters: "High Priest of Mu"],
          [film: {"Dogora, the Space Monster", 1964}, characters: "Gangster"],
          [film: {"Ghidorah, the Three-Headed Monster", 1964}, characters: "Salno's Manservant"],
          [film: {"The Adventures of Taklamakan", 1966}, characters: "Granny the Witch"],
          [film: {"Godzilla vs. the Sea Monster", 1966}, characters: "Red Bamboo Officer"],
          [film: {"King Kong Escapes", 1967}, characters: "Dr. Who"],
          [film: {"Godzilla's Revenge", 1969}, characters: "Shinpei Minami"],
          [film: {"Message from Space", 1978}, characters: "Elder Dark"],
          [film: {"The Red Spectacles", 1987}, characters: "Moongaze Ginji"],
          [film: {"Eko Eko Azarak II: Birth of the Wizard", 1996}, characters: "Elder"],
          [film: {"GMK", 2001}, characters: "Yoshitoshi Isayama"],
          [film: {"Masked Rider: The First", 2005}, characters: "Shocker Executive"]
        ]
      ]
    ]
  ],
  bio: true
]
