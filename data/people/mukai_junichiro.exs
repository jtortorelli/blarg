[
  given_name: "Junichiro",
  family_name: "Mukai",
  path: "mukai-junichiro",
  original_name: "向井 淳一郎",
  dob: [year: 1927, month: 7, day: 4],
  birth_place: "Shibuya, Tokyo, Japan",
  selected_filmography: [
    roles: [
      [
        role: "Actor",
        films: [
          [film: {"Samurai I: Musashi Miyamoto", 1954}, characters: "Ruffian"],
          [
            film: {"Godzilla, King of the Monsters", 1954},
            characters: ["Reporter", "Defense Official"]
          ],
          [film: {"The Invisible Man", 1954}, characters: ["Bus Passenger", "Detective"]],
          [film: {"Samurai III: Duel at Ganryu Island", 1956}, characters: "Gambler"],
          [film: {"Rodan", 1956}, characters: "Air Force Officer"],
          [film: {"The Mysterians", 1957}, characters: "Pilot"],
          [film: {"The H-Man", 1958}, characters: "Soldier"],
          [film: {"Varan the Unbelievable", 1958}, characters: "Soldier"],
          [film: {"The Hidden Fortress", 1958}, characters: "Yamana Prisoner Guard"],
          [film: {"The Birth of Japan", 1959}, characters: "Moroto"],
          [film: {"Battle in Outer Space", 1959}, characters: "Policeman"],
          [film: {"The Secret of the Telegian", 1960}, characters: "Police Executive"],
          [film: {"Daredevil in the Castle", 1961}, characters: "Kumoi"],
          [film: {"Yojimbo", 1961}, characters: "Seibei Underling"],
          [film: {"Sanjuro", 1962}, characters: "Samurai Reading Notice"],
          [film: {"Gorath", 1962}, characters: "Security Guard"],
          [film: {"Samurai Pirate", 1963}, characters: "Jail Keeper"],
          [film: {"Whirlwind", 1964}, characters: "Kurahashi"],
          [film: {"Ghidorah, the Three-Headed Monster", 1964}, characters: "Parliamentarian"],
          [film: {"Frankenstein Conquers the World", 1965}, characters: "Village Policeman"]
        ]
      ]
    ]
  ],
  bio: true
]
