[
  given_name: "Hiroshi",
  family_name: "Tachikawa",
  path: "tachikawa-hiroshi",
  original_name: "太刀川 寛",
  birth_name: "Yoichi Tachikawa (太刀川 洋一)",
  dob: [year: 1931, month: 5, day: 7],
  birth_place: "Ogimachi, Tama, Tokyo, Japan",
  selected_filmography: [
    roles: [
      [
        role: "Actor",
        films: [
          [film: {"Throne of Blood", 1957}, characters: "Kunimaru Tsuzuki"],
          [film: {"Yojimbo", 1961}, characters: "Yoichiro"],
          [film: {"Sanjuro", 1962}, characters: "Samurai Susumu Kawahara"],
          [film: {"Gorath", 1962}, characters: "Astronaut Wakabayashi"],
          [film: {"Matango", 1963}, characters: "Etsuro Yoshida"]
        ]
      ]
    ]
  ],
  bio: true
]
