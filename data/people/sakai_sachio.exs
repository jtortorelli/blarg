[
  given_name: "Sachio",
  family_name: "Sakai",
  path: "sakai-sachio",
  original_name: "堺 左千夫",
  birth_name: "Yukio Abe (阿部 幸男)",
  dob: [year: 1925, month: 9, day: 8],
  birth_place: "Tokyo, Japan",
  dod: [year: 1998, month: 3, day: 11],
  selected_filmography: [
    roles: [
      [
        role: "Actor",
        films: [
          [film: {"Seven Samurai", 1954}, characters: "Villager"],
          [film: {"Godzilla, King of the Monsters", 1954}, characters: "Hagiwara"],
          [film: {"Samurai II: Duel at Ichijoji Temple", 1955}, characters: "Matahachi"],
          [film: {"Throne of Blood", 1957}, characters: "Washizu Soldier"],
          [film: {"The Hidden Fortress", 1958}, characters: "Kidnapped Yamana Soldier"],
          [film: {"The Secret of the Telegian", 1960}, characters: "Taki"],
          [film: {"Daredevil in the Castle", 1961}, characters: "Kai Hayami"],
          [film: {"Yojimbo", 1961}, characters: "Corrupt Official"],
          [film: {"Sanjuro", 1962}, characters: "Kikui Samurai"],
          [film: {"Gorath", 1962}, characters: "Doctor"],
          [film: {"King Kong vs. Godzilla", 1962}, characters: "Obayashi"],
          [film: {"Whirlwind", 1964}, characters: "Hashima"],
          [film: {"The Adventures of Taklamakan", 1966}, characters: "Caravan Leader"],
          [film: {"King Kong Escapes", 1967}, characters: "Who Henchman"],
          [film: {"Godzilla's Revenge", 1969}, characters: "Sembayashi"],
          [film: {"Vampire Doll", 1970}, characters: "Taxi Driver"],
          [film: {"Space Amoeba", 1970}, characters: "Kudo's Editor"],
          [film: {"Zatoichi", 1989}, characters: "Yakuza"]
        ]
      ]
    ]
  ],
  bio: true
]
