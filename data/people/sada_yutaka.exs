[
  given_name: "Yutaka",
  family_name: "Sada",
  path: "sada-yutaka",
  original_name: "佐田 豊",
  dob: [year: 1911, month: 3, day: 30],
  birth_place: "Sendagi, Hongo, Tokyo, Japan",
  dod: [year: 2017],
  selected_filmography: [
    roles: [
      [
        role: "Actor",
        films: [
          [film: {"Godzilla, King of the Monsters", 1954}, characters: "Defense Official"],
          [film: {"The Invisible Man", 1954}, characters: ["Nightclub Mascot", "Bus Passenger"]],
          [film: {"Throne of Blood", 1957}, characters: "Washizu Servant"],
          [film: {"The Mysterians", 1957}, characters: "Policeman"],
          [film: {"The H-Man", 1958}, characters: "Taxi Driver"],
          [film: {"The Hidden Fortress", 1958}, characters: "Yamana Checkpoint Guard"],
          [film: {"The Birth of Japan", 1959}, characters: "Yamato Villager"],
          [film: {"The Secret of the Telegian", 1960}, characters: "Policeman"],
          [film: {"Yojimbo", 1961}, characters: "Seibei Underling"],
          [film: {"The Last War", 1961}, characters: "Reporter"],
          [film: {"Sanjuro", 1962}, characters: "Kikui Deputy"],
          [film: {"High and Low", 1963}, characters: "Aoki the Chauffeur"],
          [film: {"Mothra vs. Godzilla", 1964}, characters: "School Principal"],
          [
            film: {"Frankenstein Conquers the World", 1965},
            characters: "Laboratory Administrator"
          ],
          [film: {"Godzilla vs. the Sea Monster", 1966}, characters: "Villager"],
          [film: {"Destroy All Monsters", 1968}, characters: "Village Policeman"],
          [film: {"Godzilla's Revenge", 1969}, characters: "Taira, Kenichi's Co-Worker"]
        ]
      ]
    ]
  ],
  bio: true
]
