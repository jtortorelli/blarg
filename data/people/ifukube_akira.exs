[
  given_name: "Akira",
  family_name: "Ifukube",
  path: "ifukube-akira",
  original_name: "伊福部 昭",
  dob: [year: 1914, month: 5, day: 31],
  birth_place: "Kushiro, Hokkaido, Japan",
  dod: [year: 2006, month: 2, day: 8],
  death_place: "Meguro, Tokyo, Japan",
  selected_filmography: [
    roles: [
      [
        role: "Composer",
        films: [
          [film: {"Godzilla, King of the Monsters", 1954}],
          [film: {"Rodan", 1956}],
          [film: {"The Mysterians", 1957}],
          [film: {"Varan the Unbelievable", 1958}],
          [film: {"The Birth of Japan", 1959}],
          [film: {"Battle in Outer Space", 1959}],
          [film: {"Daredevil in the Castle", 1961}],
          [film: {"The Tale of Zatoichi", 1962}],
          [film: {"King Kong vs. Godzilla", 1962}],
          [film: {"New Tale of Zatoichi", 1963}],
          [film: {"Zatoichi the Fugitive", 1963}],
          [film: {"Zatoichi on the Road", 1963}],
          [film: {"Atragon", 1963}],
          [film: {"Mothra vs. Godzilla", 1964}],
          [film: {"Dogora, the Space Monster", 1964}],
          [film: {"Fight, Zatoichi, Fight", 1964}],
          [film: {"Ghidorah, the Three-Headed Monster", 1964}],
          [film: {"Zatoichi's Revenge", 1965}],
          [film: {"Frankenstein Conquers the World", 1965}],
          [film: {"Monster Zero", 1965}],
          [film: {"Zatoichi and the Chess Expert", 1965}],
          [film: {"Daimajin", 1966}],
          [film: {"The Adventures of Taklamakan", 1966}],
          [film: {"Zatoichi's Vengeance", 1966}],
          [film: {"War of the Gargantuas", 1966}],
          [film: {"Return of Daimajin", 1966}],
          [film: {"Daimajin Strikes Again", 1966}],
          [film: {"King Kong Escapes", 1967}],
          [film: {"Zatoichi Challenged", 1967}],
          [film: {"Destroy All Monsters", 1968}],
          [film: {"Latitude Zero", 1969}],
          [film: {"Zatoichi Meets Yojimbo", 1970}],
          [film: {"Space Amoeba", 1970}],
          [film: {"Godzilla vs. Gigan", 1972}],
          [film: {"Zatoichi's Conspiracy", 1973}],
          [film: {"Terror of Mechagodzilla", 1975}],
          [film: {"Godzilla VS King Ghidorah", 1991}],
          [film: {"Godzilla VS Mothra", 1992}],
          [film: {"Godzilla VS Mechagodzilla", 1993}],
          [film: {"Godzilla VS Destroyer", 1995}]
        ]
      ]
    ]
  ],
  bio: true
]
