[
  given_name: "Seizaburo",
  family_name: "Kawazu",
  path: "kawazu-seizaburo",
  original_name: "河津 清三郎",
  birth_name: "Seiichi Nakajima (中島 誠一)",
  dob: [year: 1908, month: 8, day: 30],
  birth_place: "Nihonbashi, Tokyo, Japan",
  dod: [year: 1983, month: 2, day: 20],
  selected_filmography: [
    roles: [
      [
        role: "Actor",
        films: [
          [film: {"The Invisible Man", 1954}, characters: "Nanjo"],
          [film: {"The Secret of the Telegian", 1960}, characters: "Onishi"],
          [film: {"Daredevil in the Castle", 1961}, characters: "Harunaga Ono"],
          [film: {"Yojimbo", 1961}, characters: "Seibei"],
          [film: {"Mothra", 1961}, characters: "General"],
          [film: {"The Last War", 1961}, characters: "Minister"],
          [film: {"Gorath", 1962}, characters: "Minister Tada"],
          [film: {"New Tale of Zatoichi", 1963}, characters: "Yajuro Banno"],
          [film: {"Dogora, the Space Monster", 1964}, characters: "Gang Leader"]
        ]
      ]
    ]
  ],
  bio: true
]
