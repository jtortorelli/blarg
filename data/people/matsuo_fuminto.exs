[
  given_name: "Fuminto",
  family_name: "Matsuo",
  path: "matsuo-fuminto",
  original_name: "松尾 文人",
  dob: [year: 1916, month: 8, day: 6],
  birth_place: "Higashi, Yokohama, Kanagawa, Japan",
  dod: [unknown: true],
  selected_filmography: [
    roles: [
      [
        role: "Actor",
        films: [
          [film: {"Samurai I: Musashi Miyamoto", 1954}, characters: "Soldier"],
          [film: {"The Invisible Man", 1954}, characters: "Yajima's Henchman"],
          [film: {"Samurai II: Duel at Ichijoji Temple", 1955}, characters: "Jinbei Ichikawa"],
          [film: {"Samurai III: Duel at Ganryu Island", 1956}, characters: "Samurai in Retinue"],
          [film: {"Rodan", 1956}, characters: "Dr. Hayama"],
          [film: {"Varan the Unbelievable", 1958}, characters: "Horiguchi"],
          [film: {"The Birth of Japan", 1959}, characters: "Yamato Villager"],
          [film: {"The Secret of the Telegian", 1960}, characters: "Reporter"]
        ]
      ]
    ]
  ],
  bio: true
]
