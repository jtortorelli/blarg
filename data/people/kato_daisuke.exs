[
  given_name: "Daisuke",
  family_name: "Kato",
  path: "kato-daisuke",
  original_name: "加東 大介",
  birth_name: "Tokunosuke Kato (加藤 徳之助)",
  dob: [year: 1911, month: 2, day: 8],
  birth_place: "Akasuka, Tokyo, Japan",
  dod: [year: 1975, month: 7, day: 31],
  selected_filmography: [
    roles: [
      [
        role: "Actor",
        films: [
          [film: {"Rashomon", 1950}, characters: "The Bailiff"],
          [film: {"Seven Samurai", 1954}, characters: "Shichiroji"],
          [film: {"Samurai I: Musashi Miyamoto", 1954}, characters: "Toji Gion"],
          [film: {"Samurai II: Duel at Ichijoji Temple", 1955}, characters: "Toji Gion"],
          [film: {"Samurai III: Duel at Ganryu Island", 1956}, characters: "Toji Gion"],
          [film: {"The Birth of Japan", 1959}, characters: "Futodama"],
          [film: {"Yojimbo", 1961}, characters: "Inokichi"]
        ]
      ]
    ]
  ],
  bio: true
]
