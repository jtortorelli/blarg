[
  given_name: "Eitaro",
  family_name: "Ozawa",
  path: "ozawa-eitaro",
  original_name: "小沢 栄太郎",
  dob: [year: 1909, month: 3, day: 27],
  birth_place: "Tamura, Shiba, Tokyo, Japan",
  dod: [year: 1988, month: 4, day: 23],
  death_place: "Zushi, Kanagawa, Japan",
  selected_filmography: [
    roles: [
      [
        role: "Actor",
        films: [
          [film: {"Samurai I: Musashi Miyamoto", 1954}, characters: "Terumasa Ikeda"],
          [film: {"The H-Man", 1958}, characters: "Detective Miyashita"],
          [film: {"Gorath", 1962}, characters: "Minister Kinami"],
          [film: {"Zatoichi Challenged", 1967}, characters: "Torikoshi"],
          [film: {"The Return of Godzilla", 1984}, characters: "Minister Kanzaki"]
        ]
      ]
    ]
  ],
  bio: true
]
