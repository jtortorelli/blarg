[
  given_name: "Makoto",
  family_name: "Sato",
  path: "sato-makoto",
  original_name: "佐藤 允",
  dob: [year: 1934, month: 3, day: 18],
  birth_place: "Kanzaki, Saga, Japan",
  dod: [year: 2012, month: 12, day: 6],
  death_place: "Kawasaki, Japan",
  selected_filmography: [
    roles: [
      [
        role: "Actor",
        films: [
          [film: {"The H-Man", 1958}, characters: "Uchida"],
          [film: {"The Hidden Fortress", 1958}, characters: "Man at Fire Festival"],
          [film: {"Samurai Pirate", 1963}, characters: "The Black Pirate"],
          [film: {"Whirlwind", 1964}, characters: "Shuri Kusanagi"],
          [film: {"The Adventures of Taklamakan", 1966}, characters: "Gorjaka the Bandit"],
          [film: {"Samaritan Zatoichi", 1968}, characters: "Kashiwazaki"],
          [film: {"Message from Space", 1978}, characters: "Urocco"],
          [film: {"Princess Mononoke", 1997}, characters: "Nago"]
        ]
      ]
    ]
  ],
  bio: true
]
