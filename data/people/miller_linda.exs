[
  given_name: "Linda",
  family_name: "Miller",
  path: "miller-linda",
  japanese_name: "リンダ・ミラー",
  dob: [year: 1947, month: 12, day: 26],
  birth_place: "Pennsylvania, United States",
  selected_filmography: [
    roles: [
      [
        role: "Actress",
        films: [
          [film: {"King Kong Escapes", 1967}, characters: "Susan Watson"]
        ]
      ]
    ]
  ],
  bio: true
]
