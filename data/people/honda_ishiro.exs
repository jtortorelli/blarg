[
  given_name: "Ishiro",
  family_name: "Honda",
  path: "honda-ishiro",
  original_name: "本多 猪四郎",
  dob: [year: 1911, month: 5, day: 7],
  birth_place: "Asahi, Higashi-Tagawa, Yamagata, Japan",
  dod: [year: 1993, month: 2, day: 28],
  selected_filmography: [
    roles: [
      [
        role: "Chief Assistant Director",
        films: [
          [film: {"Kagemusha", 1980}]
        ]
      ],
      [
        role: "Director",
        films: [
          [film: {"Godzilla, King of the Monsters", 1954}],
          [film: {"Rodan", 1956}],
          [film: {"The Mysterians", 1957}],
          [film: {"The H-Man", 1958}],
          [film: {"Varan the Unbelievable", 1958}],
          [film: {"Battle in Outer Space", 1959}],
          [film: {"The Human Vapor", 1960}],
          [film: {"Mothra", 1961}],
          [film: {"Gorath", 1962}],
          [film: {"King Kong vs. Godzilla", 1962}],
          [film: {"Matango", 1963}],
          [film: {"Atragon", 1963}],
          [film: {"Mothra vs. Godzilla", 1964}],
          [film: {"Dogora, the Space Monster", 1964}],
          [film: {"Ghidorah, the Three-Headed Monster", 1964}],
          [film: {"Frankenstein Conquers the World", 1965}],
          [film: {"Monster Zero", 1965}],
          [film: {"War of the Gargantuas", 1966}],
          [film: {"King Kong Escapes", 1967}],
          [film: {"Destroy All Monsters", 1968}],
          [film: {"Latitude Zero", 1969}],
          [film: {"Godzilla's Revenge", 1969}],
          [film: {"Space Amoeba", 1970}],
          [film: {"Terror of Mechagodzilla", 1975}]
        ]
      ],
      [
        role: "Screenwriter",
        films: [
          [film: {"Godzilla, King of the Monsters", 1954}],
          [film: {"War of the Gargantuas", 1966}],
          [film: {"Destroy All Monsters", 1968}]
        ]
      ]
    ]
  ],
  bio: true
]
