[
  given_name: "Jojiro",
  family_name: "Okami",
  path: "okami-jojiro",
  original_name: "丘美 丈二郎",
  dob: [year: 1918, month: 10, day: 31],
  birth_place: "Osaka, Japan",
  dod: [year: 2003, month: 12, day: 11],
  selected_filmography: [
    roles: [
      [
        role: "Writer",
        films: [
          [film: {"The Mysterians", 1957}],
          [film: {"Battle in Outer Space", 1959}],
          [film: {"Gorath", 1962}],
          [film: {"Dogora, the Space Monster", 1964}]
        ]
      ]
    ]
  ],
  bio: true
]
