[
  given_name: "Yu",
  family_name: "Fujiki",
  path: "fujiki-yu",
  original_name: "藤木 悠",
  birth_name: "Yuzo Suzuki (鈴木 悠蔵)",
  dob: [year: 1931, month: 3, day: 2],
  birth_place: "Ebaramachi, Ebara, Tokyo, Japan",
  dod: [year: 2005, month: 12, day: 19],
  death_place: "Chuo, Tokyo, Japan",
  selected_filmography: [
    roles: [
      [
        role: "Actor",
        films: [
          [film: {"Godzilla, King of the Monsters", 1954}, characters: "Ship's Radio Operator"],
          [film: {"Samurai II: Duel at Ichijoji Temple", 1955}, characters: "Shichiro Yoshioka"],
          [film: {"Throne of Blood", 1957}, characters: "Washizu Soldier"],
          [film: {"The Hidden Fortress", 1958}, characters: "Yamana Border Guard"],
          [film: {"The Birth of Japan", 1959}, characters: "Okabi"],
          [film: {"Daredevil in the Castle", 1961}, characters: "Danemon Ban"],
          [film: {"King Kong vs. Godzilla", 1962}, characters: "Kinsaburo Furue"],
          [film: {"Atragon", 1963}, characters: "Yoshito Nishibe"],
          [film: {"Mothra vs. Godzilla", 1964}, characters: "Jiro Nakamura"],
          [film: {"Space Amoeba", 1970}, characters: "Advertising Chief"]
        ]
      ]
    ]
  ],
  bio: true
]
