[
  given_name: "Kamatari",
  family_name: "Fujiwara",
  path: "fujiwara-kamatari",
  original_name: "藤原 釜足",
  dob: [year: 1905, month: 1, day: 15],
  birth_place: "Tokyo, Japan",
  dod: [year: 1985, month: 12, day: 21],
  selected_filmography: [
    roles: [
      [
        role: "Actor",
        films: [
          [film: {"Seven Samurai", 1954}, characters: "Manzo"],
          [film: {"The Invisible Man", 1954}, characters: "Mari's Grandfather"],
          [film: {"The Hidden Fortress", 1958}, characters: "Matashichi"],
          [film: {"Yojimbo", 1961}, characters: "Silk Merchant Tazaemon"],
          [film: {"Sanjuro", 1962}, characters: "Takebayashi"],
          [film: {"High and Low", 1963}, characters: "Hospital Waste Collector"],
          [film: {"Three Outlaw Samurai", 1964}, characters: "Jinbei"],
          [film: {"Kagemusha", 1980}, characters: "Doctor"]
        ]
      ]
    ]
  ],
  bio: true
]
