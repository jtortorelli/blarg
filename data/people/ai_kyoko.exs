[
  given_name: "Kyoko",
  family_name: "Ai",
  path: "ai-kyoko",
  original_name: "愛 京子",
  birth_name: "Misho Tabuchi (田渕 美粧)",
  dob: [year: 1943, month: 3, day: 2],
  selected_filmography: [
    roles: [
      [
        role: "Actress",
        films: [
          [film: {"Destroy All Monsters", 1968}, characters: "Kilaak Queen"]
        ]
      ]
    ]
  ],
  bio: true
]
