[
  given_name: "Haruo",
  family_name: "Suzuki",
  path: "suzuki-haruo",
  original_name: "鈴木 治夫",
  dob: [year: 1926, month: 10, day: 4],
  birth_place: "Hongo, Tokyo, Japan",
  selected_filmography: [
    roles: [
      [
        role: "Actor",
        films: [
          [film: {"Seven Samurai", 1954}, characters: "Peasant"],
          [film: {"Samurai I: Musashi Miyamoto", 1954}, characters: "Soldier"],
          [film: {"The Invisible Man", 1954}, characters: "Yajima's Henchman"],
          [film: {"Rodan", 1956}, characters: "Coal Car Staff"],
          [film: {"The Mysterians", 1957}, characters: "Policeman"],
          [film: {"The H-Man", 1958}, characters: "Policeman"],
          [film: {"The Hidden Fortress", 1958}, characters: "Yamana Cavalry"],
          [film: {"Battle in Outer Space", 1959}, characters: "Detective"],
          [film: {"The Human Vapor", 1960}, characters: "Policeman"],
          [film: {"Daredevil in the Castle", 1961}, characters: "Tokugawa Emissary"],
          [film: {"Yojimbo", 1961}, characters: "Ushitora Underling"],
          [film: {"The Last War", 1961}, characters: "Defense Officer"],
          [film: {"Sanjuro", 1962}, characters: "Samurai Reading Notice"],
          [film: {"Gorath", 1962}, characters: "Minister"],
          [film: {"King Kong vs. Godzilla", 1962}, characters: "Sailor"],
          [film: {"High and Low", 1963}, characters: "Detective"],
          [film: {"Samurai Pirate", 1963}, characters: "Samurai"],
          [film: {"Atragon", 1963}, characters: "Atragon Crew"],
          [film: {"Whirlwind", 1964}, characters: "Toyotomi Loyalist"],
          [film: {"Mothra vs. Godzilla", 1964}, characters: "Radio Operator"],
          [film: {"Dogora, the Space Monster", 1964}, characters: "Soldier"],
          [film: {"Frankenstein Conquers the World", 1965}, characters: "Reporter"],
          [film: {"Monster Zero", 1965}, characters: "Scientist"],
          [film: {"King Kong Escapes", 1967}, characters: "Who Henchman"],
          [film: {"Destroy All Monsters", 1968}, characters: "Military Advisor"],
          [film: {"Godzilla's Revenge", 1969}, characters: "Reporter"],
          [film: {"Lake of Dracula", 1971}, characters: "Policeman"],
          [film: {"Godzilla vs. Hedorah", 1971}, characters: "Military Officer"],
          [film: {"The Submersion of Japan", 1973}, characters: "Helicopter Pilot"],
          [film: {"Evil of Dracula", 1974}, characters: "Security Guard"],
          [film: {"Prophecies of Nostradamus", 1974}, characters: "Policeman"],
          [film: {"Terror of Mechagodzilla", 1975}, characters: "Submarine Tech"]
        ]
      ]
    ]
  ],
  bio: true
]
