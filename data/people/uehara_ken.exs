[
  given_name: "Ken",
  family_name: "Uehara",
  path: "uehara-ken",
  original_name: "上原 謙",
  birth_name: "Kiyoaki Ikehata (池端 清亮)",
  dob: [year: 1909, month: 11, day: 7],
  birth_place: "Ushigome, Tokyo, Japan",
  dod: [year: 1991, month: 11, day: 23],
  selected_filmography: [
    roles: [
      [
        role: "Actor",
        films: [
          [film: {"Mothra", 1961}, characters: "Dr. Harada"],
          [film: {"The Last War", 1961}, characters: "Minister"],
          [film: {"Gorath", 1962}, characters: "Dr. Kono"],
          [film: {"Atragon", 1963}, characters: "Admiral Kusumi"]
        ]
      ]
    ]
  ],
  bio: true
]
