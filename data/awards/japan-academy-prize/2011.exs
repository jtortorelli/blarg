[
  name: "Japan Academy Prize",
  ordinal: "34th",
  date: ~D[2011-02-18],
  hosts: ["Tsutomu Sekine", "Takako Matsu"],
  competitive_awards: [
    [
      name: "Best Picture",
      nominees: [
        [films: "Confessions"],
        [films: "Villain"],
        [films: "About Her Brother"],
        [films: "A Lone Scalpel"],
        [films: "13 Assassins"]
      ]
    ],
    [
      name: "Best Animated Picture",
      nominees: [
        [films: "The Secret World of Arrietty"],
        [films: "Colorful"],
        [films: "Doraemon: Nobita's Great Battle of the Mermaid King"],
        [films: "Detective Conan: The Lost Ship in the Sky"],
        [films: "One Piece Film: Strong World"]
      ]
    ],
    [
      name: "Best Director",
      nominees: [
        [films: "Confessions", recipients: "Tetsuya Nakashima"],
        [films: "A Lone Scalpel", recipients: "Izuru Narishima"],
        [films: "13 Assassins", recipients: "Takashi Miike"],
        [films: "About Her Brother", recipients: "Yoji Yamada"],
        [films: "Villain", recipients: "Lee Sang-il"]
      ]
    ],
    [
      name: "Best Screenplay",
      nominees: [
        [films: "Confessions", recipients: "Tetsuya Nakashima"],
        [films: "A Lone Scalpel", recipients: "Masato Kato"],
        [films: "13 Assassins", recipients: "Daisuke Tengan"],
        [films: "About Her Brother", recipients: ["Yoji Yamada", "Emiko Hiramatsu"]],
        [films: "Villain", recipients: ["Shuichi Yoshida", "Lee Sang-il"]]
      ]
    ],
    [
      name: "Best Leading Actor",
      nominees: [
        [films: "Villain", recipients: "Satoshi Tsumabuki"],
        [films: "About Her Brother", recipients: "Tsurube Shofukutei"],
        [films: "A Lone Scalpel", recipients: "Shinichi Tsutsumi"],
        [films: "Sword of Desperation", recipients: "Etsushi Toyokawa"],
        [films: "13 Assassins", recipients: "Koji Yakusho"]
      ]
    ],
    [
      name: "Best Leading Actress",
      nominees: [
        [films: "Villain", recipients: "Eri Fukatsu"],
        [films: "Caterpillar", recipients: "Shinobu Terajima"],
        [films: "Confessions", recipients: "Takako Matsu"],
        [films: "A Good Husband", recipients: "Hiroko Yakushimaru"],
        [films: "About Her Brother", recipients: "Sayuri Yoshinaga"]
      ]
    ],
    [
      name: "Best Supporting Actor",
      nominees: [
        [films: "Villain", recipients: "Akira Emoto"],
        [films: "A Good Husband", recipients: "Renji Ishibashi"],
        [films: "Villain", recipients: "Masaki Okada"],
        [films: "Confessions", recipients: "Masaki Okada"],
        [films: "Sword of Desperation", recipients: "Koji Kikkawa"]
      ]
    ],
    [
      name: "Best Supporting Actress",
      nominees: [
        [films: "Villain", recipients: "Kirin Kiki"],
        [films: "About Her Brother", recipients: "Yu Aoi"],
        [films: "Confessions", recipients: "Yoshino Kimura"],
        [films: "A Lone Scalpel", recipients: "Yui Natsukawa"],
        [films: "Villain", recipients: "Hikari Mitsushima"]
      ]
    ],
    [
      name: "Best Music",
      nominees: [
        [films: "Villain", recipients: "Joe Hisaishi"],
        [films: "13 Assassins", recipients: "Koji Endo"],
        [films: "Beck", recipients: "GrandFunk"],
        [films: "About Her Brother", recipients: "Isao Tomita"],
        [films: "A Good Husband", recipients: "Meyna Company"]
      ]
    ],
    [
      name: "Best Photography",
      nominees: [
        [films: "13 Assassins", recipients: "Nobuyasu Kita"],
        [films: "Confessions", recipients: ["Masakazu Ato", "Atsushi Ozawa"]],
        [films: "Sword of Desperation", recipients: "Koichi Ishii"],
        [films: "Villain", recipients: "Norimichi Kasamatsu"],
        [films: "About Her Brother", recipients: "Masashi Chikamori"]
      ]
    ],
    [
      name: "Best Lighting",
      nominees: [
        [films: "13 Assassins", recipients: "Yoshi Watabe"],
        [films: "Confessions", recipients: "Susumu Takakura"],
        [films: "Sword of Desperation", recipients: "Noritaka Shiihara"],
        [films: "Villain", recipients: "Kazuhiro Iwashita"],
        [films: "About Her Brother", recipients: "Koichi Watanabe"]
      ]
    ],
    [
      name: "Best Art",
      nominees: [
        [films: "13 Assassins", recipients: "Yuji Hayashida"],
        [films: "Confessions", recipients: "Towako Kuwashima"],
        [films: "Villain", recipients: ["Yohei Taneda", "Ryo Sugimoto"]],
        [films: "The Lady Shogun and Her Men", recipients: "Hidefumi Hanatani"],
        [films: "Sakurada Gate Incident", recipients: "Toshiyuki Matsumiya"]
      ]
    ],
    [
      name: "Best Sound",
      nominees: [
        [films: "13 Assassins", recipients: "Jun Nakamura"],
        [films: "About Her Brother", recipients: "Kazumi Kishida"],
        [films: "Villain", recipients: "Mitsugu Shiratori"],
        [films: "Sword of Desperation", recipients: "Yasushi Tanaka"],
        [films: "Confessions", recipients: "Masato Yano"]
      ]
    ],
    [
      name: "Best Editing",
      nominees: [
        [films: "Confessions", recipients: "Yoshiyuki Koike"],
        [films: "About Her Brother", recipients: "Iwao Ishii"],
        [films: "Villain", recipients: "Tsuyoshi Imai"],
        [films: "Sword of Desperation", recipients: "Chieko Suzaki"],
        [films: "13 Assassins", recipients: "Kenji Yamashita"]
      ]
    ],
    [
      name: "Best Foreign Picture",
      nominees: [
        [films: "Avatar"],
        [films: "Inception"],
        [films: "Invictus"],
        [films: "Toy Story 3"],
        [films: "The Hurt Locker"]
      ]
    ]
  ],
  non_competitive_awards: [
    [
      name: "New Actor Awards",
      nominees: [
        [films: "Ghost", recipients: "Mana Ashida"],
        [films: "Kinako", recipients: "Momoka Ono"],
        [
          films: ["Zebraman 2: Attack on Zebra City", "The Girl Who Leapt Through Time"],
          recipients: "Riisa Naka"
        ],
        [films: "Softball Boys", recipients: "Kento Nagayama"],
        [films: "Umizaru 3: The Last Message", recipients: "Shohei Miura"],
        [
          films: "Railways: The Story of a Man Who Became a Train Driver at the Age of 49",
          recipients: "Takahiro Miura"
        ]
      ]
    ],
    [
      name: "Special Award of the Association",
      nominees: [
        [recipients: "Ryuzo Ueno", notes: "Swordmaster"],
        [recipients: "Ryu Kuze", notes: "Swordmaster"],
        [recipients: "Ryoji Matsumoto", notes: "Set Decorator"],
        [recipients: "Nobuo Yajima", notes: "Special Effects Director"],
        [recipients: "Teruyo Nogami", notes: "Historian, Kurosawa Group Production Manager"]
      ]
    ],
    [
      name: "Shigeru Okada Award",
      nominees: [
        [recipients: "Video Kyoto Co., Ltd."]
      ]
    ],
    [
      name: "Chairman's Special Award",
      nominees: [
        [recipients: "Umetsugu Inoue", notes: "Director, d. Feb 11, aged 86"],
        [recipients: "Tanie Kitabayashi", notes: "Actress, d. Apr 27, aged 98"],
        [recipients: "Takeo Kimura", notes: "Art Director, d. Mar 21, aged 91"],
        [recipients: "Keiju Kobayashi", notes: "Actor, d. Sep 16, aged 86"],
        [recipients: "Katsumi Nishikawa", notes: "Director, d. Apr 6, aged 91"]
      ]
    ],
    [
      name: "Popularity Awards",
      nominees: [
        [films: "SP The Motion Picture: Ambition"],
        [films: "Sunshine Ahead", recipients: "Takashi Okamura"]
      ]
    ]
  ],
  in_memoriam: [
    [recipients: "Naoki Togawa", notes: "Film Critic"],
    [recipients: "Kohei Tsuka", notes: "Playwright, Screenwriter"],
    [recipients: "Mie Minami", notes: "Actor"],
    [recipients: "Junko Ikeuchi", notes: "Actress"],
    [recipients: "Tetsuro Hoshino", notes: "Lyricist"],
    [recipients: "Makoto Fujita", notes: "Actor"],
    [recipients: "Akinori Matsuo", notes: "Director"],
    [recipients: "Toshi Ima", notes: "Anime Director"],
    [recipients: "Ryo Ikebe", notes: "Actor"],
    [recipients: "Mitsuo Watanabe", notes: "Lighting Technician"],
    [recipients: "Kei Sato", notes: "Actor"],
    [recipients: "Ai Saotome", notes: "Actress"],
    [recipients: "Kei Tani", notes: "Actor"],
    [recipients: "Nachi Nozawa", notes: "Voice Actor"],
    [recipients: "Hideko Takamine", notes: "Actress"],
    [recipients: "Kimihiko Nakamura", notes: "Art Director"],
    [recipients: "Yumiko Hasegawa", notes: "Actor"],
    [recipients: "Yukinori Tachibana", notes: "Director"],
    [recipients: "Yoshinobu Nishizaki", notes: "Producer"]
  ]
]
