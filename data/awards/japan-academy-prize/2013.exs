[
  name: "Japan Academy Prize",
  ordinal: "36th",
  date: ~D[2013-03-08],
  hosts: ["Tsutomu Sekine", "Mao Inoue"],
  competitive_awards: [
    [
      name: "Best Picture",
      nominees: [
        [films: "The Kirishima Thing"],
        [films: "Dearest"],
        [films: "A Chorus of Angels"],
        [films: "The Floating Castle"],
        [films: "Chronicle of My Mother"]
      ]
    ],
    [
      name: "Best Animated Picture",
      nominees: [
        [films: "Wolf Children"],
        [films: "Evangelion: 3.0 You Can (Not) Redo"],
        [films: "A Letter to Momo"],
        [films: "Friends"],
        [films: "One Piece Film: Z"]
      ]
    ],
    [
      name: "Best Director",
      nominees: [
        [films: "The Kirishima Thing", recipients: "Daihachi Yoshida"],
        [films: "The Floating Castle", recipients: "Isshin Inudo"],
        [films: "The Floating Castle", recipients: "Shinji Higuchi"],
        [films: "A Chorus of Angels", recipients: "Junji Sakamoto"],
        [films: "Chronicle of My Mother", recipients: "Masato Harada"],
        [films: "Dearest", recipients: "Yasuo Furuhata"]
      ]
    ],
    [
      name: "Best Screenplay",
      nominees: [
        [films: "Key of Life", recipients: "Kenji Uchida"],
        [films: "Dearest", recipients: "Takeshi Aoshima"],
        [films: "The Kirishima Thing", recipients: "Kohei Kiyasu"],
        [films: "The Kirishima Thing", recipients: "Daihachi Yoshida"],
        [films: "A Chorus of Angels", recipients: "Machiko Nasu"],
        [films: "Chronicle of My Mother", recipients: "Masato Harada"]
      ]
    ],
    [
      name: "Best Leading Actor",
      nominees: [
        [films: "Thermae Romae", recipients: "Hiroshi Abe"],
        [films: "Key of Life", recipients: "Masato Sakai"],
        [films: "The Floating Castle", recipients: "Mansai Nomura"],
        [films: "The Drudgery Train", recipients: "Mirai Moriyama"],
        [
          films: "Isoroku Yamamoto, The Commander-in-Chief of the Combined Fleet",
          recipients: "Koji Yakusho"
        ],
        [films: "Chronicle of My Mother", recipients: "Koji Yakusho"]
      ]
    ],
    [
      name: "Best Leading Actress",
      nominees: [
        [films: "Chronicle of My Mother", recipients: "Kirin Kiki"],
        [films: "A Terminal Trust", recipients: "Tamiyo Kusakari"],
        [films: "Helter Skelter", recipients: "Erika Sawajiri"],
        [films: "Dreams for Sale", recipients: "Takako Matsu"],
        [films: "A Chorus of Angels", recipients: "Sayuri Yoshinaga"]
      ]
    ],
    [
      name: "Best Supporting Actor",
      nominees: [
        [films: "Dearest", recipients: "Hideji Otaki"],
        [films: "Key of Life", recipients: "Teruyuki Kagawa"],
        [films: "The Drudgery Train", recipients: "Kengo Kora"],
        [films: "Dearest", recipients: "Koichi Sato"],
        [films: "The Floating Castle", recipients: "Koichi Sato"],
        [films: "A Chorus of Angels", recipients: "Mirai Moriyama"]
      ]
    ],
    [
      name: "Best Supporting Actress",
      nominees: [
        [films: "Dearest", recipients: "Kimiko Yo"],
        [films: "Helter Skelter", recipients: "Shinobu Terajima"],
        [films: "Key of Life", recipients: "Ryoko Hirosue"],
        [films: "A Chorus of Angels", recipients: "Hikari Mitsushima"],
        [films: "Chronicle of My Mother", recipients: "Aoi Miyazaki"]
      ]
    ],
    [
      name: "Best Music",
      nominees: [
        [films: "A Chorus of Angels", recipients: "Ikuko Kawai"],
        [films: "The Floating Castle", recipients: "Koji Ueno"],
        [films: "Dearest", recipients: "Yusuke Hayashi"],
        [films: "Tenchi: The Samurai Astronomer", recipients: "Joe Hisaishi"],
        [films: "Chronicle of My Mother", recipients: "Harumi Fuuki"]
      ]
    ],
    [
      name: "Best Photography",
      nominees: [
        [films: "A Chorus of Angels", recipients: "Daisaku Kimura"],
        [films: "Chronicle of My Mother", recipients: "Akiko Ashizawa"],
        [films: "The Floating Castle", recipients: "Motonobu Kiyoku"],
        [films: "The Floating Castle", recipients: "Shoji Ehara"],
        [films: "Tenchi: The Samurai Astronomer", recipients: "Takeshi Hamada"],
        [films: "Dearest", recipients: "Junichiro Hayashi"]
      ]
    ],
    [
      name: "Best Lighting",
      nominees: [
        [films: "A Chorus of Angels", recipients: "Takashi Sugimoto"],
        [films: "Chronicle of My Mother", recipients: "Hidenori Nagata"],
        [films: "The Floating Castle", recipients: "Takashi Sugimoto"],
        [films: "Tenchi: The Samurai Astronomer", recipients: "Kiyoto Ando"],
        [films: "Dearest", recipients: "Yuki Nakamura"]
      ]
    ],
    [
      name: "Best Art",
      nominees: [
        [films: "The Floating Castle", recipients: ["Norihiro Isoda", "Noriyuki Kondo"]],
        [films: ["A Chorus of Angels", "Thermae Romae"], recipients: "Mitsuo Harada"],
        [films: "Tenchi: The Samurai Astronomer", recipients: "Kyoko Heya"],
        [films: "Dearest", recipients: "Kyoko Yauchi"],
        [films: "Chronicle of My Mother", recipients: "Hidemitsu Yamazaki"]
      ]
    ],
    [
      name: "Best Sound",
      nominees: [
        [
          films: "Isoroku Yamamoto, The Commander-in-Chief of the Combined Fleet",
          recipients: "Fumio Hashimoto"
        ],
        [films: "Tenchi: The Samurai Astronomer", recipients: "Osamu Onodera"],
        [films: "A Chorus of Angels", recipients: "Junichi Shima"],
        [films: "The Floating Castle", recipients: "Junichi Shima"],
        [films: "Dearest", recipients: "Tsutomu Honda"],
        [films: "Chronicle of My Mother", recipients: "Showa Matsumoto"],
        [films: "Chronicle of My Mother", recipients: "Masato Yano"]
      ]
    ],
    [
      name: "Best Editing",
      nominees: [
        [films: "The Kirishima Thing", recipients: "Mototaka Kusakabe"],
        [films: "The Floating Castle", recipients: "Soichi Ueno"],
        [films: "Dearest", recipients: "Junichi Kikuchi"],
        [films: "Chronicle of My Mother", recipients: "Eugene Harada"],
        [films: "A Chorus of Angels", recipients: "Shinichi Fushima"]
      ]
    ],
    [
      name: "Best Foreign Picture",
      nominees: [
        [films: "The Intouchables"],
        [films: "Argo"],
        [films: "The Dark Knight Rises"],
        [films: "The Girl with the Dragon Tattoo"],
        [films: "Skyfall"]
      ]
    ]
  ],
  non_competitive_awards: [
    [
      name: "New Actor Awards",
      nominees: [
        [films: ["Himizu", "Lesson of the Evil"], recipients: "Shota Sometani"],
        [films: "Fly with the Gold", recipients: "Changmin"],
        [films: "The Kirishima Thing", recipients: "Masahiro Higashide"],
        [
          films: ["Until the Break of Dawn", "The Wings of the Kirin", "Kyo, Koi o Hajimemasu"],
          recipients: "Tori Matsuzaka"
        ],
        [
          films: ["Rurouni Kenshin", "For Love's Sake", "Kyo, Koi o Hajimemasu"],
          recipients: "Emi Takei"
        ],
        [films: ["Himizu", "Lesson of the Evil"], recipients: "Fumi Nikaido"],
        [
          films: ["The Kirishima Thing", "Home: Itoshi no Zashiki", "Another Face"],
          recipients: "Ai Hashimoto"
        ]
      ]
    ],
    [
      name: "Special Award of the Association",
      nominees: [
        [recipients: "Toshio Sugawara", notes: "Swordmaster"],
        [recipients: "Masao Baba", notes: "Art Manager"],
        [recipients: "Masae Miyamoto", notes: "Costume Designer"],
        [recipients: "Okayasu Productions"],
        [recipients: "Sanyo Editing Room"]
      ]
    ],
    [
      name: "Chairman's Achievement Award",
      nominees: [
        [films: "Bayside Shakedown", notes: "Film Series"]
      ]
    ],
    [
      name: "Association Honor Award",
      nominees: [
        [recipients: "Yoji Yamada"]
      ]
    ],
    [
      name: "Shigeru Okada Award",
      nominees: [
        [recipients: "Central Arts Co., Ltd."]
      ]
    ],
    [
      name: "Chairman's Special Award",
      nominees: [
        [recipients: "Kaneto Shindo", notes: "Director, Screenwriter, d. May 29, aged 100"],
        [recipients: "Fumio Hashimoto", notes: "Sound Technician, d. Nov 2, aged 84"],
        [recipients: "Mitsuko Mori", notes: "Actress, d. Nov 10, aged 92"],
        [recipients: "Isuzu Yamada", notes: "Actress, d. Jul 9, aged 95"],
        [recipients: "Koji Wakamatsu", notes: "Director, d. Oct 17, aged 76"]
      ]
    ],
    [
      name: "Popularity Awards",
      nominees: [
        [films: "The Kirishima Thing"],
        [films: "Ushijima the Loan Shark", recipients: "Yuko Oshima"]
      ]
    ]
  ],
  in_memoriam: [
    [recipients: "Hikaru Hayashi", notes: "Compositor, d. Jan 5, aged 80"],
    [recipients: "Hideaki Nitani", notes: "Actor, d. Jan 28, aged 80"],
    [recipients: "Chieko Misaki", notes: "Actress, d. Feb 20, aged 90"],
    [recipients: "Chikage Awashima", notes: "Actress, d. Feb 16, aged 87"],
    [recipients: "Yasuyuki Inoue", notes: "Special Effects Art Designer, d. Feb 19, aged 89"],
    [recipients: "Fumio Konami", notes: "Screenwriter, d. Mar 4, aged 78"],
    [recipients: "Sanae Nakahara", notes: "Actress, d. May 15, aged 76"],
    [recipients: "Takeo Chii", notes: "Actor, d. Jun 29, aged 70"],
    [recipients: "Keiko Tsushima", notes: "Actress, d. Aug 1, aged 86"],
    [recipients: "Taketoshi Naito", notes: "Actor, d. Aug 21, aged 86"],
    [recipients: "Hiromichi Horikawa", notes: "Director, d. Sep 5, aged 95"],
    [recipients: "Hideji Otaki", notes: "Actor, d. Oct 2, aged 87"],
    [recipients: "Giichi Fujimoto", notes: "Author, Screenwriter, d. Oct 30, aged 79"],
    [recipients: "Mitsutoshi Ishigami", notes: "Film Critic, d. Nov 6, aged 73"],
    [recipients: "Kosei Saito", notes: "Director, d. Nov 25, aged 80"],
    [recipients: "Kanzaburo Nakamura", notes: "Actor, d. Dec 5, aged 57"],
    [recipients: "Makoto Sato", notes: "Actor, d. Dec 6, aged 78"],
    [recipients: "Shoichi Ozawa", notes: "Actor, d. Dec 10, aged 83"],
    [recipients: "Noriko Sengoku", notes: "Actress, d. Dec 27, aged 90"]
  ]
]
