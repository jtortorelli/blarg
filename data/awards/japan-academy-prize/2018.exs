[
  name: "Japan Academy Prize",
  ordinal: "41st",
  date: ~D[2018-03-02],
  hosts: ["Toshiyuki Nishida", "Rie Miyazawa"],
  competitive_awards: [
    [
      name: "Best Picture",
      nominees: [
        [films: "The Third Murder"],
        [films: "I Want to Eat Your Pancreas"],
        [films: "Sekigahara"],
        [films: "Miracles of the Namiya General Store"],
        [films: "Flower and Sword"]
      ]
    ],
    [
      name: "Best Animated Picture",
      nominees: [
        [films: "The Night is Short, Walk On Girl"],
        [films: "Fireworks"],
        [films: "Napping Princess"],
        [films: "Mary and the Witch's Flower"],
        [films: "Detective Conan: The Crimson Love Letter"]
      ]
    ],
    [
      name: "Best Director",
      nominees: [
        [films: "The Third Murder", recipients: "Hirokazu Kore-eda"],
        [films: "Before We Vanish", recipients: "Kiyoshi Kurosawa"],
        [films: "Flower and Sword", recipients: "Tetsuo Shinohara"],
        [films: "Sekigahara", recipients: "Masato Harada"],
        [films: "Miracles of the Namiya General Store", recipients: "Ryuichi Hiroki"]
      ]
    ],
    [
      name: "Best Screenplay",
      nominees: [
        [films: "The Third Murder", recipients: "Hirokazu Kore-eda"],
        [films: "Miracles of the Namiya General Store", recipients: "Hiroshi Saito"],
        [films: "Flower and Sword", recipients: "Yoshiko Morishita"],
        [films: "What a Wonderful Family! 2", recipients: ["Yoji Yamada", "Emiko Hiramatsu"]],
        [films: "I Want to Eat Your Pancreas", recipients: "Tomoko Yoshida"]
      ]
    ],
    [
      name: "Best Leading Actor",
      nominees: [
        [films: "Wilderness: Part One", recipients: "Masaki Suda"],
        [films: "The Detective is in the Bar 3", recipients: "Yo Oizumi"],
        [films: "Sekigahara", recipients: "Junichi Okada"],
        [films: "The 8-Year Engagement", recipients: "Ken Sato"],
        [films: "Memoirs of a Murderer", recipients: "Tatsuya Fujiwara"]
      ]
    ],
    [
      name: "Best Leading Actress",
      nominees: [
        [films: "Birds Without Names", recipients: "Yu Aoi"],
        [films: "Mixed Doubles", recipients: "Yui Aragaki"],
        [films: "The 8-Year Engagement", recipients: "Tao Tsuchiya"],
        [films: "Before We Vanish", recipients: "Masami Nagasawa"],
        [films: "Yurigokoro", recipients: "Yuriko Yoshitaka"]
      ]
    ],
    [
      name: "Best Supporting Actor",
      nominees: [
        [films: "The Third Murder", recipients: "Koji Yakusho"],
        [films: "Miracles of the Namiya General Store", recipients: "Toshiyuki Nishida"],
        [films: "What a Wonderful Family! 2", recipients: "Masahiko Nishimura"],
        [films: "The Detective is in the Bar 3", recipients: "Ryuhei Matsuda"],
        [films: "Mukoku", recipients: "Nijiro Murakami"],
        [films: "Sekigahara", recipients: "Koji Yakusho"]
      ]
    ],
    [
      name: "Best Supporting Actress",
      nominees: [
        [films: "The Third Murder", recipients: "Suzu Hirose"],
        [films: "Miracles of the Namiya General Store", recipients: "Machiko Ono"],
        [films: "The Detective is in the Bar 3", recipients: "Keiko Kitagawa"],
        [films: "What a Wonderful Family! 2", recipients: "Yui Natsukawa"],
        [films: "The 8-Year Engagement", recipients: "Hiroko Yakushimaru"]
      ]
    ],
    [
      name: "Best Music",
      nominees: [
        [films: "Outrage Coda", recipients: "Keiichi Suzuki"],
        [films: "Sekigahara", recipients: "Harumi Fuuki"],
        [films: "The 8-Year Engagement", recipients: "Takatsugu Muramatsu"],
        [films: "The Third Murder", recipients: "Ludovico Einaudi"],
        [films: "Kiseki: Anohi No Sobito", recipients: "JIN"]
      ]
    ],
    [
      name: "Best Photography",
      nominees: [
        [films: "Sekigahara", recipients: "Takahide Shibanushi"],
        [films: "Flower and Sword", recipients: "Tokusho Kikumura"],
        [films: "Destiny: The Tale of Kamakura", recipients: "Kozo Shibasaki"],
        [films: "The Third Murder", recipients: "Mikiya Takimoto"],
        [films: "What a Wonderful Family! 2", recipients: "Masashi Chikamori"]
      ]
    ],
    [
      name: "Best Lighting",
      nominees: [
        [films: "Sekigahara", recipients: "Takaaki Miyanishi"],
        [films: "Flower and Sword", recipients: "Tatsuya Osada"],
        [films: "Destiny: The Tale of Kamakura", recipients: "Nariyuki Ueda"],
        [films: "The Third Murder", recipients: "Norikiyo Fujii"],
        [films: "What a Wonderful Family! 2", recipients: "Koichi Watanabe"]
      ]
    ],
    [
      name: "Best Art",
      nominees: [
        [films: "Flower and Sword", recipients: "Tomoko Kurata"],
        [
          films: "What a Wonderful Family! 2",
          recipients: ["Tomoko Kurata", "Hisayuki Kobayashi"]
        ],
        [films: "Destiny: The Tale of Kamakura", recipients: "Anri Jojo"],
        [films: "Sekigahara", recipients: "Tetsuo Harada"],
        [
          films: "Miracles of the Namiya General Store",
          recipients: ["Tomoyuki Maruo", "Rihito Nakagawa"]
        ]
      ]
    ],
    [
      name: "Best Sound",
      nominees: [
        [films: "Sekigahara", recipients: "Masato Yano"],
        [films: "Flower and Sword", recipients: "Satoshi Ozaki"],
        [films: "What a Wonderful Family! 2", recipients: "Kazumi Kishida"],
        [films: "Outrage Coda", recipients: "Yoshifumi Kureishi"],
        [films: "The Third Murder", recipients: "Kazuhiko Tomita"]
      ]
    ],
    [
      name: "Best Editing",
      nominees: [
        [films: "The Third Murder", recipients: "Hirokazu Kore-eda"],
        [films: "Flower and Sword", recipients: "Hirohide Abe"],
        [films: "What a Wonderful Family! 2", recipients: "Iwao Ishii"],
        [films: "Outrage Coda", recipients: ["Takeshi Kitano", "Yoshinori Oda"]],
        [films: "Sekigahara", recipients: "Eugene Harada"]
      ]
    ],
    [
      name: "Best Foreign Picture",
      nominees: [
        [films: "La La Land"],
        [films: "Dunkirk"],
        [films: "Hidden Figures"],
        [films: "Beauty and the Beast"],
        [films: "Miss Sloane"]
      ]
    ]
  ],
  non_competitive_awards: [
    [
      name: "New Actor Awards",
      nominees: [
        [
          films: "Let's Go, JETS! From Small Girls to U.S. Champions?!",
          recipients: "Ayami Nakajo"
        ],
        [films: "I Want to Eat Your Pancreas", recipients: "Minami Hamabe"],
        [films: "I Want to Eat Your Pancreas", recipients: "Takumi Kitamura"],
        [films: "Teiichi: Battle of Supreme High", recipients: "Ryoma Takeuchi"]
      ]
    ],
    [
      name: "Special Award of the Association",
      nominees: [
        [recipients: "Etsuko Egawa", notes: "Special Makeup Artist, Special Modeling"],
        [recipients: "Katsutoshi Osawa", notes: "Art, Railway Equipment"],
        [recipients: "Midori Onuma", notes: "Hair and Makeup Artist"],
        [recipients: "Hiroshi Koto", notes: "Costume Designer"],
        [recipients: "Takeshi Yoshikawa", notes: "Casting Director"]
      ]
    ],
    [
      name: "Chairman's Achievement Award",
      nominees: [
        [recipients: "Kyogo Kagawa", notes: "Actress"],
        [recipients: "Takashi Kawamata", notes: "Cinematographer"],
        [recipients: "Masahiro Shinoda", notes: "Director"],
        [recipients: "Kenichi Benitani", notes: "Sound Engineer"],
        [recipients: "Toshio Masuda", notes: "Director"],
        [recipients: "Fujiko Yamamoto", notes: "Actress"]
      ]
    ],
    [
      name: "Chairman's Special Award",
      nominees: [
        [recipients: "Hiroki Matsukata", notes: "Actor, d. Jan 21, aged 74"],
        [recipients: "Tsunehiko Watase", notes: "Actor, d. Mar 14, aged 72"],
        [recipients: "Iwao Otani", notes: "Sound Engineer, d. Aug 3, aged 98"],
        [recipients: "Akira Hayasaka", notes: "Screenwriter, d. Dec 16, aged 88"]
      ]
    ],
    [
      name: "Popularity Awards",
      nominees: [
        [films: "I Want to Eat Your Pancreas"],
        [recipients: "Masaki Suda"]
      ]
    ]
  ],
  in_memoriam: [
    [recipients: "Masaharu Segawa", notes: "Director, Screenwriter, d. Jun 20 2016, aged 90"],
    [recipients: "Shigeru Koyama", notes: "Actor, d. Jan 3, aged 87"],
    [recipients: "Shuji Nakamura", notes: "Art Director, d. Jan 21, aged 89"],
    [recipients: "Yoshio Tsuchiya", notes: "Actor, d. Feb 8, aged 89"],
    [recipients: "Toshio Matsumoto", notes: "Director, d. Apr 12, aged 85"],
    [recipients: "Yumeji Tsukioka", notes: "Actor, d. May 3, aged 94"],
    [recipients: "Genkichi Hasegawa", notes: "Cinematographer, d. Jun 25, aged 77"],
    [recipients: "Yoshinaga Yoko", notes: "Screenwriter, Director, d. Jul 18, aged 87"],
    [
      recipients: "Shoichi Masuo",
      notes: "Special Effects Director, Animator, d. Jul 24, aged 57"
    ],
    [recipients: "Shogoro Nishimura", notes: "Director, d. Aug 1, aged 87"],
    [recipients: "Haruo Nakajima", notes: "Suit Actor, d. Aug 7, aged 88"],
    [recipients: "Chikara Hashimoto", notes: "Actor, d. Oct 11, aged 83"],
    [recipients: "Seijun Suzuki", notes: "Director, d. Feb 13, aged 93"]
  ]
]
