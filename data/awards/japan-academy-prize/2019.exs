[
  name: "Japan Academy Prize",
  ordinal: "42nd",
  date: ~D[2019-03-01],
  hosts: ["Toshiyuki Nishida", "Yu Aoi"],
  competitive_awards: [
    [
      name: "Best Picture",
      nominees: [
        [films: "Shoplifters"],
        [films: "One Cut of the Dead"],
        [films: "Sakura Guardian in the North"],
        [films: "The Blood of Wolves"],
        [films: "Recall"]
      ]
    ],
    [
      name: "Best Animated Picture",
      nominees: [
        [films: "Mirai"],
        [films: "Dragon Ball Super: Broly"],
        [films: "Penguin Highway"],
        [films: "Detective Conan: Zero the Enforcer"],
        [films: "Waka Okami wa Shogakusei!"]
      ]
    ],
    [
      name: "Best Director",
      nominees: [
        [films: "Shoplifters", recipients: "Hirokazu Kore-eda"],
        [films: "One Cut of the Dead", recipients: "Shinichiro Ueda"],
        [films: "The Blood of Wolves", recipients: "Kazuya Shiraishi"],
        [films: "Sakura Guardian in the North", recipients: "Yojiro Takita"],
        [films: "Recall", recipients: "Katsuhide Motoki"]
      ]
    ],
    [
      name: "Best Screenplay",
      nominees: [
        [films: "Shoplifters", recipients: "Hirokazu Kore-eda"],
        [films: "The Blood of Wolves", recipients: "Junya Ikegami"],
        [films: "One Cut of the Dead", recipients: "Shinichiro Ueda"],
        [films: "Sakura Guardian in the North", recipients: "Machiko Nasu"],
        [films: "Recall", recipients: "Tamiyo Hayashi"]
      ]
    ],
    [
      name: "Best Leading Actor",
      nominees: [
        [films: "The Blood of Wolves", recipients: "Koji Yakusho"],
        [films: "Samurai's Promise", recipients: "Junichi Okada"],
        [films: "Life in Overtime", recipients: "Hiroshi Tachi"],
        [films: "One Cut of the Dead", recipients: "Takayuki Hamatsu"],
        [films: "Shoplifters", recipients: "Lily Franky"]
      ]
    ],
    [
      name: "Best Leading Actress",
      nominees: [
        [films: "Shoplifters", recipients: "Sakura Ando"],
        [films: "Every Day A Good Day", recipients: "Haru Kuroki"],
        [films: "The House Where the Mermaid Sleeps", recipients: "Ryoko Shinohara"],
        [films: "Tremble All You Want", recipients: "Mayu Matsuoka"],
        [films: "Sakura Guardian in the North", recipients: "Sayuri Yoshinaga"]
      ]
    ],
    [
      name: "Best Supporting Actor",
      nominees: [
        [films: "The Blood of Wolves", recipients: "Tori Matsuzaka"],
        [films: "Sakura Guardian in the North", recipients: "Ittoku Kishibe"],
        [films: "Recall", recipients: "Dean Fujioka"],
        [films: "Samurai's Promise", recipients: "Hidetoshi Nishijima"],
        [films: "Killing for the Prosecution", recipients: "Kazunari Ninomiya"]
      ]
    ],
    [
      name: "Best Supporting Actress",
      nominees: [
        [films: "Shoplifters", recipients: "Kirin Kiki"],
        [films: "Every Day A Good Day", recipients: "Kirin Kiki"],
        [films: "Sakura Guardian in the North", recipients: "Ryoko Shinohara"],
        [films: "Recall", recipients: "Kyoko Fukada"],
        [films: "The Blood of Wolves", recipients: "Yoko Maki"],
        [films: "Shoplifters", recipients: "Mayu Matsuoka"]
      ]
    ],
    [
      name: "Best Music",
      nominees: [
        [films: "Shoplifters", recipients: "Haruomi Hosono"],
        [
          films: "Sakura Guardian in the North",
          recipients: ["Kei Ogura", "Katsu Hoshi", "Shogo Kaida"]
        ],
        [films: "Samurai's Promise", recipients: "Takashi Kato"],
        [
          films: "One Cut of the Dead",
          recipients: ["Nobuhiro Suzuki", "Shoma Ito", "Kyle Nagai"]
        ],
        [films: "The Blood of Wolves", recipients: "Goro Yasukawa"],
        [films: "Recall", recipients: "Goro Yasukawa"]
      ]
    ],
    [
      name: "Best Photography",
      nominees: [
        [films: "Shoplifters", recipients: "Ryuto Kondo"],
        [films: "Samurai's Promise", recipients: "Daisaku Kimura"],
        [films: "One Cut of the Dead", recipients: "Takeshi Sone"],
        [films: "The Blood of Wolves", recipients: "Takahiro Haibara"],
        [films: "Sakura Guardian in the North", recipients: "Takeshi Hamada"]
      ]
    ],
    [
      name: "Best Lighting",
      nominees: [
        [films: "Shoplifters", recipients: "Isamu Fujii"],
        [films: "Samurai's Promise", recipients: "Kenjiro So"],
        [films: "The Blood of Wolves", recipients: "Minoru Kawai"],
        [films: "Sakura Guardian in the North", recipients: "Sai Takaya"]
      ]
    ],
    [
      name: "Best Art",
      nominees: [
        [films: "The Blood of Wolves", recipients: "Tsutomu Imamura"],
        [films: "Recall", recipients: "Takashi Nishimura"],
        [films: "Samurai's Promise", recipients: "Mitsuo Harada"],
        [films: "Sakura Guardian in the North", recipients: "Kyoko Heya"],
        [films: "Shoplifters", recipients: "Keiko Mitsumatsu"]
      ]
    ],
    [
      name: "Best Sound",
      nominees: [
        [films: "The Blood of Wolves", recipients: "Tomoharu Urata"],
        [films: "Sakura Guardian in the North", recipients: "Osamu Onodera"],
        [films: "Recall", recipients: "Kazuhiro Kurihara"],
        [films: "One Cut of the Dead", recipients: "Kokichi Kokita"],
        [films: "Shoplifters", recipients: "Kazuhiko Tomita"]
      ]
    ],
    [
      name: "Best Editing",
      nominees: [
        [films: "One Cut of the Dead", recipients: "Shinichiro Ueda"],
        [films: "The Blood of Wolves", recipients: "Hitomi Kato"],
        [films: "Recall", recipients: "Isao Kawase"],
        [films: "Shoplifters", recipients: "Hirokazu Kore-eda"],
        [films: "Sakura Guardian in the North", recipients: "Yeong-mi Lee"]
      ]
    ],
    [
      name: "Best Foreign Picture",
      nominees: [
        [films: "Bohemian Rhapsody"],
        [films: "The Greatest Showman"],
        [films: "The Shape of Water"],
        [films: "Three Billboards Outside Ebbing, Missouri"],
        [films: "Mission: Impossible - Fallout"]
      ]
    ]
  ],
  non_competitive_awards: [
    [
      name: "New Actor Awards",
      nominees: [
        [films: "A Forest of Wool and Steel", recipients: "Moka Kamishiraishi"],
        [films: "Love At Least", recipients: "Shuri"],
        [films: "Hibiki", recipients: "Yurina Hirate"],
        [films: ["Kasane: Beauty and Fate", "Samurai's Promise"], recipients: "Kyoko Yoshine"],
        [films: "Cafe Funiculi Funicula", recipients: "Kentaro Ito"],
        [films: ["Kids on the Slope", "Lock-On Love"], recipients: "Taishi Nakagawa"],
        [films: ["Stolen Identity", "Biblia Used Bookstore Casebook"], recipients: "Ryo Narita"],
        [films: "River's Edge", recipients: "Ryo Yoshizawa"]
      ]
    ],
    [
      name: "Special Award of the Association",
      nominees: [
        [recipients: "Yasuo Otsuka", notes: "Animator"],
        [recipients: "Tadashi Kaneda", notes: "Film Steel Man"],
        [recipients: "Tsutomu Sakurai", notes: "Producer"],
        [recipients: "Keisuke Chiyoda", notes: "Costume Designer"]
      ]
    ],
    [
      name: "Chairman's Achievement Award",
      nominees: [
        [recipients: "Mariko Okada", notes: "Actress, Producer"],
        [recipients: "Keiko Kishi", notes: "Actress"],
        [recipients: "Junya Sato", notes: "Director, Screenwriter"],
        [recipients: "Yoshishige Yoshida", notes: "Director, Screenwriter"]
      ]
    ],
    [
      name: "Special Memorial",
      nominees: [
        [recipients: "Tadashi Sawashima", notes: "Director, d. Jan 27, aged 91"],
        [recipients: "Chuji Kinoshita", notes: "Composer, d. Apr 30, aged 102"],
        [
          recipients: "Shinobu Hashimoto",
          notes: "Screenwriter, Producer, Director, d. Jul 19, aged 100"
        ]
      ]
    ],
    [
      name: "Chairman's Special Award",
      nominees: [
        [recipients: "Isao Takahata", notes: "Animation Director, d. Apr 5, aged 82"],
        [recipients: "Yuriko Hoshi", notes: "Actress, d. May 16, aged 74"],
        [recipients: "Masahiko Tsugawa", notes: "Director, Actor, d. Aug 4, aged 78"],
        [recipients: "Kirin Kiki", notes: "Actress, d. Sep 15, aged 75"],
        [recipients: "Sadaji Yoshida", notes: "Cinematographer, d. Oct 28, aged 100"],
        [recipients: "Mitsuru Kurosawa", notes: "Producer, d. Nov 30, aged 85"]
      ]
    ],
    [
      name: "Popularity Awards",
      nominees: [
        [films: "One Cut of the Dead"],
        [recipients: "Kentaro Ito"]
      ]
    ]
  ],
  in_memoriam: [
    [recipients: "Sansho Shinsui", notes: "Actor, d. Dec 30, aged 70"],
    [recipients: "Yosuke Natsuki", notes: "Actor, d. Jan 14, aged 81"],
    [recipients: "Tamio Kawachi", notes: "Actor, d. Feb 10, aged 79"],
    [recipients: "Ren Osugi", notes: "Actor, d. Feb 21, aged 66"],
    [recipients: "Tonpei Hidari", notes: "Actor, d. Feb. 24, aged 80"],
    [recipients: "Yukiji Asaoka", notes: "Actor, d. Apr 27, aged 82"],
    [recipients: "Takayuki Inoue", notes: "Composer, d. May 2, aged 77"],
    [recipients: "Masaki Tamura", notes: "Cinematographer, d. May 23, aged 79"],
    [recipients: "Jiro Terao", notes: "Subtitle Translator, d. Jun 6, aged 62"],
    [recipients: "Go Kato", notes: "Actor, d. Jun 18, aged 80"],
    [recipients: "Tomoko Fujiwara", notes: "Director, d. Jun 19, aged 86"],
    [recipients: "Hiroshi Nawa", notes: "Actor, d. Jun 26, aged 85"],
    [recipients: "Shinichiro Mikami", notes: "Actor, d. Jul 14, aged 77"],
    [recipients: "Etsuko Ikuta", notes: "Actress, d. Jul 15, aged 71"],
    [recipients: "Fujio Tokita", notes: "Actor, d. Jul 18, aged 81"],
    [recipients: "Kin Sugai", notes: "Actress, d. Aug 10, aged 92"],
    [recipients: "Ryunosuke Ono", notes: "Screenwriter, d. Sep 27, aged 84"],
    [recipients: "Hideo Osabe", notes: "Author, d. Oct 18, aged 84"],
    [recipients: "Kyoko Enami", notes: "Actress, d. Oct 27, aged 76"],
    [recipients: "Kazue Tsunogae", notes: "Actor, d. Oct 27, aged 64"],
    [recipients: "Harue Akagi", notes: "Actor, d. Nov 29, aged 94"]
  ]
]
