[
  name: "Japan Academy Prize",
  ordinal: "35th",
  date: ~D[2012-03-02],
  hosts: ["Tsutomu Sekine", "Eri Fukatsu"],
  competitive_awards: [
    [
      name: "Best Picture",
      nominees: [
        [films: "The Eighth Day"],
        [films: "Some Day"],
        [films: "The Last Ronin"],
        [films: "A Ghost of a Chance"],
        [films: "The Detective is in the Bar"]
      ]
    ],
    [
      name: "Best Animated Picture",
      nominees: [
        [films: "From Up on Poppy Hill"],
        [films: "K-On! The Movie"],
        [films: "Buddha: The Great Departure"],
        [films: "Little Ghostly Adventures of Tofu Boy"],
        [films: "Detective Conan: Quarter of Silence"]
      ]
    ],
    [
      name: "Best Director",
      nominees: [
        [films: "The Eighth Day", recipients: "Izuru Narishima"],
        [films: "Some Day", recipients: "Junji Sakamoto"],
        [films: "Postcard", recipients: "Kaneto Shindo"],
        [films: "The Last Ronin", recipients: "Shigemichi Sugita"],
        [films: "A Ghost of a Chance", recipients: "Koki Mitani"]
      ]
    ],
    [
      name: "Best Screenplay",
      nominees: [
        [films: "The Eighth Day", recipients: "Satoko Okudera"],
        [films: "Some Day", recipients: ["Haruhiko Arai", "Junji Sakamoto"]],
        [films: "The Detective is in the Bar", recipients: ["Ryota Kosawa", "Yasushi Suto"]],
        [films: "The Last Ronin", recipients: "Yozo Tanaka"],
        [films: "A Ghost of a Chance", recipients: "Koki Mitani"]
      ]
    ],
    [
      name: "Best Leading Actor",
      nominees: [
        [films: "Some Day", recipients: "Yoshio Harada"],
        [films: "The Detective is in the Bar", recipients: "Yo Oizumi"],
        [films: "Abacus and Sword", recipients: "Masato Sakai"],
        [films: "Railways: For Adults Who Can't Convey Love", recipients: "Tomokazu Miura"],
        [films: "The Last Ronin", recipients: "Koji Yakusho"]
      ]
    ],
    [
      name: "Best Leading Actress",
      nominees: [
        [films: "The Eighth Day", recipients: "Mao Inoue"],
        [films: "Moteki", recipients: "Masami Nagasawa"],
        [films: "Hankyu Railways: A 15-Minute Miracle", recipients: "Miki Nakatani"],
        [films: "A Ghost of a Chance", recipients: "Eri Fukatsu"],
        [films: "Tsure Ga Utsu Ni Narimashite", recipients: "Aoi Miyazaki"]
      ]
    ],
    [
      name: "Best Supporting Actor",
      nominees: [
        [films: "Cold Fish", recipients: "Denden"],
        [films: "Tomorrow's Joe", recipients: "Yusuke Iseya"],
        [films: "Some Day", recipients: "Ittoku Kishibe"],
        [films: "The Last Ronin", recipients: "Koichi Sato"],
        [films: "The Detective is in the Bar", recipients: "Ryuhei Matsuda"]
      ]
    ],
    [
      name: "Best Supporting Actress",
      nominees: [
        [films: "The Eighth Day", recipients: "Hiromi Nagasaku"],
        [films: "Moteki", recipients: "Kumiko Aso"],
        [films: "The Eighth Day", recipients: "Eiko Koike"],
        [films: "Hara-Kiri: Death of a Samurai", recipients: "Hikari Matsushima"],
        [films: "Hankyu Railways: A 15-Minute Miracle", recipients: "Nobuko Miyamoto"]
      ]
    ],
    [
      name: "Best Music",
      nominees: [
        [films: "The Eighth Day", recipients: "Goro Yasukawa"],
        [films: "The Detective is in the Bar", recipients: "Yoshihiro Ike"],
        [films: "Moteki", recipients: "Taisei Iwasaki"],
        [films: "A Ghost of a Chance", recipients: "Kiyoko Ogino"],
        [films: "The Last Ronin", recipients: "Takashi Kako"]
      ]
    ],
    [
      name: "Best Photography",
      nominees: [
        [films: "The Eighth Day", recipients: "Junichi Fujisawa"],
        [films: "Some Day", recipients: "Norimichi Kasamatsu"],
        [films: "The Last Ronin", recipients: "Mutsuo Naganuma"],
        [films: "Gaku", recipients: "Osamu Fujiishi"],
        [films: "A Ghost of a Chance", recipients: "Hideo Yamamoto"]
      ]
    ],
    [
      name: "Best Lighting",
      nominees: [
        [films: "The Eighth Day", recipients: "Masao Kanazawa"],
        [films: "Some Day", recipients: "Kazuhiro Iwashita"],
        [films: "The Last Ronin", recipients: "Takaaki Miyanishi"],
        [films: "Gaku", recipients: "Takayuki Kawabe"],
        [films: "A Ghost of a Chance", recipients: "Akira Ono"]
      ]
    ],
    [
      name: "Best Art",
      nominees: [
        [films: "The Last Ronin", recipients: ["Yoshinobu Nishioka", "Tetsuo Harada"]],
        [films: "Abacus and Sword", recipients: "Noriyuki Kondo"],
        [films: "A Ghost of a Chance", recipients: "Yohei Taneda"],
        [films: "Hara-Kiri: Death of a Samurai", recipients: "Yuji Hayashida"],
        [films: "The Eighth Day", recipients: "Tomoe Matsumoto"]
      ]
    ],
    [
      name: "Best Sound",
      nominees: [
        [films: "The Eighth Day", recipients: "Kenichi Fujimoto"],
        [films: "A Ghost of a Chance", recipients: "Tetsuo Segawa"],
        [
          films: "The Detective is in the Bar",
          recipients: ["Tomoaki Tamura", "Tsuyoshi Murozono"]
        ],
        [films: "Some Day", recipients: "Yasumasa Terui"],
        [films: "The Last Ronin", recipients: ["Toyotaka Nakaji", "Tetsuo Segawa"]]
      ]
    ],
    [
      name: "Best Editing",
      nominees: [
        [films: "The Eighth Day", recipients: "Chise Sanjo"],
        [films: "Moteki", recipients: "Yusuke Ishida"],
        [films: "A Ghost of a Chance", recipients: "Soichi Ueno"],
        [films: "The Last Ronin", recipients: "Chizuko Osada"],
        [films: "The Detective is in the Bar", recipients: "Shinya Tadano"]
      ]
    ],
    [
      name: "Best Foreign Picture",
      nominees: [
        [films: "The King's Speech"],
        [films: "Rise of the Planet of the Apes"],
        [films: "The Social Network"],
        [films: "Black Swan"],
        [films: "Moneyball"]
      ]
    ]
  ],
  non_competitive_awards: [
    [
      name: "New Actor Awards",
      nominees: [
        [films: "Scabbard Samurai", recipients: "Sea Kumada"],
        [films: "The Last Ronin", recipients: "Nanami Sakuraba"],
        [films: "The Eighth Day", recipients: "Konomi Watanabe"],
        [films: "Slapstick Brothers", recipients: "Yusuke Kamiji"],
        [films: "The Egoists", recipients: "Kengo Kora"],
        [films: "Scabbard Samurai", recipients: "Takaaki Nomi"],
        [films: "Second Virgin", recipients: "Hiroki Hasegawa"]
      ]
    ],
    [
      name: "Special Award of the Association",
      nominees: [
        [recipients: "Kuri Goshoen", notes: "Propmaster"]
      ]
    ],
    [
      name: "Chairman's Special Award",
      nominees: [
        [recipients: "Shigeru Okada", notes: "Producer, d. May 9, aged 87"],
        [recipients: "Takeji Sano", notes: "Lighting Technician, d. Mar 4, aged 80"],
        [recipients: "Hideko Takamine", notes: "Actress, d. Dec 28, 2010, aged 86"],
        [recipients: "Yoshio Harada", notes: "Actor, d. Jul 9, aged 71"],
        [recipients: "Yoshimitsu Morita", notes: "Director, d. Dec 20, aged 61"]
      ]
    ],
    [
      name: "Popularity Awards",
      nominees: [
        [films: "Moteki"],
        [films: "Moshidora", recipients: "Atsuko Maeda"]
      ]
    ]
  ],
  in_memoriam: [
    [recipients: "Yasuko Ono", notes: "Screenwriter, d. Jan 6, aged 82"],
    [recipients: "Toshiyuki Hosokawa", notes: "Actor, d. Jan 14, aged 70"],
    [recipients: "Jiro Sakagami", notes: "Comedian, Actor, d. Mar 10, aged 76"],
    [recipients: "Hideko Okiyama", notes: "Actress, d. Mar 21, aged 65"],
    [recipients: "Osamu Dezaki", notes: "Animation Director, d. Apr 17, aged 67"],
    [recipients: "Yoshiko Tanaka", notes: "Actress, d. Apr 21, aged 55"],
    [recipients: "Kiyoshi Kodama", notes: "Actor, d. May 16, aged 77"],
    [recipients: "Shohei Ando", notes: "Cinematographer, d. May 20, aged 77"],
    [recipients: "Hiroyuki Nagato", notes: "Actor, d. May 21, aged 77"],
    [recipients: "Suketsugu Noda", notes: "Association Officer, Producer, d. Jun 1, aged 65"],
    [recipients: "Masaru Baba", notes: "Screenwriter, d. Jun 29, aged 84"],
    [recipients: "Atsushi Takeda", notes: "Director, Screenwriter, Producer, d. Jul 12, aged 84"],
    [recipients: "Masahisa Sadanaga", notes: "Director, d. Jul 14, aged 79"],
    [recipients: "Muga Takewaki", notes: "Actor, d. Jul 21, aged 67"],
    [recipients: "Naoki Sugiura", notes: "Actor, d. Sep 21, aged 79"],
    [recipients: "Ken Yamauchi", notes: "Actor, d. Sep 24, aged 67"],
    [recipients: "Toshiro Ishido", notes: "Screenwriter, d. Nov 1, aged 79"],
    [recipients: "Shinichi Ichikawa", notes: "Screenwriter, d. Dec 10, aged 70"],
    [recipients: "Tetsuro Yoshida", notes: "Screenwriter, d. Dec 20, aged 82"]
  ]
]
