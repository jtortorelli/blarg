[
  name: "Japan Academy Prize",
  ordinal: "37th",
  date: ~D[2014-03-07],
  hosts: ["Toshiyuki Nishida", "Kirin Kiki"],
  competitive_awards: [
    [
      name: "Best Picture",
      nominees: [
        [films: "The Great Passage"],
        [films: "The Devil's Path"],
        [films: "A Boy Called H"],
        [films: "Like Father, Like Son"],
        [films: "Tokyo Family"],
        [films: "Ask This of Rikyu"]
      ]
    ],
    [
      name: "Best Animated Picture",
      nominees: [
        [films: "The Wind Rises"],
        [films: "The Tale of Princess Kaguya"],
        [films: "Space Pirate Captain Harlock"],
        [films: "Puella Magi Madoka: The Movie"],
        [films: "Lupin III vs. Conan"]
      ]
    ],
    [
      name: "Best Director",
      nominees: [
        [films: "The Great Passage", recipients: "Yuya Ishii"],
        [films: "Like Father, Like Son", recipients: "Hirokazu Kore-eda"],
        [films: "The Devil's Path", recipients: "Kazuya Shiraishi"],
        [films: "The Kiyosu Conference", recipients: "Koki Mitani"],
        [films: "Tokyo Family", recipients: "Yoji Yamada"]
      ]
    ],
    [
      name: "Best Screenplay",
      nominees: [
        [films: "The Great Passage", recipients: "Kensaku Watanabe"],
        [films: "Like Father, Like Son", recipients: "Hirokazu Kore-eda"],
        [films: "The Devil's Path", recipients: ["Izumi Takahashi", "Kazuya Shiraishi"]],
        [films: "The Kiyosu Conference", recipients: "Koki Mitani"],
        [films: "Tokyo Family", recipients: ["Yoji Yamada", "Emiko Hiramatsu"]]
      ]
    ],
    [
      name: "Best Leading Actor",
      nominees: [
        [films: "The Great Passage", recipients: "Ryuhei Matsuda"],
        [films: "Ask This of Rikyu", recipients: "Ebizo Ichikawa"],
        [films: "Tokyo Family", recipients: "Isao Hashizume"],
        [films: "Like Father, Like Son", recipients: "Masaharu Fukuyama"],
        [films: "Unforgiven", recipients: "Ken Watanabe"]
      ]
    ],
    [
      name: "Best Leading Actress",
      nominees: [
        [films: "The Ravine of Goodbye", recipients: "Yoko Maki"],
        [films: "A Tale of Samurai Cooking: A Love Story", recipients: "Aya Ueto"],
        [films: "Like Father, Like Son", recipients: "Machiko Ono"],
        [films: "The Great Passage", recipients: "Aoi Miyazaki"],
        [films: "Tokyo Family", recipients: "Kazuko Yoshiyuki"]
      ]
    ],
    [
      name: "Best Supporting Actor",
      nominees: [
        [films: "Like Father, Like Son", recipients: "Lily Franky"],
        [films: "The Great Passage", recipients: "Joe Odagiri"],
        [films: "Tokyo Family", recipients: "Satoshi Tsumabuki"],
        [films: "The Devil's Path", recipients: "Pierre Taki"],
        [films: "Phone Call to the Bar 2", recipients: "Ryuhei Matsuda"],
        [films: "The Devil's Path", recipients: "Lily Franky"]
      ]
    ],
    [
      name: "Best Supporting Actress",
      nominees: [
        [films: "Like Father, Like Son", recipients: "Yoko Maki"],
        [films: "Tokyo Family", recipients: "Yu Aoi"],
        [films: "Phone Call to the Bar 2", recipients: "Machiko Ono"],
        [films: "Ask This of Rikyu", recipients: "Miki Nakatani"],
        [films: "A Tale of Samurai Cooking: A Love Story", recipients: "Kimiko Yo"]
      ]
    ],
    [
      name: "Best Music",
      nominees: [
        [films: "The Wind Rises", recipients: "Joe Hisaishi"],
        [films: ["Tokyo Family", "The Tale of Princess Kaguya"], recipients: "Joe Hisaishi"],
        [films: "Ask This of Rikyu", recipients: "Taro Iwashiro"],
        [films: "The Kiyosu Conference", recipients: "Kiyoko Ogino"],
        [
          films: "Like Father, Like Son",
          recipients: ["Junichi Matsumoto", "Takashi Mori", "Takeshi Matsubara"]
        ],
        [films: "The Great Passage", recipients: "Takashi Watanabe"]
      ]
    ],
    [
      name: "Best Photography",
      nominees: [
        [films: "Unforgiven", recipients: "Norimichi Kasamatsu"],
        [films: "Like Father, Like Son", recipients: "Mikiya Takimoto"],
        [films: "Tokyo Family", recipients: "Masashi Chikamori"],
        [films: "Ask This of Rikyu", recipients: "Takeshi Hamada"],
        [films: "The Great Passage", recipients: "Junichi Fujisawa"]
      ]
    ],
    [
      name: "Best Lighting",
      nominees: [
        [films: "Unforgiven", recipients: "Koichi Watanabe"],
        [films: "Like Father, Like Son", recipients: "Norikiyo Fujii"],
        [films: "Tokyo Family", recipients: "Koichi Watanabe"],
        [films: "Ask This of Rikyu", recipients: "Kiyoto Ando"],
        [films: "The Great Passage", recipients: "Tatsuya Osada"]
      ]
    ],
    [
      name: "Best Art",
      nominees: [
        [films: "Ask This of Rikyu", recipients: "Takashi Yoshida"],
        [films: "The Kiyosu Conference", recipients: "Yohei Taneda"],
        [films: "The Kiyosu Conference", recipients: "Kimie Kurotaki"],
        [films: "A Boy Called H", recipients: "Katsumi Nakazawa"],
        [films: "The Great Passage", recipients: "Mitsuo Harada"],
        [films: "Unforgiven", recipients: ["Mitsuo Harada", "Ryo Sugimoto"]]
      ]
    ],
    [
      name: "Best Sound",
      nominees: [
        [films: "The Great Passage", recipients: "Hirokazu Kato"],
        [films: "Tokyo Family", recipients: "Kazumi Kishida"],
        [films: "The Kiyosu Conference", recipients: "Tetsuo Segawa"],
        [films: "Like Father, Like Son", recipients: "Yutaka Tsurumaki"],
        [films: "Ask This of Rikyu", recipients: "Nobuhiko Matsukage"]
      ]
    ],
    [
      name: "Best Editing",
      nominees: [
        [films: "The Great Passage", recipients: "Shinichi Fushima"],
        [films: "Tokyo Family", recipients: "Iwao Ishii"],
        [films: "The Kiyosu Conference", recipients: "Soichi Ueno"],
        [films: "Like Father, Like Son", recipients: "Hirokazu Kore-eda"],
        [films: "Ask This of Rikyu", recipients: "Kazunobu Fujita"]
      ]
    ],
    [
      name: "Best Foreign Picture",
      nominees: [
        [films: "Les Miserables"],
        [films: "3 Idiots"],
        [films: "Captain Phillips"],
        [films: "Django Unchained"],
        [films: "Gravity"]
      ]
    ]
  ],
  non_competitive_awards: [
    [
      name: "New Actor Awards",
      nominees: [
        [films: ["Unforgiven", "Tsuya's Nights"], recipients: "Shiori Kutsuna"],
        [films: ["The Great Passage", "A Chair in the Plains"], recipients: "Haru Kuroki"],
        [films: "Sweet Whip", recipients: "Mitsu Dan"],
        [films: "Oshin", recipients: "Kokone Hamada"],
        [films: ["A Story of Yonosuke", "The End of Summer"], recipients: "Go Ayano"],
        [films: "The Backwater", recipients: "Masaki Suda"],
        [films: ["Blindly in Love", "Why Don't You Play in Hell?"], recipients: "Gen Hoshino"],
        [films: "A Boy Called H", recipients: "Tatsuki Yoshioka"]
      ]
    ],
    [
      name: "Special Award of the Association",
      nominees: [
        [recipients: "Akane Shiratori", notes: "Script Girl, Screenwriter"],
        [recipients: "Akira Fukuda", notes: "Costume Designer"],
        [recipients: "Harumi Yoshida", notes: "Propmaster"]
      ]
    ],
    [
      name: "Association Honor Award",
      nominees: [
        [recipients: "Ken Takakura", notes: "Actor"]
      ]
    ],
    [
      name: "Chairman's Special Award",
      nominees: [
        [recipients: "Nagisa Oshima", notes: "Director, d. Jan 15, aged 80"],
        [
          recipients: "Etsuko Takano",
          notes: "Iwanami Hall General Manager, President of Equipe de Cinema, d. Feb 9, aged 83"
        ],
        [recipients: "Hideo Kumagai", notes: "Lighting Technician, d. Mar 26, aged 84"],
        [recipients: "Rentaro Mikuni", notes: "Actor, d. Apr 14, aged 90"],
        [recipients: "Isao Natsuyagi", notes: "Actor, d. May 11, aged 73"]
      ]
    ],
    [
      name: "Popularity Awards",
      nominees: [
        [films: "Midsummer's Equation"],
        [films: "Seven Days of Hiwamari and Her Puppies", recipients: "Masayasu Wakabayashi"]
      ]
    ]
  ],
  in_memoriam: [
    [recipients: "Kojiro Hongo", notes: "Actor, d. Feb 14, aged 74"],
    [recipients: "Sachiko Mitsumoto", notes: "Actress, d. Feb 22, aged 69"],
    [recipients: "Ryoko Sakaguchi", notes: "Actress, d. Mar 27, aged 57"],
    [recipients: "Isamu Nagato", notes: "Actor, d. Jun 4, aged 81"],
    [recipients: "Tatsuo Nogami", notes: "Screenwriter, d. Jul 20, aged 85"],
    [recipients: "Taro Ishida", notes: "Actor, d. Sep 21, aged 69"],
    [recipients: "Toyoko Yamasaki", notes: "Author, d. Sep 29, aged 88"],
    [recipients: "Ryuzo Nakanishi", notes: "Screenwriter, d. Oct 9, aged 81"],
    [recipients: "Takashi Yanase", notes: "Author, d. Oct 13, aged 94"],
    [recipients: "Toshiaki Tsushima", notes: "Composer, d. Nov 25, aged 77"]
  ]
]
