[
  name: "Japan Academy Prize",
  ordinal: "38th",
  date: ~D[2015-02-27],
  hosts: ["Toshiyuki Nishida", "Yuko Maki"],
  competitive_awards: [
    [
      name: "Best Picture",
      nominees: [
        [films: "The Eternal Zero"],
        [films: "Pale Moon"],
        [films: "The Little House"],
        [films: "A Samurai Chronicle"],
        [films: "Cape Nostalgia"]
      ]
    ],
    [
      name: "Best Animated Picture",
      nominees: [
        [films: "Stand By Me, Doraemon"],
        [films: "When Marnie Was There"],
        [films: "Giovanni's Island"],
        [films: "Detective Conan: The Sniper from Another Dimension"],
        [films: "Buddha 2"]
      ]
    ],
    [
      name: "Best Director",
      nominees: [
        [films: "The Eternal Zero", recipients: "Takashi Yamazaki"],
        [films: "A Samurai Chronicle", recipients: "Takashi Koizumi"],
        [films: "Cape Nostalgia", recipients: "Izuru Narishima"],
        [films: "Samurai Hustle", recipients: "Katsuhide Motoki"],
        [films: "Pale Moon", recipients: "Daihachi Yoshida"]
      ]
    ],
    [
      name: "Best Screenplay",
      nominees: [
        [films: "Samurai Hustle", recipients: "Akihiro Dobashi"],
        [films: "Cape Nostalgia", recipients: "Masato Kato"],
        [films: "Cape Nostalgia", recipients: "Teruo Abe"],
        [films: "Pale Moon", recipients: "Kaeko Hayafune"],
        [films: "The Eternal Zero", recipients: "Takashi Yamazaki"],
        [films: "The Eternal Zero", recipients: "Tamiyo Hayashi"],
        [films: "The Little House", recipients: "Yoji Yamada"],
        [films: "The Little House", recipients: "Emiko Hiramatsu"]
      ]
    ],
    [
      name: "Best Leading Actor",
      nominees: [
        [films: "The Eternal Zero", recipients: "Junichi Okada"],
        [films: "Cape Nostalgia", recipients: "Hiroshi Abe"],
        [films: "Samurai Hustle", recipients: "Kuranosuke Sasaki"],
        [films: "Snow on the Blades", recipients: "Kiichi Nakai"],
        [films: "A Samurai Chronicle", recipients: "Koji Yakusho"]
      ]
    ],
    [
      name: "Best Leading Actress",
      nominees: [
        [films: "Pale Moon", recipients: "Rie Miyazawa"],
        [films: "0.5mm", recipients: "Sakura Ando"],
        [films: "The Light Shines Only There", recipients: "Chizuru Ikewaki"],
        [films: "The Snow White Murder Case", recipients: "Mao Inoue"],
        [films: "My Man", recipients: "Fumi Nikaido"],
        [films: "Cape Nostalgia", recipients: "Sayuri Yoshinaga"]
      ]
    ],
    [
      name: "Best Supporting Actor",
      nominees: [
        [films: "A Samurai Chronicle", recipients: "Junichi Okada"],
        [films: "Snow on the Blades", recipients: "Hiroshi Abe"],
        [films: "Wood Job!", recipients: "Hideaki Ito"],
        [films: "Cape Nostalgia", recipients: "Tsurube Shofukutei"],
        [films: "The Eternal Zero", recipients: "Haruma Miura"]
      ]
    ],
    [
      name: "Best Supporting Actress",
      nominees: [
        [films: "The Little House", recipients: "Haru Kuroki"],
        [films: "Pale Moon", recipients: "Yuko Oshima"],
        [films: "Pale Moon", recipients: "Satomi Kobayashi"],
        [films: "Cape Nostalgia", recipients: "Yuko Takeuchi"],
        [films: "Lady Maiko", recipients: "Sumiko Fuji"]
      ]
    ],
    [
      name: "Best Music",
      nominees: [
        [films: "Lady Maiko", recipients: "Yoshikazu Suo"],
        [films: "A Samurai Chronicle", recipients: "Takashi Kako"],
        [films: "The Eternal Zero", recipients: "Naoki Sato"],
        [films: "The Little House", recipients: "Joe Hisaishi"],
        [films: "Cape Nostalgia", recipients: "Goro Yasukawa"]
      ]
    ],
    [
      name: "Best Photography",
      nominees: [
        [films: "The Eternal Zero", recipients: "Kozo Shibasaki"],
        [films: "A Samurai Chronicle", recipients: "Shoji Ueda"],
        [films: "A Samurai Chronicle", recipients: "Horyuki Kitazawa"],
        [films: "Pale Moon", recipients: "Makoto Shiguma"],
        [films: "The Little House", recipients: "Masashi Chikamori"],
        [films: "Cape Nostalgia", recipients: "Mutsuo Naganuma"]
      ]
    ],
    [
      name: "Best Lighting",
      nominees: [
        [films: "The Eternal Zero", recipients: "Nariyuki Ueda"],
        [films: "A Samurai Chronicle", recipients: "Hideaki Yamakawa"],
        [films: "Pale Moon", recipients: "Keita Nishio"],
        [films: "The Little House", recipients: "Koichi Watanabe"],
        [films: "Cape Nostalgia", recipients: "Takaaki Miyanishi"]
      ]
    ],
    [
      name: "Best Art",
      nominees: [
        [films: "The Eternal Zero", recipients: "Anri Jojo"],
        [films: "Snow on the Blades", recipients: "Fumio Ogawa"],
        [films: "A Samurai Chronicle", recipients: "Tadashi Sakai"],
        [films: "The Little House", recipients: "Mitsuo Degawa"],
        [films: "The Little House", recipients: "Daisuke Sue"],
        [films: "Cape Nostalgia", recipients: "Yutaka Yokoyama"]
      ]
    ],
    [
      name: "Best Sound",
      nominees: [
        [films: "The Eternal Zero", recipients: "Kenichi Fujimoto"],
        [films: "Snow on the Blades", recipients: "Osamu Onodera"],
        [films: "Pale Moon", recipients: "Akihiko Kaku"],
        [films: "Pale Moon", recipients: "Masato Yano"],
        [films: "The Little House", recipients: "Kazumi Kishida"],
        [films: "Cape Nostalgia", recipients: "Kenichi Fujimoto"],
        [films: "A Samurai Chronicle", recipients: "Masato Yano"]
      ]
    ],
    [
      name: "Best Editing",
      nominees: [
        [films: "The Eternal Zero", recipients: "Ryuji Miyajima"],
        [films: "A Samurai Chronicle", recipients: "Hideto Aga"],
        [films: "The Little House", recipients: "Iwao Ishii"],
        [films: "Cape Nostalgia", recipients: "Hideaki Ohata"],
        [films: "Pale Moon", recipients: "Takashi Sato"]
      ]
    ],
    [
      name: "Best Foreign Picture",
      nominees: [
        [films: "Frozen"],
        [films: "Interstellar"],
        [films: "Jersey Boys"],
        [films: "Fury"],
        [films: "Godzilla"]
      ]
    ]
  ],
  non_competitive_awards: [
    [
      name: "New Actor Awards",
      nominees: [
        [films: "Lady Maiko", recipients: "Mone Kamishiraishi"],
        [films: "The World of Kanako", recipients: "Nana Komatsu"],
        [films: "Hot Road", recipients: "Rena Nonen"],
        [films: ["Pale Moon", "Love's Whirlpool", "Our Family"], recipients: "Sosuke Ikematsu"],
        [films: "Hot Road", recipients: "Hiromi Tosaka"],
        [films: ["In the Hero", "As the Gods Will", "Say I Love You"], recipients: "Sota Fukushi"]
      ]
    ],
    [
      name: "Special Award of the Association",
      nominees: [
        [recipients: "Kazumi Osaka", notes: "Set Decorator"],
        [recipients: "Tsuneo Soga", notes: "Wig Maker"],
        [recipients: "Masashi Tara", notes: "Sound Engineer"]
      ]
    ],
    [
      name: "Chairman's Special Award",
      nominees: [
        [recipients: "Akira Suzuki", notes: "Editor, d. Apr 17, aged 85"],
        [recipients: "Norifumi Suzuki", notes: "Director, d. May 15, aged 80"],
        [recipients: "Fujio Morita", notes: "Cinematographer, d. Jun 11, aged 86"],
        [recipients: "Yoshiko Yamaguchi", notes: "Actress, d. Sep 7, aged 94"],
        [recipients: "Bunta Sugawara", notes: "Actor, d. Nov 28, aged 81"]
      ]
    ],
    [
      name: "Popularity Awards",
      nominees: [
        [films: ["Rurouni Kenshin: Kyoto Inferno", "Rurouni Kenshin: The Legend Ends"]],
        [films: "The Eternal Zero", recipients: "Junichi Okada"]
      ]
    ]
  ],
  in_memoriam: [
    [recipients: "Hisayuki Miyamoto", notes: "Sound Recordist, d. Jan 8, aged 71"],
    [recipients: "Keiko Awaji", notes: "Actress, d. Jan 11, aged 80"],
    [recipients: "Koichi Tsunoda", notes: "Animator, d. Jan 14, aged 74"],
    [recipients: "Masaya Takahashi", notes: "Actor, d. Jan 16, aged 83"],
    [recipients: "Ichiro Nagai", notes: "Voice Actor, d. Jan 27, aged 82"],
    [recipients: "Shoji Yasui", notes: "Actor, d. Mar 5, aged 85"],
    [recipients: "Ken Utsui", notes: "Actor, d. Mar 14, aged 82"],
    [recipients: "Nobumasa Mizuno", notes: "Cinematographer, d. Mar 22, aged 80"],
    [recipients: "Setsu Asakura", notes: "Art Director, d. Mar 27, aged 91"],
    [recipients: "Keizo Kanie", notes: "Actor, d. Mar 30, aged 69"],
    [recipients: "Junichi Watanabe", notes: "Author, d. Apr 30, aged 80"],
    [recipients: "Ryuzo Hayashi", notes: "Actor, d. Jun 4, aged 80"],
    [recipients: "Noritaka Sakamoto", notes: "Cinematographer, d. Jun 13, aged 79"],
    [recipients: "Hiroaki Fuji", notes: "Producer, d. Jun 21, aged 86"],
    [recipients: "Chusei Sone", notes: "Director, d. Aug 26, aged 76"],
    [recipients: "Masakuni Yonekura", notes: "Actor, d. Aug 26, aged 80"],
    [recipients: "Kazuo Baba", notes: "Producer, d. Oct 1, aged 91"],
    [recipients: "Anna Nakagawa", notes: "Actress, d. Oct 17, aged 49"],
    [recipients: "Kokinji Katsura", notes: "Actor, d. Nov 3, aged 88"],
    [recipients: "Hajime Okayasu", notes: "Editor, d. Nov 5, aged 77"],
    [recipients: "Koichi Kawakita", notes: "Special Effects Director, d. Dec 5, aged 72"],
    [recipients: "Takao Saito", notes: "Cinematographer, d. Dec 6, aged 85"],
    [recipients: "Yukichi Shinada", notes: "Film Critic, d. Dec 13, aged 84"]
  ]
]
