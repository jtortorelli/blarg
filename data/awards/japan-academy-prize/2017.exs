[
  name: "Japan Academy Prize",
  ordinal: "40th",
  date: ~D[2017-03-03],
  hosts: ["Toshiyuki Nishida", "Sakura Ando"],
  competitive_awards: [
    [
      name: "Best Picture",
      nominees: [
        [films: "Shin Godzilla"],
        [films: "Rage"],
        [films: "What a Wonderful Family!"],
        [films: "Her Love Boils Bathwater"],
        [films: "64: Part 1"]
      ]
    ],
    [
      name: "Best Animated Picture",
      nominees: [
        [films: "In This Corner of the World"],
        [films: "Your Name"],
        [films: "A Silent Voice"],
        [films: "Rudolph the Black Cat"],
        [films: "One Piece Film: Gold"]
      ]
    ],
    [
      name: "Best Director",
      nominees: [
        [films: "Shin Godzilla", recipients: ["Hideaki Anno", "Shinji Higuchi"]],
        [films: "Your Name", recipients: "Makoto Shinkai"],
        [films: "64: Part 1", recipients: "Takahisa Zeze"],
        [films: "Her Love Boils Bathwater", recipients: "Ryota Nakano"],
        [films: "Rage", recipients: "Lee Sang-il"]
      ]
    ],
    [
      name: "Best Screenplay",
      nominees: [
        [films: "Your Name", recipients: "Makoto Shinkai"],
        [films: "Her Love Boils Bathwater", recipients: "Ryota Nakano"],
        [films: "64: Part 1", recipients: ["Shinichi Hisamatsu", "Takahisa Zeze"]],
        [films: "What a Wonderful Family!", recipients: ["Yoji Yamada", "Emiko Hiramatsu"]],
        [films: "Rage", recipients: "Lee Sang-il"]
      ]
    ],
    [
      name: "Best Leading Actor",
      nominees: [
        [films: "64: Part 1", recipients: "Koichi Sato"],
        [films: "Twisted Justice", recipients: "Go Ayano"],
        [films: "A Man Called Pirate", recipients: "Junichi Okada"],
        [films: "Shin Godzilla", recipients: "Hiroki Hasegawa"],
        [films: "Satoshi: A Move for Tomorrow", recipients: "Kenichi Matsuyama"]
      ]
    ],
    [
      name: "Best Leading Actress",
      nominees: [
        [films: "Her Love Boils Bathwater", recipients: "Rie Miyazawa"],
        [films: "Black Widow Business", recipients: "Shinobu Otake"],
        [films: "A Bride for Rip Van Winkle", recipients: "Haru Kuroki"],
        [films: "Chihayafuru Part 1", recipients: "Suzu Hirose"],
        [films: "Rage", recipients: "Aoi Miyazaki"]
      ]
    ],
    [
      name: "Best Supporting Actor",
      nominees: [
        [films: "Rage", recipients: "Satoshi Tsumabuki"],
        [films: "The Long Excuse", recipients: "Takehara Pistol"],
        [films: "Satoshi: A Move for Tomorrow", recipients: "Masahiro Higashide"],
        [films: "Rage", recipients: "Mirai Moriyama"],
        [films: "Scoop!", recipients: "Lily Franky"]
      ]
    ],
    [
      name: "Best Supporting Actress",
      nominees: [
        [films: "Her Love Boils Bathwater", recipients: "Hana Sugisaki"],
        [films: "Shin Godzilla", recipients: "Satomi Ishihara"],
        [films: "Shin Godzilla", recipients: "Mikako Ichikawa"],
        [films: "Rage", recipients: "Suzu Hirose"],
        [films: "Birthday Card", recipients: "Aoi Miyazaki"]
      ]
    ],
    [
      name: "Best Music",
      nominees: [
        [films: "Your Name", recipients: "Radwimps"],
        [films: "In This Corner of the World", recipients: "Kotringo"],
        [films: "Shin Godzilla", recipients: "Shiro Sagisu"],
        [films: "A Man Called Pirate", recipients: "Naoki Sato"],
        [films: "64: Part 1", recipients: "Takatsugu Muramatsu"]
      ]
    ],
    [
      name: "Best Photography",
      nominees: [
        [films: "Shin Godzilla", recipients: "Kosuke Yamada"],
        [films: "Rage", recipients: "Norimichi Kasamatsu"],
        [films: "64: Part 1", recipients: "Koichi Saito"],
        [films: "A Man Called Pirate", recipients: "Kozo Shibasaki"],
        [films: "What a Wonderful Family!", recipients: "Masashi Chikamori"]
      ]
    ],
    [
      name: "Best Lighting",
      nominees: [
        [films: "Shin Godzilla", recipients: "Takayuki Kawabe"],
        [films: "Rage", recipients: "Yuki Nakamura"],
        [films: "64: Part 1", recipients: "Meicho Tomiyama"],
        [films: "A Man Called Pirate", recipients: "Nariyuki Ueda"],
        [films: "What a Wonderful Family!", recipients: "Koichi Watanabe"]
      ]
    ],
    [
      name: "Best Art",
      nominees: [
        [films: "Shin Godzilla", recipients: ["Yuji Hayashida", "Eri Sakushima"]],
        [films: "64: Part 1", recipients: "Toshihiro Isomi"],
        [films: "What a Wonderful Family!", recipients: "Tomoko Kurata"],
        [films: "Rage", recipients: ["Yuji Tsuzuki", "Ayako Sakahara"]],
        [films: "The Magnificent Nine", recipients: "Takayuki Nitta"]
      ]
    ],
    [
      name: "Best Sound",
      nominees: [
        [films: "Shin Godzilla", recipients: ["Jun Nakamura", "Haru Yamada"]],
        [films: "What a Wonderful Family!", recipients: "Kazumi Kishida"],
        [films: "Rage", recipients: "Mitsugu Shiratori"],
        [films: "64: Part 1", recipients: "Shinya Takada"],
        [films: "A Man Called Pirate", recipients: "Kenichi Fujimoto"]
      ]
    ],
    [
      name: "Best Editing",
      nominees: [
        [films: "Shin Godzilla", recipients: ["Hideaki Anno", "Atsuki Sato"]],
        [films: "What a Wonderful Family!", recipients: "Iwao Ishii"],
        [films: "Rage", recipients: "Tsuyoshi Imai"],
        [films: "64: Part 1", recipients: "Ryo Hayano"],
        [films: "A Man Called Pirate", recipients: "Ryuji Miyajima"]
      ]
    ],
    [
      name: "Best Foreign Picture",
      nominees: [
        [films: "Sully"],
        [films: "The Martian"],
        [films: "Zootopia"],
        [films: "Star Wars: The Force Awakens"],
        [films: "The Revenant"]
      ]
    ]
  ],
  non_competitive_awards: [
    [
      name: "New Actor Awards",
      nominees: [
        [films: "Her Love Boils Bathwater", recipients: "Hana Sugisaki"],
        [films: "Evergreen Love", recipients: "Mitsuki Takahata"],
        [films: "Sailor Suit and Machine Gun: Graduation", recipients: "Kanna Hashimoto"],
        [films: "Evergreen Love", recipients: "Takanori Iwata"],
        [films: ["64: Part 1", "64: Part 2"], recipients: "Kentaro Sakaguchi"],
        [films: "Rage", recipients: "Takara Sakumoto"],
        [films: "The Magnificent Nine", recipients: "Yudai Chiba"],
        [films: ["Chihayafuru Part 1", "Chihayafuru Part 2"], recipients: "Mackenyu"]
      ]
    ],
    [
      name: "Special Award of the Association",
      nominees: [
        [recipients: "Toshiharu Aida", notes: "Set Decorator, Propmaster"],
        [recipients: "Yokozo Akamatsu", notes: "Title Designer"],
        [recipients: "Akihiko Okase", notes: "Sound Effects"]
      ]
    ],
    [
      name: "40th Anniversary Special Award",
      nominees: [
        [recipients: "Kazuo Ikehiro", notes: "Director"],
        [recipients: "Tadashi Sawashima", notes: "Director"],
        [recipients: "Kenji Hagiwara", notes: "Cinematographer"]
      ]
    ],
    [
      name: "Chairman's Achievement Award",
      nominees: [
        [recipients: "Chuji Kinoshita", notes: "Composer"],
        [recipients: "Machiko Kyo", notes: "Actress"],
        [recipients: "Seijun Suzuki", notes: "Director"],
        [recipients: "Yoshinobu Nishioka", notes: "Art Director"],
        [recipients: "Shinobu Hashimoto", notes: "Screenwriter, Producer, Director"],
        [recipients: "Kaoru Yachigusa", notes: "Actress"]
      ]
    ],
    [
      name: "Chairman's Special Award",
      nominees: [
        [recipients: "Masanobu Deme", notes: "Director"],
        [recipients: "Isao Tomita", notes: "Composer"],
        [recipients: "Zenzo Matsuyama", notes: "Director, Screenwriter"]
      ]
    ],
    [
      name: "Popularity Awards",
      nominees: [
        [films: "Your Name"],
        [films: "Evergreen Love", recipients: "Takanori Iwata"]
      ]
    ]
  ],
  in_memoriam: [
    [recipients: "Yukio Ninagawa", notes: "Director, Actor, d. May 12, aged 80"],
    [recipients: "Yumi Shirakawa", notes: "Actress, d. Jun 14, aged 79"],
    [recipients: "Akiko Kazami", notes: "Actress, d. Sep 28, aged 95"],
    [recipients: "Mikijiro Hira", notes: "Actor, d. Oct 22, aged 82"],
    [recipients: "Noriyoshi Ikeya", notes: "Art Director, d. Oct 25, aged 76"],
    [recipients: "Genjiro Arato", notes: "Producer, Director, d. Nov 7, aged 70"],
    [recipients: "Lily", notes: "Singer, Actress, d. Nov 11, aged 64"],
    [recipients: "Masaru Mori", notes: "Cinematographer, d. Nov 22, aged 86"],
    [recipients: "Jinpachi Nezu", notes: "Actor, d. Dec 29, aged 69"]
  ]
]
