[
  name: "Japan Academy Prize",
  ordinal: "39th",
  date: ~D[2016-03-04],
  hosts: ["Toshiyuki Nishida", "Rie Miyazawa"],
  competitive_awards: [
    [
      name: "Best Picture",
      nominees: [
        [films: "Our Little Sister"],
        [films: "125 Years Memory"],
        [films: "The Emperor in August"],
        [films: "Living With My Mother"],
        [films: "100 Yen Love"]
      ]
    ],
    [
      name: "Best Animated Picture",
      nominees: [
        [films: "The Boy and the Beast"],
        [films: "The Anthem of the Heart"],
        [films: "Miss Hokusai"],
        [films: "Dragon Ball Z: Resurrection 'F'"],
        [films: "Love Life! The School Idol Movie"]
      ]
    ],
    [
      name: "Best Director",
      nominees: [
        [films: "Our Little Sister", recipients: "Hirokazu Kore-eda"],
        [films: "Bakuman", recipients: "Hiroshi One"],
        [films: "100 Yen Love", recipients: "Masaharu Take"],
        [films: "125 Years Memory", recipients: "Mitsutoshi Tanaka"],
        [films: "The Emperor in August", recipients: "Masato Harada"]
      ]
    ],
    [
      name: "Best Screenplay",
      nominees: [
        [films: "100 Yen Love", recipients: "Shin Adachi"],
        [films: "125 Years Memory", recipients: "Eriko Komatsu"],
        [films: "Our Little Sister", recipients: "Hirokazu Kore-eda"],
        [films: "The Emperor in August", recipients: "Masato Harada"],
        [films: "Living With My Mother", recipients: ["Yoji Yamada", "Emiko Hiramatsu"]]
      ]
    ],
    [
      name: "Best Leading Actor",
      nominees: [
        [films: "Living With My Mother", recipients: "Kazunari Ninomiya"],
        [films: "125 Years Memory", recipients: "Seiyo Uchino"],
        [films: "Kakekomi", recipients: "Yo Oizumi"],
        [films: "Terminal", recipients: "Koichi Sato"],
        [films: "The Emperor in August", recipients: "Koji Yakusho"]
      ]
    ],
    [
      name: "Best Leading Actress",
      nominees: [
        [films: "100 Yen Love", recipients: "Sakura Ando"],
        [films: "Our Little Sister", recipients: "Haruka Ayase"],
        [films: "Flying Colors", recipients: "Kasumi Arimura"],
        [films: "Sweet Bean", recipients: "Kirin Kiki"],
        [films: "Living With My Mother", recipients: "Sayuri Yoshinaga"]
      ]
    ],
    [
      name: "Best Supporting Actor",
      nominees: [
        [films: "The Emperor in August", recipients: "Masahiro Motoki"],
        [films: "Living With My Mother", recipients: "Tadanobu Asano"],
        [films: "100 Yen Love", recipients: "Hirofumi Arai"],
        [films: "Flying Colors", recipients: "Atsushi Ito"],
        [films: "Bakuman", recipients: "Shota Sometani"],
        [films: "The Big Bee", recipients: "Masahiro Motoki"]
      ]
    ],
    [
      name: "Best Supporting Actress",
      nominees: [
        [films: "Living With My Mother", recipients: "Haru Kuroki"],
        [films: "Our Little Sister", recipients: "Kaho"],
        [films: "Our Little Sister", recipients: "Masami Nagasawa"],
        [films: "Kakekomi", recipients: "Hikari Mitsushima"],
        [films: "Flying Colors", recipients: "Yo Yoshida"]
      ]
    ],
    [
      name: "Best Music",
      nominees: [
        [films: "Bakuman", recipients: "Sakanaction"],
        [films: "125 Years Memory", recipients: "Michiru Oshima"],
        [films: "Our Little Sister", recipients: "Yoko Kanno"],
        [films: "The Emperor in August", recipients: "Harumi Fuuki"],
        [films: "Solomon's Perjury Part 1: Suspicion", recipients: "Goro Yasukawa"]
      ]
    ],
    [
      name: "Best Photography",
      nominees: [
        [films: "Our Little Sister", recipients: "Mikiya Takimoto"],
        [films: "The Emperor in August", recipients: "Takahide Shibanushi"],
        [films: "Living With My Mother", recipients: "Masashi Chikamori"],
        [films: "125 Years Memory", recipients: "Tetsuo Nagata"],
        [films: "Solomon's Perjury Part 1: Suspicion", recipients: "Junichi Fujisawa"]
      ]
    ],
    [
      name: "Best Lighting",
      nominees: [
        [films: "Our Little Sister", recipients: "Norikiyo Fujii"],
        [films: "The Emperor in August", recipients: "Takaaki Miyanishi"],
        [films: "Living With My Mother", recipients: "Koichi Watanabe"],
        [films: "125 Years Memory", recipients: "Kiyoto Ando"],
        [films: "Solomon's Perjury Part 1: Suspicion", recipients: "Masao Kanazawa"]
      ]
    ],
    [
      name: "Best Art",
      nominees: [
        [films: "125 Years Memory", recipients: "Hidefumi Hanatani"],
        [films: "Bakuman", recipients: "Yuji Tsuzuki"],
        [films: "Living With My Mother", recipients: "Mitsuo Degawa"],
        [films: "The Emperor in August", recipients: "Tetsuo Harada"],
        [films: "Our Little Sister", recipients: "Keiko Mitsumatsu"]
      ]
    ],
    [
      name: "Best Sound",
      nominees: [
        [films: "125 Years Memory", recipients: "Nobuhiko Matsukage"],
        [films: "Living With My Mother", recipients: "Kazumi Kishida"],
        [films: "Our Little Sister", recipients: "Yutaka Tsurumaki"],
        [films: "The Emperor in August", recipients: "Yasumasa Terui", notes: "Recording"],
        [films: "The Emperor in August", recipients: "Masato Yano", notes: "Dubbing"],
        [films: "Bakuman", recipients: "Shinji Watanabe"]
      ]
    ],
    [
      name: "Best Editing",
      nominees: [
        [films: "Bakuman", recipients: "Yasuyuki Ozeki"],
        [films: "Living With My Mother", recipients: "Iwai Ishii"],
        [films: "125 Years Memory", recipients: "Akimasa Kawashima"],
        [films: "Our Little Sister", recipients: "Hirokazu Kore-eda"],
        [films: "The Emperor in August", recipients: "Eugene Harada"]
      ]
    ],
    [
      name: "Best Foreign Picture",
      nominees: [
        [films: "American Sniper"],
        [films: "Kingsman"],
        [films: "Whiplash"],
        [films: "Mad Max: Fury Road"],
        [films: "Spectre"]
      ]
    ]
  ],
  non_competitive_awards: [
    [
      name: "New Actor Awards",
      nominees: [
        [films: "Flying Colors", recipients: "Kasumi Arimura"],
        [films: "Orange", recipients: "Tao Tsuchiya"],
        [films: "Our Little Sister", recipients: "Suzu Hirose"],
        [
          films: ["Solomon's Perjury Part 1: Suspicion", "Solomon's Perjury Part 2: Judgment"],
          recipients: "Ryoko Fujino"
        ],
        [films: "Three Stories of Love", recipients: "Atsushi Shinohara"],
        [films: "Pieta in the Toilet", recipients: "Yojiro Noda"],
        [films: ["Orange", "Heroine Shikkaku"], recipients: "Kento Yamazaki"],
        [films: "Assassination Classroom", recipients: "Ryosuke Yamada"]
      ]
    ],
    [
      name: "Special Award of the Association",
      nominees: [
        [recipients: "Kazuo Matsuda", notes: "Costume Designer"],
        [recipients: "Gal Enterprise", notes: "Trailer Production"]
      ]
    ],
    [
      name: "Association Honor Award",
      nominees: [
        [recipients: "Tatsuya Nakadai"]
      ]
    ],
    [
      name: "Popularity Awards",
      nominees: [
        [films: "Bakuman"],
        [films: "Maku Ga Agaru", recipients: "Momoiro Clover Z"]
      ]
    ]
  ],
  in_memoriam: [
    [recipients: "Tomiko Miyao", notes: "Author, d. Dec 30, aged 88"],
    [recipients: "Yoshio Shirasaka", notes: "Screenwriter, d. Jan 2, aged 82"],
    [recipients: "Katsuya Susaki", notes: "Screenwriter, d. Jan 9, aged 93"],
    [recipients: "Yoshiyuki Kuroda", notes: "Director, d. Jan 22, aged 86"],
    [recipients: "Yuzo Irie", notes: "Association Officer, Producer, d. Jan 31, aged 84"],
    [recipients: "Mitsugoro Bando", notes: "Actor, d. Feb 21, aged 59"],
    [recipients: "Miki Sanjo", notes: "Actress, d. Apr 9, aged 86"],
    [recipients: "Kinya Aikawa", notes: "Actor, Voice Actor, Director, d. Apr 15, aged 80"],
    [recipients: "Nagare Hagiwara", notes: "Actor, d. Apr 22, aged 62"],
    [recipients: "Hiroshi Yamagata", notes: "Sound Recordist, d. Apr 26, aged 56"],
    [recipients: "Yusuke Takita", notes: "Actor, d. May 3, aged 84"],
    [recipients: "Nisan Takahashi", notes: "Screenwriter, d. May 5, aged 89"],
    [recipients: "Masayuki Imai", notes: "Actor, d. May 28, aged 54"],
    [recipients: "Hiroshi Koizumi", notes: "Actor, d. May 31, aged 88"],
    [recipients: "Tsuchitaro Hayashi", notes: "Sound Recordist, d. Jul 9, aged 93"],
    [recipients: "Keizo Kawasaki", notes: "Actor, d. Jul 21, aged 82"],
    [recipients: "Takeshi Kato", notes: "Actor, d. Jul 31, aged 86"],
    [recipients: "Setsuko Hara", notes: "Actress, d. Sep 5, aged 95"],
    [recipients: "Hisashi Yamanauchi", notes: "Screenwriter, d. Sep 29, aged 90"],
    [recipients: "Kazuo Kumakura", notes: "Actor, Voice Actor, d. Oct 12, aged 88"],
    [recipients: "Ryuzo Saki", notes: "Author, d. Oct 31, aged 78"],
    [recipients: "Haruko Kato", notes: "Actress, d. Nov 2, aged 92"],
    [recipients: "Kai Ato", notes: "Actress, d. Nov 14, aged 69"],
    [recipients: "Shigeru Mizuki", notes: "Author, d. Nov 30, aged 93"],
    [recipients: "Masao Kanazawa", notes: "Lighting Technician, d. Dec 8, aged 63"],
    [recipients: "Akiyuki Nosaka", notes: "Author, d. Dec 9, aged 85"]
  ]
]
