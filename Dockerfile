FROM elixir:latest

RUN useradd -ms /bin/bash ubuntu
USER ubuntu
WORKDIR /home/ubuntu

RUN mix local.hex --force
RUN mix local.rebar --force

ENV MIX_ENV=prod
ENV PORT=4000

ADD start.sh .

CMD ./start.sh
