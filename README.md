# The Godzilla Cineaste

A fan made website detailing a wide variety of Japanese science fiction and fantasy films.

## History
This is actually version 3 of the website.

- Version 1 @ `cineaste-phoenix` was the initial Phoenix build of the site launched in 2016 to Heroku.
- Version 2 @ `cineaste_umbrella` was the rebuild of the Phoenix app as an umbrella, attempting to use more idiomatic Elixir. This build initially continued to use PostgreSQL as the data store and then was refactored to serve data from static Elixir data modules. Development began in 2018, deployed to Amazon S3/CloudFront in 2019.
- Version 3 condensed `cineaste_umbrella` to a single app (no umbrella) and was deployed to Amazon LightSail.

Subsequent updates to Version 3 resulted in the addition of a live data reloader and the re-expansion of the project to an umbrella app to be more extensible for future endeavors (particularly sister sites).

I turned around immediately after launching version 2 and started work on version 3 when I realized that CloudFront wasn't going to be ideal for my web hosting needs and I named it `blarg` in a fit of frustration while trying to get releases to work. `Blarg` used to be the primary application name before it was renamed back to `GodzillaCineaste`.

## Running
This app is a fully self-contained website. There is no external database required. All data is loaded into ETS from `.exs`, `.csv`, and `.md` files loaded from the `/data` directory. All image resources are also contained here. All that is required to launch the site locally is your standard interactive Elixir session:

```
iex -S mix phx.server
```

## Structure
This app consists of several sub applications under an umbrella:
1. `Aratrum`: defines the "domain" structs for the site and their associated utility functions.
2. `Orga`: handles importing of data files into `SpaceHunter`, using `Aratrum` to initialize the data into Elixir structs.
1. `SpaceHunter`: manages loading data into and retrieving data from ETS.
2. `Hipac`: a reporting app that helps ensure data integrity for the denormalized data in `SpaceHunter`.
3. `GodzillaCineaste`: the Phoenix app responsible for serving up the Godzilla Cineaste website.

`Aratrum` is the base dependency for all the other apps. In addition, `Orga` depends on `Aratrum` and `SpaceHunter`, and `GodzillaCineaste` depends on `SpaceHunter`. Future Phoenix apps will also be dependent on `Aratrum` and `SpaceHunter`.

## Deploy
The site is live at https://www.godzillacineaste.net/.

The site is hosted on AWS LightSail via Docker.

There are two deploy processes in practice for different contexts:

1. Soft Deploy: or "data deploy," this is a deploy that occurs without having to reboot the app in LightSail, in which the files are updated via `git pull` and `Orga` handles updating the affected data in `SpaceHunter`, which in turn affects the appearance on the site.
2. Hard Deploy: this is a deploy that requires first bringing down the docker container, doing a `git pull`, and then bringing the container back up. This is required for any deploy that is not strictly data related (changes to page structure, application logic, etc.).