defmodule Aratrum.Entities do
  @moduledoc """
  This module contains an API for processing entity structs, e.g. either a `Aratrum.Entities.Person` or `Aratrum.Entities.Group`.
  """

  alias Aratrum.Entities.{Group, Person}

  def derive_entity_display_name(%Group{} = group) do
    Group.derive_display_name(group)
  end

  def derive_entity_display_name(%Person{} = person) do
    Person.derive_display_name(person)
  end

  def derive_entity_id(%Group{} = group) do
    Group.derive_id(group)
  end

  def derive_entity_id(%Person{} = person) do
    Person.derive_id(person)
  end

  def get_all_roles(%Group{} = group) do
    Group.get_all_roles(group)
  end

  def get_all_roles(%Person{} = person) do
    Person.get_all_roles(person)
  end

  def infer_entity_id(entity) when is_tuple(entity), do: {:ok, entity}
  def infer_entity_id(entity) when is_binary(entity), do: {:ok, {entity, 1}}
  def infer_entity_id(_), do: {:error, :cannot_infer}

  def is_alive?(%Person{} = person), do: Person.is_alive?(person)
  def is_alive?(_), do: false

  def new_entity(keywords) do
    case Keyword.get(keywords, :type) do
      :group -> Group.new(keywords)
      _ -> Person.new(keywords)
    end
  end

  def person_age(person) do
    Person.calculate_age(person)
  end

  def person_sort_name(person) do
    Person.derive_sort_name(person)
  end

  def person_status(person) do
    Person.status(person)
  end
end
