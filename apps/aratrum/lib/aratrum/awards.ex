defmodule Aratrum.Awards do
  @moduledoc """
  This module contains the API for initializing new `Aratrum.Awards.Ceremony` structs.
  """

  alias Aratrum.Awards.Ceremony

  def new_ceremony(keywords) do
    Ceremony.new(keywords)
  end
end
