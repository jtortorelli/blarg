defmodule Aratrum.Entities.SelectedFilmographyFilm do
  @moduledoc """
  This module describes a struct containing selected filmography film data.
  """

  alias Aratrum.DomainUtil
  defstruct ~w[film characters]a

  def new(keywords) do
    struct!(__MODULE__, format_fields(keywords))
  end

  defp format_fields(keywords) do
    keywords
    |> DomainUtil.wrap_field(:characters)
  end
end
