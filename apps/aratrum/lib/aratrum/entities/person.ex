defmodule Aratrum.Entities.Person do
  @moduledoc """
  This module desribes a struct containing person data and the helper functions for processing a `Aratrum.Entities.Person` struct.
  """
  alias Aratrum.{DomainUtil, PartialDate}
  alias Aratrum.Entities.{Person, SelectedFilmography}
  defstruct ~w[
    aliases
    bio
    birth_name
    birth_place
    death_place
    dob
    dod
    draft
    family_name
    given_name
    japanese_name
    middle_name
    original_name
    path
    qualifier
    selected_filmography
    stage_name
    top_roles
    type
  ]a

  def new(keywords) do
    struct!(__MODULE__, format_fields(keywords))
  end

  defp format_fields(keywords) do
    keywords
    |> DomainUtil.wrap_field(:aliases)
    |> initialize_partial_date(:dob)
    |> initialize_partial_date(:dod)
    |> initialize_selected_filmography()
  end

  defp initialize_partial_date(keywords, key) do
    case keywords[key] do
      nil ->
        keywords

      val ->
        partial_date = PartialDate.new(val)
        Keyword.put(keywords, key, partial_date)
    end
  end

  defp initialize_selected_filmography(keywords) do
    case keywords[:selected_filmography] do
      nil ->
        keywords

      selected_filmography ->
        initialized = SelectedFilmography.new(selected_filmography)
        Keyword.put(keywords, :selected_filmography, initialized)
    end
  end

  def derive_id(%{qualifier: qualifier} = person) when not is_nil(qualifier),
    do: {derive_display_name(person), qualifier}

  def derive_id(person), do: {derive_display_name(person), 1}

  def derive_display_name(person) do
    person
    |> DomainUtil.struct_to_map()
    |> _derive_display_name()
  end

  defp _derive_display_name(%{stage_name: stage_name}), do: stage_name

  defp _derive_display_name(%{
         given_name: given_name,
         family_name: family_name,
         middle_name: middle_name
       }),
       do: "#{given_name} #{middle_name} #{family_name}"

  defp _derive_display_name(%{given_name: given_name, family_name: family_name}),
    do: "#{given_name} #{family_name}"

  def derive_sort_name(person) do
    person
    |> DomainUtil.struct_to_map()
    |> _derive_sort_name()
  end

  defp _derive_sort_name(%{
         given_name: given_name,
         family_name: family_name,
         middle_name: middle_name
       }) do
    ["#{family_name}", "#{given_name} #{middle_name}"]
  end

  defp _derive_sort_name(%{
         given_name: given_name,
         family_name: family_name
       }) do
    ["#{family_name}", "#{given_name}"]
  end

  defp _derive_sort_name(%{
         stage_name: stage_name
       }) do
    ["#{stage_name}"]
  end

  def is_alive?(person) do
    not is_nil(person.dob) and is_nil(person.dod)
  end

  def status(person) do
    cond do
      not is_nil(person.dob) and is_nil(person.dod) ->
        :alive

      not is_nil(person.dob) and not is_nil(person.dod) and PartialDate.is_unknown?(person.dod) ->
        :deceased_unknown_date

      not is_nil(person.dob) and not is_nil(person.dod) ->
        :deceased

      true ->
        :unknown
    end
  end

  def calculate_age(person) do
    case Person.status(person) do
      :alive -> PartialDate.calculate_diff_from_today(person.dob)
      :deceased -> PartialDate.calculate_diff_in_years(person.dob, person.dod)
      _ -> nil
    end
  end

  def get_all_roles(person) do
    person
    |> comprehend_roles()
    |> List.flatten()
  end

  defp comprehend_roles(person) do
    for role <- person.selected_filmography.roles do
      for film <- role.films do
        %{
          film: film.film,
          entity: derive_display_name(person),
          role: role.role,
          characters: film.characters
        }
      end
    end
  end
end
