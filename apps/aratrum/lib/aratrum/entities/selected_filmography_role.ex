defmodule Aratrum.Entities.SelectedFilmographyRole do
  @moduledoc """
  This module describes a struct containing selected filmography role data.
  """

  alias Aratrum.DomainUtil
  alias Aratrum.Entities.SelectedFilmographyFilm
  defstruct ~w[role films]a

  def new(keywords) do
    struct!(__MODULE__, format_fields(keywords))
  end

  defp format_fields(keywords) do
    keywords
    |> DomainUtil.initialize_collection(:films, SelectedFilmographyFilm)
  end
end
