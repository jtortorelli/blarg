defmodule Aratrum.Entities.SelectedFilmography do
  @moduledoc """
  This module describes a struct containing selected filmography data.
  """

  alias Aratrum.DomainUtil
  alias Aratrum.Entities.SelectedFilmographyRole
  defstruct ~w[roles]a

  def new(keywords) do
    struct!(__MODULE__, format_fields(keywords))
  end

  defp format_fields(keywords) do
    keywords
    |> DomainUtil.initialize_collection(:roles, SelectedFilmographyRole)
  end
end
