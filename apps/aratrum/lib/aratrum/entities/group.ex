defmodule Aratrum.Entities.Group do
  @moduledoc """
  This module describes a struct containing group data and the helper functions for processing a `Aratrum.Entities.Group` struct.
  """

  alias Aratrum.Entities.SelectedFilmography
  defstruct ~w[
    active_period
    bio
    draft
    group_name
    members
    original_name
    path
    qualifier
    selected_filmography
    top_roles
    type
  ]a

  def new(keywords) do
    struct!(__MODULE__, format_fields(keywords))
  end

  defp format_fields(keywords) do
    keywords
    |> initialize_selected_filmography()
  end

  defp initialize_selected_filmography(keywords) do
    case keywords[:selected_filmography] do
      nil ->
        keywords

      selected_filmography ->
        initialized = SelectedFilmography.new(selected_filmography)
        Keyword.put(keywords, :selected_filmography, initialized)
    end
  end

  def derive_id(%{qualifier: qualifier} = group) when not is_nil(qualifier),
    do: {derive_display_name(group), qualifier}

  def derive_id(group), do: {derive_display_name(group), 1}

  def derive_display_name(%{group_name: group_name}) when not is_nil(group_name) do
    "#{group_name}"
  end

  def get_all_roles(group) do
    group
    |> comprehend_roles()
    |> List.flatten()
  end

  defp comprehend_roles(group) do
    for role <- group.selected_filmography.roles do
      for film <- role.films do
        %{
          film: film.film,
          entity: derive_display_name(group),
          role: role.role,
          characters: film.characters
        }
      end
    end
  end
end
