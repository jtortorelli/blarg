defmodule Aratrum.PartialDate do
  @moduledoc """
  This module describes the struct for a partial date, i.e. a date which may contain a year OR a month OR a day, but not necessarily all three, OR may have an unknown value.
  """
  alias Aratrum.DomainUtil
  defstruct ~w[year month day unknown]a

  def new(keywords) do
    struct!(__MODULE__, keywords)
  end

  def is_unknown?(partial_date) do
    case partial_date.unknown do
      true -> true
      _ -> false
    end
  end

  def is_complete?(partial_date) do
    partial_date.year and partial_date.month and partial_date.day
  end

  def calculate_diff_in_years(partial_date_a, partial_date_b) do
    with a_map <- DomainUtil.struct_to_map(partial_date_a),
         b_map <- DomainUtil.struct_to_map(partial_date_b) do
      _calculate_diff_in_years(a_map, b_map)
    end
  end

  def calculate_diff_from_today(partial_date) do
    with map <- DomainUtil.struct_to_map(partial_date) do
      _calculate_diff_from_today(map)
    end
  end

  defp _calculate_diff_in_years(
         %{
           year: year_a,
           month: month_a,
           day: day_a
         },
         %{
           year: year_b,
           month: month_b,
           day: day_b
         }
       ) do
    with {:ok, date_a} <- Date.new(year_a, month_a, day_a),
         {:ok, date_b} <- Date.new(year_b, month_b, day_b) do
      Timex.diff(date_b, date_a, :years)
    end
  end

  defp _calculate_diff_in_years(
         %{
           year: year_a,
           month: month_a
         },
         %{
           year: year_b,
           month: month_b
         }
       ) do
    with {:ok, date_a} <- Date.new(year_a, month_a, 1),
         {:ok, date_b} <- Date.new(year_b, month_b, 1) do
      Timex.diff(date_b, date_a, :years)
    end
  end

  defp _calculate_diff_in_years(
         %{
           year: year_a
         },
         %{
           year: year_b
         }
       ) do
    year_b - year_a
  end

  defp _calculate_diff_from_today(%{
         year: year,
         month: month,
         day: day
       }) do
    with {:ok, date} <- Date.new(year, month, day) do
      current_date = Timex.now()
      Timex.diff(current_date, date, :years)
    end
  end

  defp _calculate_diff_from_today(%{
         year: year,
         month: month
       }) do
    with {:ok, date} <- Date.new(year, month, 1) do
      current_date = Timex.now()
      Timex.diff(current_date, date, :years)
    end
  end

  defp _calculate_diff_from_today(%{
         year: year
       }) do
    with {:ok, date} <- Date.new(year, 1, 1) do
      current_date = Timex.now()
      Timex.diff(current_date, date, :years)
    end
  end

  def display_date(partial_date) do
    map = DomainUtil.struct_to_map(partial_date)
    _display_date(map)
  end

  defp _display_date(%{year: year, month: month, day: day}) do
    {:ok, date} = Date.new(year, month, day)
    Timex.format!(date, "{Mfull} {D}, {YYYY}")
  end

  defp _display_date(%{year: year, month: month}) do
    {:ok, date} = Date.new(year, month, 1)
    Timex.format!(date, "{Mfull}, {YYYY}")
  end

  defp _display_date(%{year: year}) do
    {:ok, date} = Date.new(year, 1, 1)
    Timex.format!(date, "{YYYY}")
  end

  defp _display_date(_) do
    nil
  end
end
