defmodule Aratrum.DomainUtil do
  @moduledoc """
  This module contains utility functions for processing and standarizing domain data.
  """

  def wrap_field(keywords, key) do
    case keywords[key] do
      [_ | _] -> keywords
      nil -> keywords
      unwrapped -> Keyword.put(keywords, key, [unwrapped])
    end
  end

  def initialize_collection(keywords, key, module) do
    case keywords[key] do
      nil ->
        keywords

      collection ->
        initialized = Enum.map(collection, &module.new(&1))
        Keyword.put(keywords, key, initialized)
    end
  end

  def struct_to_map(struct) do
    struct
    |> Map.from_struct()
    |> Enum.filter(fn {_, v} -> v != nil end)
    |> Enum.into(%{})
  end
end
