defmodule Aratrum.Awards.CompetitiveAward do
  @moduledoc """
  This module describes a struct containing competitive award data.
  """

  alias Aratrum.Awards.Nominee
  alias Aratrum.DomainUtil
  defstruct ~w[name winner nominees]a

  def new(keywords) do
    struct!(__MODULE__, format_fields(keywords))
  end

  defp format_fields(keywords) do
    keywords
    |> DomainUtil.wrap_field(:nominees)
    |> DomainUtil.initialize_collection(:nominees, Nominee)
    |> set_winner()
  end

  # by convention the first in the list of nominees is the winner of the competitive award
  defp set_winner(keywords) do
    with [winner | rest] <- keywords[:nominees] do
      keywords
      |> Keyword.put(:winner, winner)
      |> Keyword.put(:nominees, rest)
    end
  end
end
