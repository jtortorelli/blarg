defmodule Aratrum.Awards.Ceremony do
  @moduledoc """
  This module describes a struct containing award ceremony data.
  """

  alias Aratrum.DomainUtil
  alias Aratrum.Awards.{CompetitiveAward, Nominee, NonCompetitiveAward}
  defstruct ~w[name date ordinal hosts competitive_awards non_competitive_awards in_memoriam]a

  def new(keywords) do
    struct!(__MODULE__, format_fields(keywords))
  end

  defp format_fields(keywords) do
    keywords
    |> DomainUtil.initialize_collection(:competitive_awards, CompetitiveAward)
    |> DomainUtil.initialize_collection(:non_competitive_awards, NonCompetitiveAward)
    |> DomainUtil.initialize_collection(:in_memoriam, Nominee)
  end
end
