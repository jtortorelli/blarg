defmodule Aratrum.Awards.NonCompetitiveAward do
  @moduledoc """
  This module describes a struct containing non competitive award data.
  """

  alias Aratrum.Awards.Nominee
  alias Aratrum.DomainUtil
  defstruct ~w[name nominees]a

  def new(keywords) do
    struct!(__MODULE__, format_fields(keywords))
  end

  defp format_fields(keywords) do
    keywords
    |> DomainUtil.wrap_field(:nominees)
    |> DomainUtil.initialize_collection(:nominees, Nominee)
  end
end
