defmodule Aratrum.Awards.Nominee do
  @moduledoc """
  This module describes a struct containing nominee data.
  """

  alias Aratrum.DomainUtil
  defstruct ~w[films recipients notes]a

  def new(keywords) do
    struct!(__MODULE__, format_fields(keywords))
  end

  defp format_fields(keywords) do
    keywords
    |> DomainUtil.wrap_field(:films)
    |> DomainUtil.wrap_field(:recipients)
  end
end
