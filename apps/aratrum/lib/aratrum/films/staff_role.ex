defmodule Aratrum.Films.StaffRole do
  @moduledoc """
  This module describes a struct containing staff role data.
  """

  alias Aratrum.DomainUtil
  defstruct ~w[roles people]a

  def new(keywords) do
    struct!(__MODULE__, format_fields(keywords))
  end

  defp format_fields(keywords) do
    keywords
    |> DomainUtil.wrap_field(:roles)
    |> DomainUtil.wrap_field(:people)
  end
end
