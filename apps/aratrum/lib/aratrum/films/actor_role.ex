defmodule Aratrum.Films.ActorRole do
  @moduledoc """
  This module describes a struct containing actor role data.
  """

  alias Aratrum.DomainUtil
  defstruct ~w[actor characters]a

  def new(keywords) do
    struct!(__MODULE__, format_fields(keywords))
  end

  defp format_fields(keywords) do
    keywords
    |> DomainUtil.wrap_field(:characters)
  end
end
