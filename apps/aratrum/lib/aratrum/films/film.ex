defmodule Aratrum.Films.Film do
  @moduledoc """
  This module describes a struct for representing film data and contains helper functions for processing `Aratrum.Films.Film` structs.
  """
  alias Aratrum.DomainUtil
  alias Aratrum.Films.{ActorRole, StaffRole}
  defstruct ~w[
    aliases
    credits
    draft
    followed_by
    original_title
    other_cast
    path
    preceded_by
    produced_by
    release_date
    runtime
    sort
    staff
    synopsis
    title
    top_billed_cast
    translation
    transliteration
  ]a

  def new(keywords) do
    struct!(__MODULE__, format_fields(keywords))
  end

  defp format_fields(keywords) do
    keywords
    |> DomainUtil.wrap_field(:produced_by)
    |> DomainUtil.wrap_field(:aliases)
    |> DomainUtil.initialize_collection(:staff, StaffRole)
    |> DomainUtil.initialize_collection(:top_billed_cast, ActorRole)
    |> DomainUtil.initialize_collection(:other_cast, ActorRole)
    |> initialize_sort_field()
  end

  defp initialize_sort_field(keywords) do
    path = Keyword.get(keywords, :path)

    if !!path do
      Keyword.put(keywords, :sort, String.slice(path, 0..-6))
    else
      keywords
    end
  end

  def derive_film_id(%{title: title, release_date: release_date}),
    do: {title, release_date.year}

  def get_all_roles(film) do
    film
    |> comprehend_roles()
    |> List.flatten()
  end

  defp comprehend_roles(film) do
    comprehend_staff_roles(film) ++ comprehend_actor_roles(film)
  end

  defp comprehend_staff_roles(film) do
    for staff <- film.staff do
      for role <- staff.roles do
        for entity <- staff.people do
          %{film: derive_film_id(film), entity: entity, role: role, characters: nil}
        end
      end
    end
  end

  defp comprehend_actor_roles(film) do
    for role <- get_all_acting_roles(film) do
      %{film: derive_film_id(film), entity: role.actor, role: "Cast", characters: role.characters}
    end
  end

  defp get_all_acting_roles(film) do
    cond do
      not is_nil(film.top_billed_cast) and not is_nil(film.other_cast) ->
        film.top_billed_cast ++ film.other_cast

      not is_nil(film.top_billed_cast) ->
        film.top_billed_cast

      true ->
        []
    end
  end
end
