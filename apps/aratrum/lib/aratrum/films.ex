defmodule Aratrum.Films do
  @moduledoc """
  This module contains the API for creating and processing `Aratrum.Films.Film` structs.
  """

  alias Aratrum.Films.Film

  def new_film(keywords) do
    Film.new(keywords)
  end

  def derive_film_id(film) do
    Film.derive_film_id(film)
  end

  def get_all_roles(film) do
    Film.get_all_roles(film)
  end
end
