defmodule Aratrum do
  @moduledoc """
  This module contains an API for creating and processing structs in the domain layer.
  """

  alias Aratrum.{Awards, Entities, Films, PartialDate}

  def derive_entity_display_name(entity) do
    Entities.derive_entity_display_name(entity)
  end

  def derive_entity_id(entity) do
    Entities.derive_entity_id(entity)
  end

  def derive_film_id(film) do
    Films.derive_film_id(film)
  end

  def entity_is_alive?(entity) do
    Entities.is_alive?(entity)
  end

  def entity_roles(entity) do
    Entities.get_all_roles(entity)
  end

  def film_roles(film) do
    Films.get_all_roles(film)
  end

  def infer_entity_id(entity) do
    Entities.infer_entity_id(entity)
  end

  def new_ceremony(keywords) do
    Awards.new_ceremony(keywords)
  end

  def new_entity(keywords) do
    Entities.new_entity(keywords)
  end

  def new_film(keywords) do
    Films.new_film(keywords)
  end

  def partial_date_display(partial_date) do
    PartialDate.display_date(partial_date)
  end

  def person_age(person) do
    Entities.person_age(person)
  end

  def person_sort_name(person) do
    Entities.person_sort_name(person)
  end

  def person_status(person) do
    Entities.person_status(person)
  end
end
