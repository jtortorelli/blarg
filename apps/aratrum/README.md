# Aratrum

The app where the domain lives for the `XCineaste` projects. Named for the human mothership from the Godzilla anime films.

## Domain
### Types
`Aratrum` manages three (3) primary classes of domain structs (subject to change):

1. Films
2. Entities (People, Groups, etc.)
3. Awards

Future domain structs could potentially include TV Series or more diverse kinds of Awards (which will necessitate the necessary refactors)

### Working with types
`Aratrum` exposes factory and constructor functions for generating Elixir structs from keyword lists; all domain data is stored as keyword lists in `.exs` files in this project.

`Aratrum` also exposes helper functions for processing domain structs, e.g. get all roles from a film or entity.