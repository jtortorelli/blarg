# Hipac

The reporting app for helping to maintain data integrity. Named for the model supercomputer used in the Earth Defense Force HQ in *Battle in Outer Space* (1959).

Hipac is configured to only run in dev (i.e., `MIX_ENV=dev`). It will run a series of processes to interrogate the contents of the data cache in `SpaceHunter` and determine if there are any gaps or broken linkages that need to be repaired. Reports are saved to the `/tmp` directory as plain `.txt` files.

## Reports
1. Age Report: a list of showcased people sorted in decreasing order by age. Used to periodically audit showcased people in case of recent deaths.
2. Credits Report: checks to make sure that all credits in showcased films are also reflected in the corresponding credits of showcased people.
3. Filmography Report: checks to make sure that all credits in the selected filmographies of showcased people are also reflected in the credits of the corresponding showcased films. The inverse of the credits report.
4. Unshowcased Entities: compiles a list of unshowcased entities ordered by number of credits. Useful for determining which entities are prime candidates to be showcased.