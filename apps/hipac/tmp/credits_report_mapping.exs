%{
  "Actor" => ["Cast"],
  "Actress" => ["Cast"],
  "Actresses" => ["Cast"],
  "Art Director" => ["Art Director", "Art"],
  "Co-Director" => ["Co-Director", "Associate Director"],
  "Concept Artist" => [
    "Concept Artist",
    "Battle Scene Storyboards",
    "Conceptual Design",
    "Key Art Design",
    "Monster Design"
  ],
  "VFX" => ["VFX", "Digital Visual Effects", "SFX Supervisor", "VFX Cooperation"],
  "Writer" => ["Writer", "Original Story"],
  "Cinematographer" => ["Cinematographer", "Photography"],
  "Screenwriter" => ["Screenplay"],
  "Composer" => ["Music", "Music Director"],
  "SFX Director" => ["Special Effects", "Special Effects Director"],
  "SFX Assistant Director" => ["Special Assistant Director", "Special Effects Assistant Director"],
  "SFX Art Director" => ["Special Effects Art", "Special Effects Art Director"],
  "SFX Supervisor" => ["Special Effects Supervisor"],
  "SFX Cinematographer" => ["Special Effects Photography"],
  "Physical FX Assistant" => ["Physical Effects Assistant"],
  "SFX Co-Director" => ["Special Effects Co-Director"],
  "Physical FX" => ["Physical Effects"],
  "Co-Screenwriter" => ["Screenplay Cooperation"],
  "Planning Coordinator" => ["Planning Cooperation"]
}
