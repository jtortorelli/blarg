MapSet.new([
  %{
    entity: "Hayao Miyazaki",
    role: "Original Story",
    film: {"The Wind Rises", 2013},
    characters: nil
  },
  %{entity: "Hayao Miyazaki", role: "Original Story", film: {"Ponyo", 2008}, characters: nil},
  %{
    entity: "Hayao Miyazaki",
    role: "Original Story",
    film: {"Princess Mononoke", 1997},
    characters: nil
  },
  %{
    entity: "Hayao Miyazaki",
    role: "Original Story",
    film: {"Nausicaä of the Valley of the Wind", 1984},
    characters: nil
  },
  %{
    entity: "Hayao Miyazaki",
    role: "Original Story",
    film: {"My Neighbor Totoro", 1988},
    characters: nil
  },
  %{
    entity: "Hayao Miyazaki",
    role: "Original Story",
    film: {"Castle in the Sky", 1986},
    characters: nil
  },
  %{
    entity: "Hayao Miyazaki",
    role: "Original Story",
    film: {"Porco Rosso", 1992},
    characters: nil
  },
  %{
    entity: "Hayao Miyazaki",
    role: "Original Story",
    film: {"Spirited Away", 2001},
    characters: nil
  },
  %{
    entity: "Akira Kurosawa",
    role: "Original Screenplay",
    film: {"Hidden Fortress: The Last Princess", 2008},
    characters: nil
  },
  %{
    entity: "Akira Kurosawa",
    role: "Screenplay",
    film: {"Tsubaki Sanjuro", 2007},
    characters: nil
  }
])
