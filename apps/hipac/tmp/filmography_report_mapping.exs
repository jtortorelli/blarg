%{
  "Art" => ["Art Director"],
  "Associate Director" => ["Co-Director"],
  "Battle Scene Storyboards" => ["Concept Artist"],
  "Cast" => ["Actor", "Actress", "Actresses"],
  "Conceptual Design" => ["Concept Artist"],
  "Digital Visual Effects" => ["VFX"],
  "Key Art Design" => ["Concept Artist"],
  "Monster Design" => ["Concept Artist"],
  "Original Story" => ["Writer"],
  "Photography" => ["Cinematographer"],
  "Physical Effects" => ["Physical FX"],
  "Physical Effects Assistant" => ["Physical FX Assistant"],
  "Planning Cooperation" => ["Planning Coordinator"],
  "Screenplay" => ["Screenwriter"],
  "Screenplay Cooperation" => ["Co-Screenwriter"],
  "SFX Supervisor" => ["VFX"],
  "Special Assistant Director" => ["SFX Assistant Director"],
  "Special Effects Art" => ["SFX Art Director"],
  "Special Effects Assistant Director" => ["SFX Assistant Director"],
  "Special Effects" => ["SFX Director"],
  "Special Effects Co-Director" => ["SFX Co-Director"],
  "Special Effects Director" => ["SFX Director"],
  "Special Effects Photography" => ["SFX Cinematographer"],
  "Special Effects Supervisor" => ["SFX Supervisor"],
  "Music" => ["Composer"],
  "Music Director" => ["Composer"],
  "VFX Cooperation" => ["VFX"]
}
