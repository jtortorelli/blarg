defmodule Hipac do
  @moduledoc """
  This module contains the API for interacting with `Hipac` the reporting module.
  """

  def report do
    Hipac.MasterReporter.report()
  end
end
