defmodule Hipac.Application do
  @moduledoc """
  This module defines the supervision tree for `Hipac` and starts the prerequisite child processes.
  """

  use Application

  def start(_type, _args) do
    children = [
      {Hipac.MasterReporter, []}
    ]

    opts = [strategy: :one_for_one, name: Hipac.Supervisor]
    Supervisor.start_link(children, opts)
  end
end
