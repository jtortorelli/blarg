defmodule Hipac.Publisher do
  @moduledoc """
  This module handles writing report findings to disk.
  """

  require Logger

  def publish_report(findings, report_path, write_function) do
    Logger.debug(findings)
    Logger.debug(report_path)

    File.open(report_path, [:write, :utf8], fn file ->
      IO.puts(file, inspect(DateTime.utc_now()))
      write_function.(findings, file)
    end)
  end
end
