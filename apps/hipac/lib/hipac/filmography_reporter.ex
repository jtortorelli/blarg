defmodule Hipac.FilmographyReporter do
  @moduledoc """
  This module generates a report of credits listed in film records that are not also listed in the corresponding entity records.
  """
  alias Hipac.{Evaluator, Publisher, Reporter}
  require Logger
  @behaviour Reporter
  @filmography_report_path Path.join(File.cwd!(), "tmp/filmography-report.txt")
  @filmography_report_mapping Path.join(File.cwd!(), "tmp/filmography_report_mapping.exs")
  @filmography_report_exemptions Path.join(File.cwd!(), "tmp/filmography_report_exemptions.exs")

  @impl Reporter
  def take_report do
    exemptions = Evaluator.evaluate(@filmography_report_exemptions)

    SpaceHunter.get_all_films()
    |> Enum.map(&Task.async(fn -> Aratrum.film_roles(&1) end))
    |> Enum.flat_map(&Task.await(&1))
    |> Enum.filter(&is_not_exempt(&1, exemptions))
    |> Enum.group_by(& &1.entity)
    |> Enum.map(&Task.async(fn -> validate_filmography(&1) end))
    |> Enum.map(&Task.await(&1))
    |> Enum.filter(fn {_k, v} -> v != [] end)
    |> Publisher.publish_report(@filmography_report_path, &write_function/2)
  end

  defp write_function(findings, file) do
    findings
    |> Enum.each(fn {k, v} ->
      IO.puts(file, inspect(k))
      Enum.each(v, &IO.puts(file, "\t #{inspect(&1)}"))
    end)
  end

  defp is_not_exempt(role, exemptions) do
    not MapSet.member?(exemptions, role)
  end

  defp validate_filmography({entity_name, film_roles}) do
    with {:ok, inferred_entity_id} <- Aratrum.infer_entity_id(entity_name),
         {:ok, entity} <- SpaceHunter.get_person(inferred_entity_id),
         entity_roles <- Aratrum.entity_roles(entity),
         role_mapping <- Evaluator.evaluate(@filmography_report_mapping) do
      {entity_name, filter_roles(entity_roles, film_roles, role_mapping)}
    else
      _ -> {entity_name, []}
    end
  end

  defp filter_roles(entity_roles, film_roles, role_mapping) do
    Enum.filter(
      film_roles,
      &(not Enum.any?(role_exists_for_entity(entity_roles, &1, role_mapping)))
    )
  end

  defp role_exists_for_entity(entity_roles, film_role, role_mapping) do
    Logger.debug("entity_roles: #{inspect(entity_roles)}")
    Logger.debug("film_role: #{inspect(film_role)}")

    for mapping <- Map.get(role_mapping, film_role.role, [film_role.role]) do
      Enum.member?(entity_roles, %{film_role | role: mapping})
    end
  end
end
