defmodule Hipac.AgeReporter do
  @moduledoc """
  This module generates a report of living people ordered by age.
  """

  alias Hipac.{Publisher, Reporter}
  require Logger
  @behaviour Reporter
  @age_report_path Path.join(File.cwd!(), "tmp/age-report.txt")

  @impl Reporter
  def take_report do
    SpaceHunter.get_all_people()
    |> Enum.filter(&Aratrum.entity_is_alive?(&1))
    |> Enum.map(&{Aratrum.derive_entity_display_name(&1), Aratrum.person_age(&1)})
    |> Enum.sort_by(&{elem(&1, 1), elem(&1, 0)}, &>=/2)
    |> Publisher.publish_report(@age_report_path, &write_function/2)

    :ok
  end

  def write_function(findings, file) do
    Logger.debug(inspect(findings))

    findings
    |> Enum.each(fn {k, v} -> IO.puts(file, "#{inspect(v)}\t#{inspect(k)}") end)
  end
end
