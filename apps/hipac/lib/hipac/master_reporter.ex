defmodule Hipac.MasterReporter do
  @moduledoc """
  This module listens for commands to take reports and delegates to individual reporter modules.
  """

  use GenServer

  require Logger

  @reporters [
    Hipac.AgeReporter,
    Hipac.UnshowcasedEntitiesReporter,
    Hipac.CreditsReporter,
    Hipac.FilmographyReporter
  ]

  def start_link(_) do
    GenServer.start_link(__MODULE__, [], name: __MODULE__)
  end

  def report do
    GenServer.cast(__MODULE__, :report)
  end

  def init(_) do
    {:ok, []}
  end

  def handle_cast(:report, state) do
    Logger.info("Starting reports")

    @reporters
    |> Enum.map(&Task.async(fn -> &1.take_report() end))
    |> Enum.each(&Task.await(&1))

    {:noreply, state}
  end
end
