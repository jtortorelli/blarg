defmodule Hipac.Reporter do
  @moduledoc """
  This module defines common behavior for all reporter modules to implement.
  """

  @callback take_report() :: :ok
end
