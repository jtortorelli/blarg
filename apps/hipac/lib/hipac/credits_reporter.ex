defmodule Hipac.CreditsReporter do
  @moduledoc """
  This module generates a report of credits listed on entity records that are not also listed on the corresponding film records.
  """
  alias Hipac.{Evaluator, Publisher, Reporter}
  require Logger
  @behaviour Reporter
  @credits_report_path Path.join(File.cwd!(), "tmp/credits-report.txt")
  @credits_report_mapping Path.join(File.cwd!(), "tmp/credits_report_mapping.exs")

  @impl Reporter
  def take_report do
    SpaceHunter.get_all_people()
    |> Enum.map(&Task.async(fn -> Aratrum.entity_roles(&1) end))
    |> Enum.flat_map(&Task.await(&1))
    |> Enum.group_by(& &1.film)
    |> Enum.map(&Task.async(fn -> validate_credits(&1) end))
    |> Enum.map(&Task.await(&1))
    |> Enum.filter(fn {_k, v} -> v != [] end)
    |> Publisher.publish_report(@credits_report_path, &write_function/2)
  end

  defp write_function(findings, file) do
    findings
    |> Enum.each(fn {k, v} ->
      IO.puts(file, inspect(k))
      Enum.each(v, &IO.puts(file, "\t #{inspect(&1)}"))
    end)
  end

  defp validate_credits({film_id, entity_roles}) do
    with {:ok, film} <- SpaceHunter.get_film(film_id),
         film_roles <- Aratrum.film_roles(film),
         role_mapping <- Evaluator.evaluate(@credits_report_mapping) do
      {film_id, filter_roles(film_roles, entity_roles, role_mapping)}
    else
      _ ->
        {film_id, []}
    end
  end

  defp filter_roles(film_roles, entity_roles, role_mapping) do
    Enum.filter(
      entity_roles,
      &(not Enum.any?(role_exists_for_film(film_roles, &1, role_mapping)))
    )
  end

  defp role_exists_for_film(film_roles, entity_role, role_mapping) do
    for mapping <- Map.get(role_mapping, entity_role.role, [entity_role.role]) do
      Enum.member?(film_roles, %{entity_role | role: mapping})
    end
  end
end
