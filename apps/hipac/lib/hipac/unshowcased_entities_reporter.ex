defmodule Hipac.UnshowcasedEntitiesReporter do
  @moduledoc """
  This module generates a report of unshowcased entities in order of prolificness.
  """

  alias Hipac.{Publisher, Reporter}
  require Logger
  @behaviour Reporter
  @unshowcased_entities_report_path Path.join(File.cwd!(), "tmp/unshowcased_entities_report.txt")

  @impl Reporter
  def take_report do
    SpaceHunter.get_all_films()
    |> Enum.map(&Task.async(fn -> Aratrum.film_roles(&1) end))
    |> Enum.flat_map(&Task.await(&1))
    |> Enum.group_by(& &1.entity, &Map.delete(&1, :entity))
    |> Enum.reduce(%{}, &count_roles_for_unshowcased_entities(&1, &2))
    |> Map.to_list()
    |> Enum.sort_by(&{elem(&1, 1), elem(&1, 0)}, &>=/2)
    |> Publisher.publish_report(@unshowcased_entities_report_path, &write_function/2)
  end

  defp write_function(findings, file) do
    findings
    |> Enum.each(fn {k, v} -> IO.puts(file, "#{inspect(v)}\t#{inspect(k)}") end)
  end

  defp count_roles_for_unshowcased_entities({entity_name, roles}, acc) do
    with {:ok, entity_id} <- Aratrum.infer_entity_id(entity_name),
         {:error, :not_found} <- SpaceHunter.get_person(entity_id),
         number_of_roles <- Enum.count(roles) do
      Map.put(acc, entity_name, number_of_roles)
    else
      _ -> acc
    end
  end
end
