let film_search_bar = $("#film-search-bar");
film_search_bar.on("keyup", function() {
    filter_films(film_search_bar.val())
});

let people_search_bar = $("#people-search-bar");
people_search_bar.on("keyup", function() {
    filter_people(people_search_bar.val())
});

function filter_films(filter) {
    let trimmed_terms = filter.replace(/\s+/g, ' ').trim().toLowerCase();
    let filmTileSelector = $('.film-tile');
    if (trimmed_terms === '') {
        filmTileSelector.css('display', 'block');
        return;
    }
    let terms = trimmed_terms.split(' ');
    filmTileSelector.each(function() {
        let titles = $(this).attr('data-aliases') === '' ? [$(this).attr('data-title')] : [$(this).attr('data-title')].concat($(this).attr('data-aliases').split('|'));
        if (titles.some(function(title) {
            return terms.every(function(term) {
                return title.toLowerCase().includes(term);
            })
        })) {
            $(this).css("display", "block");
        } else {
            $(this).css("display", "none");
        }
    });
}

function filter_people(filter) {
    let trimmed_terms = filter.replace(/\s+/g, ' ').trim().toLowerCase();
    let personTileSelector = $('.people-tile');
    if (trimmed_terms === '') {
        personTileSelector.css('display', 'block');
        return;
    }
    let terms = trimmed_terms.split(' ');
    personTileSelector.each(function() {
        let names = $(this).attr('data-names').split('|');
        if (names.some(function(name) {
            return terms.every(function(term) {
                return name.toLowerCase().includes(term);
            });
        })) {
            $(this).css("display", "block");
        } else {
            $(this).css("display", "none");
        }
    });
}