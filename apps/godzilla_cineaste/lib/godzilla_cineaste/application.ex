defmodule GodzillaCineaste.Application do
  @moduledoc false

  use Application

  def start(_type, _args) do
    children = [
      GodzillaCineasteWeb.Endpoint
    ]

    opts = [strategy: :one_for_one, name: GodzillaCineaste.Supervisor]
    Supervisor.start_link(children, opts)
  end

  def config_change(changed, _new, removed) do
    GodzillaCineasteWeb.Endpoint.config_change(changed, removed)
    :ok
  end
end
