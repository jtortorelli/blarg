defmodule GodzillaCineasteWeb.Router do
  use GodzillaCineasteWeb, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/", GodzillaCineasteWeb do
    # Use the default browser stack
    pipe_through :browser

    get("/", PageController, :index)
    get("/films", FilmController, :index)
    get("/films/:id", FilmController, :show)
    get("/people", PeopleController, :index)
    get("/people/:id", PeopleController, :show)
    get("/about", AboutController, :index)
    get("/awards", AwardsController, :index)
    get("/awards/:ceremony/:year", AwardsController, :show)
  end

  # Other scopes may use custom stacks.
  # scope "/api", GodzillaCineasteWeb do
  #   pipe_through :api
  # end
end
