defmodule GodzillaCineasteWeb.PageTitle do
  @moduledoc """
  This module derives the value to be displayed as the page title on an HTML web page.
  """
  alias GodzillaCineasteWeb.{AboutView, FilmView, PeopleView}

  require Logger

  @suffix "The Godzilla Cineaste"

  def page_title(assigns),
    do:
      assigns
      |> get
      |> put_suffix

  defp put_suffix(nil), do: @suffix
  defp put_suffix(title), do: title <> " - " <> @suffix

  defp get(%{view_module: FilmView, view_template: "index.html"}) do
    "Films"
  end

  defp get(%{view_module: FilmView, view_template: "show.html", view: film}) do
    film.title
  end

  defp get(%{view_module: PeopleView, view_template: "index.html"}) do
    "People"
  end

  defp get(%{view_module: PeopleView, view_template: "show_person.html", view: person}) do
    person.display_name
  end

  defp get(%{view_module: PeopleView, view_template: "show_group.html", view: group}) do
    group.display_name
  end

  defp get(%{view_module: PeopleView, view_template: "show.html", view: view}) do
    GodzillaCineaste.derive_entity_display_name(view)
  end

  defp get(%{view_module: AboutView, view_template: "index.html"}) do
    "About"
  end

  defp get(_), do: nil
end
