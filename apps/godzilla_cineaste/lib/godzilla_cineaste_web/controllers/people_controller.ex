defmodule GodzillaCineasteWeb.PeopleController do
  use GodzillaCineasteWeb, :controller

  def index(conn, _params) do
    with views <- GodzillaCineaste.get_all_people() do
      render(conn, "index.html", views: views)
    end
  end

  def show(conn, %{"id" => id}) do
    case GodzillaCineaste.get_person(id) do
      {:ok, view} ->
        render(conn, "show.html", view: view)

      _ ->
        conn
        |> put_status(:not_found)
        |> put_view(GodzillaCineasteWeb.ErrorView)
        |> render("404.html")
    end
  end
end
