defmodule GodzillaCineasteWeb.AboutController do
  use GodzillaCineasteWeb, :controller

  def index(conn, _params) do
    render(conn, "index.html")
  end
end
