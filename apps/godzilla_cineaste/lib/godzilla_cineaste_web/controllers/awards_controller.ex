defmodule GodzillaCineasteWeb.AwardsController do
  use GodzillaCineasteWeb, :controller

  def index(conn, _params) do
    render(conn, "index.html")
  end

  def show(conn, %{"ceremony" => "japan-academy-prize", "year" => year}) do
    numeric_year = String.to_integer(year)
    {:ok, fetched} = GodzillaCineaste.get_awards_ceremony(:japan_academy_prize, numeric_year)
    render(conn, "japan_academy_prize_ceremony.html", ceremony: fetched)
  end
end
