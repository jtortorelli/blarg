defmodule GodzillaCineasteWeb.FilmView do
  use GodzillaCineasteWeb, :view

  require Logger

  alias GodzillaCineasteWeb.{CommonView}

  @cast_thumbs_file_path "/data/images/casts"

  def display_original_title(%{
        original_title: title,
        transliteration: same,
        translation: same
      }) do
    {title, [same]}
  end

  def display_original_title(%{
        original_title: title,
        transliteration: transliteration,
        translation: translation
      }) do
    {title, [transliteration, translation]}
  end

  def display_original_title(%{original_title: title, transliteration: transliteration}) do
    {title, [transliteration]}
  end

  def display_original_title(_), do: nil

  def display_aliases(%{aliases: [_ | _] = aliases}) do
    raw(
      Enum.map(aliases, &"<em>#{&1}</em>")
      |> Enum.join(",<br/>")
    )
  end

  def display_aliases(%{aliases: a}) do
    "#{a}"
  end

  def display_roles([_ | _] = roles),
    do:
      raw(
        roles
        |> Enum.map(&"#{&1}")
        |> Enum.join("<br/>")
      )

  def display_roles_sm([_ | _] = roles), do: raw(roles |> Enum.map(&"#{&1}") |> Enum.join(", "))

  def display_staff([_ | _] = staffs) do
    raw(
      staffs
      |> Enum.map(&link_showcased_person(&1))
      |> Enum.join("<br/>")
    )
  end

  def link_showcased_person(person_id) do
    with {:ok, inferred_person_id} <- GodzillaCineaste.infer_entity_id(person_id),
         {:ok, showcased_person} <- GodzillaCineaste.get_person(inferred_person_id) do
      "<a href=\"/people/#{showcased_person.path}\">#{display_person(person_id)}</a>"
    else
      {:error, :cannot_infer} ->
        Logger.debug("Could not infer person id from #{person_id}")

        "#{display_person(person_id)}"

      {:error, :not_found} ->
        Logger.debug(
          "Could not find showcased person from inferred id #{
            GodzillaCineaste.infer_entity_id(person_id) |> elem(1) |> inspect
          }"
        )

        "#{display_person(person_id)}"
    end
  end

  def display_person({person_name, _qualifier}) do
    String.replace("#{person_name}", " ", "&nbsp")
  end

  def display_person(person_name) do
    String.replace("#{person_name}", " ", "&nbsp")
  end

  def display_studios(%{produced_by: [_ | _] = studios}),
    do:
      studios
      |> Enum.join(", ")

  def display_studios(%{produced_by: studio}), do: "#{studio}"

  def display_linked_film({title, year} = film_id) do
    case GodzillaCineaste.get_film(film_id) do
      {:ok, showcased_film} ->
        raw("<a href=\"/films/#{showcased_film.path}\"><em>#{title}</em> (#{year})</a>")

      _ ->
        raw("<em>#{title}</em> (#{year})")
    end
  end

  def display_release_date(release_date) do
    Timex.format!(release_date, "{Mfull} {D}, {YYYY}")
  end

  def render_film_tabbed_content(view) do
    tabs =
      [:overview]
      |> check_synopsis(view)
      |> check_cast(view)
      |> check_credits(view)

    CommonView.render_tabbed_content(:film, tabs, view)
  end

  defp check_synopsis(tabs, view) do
    if view.synopsis do
      tabs ++ [:synopsis]
    else
      tabs
    end
  end

  defp check_cast(tabs, view) do
    if view.top_billed_cast do
      tabs ++ [:cast]
    else
      tabs
    end
  end

  defp check_credits(tabs, view) do
    if view.credits do
      tabs ++ [:credits]
    else
      tabs
    end
  end

  def render_content(:overview, view) do
    render("overview.html", view: view)
  end

  def render_content(:synopsis, view) do
    render("synopsis.html", view: view)
  end

  def render_content(:cast, view) do
    render("cast.html", view: view)
  end

  def render_content(:credits, view) do
    render("credits.html", view: view)
  end

  def render_aliases(%{aliases: [_ | _] = aliases}),
    do: "#{Enum.join(aliases, "|")}"

  def render_aliases(%{aliases: a}), do: a
  def render_aliases(_), do: nil

  def build_cast_thumbs(person, [_ | _] = roles, path) do
    for {role, n} <- Enum.with_index(roles) do
      build_cast_thumb(person, role, n, path)
    end
  end

  def build_cast_thumbs(person, role, path) do
    [build_cast_thumb(person, role, 0, path)]
  end

  def build_cast_thumb(person, role, n, path) do
    built =
      person
      |> String.downcase()
      |> String.replace(" ", "-")
      |> String.replace(".", "")
      |> String.replace(",", "")
      |> String.replace("'", "")
      |> String.replace("é", "e")

    {role, "#{@cast_thumbs_file_path}/#{path}/#{built}-#{n}.jpg"}
  end
end
