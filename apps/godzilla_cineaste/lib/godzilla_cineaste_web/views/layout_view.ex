defmodule GodzillaCineasteWeb.LayoutView do
  use GodzillaCineasteWeb, :view
  alias GodzillaCineasteWeb.PageTitle

  def page_title(assigns) do
    PageTitle.page_title(assigns)
  end
end
