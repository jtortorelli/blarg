defmodule GodzillaCineasteWeb.PeopleView do
  use GodzillaCineasteWeb, :view

  alias GodzillaCineasteWeb.CommonView

  require Logger

  def render_person_tabbed_content(view) do
    tabs = [:bio, :selected_filmography]
    CommonView.render_tabbed_content(:person, tabs, view)
  end

  def render_group_tabbed_content(view) do
    tabs = [:bio, :selected_filmography]
    CommonView.render_tabbed_content(:group, tabs, view)
  end

  def display_name(view) do
    GodzillaCineaste.derive_entity_display_name(view)
  end

  def sort_name(view) do
    GodzillaCineaste.entity_sort_name(view)
  end

  def display_role("Chief Assistant Director") do
    "Chief AD"
  end

  def display_role("Writer") do
    "Author"
  end

  def display_role(role) do
    role
  end

  def search_names(%{type: :group} = view) do
    with group_name <- view.group_name || "",
         original_name <- view.original_name || "",
         members <- view.members || [""] do
      member_names =
        members
        |> Enum.join(" ")

      [group_name, original_name, member_names]
      |> Enum.map(&String.trim(&1))
      |> Enum.filter(&(String.length(&1) > 0))
      |> Enum.join("|")
    end
  end

  def search_names(view) do
    with given_name <- safe_get_name(view, :given_name),
         family_name <- safe_get_name(view, :family_name),
         middle_name <- safe_get_name(view, :middle_name),
         full_name <- "#{given_name} #{middle_name} #{family_name}",
         original_name <- safe_get_name(view, :original_name),
         japanese_name <- safe_get_name(view, :japanese_name),
         stage_name <- safe_get_name(view, :stage_name),
         aliases <- safe_get_name(view, :aliases),
         birth_name <- safe_get_name(view, :birth_name) do
      trimmed_full_name = String.replace(full_name, ~r/\s+/, " ")

      joined_aliases =
        case aliases do
          [_ | _] -> Enum.join(aliases, "|")
          alias -> alias
        end

      [
        trimmed_full_name,
        original_name,
        japanese_name,
        stage_name,
        joined_aliases,
        birth_name
      ]
      |> Enum.map(&String.trim(&1))
      |> Enum.filter(&(String.length(&1) > 0))
      |> Enum.join("|")
    end
  end

  defp safe_get_name(view, key) do
    Map.get(view, key) || ""
  end

  def status(view) do
    case GodzillaCineaste.entity_status(view) do
      :unknown ->
        {:unknown}

      :alive ->
        {:alive, GodzillaCineaste.entity_age(view), GodzillaCineaste.partial_date_display(view.dob)}

      :deceased_unknown_date ->
        {:deceased_unknown_date, GodzillaCineaste.partial_date_display(view.dob)}

      :deceased ->
        {:deceased, GodzillaCineaste.entity_age(view), GodzillaCineaste.partial_date_display(view.dob),
         GodzillaCineaste.partial_date_display(view.dod)}
    end
  end

  def render_content(:person, :bio, view) do
    render("bio_person.html", view: view)
  end

  def render_content(:person, :selected_filmography, view) do
    render("selected_filmography.html", view: view)
  end

  def render_content(:group, :bio, view) do
    render("bio_group.html", view: view)
  end

  def render_content(:group, :selected_filmography, view) do
    render("selected_filmography.html", view: view)
  end

  def render_selected_filmography(view) do
    CommonView.render_accordion_content(:person, view.selected_filmography.roles)
  end

  def render_roles(roles) do
    render("filmography_roles.html", roles: roles)
  end

  def display_linked_film({{title, year}, _}) do
    display_linked_film({title, year})
  end

  def display_linked_film({title, year}) do
    case GodzillaCineaste.get_film({title, year}) do
      {:ok, showcased_film} ->
        raw("<a href=\"/films/#{showcased_film.path}\"><em>#{title}</em> (#{year})</a>")

      _ ->
        raw("<em>#{title}</em> (#{year})")
    end
  end

  def display_characters_mobile(characters) when not is_nil(characters) do
    raw("<span class=\"text-muted\">#{get_characters_for_mobile_display(characters)}</span>")
  end

  def display_characters_mobile(_), do: nil

  defp get_characters_for_mobile_display(characters) do
    characters
    |> Enum.map(&("...#{&1}"))
    |> Enum.join("<br/>")
  end

  def display_characters(characters) when not is_nil(characters) do
    raw("<span class=\"text-muted\">...#{Enum.join(characters, ", ")}</span>")
  end

  def display_characters(_), do: nil

  def display_aliases(%{aliases: [_ | _] = aliases}) do
    raw(aliases |> Enum.join(",<br/>"))
  end

  def display_aliases(%{aliases: a}) do
    "#{a}"
  end

  def display_members([_ | _] = members) do
    Enum.join(members, "<br/>")
  end
end
