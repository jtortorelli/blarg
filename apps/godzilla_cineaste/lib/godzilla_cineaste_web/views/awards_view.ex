defmodule GodzillaCineasteWeb.AwardsView do
  use GodzillaCineasteWeb, :view

  alias GodzillaCineasteWeb.{CommonView}

  def render_awards_tabbed_content do
    CommonView.render_tabbed_content(:awards, [:japan_academy_prize])
  end

  def render_ceremony_tabbed_content(ceremony) do
    CommonView.render_tabbed_content(
      :ceremony,
      [:competitive, :non_competitive, :in_memoriam],
      ceremony
    )
  end

  def render_content(page) do
    awards = GodzillaCineaste.get_all_awards(page)
    render("#{page}.html", awards: awards)
  end

  def render_content(page, view) do
    render("#{page}.html", view: view)
  end

  def get_japan_academy_prize_best_picture_winner(award) do
    [best_picture_award | _rest] = award.competitive_awards
    [title] = best_picture_award.winner.films
    title
  end

  def display_films(films), do: Enum.join(films, ", ")

  def display_recipients(nil), do: ""
  def display_recipients(recipients), do: "#{Enum.join(recipients, ", ")} | "

  def display_non_competitive_recipients(nil), do: ""
  def display_non_competitive_recipients(recipients), do: Enum.join(recipients, ", ")

  def display_non_competitive_films(nil), do: ""

  def display_non_competitive_films(films),
    do: "<span class=\"font-italic\">#{Enum.join(films, ", ")}</span>"

  def display_non_competitive_recipients_and_films(%{
        recipients: [_ | _] = recipients,
        films: [_ | _] = films
      }) do
    with display_recipients <- display_non_competitive_recipients(recipients),
         display_films <- display_non_competitive_films(films) do
      Enum.join([display_recipients, display_films], " | ")
    end
  end

  def display_non_competitive_recipients_and_films(%{recipients: [_ | _] = recipients, films: nil}) do
    display_non_competitive_recipients(recipients)
  end

  def display_non_competitive_recipients_and_films(%{recipients: nil, films: [_ | _] = films}) do
    display_non_competitive_films(films)
  end

  def display_ceremony_date(date) do
    Timex.format!(date, "{Mfull} {D}, {YYYY}")
  end
end
