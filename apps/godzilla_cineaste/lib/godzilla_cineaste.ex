defmodule GodzillaCineaste do

  ###################
  # Cache Retrieval #
  ###################

  def get_all_awards(award) do
    SpaceHunter.get_all_awards(award)
  end
  def get_awards_ceremony(ceremony, year) do
    SpaceHunter.get_awards_ceremony(ceremony, year)
  end

  def get_all_films() do
    SpaceHunter.get_all_films()
  end

  def get_film(film_id) do
    SpaceHunter.get_film(film_id)
  end

  def get_all_people() do
    SpaceHunter.get_all_people()
  end

  def get_person(person_id) do
    SpaceHunter.get_person(person_id)
  end

  ##########
  # Domain #
  ##########

  def derive_entity_display_name(entity) do
    Aratrum.derive_entity_display_name(entity)
  end

  def infer_entity_id(entity) do
    Aratrum.infer_entity_id(entity)
  end

  def entity_sort_name(entity) do
    Aratrum.person_sort_name(entity)
  end

  def entity_status(entity) do
    Aratrum.person_status(entity)
  end

  def entity_age(entity) do
    Aratrum.person_age(entity)
  end

  def partial_date_display(partial_date) do
    Aratrum.partial_date_display(partial_date)
  end
end
