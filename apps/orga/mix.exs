defmodule Orga.MixProject do
  use Mix.Project

  def project do
    [
      app: :orga,
      version: "0.1.0",
      build_path: "../../_build",
      config_path: "../../config/config.exs",
      deps_path: "../../deps",
      lockfile: "../../mix.lock",
      elixir: "~> 1.10",
      start_permanent: Mix.env() == :prod,
      deps: deps()
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: [:logger],
      mod: {Orga.Application, []}
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      {:earmark, "~> 1.4"},
      {:file_system, "~> 0.2.7"},
      {:nimble_csv, "~> 0.6.0"},
      {:aratrum, in_umbrella: true},
      {:hipac, in_umbrella: true},
      {:space_hunter, in_umbrella: true}
    ]
  end
end
