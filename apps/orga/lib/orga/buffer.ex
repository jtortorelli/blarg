defmodule Orga.Buffer do
  @moduledoc """
  This module receives notifications when files are changed and schedules fresh imports from those files. This module is also responsible for the initial loading of the `SpaceHunter` cache.
  """
  use GenServer
  alias Orga.Importer

  require Logger

  def start_link(_) do
    GenServer.start_link(__MODULE__, [], name: __MODULE__)
  end

  def film_updated(path) do
    GenServer.cast(__MODULE__, {:updated_film, path})
  end

  def person_updated(path) do
    GenServer.cast(__MODULE__, {:updated_person, path})
  end

  def award_updated(path) do
    GenServer.cast(__MODULE__, {:updated_award, path})
  end

  def init(_) do
    SpaceHunter.clear_cache()
    Importer.import_films()
    Importer.import_entities()
    Importer.import_awards()
    {:ok, MapSet.new()}
  end

  def handle_cast({:updated_film, path}, state) do
    if MapSet.member?(state, path) do
      Logger.debug("#{inspect(path)} already scheduled for import!")
      {:noreply, state}
    else
      schedule_import(:film, path)
      {:noreply, MapSet.put(state, path)}
    end
  end

  def handle_cast({:updated_person, path}, state) do
    if MapSet.member?(state, path) do
      Logger.debug("#{inspect(path)} already scheduled for import!")
      {:noreply, state}
    else
      schedule_import(:people, path)
      {:noreply, MapSet.put(state, path)}
    end
  end

  def handle_cast({:updated_award, path}, state) do
    if MapSet.member?(state, path) do
      Logger.debug("#{inspect(path)} already scheduled for import!")
      {:noreply, state}
    else
      schedule_import(:awards, path)
      {:noreply, MapSet.put(state, path)}
    end
  end

  def handle_info({:import, :film, path}, state) do
    Logger.info("#{inspect(path)} is importing!")
    Importer.import_film(path)
    take_report()
    {:noreply, MapSet.delete(state, path)}
  end

  def handle_info({:import, :people, path}, state) do
    Logger.info("#{inspect(path)} is importing!")
    Importer.import_entity(path)
    take_report()
    {:noreply, MapSet.delete(state, path)}
  end

  def handle_info({:import, :awards, path}, state) do
    Logger.info("#{inspect(path)} is importing!")
    Importer.import_awards()
    take_report()
    {:noreply, MapSet.delete(state, path)}
  end

  def schedule_import(category, path) do
    Logger.info("#{inspect(path)} import scheduled!")
    Process.send_after(self(), {:import, category, path}, 1000)
  end

  defp take_report do
    if can_take_report?(), do: Hipac.report()
    :ok
  end

  defp can_take_report?() do
    Application.get_env(:orga, :reports_enabled)
  end
end
