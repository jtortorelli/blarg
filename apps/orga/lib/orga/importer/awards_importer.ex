defmodule Orga.Importer.AwardsImporter do
  @moduledoc """
  This module handles importing award data that will be stored in the cache accessed via `SpaceHunter`.
  """

  alias Orga.PathUtil
  alias Orga.Importer.{Evaluator, ImportUtil}

  def import_awards do
    with awards_path <- PathUtil.awards_path(:japan_academy_prize) do
      awards_path
      |> PathUtil.get_paths()
      |> ImportUtil.start_import_workers(awards_path, &import_awards_routine/2)
    end
  end

  defp import_awards_routine(path, base_path) do
    [base_path, path]
    |> Path.join()
    |> import_awards_routine()
  end

  defp import_awards_routine(path) do
    path
    |> Evaluator.evaluate()
    |> Aratrum.new_ceremony()
    |> insert_awards()
  end

  defp insert_awards(award) do
    SpaceHunter.insert_awards({:japan_academy_prize, award.date.year}, award)
  end
end
