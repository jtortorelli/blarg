defmodule Orga.Importer.EntityImporter do
  @moduledoc """
  This module handles importing entity data that will be stored in the cache accessed via `SpaceHunter`.
  """
  alias Orga.PathUtil
  alias Orga.Importer.{Evaluator, ImportUtil}

  def import_entity(path) do
    path
    |> PathUtil.derive_entity_data_file_path()
    |> import_entities_routine()
  end

  def import_entities do
    PathUtil.entities_path()
    |> PathUtil.get_paths()
    |> ImportUtil.start_import_workers(PathUtil.entities_path(), &import_entities_routine/2)
  end

  defp import_entities_routine(path, base_path) do
    [base_path, path]
    |> Path.join()
    |> import_entities_routine()
  end

  defp import_entities_routine(path) do
    path
    |> Evaluator.evaluate()
    |> Aratrum.new_entity()
    |> evaluate_draft_status()
    |> evaluate_bio(path)
    |> evaluate_top_roles()
    |> insert_entity()
  end

  defp evaluate_draft_status(%{draft: true}), do: %{}
  defp evaluate_draft_status(entity), do: entity

  defp evaluate_bio(%{bio: true} = entity, path) do
    PathUtil.derive_bio_file_path(path)
    |> import_bio()
    |> update_bio(entity)
  end

  defp evaluate_bio(entity, _path), do: entity

  defp import_bio(path) do
    path
    |> File.read!()
    |> Earmark.as_html!()
  end

  defp update_bio(bio, entity) do
    %{entity | bio: bio}
  end

  defp evaluate_top_roles(%{selected_filmography: selected_filmography} = entity) do
    selected_filmography.roles
    |> Enum.map(fn role -> {role.role, Enum.count(role.films)} end)
    |> Enum.sort_by(&elem(&1, 1), &>=/2)
    |> Enum.take(3)
    |> Enum.map(&elem(&1, 0))
    |> set_top_roles(entity)
  end

  defp evaluate_top_roles(entity), do: entity

  defp set_top_roles(top_roles, entity) do
    Map.put(entity, :top_roles, top_roles)
  end

  defp insert_entity(entity) when map_size(entity) != 0, do: SpaceHunter.insert_entity(entity)
  defp insert_entity(_empty_entity), do: :ok
end
