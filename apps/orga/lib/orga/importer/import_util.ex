defmodule Orga.Importer.ImportUtil do
  @moduledoc """
  This module contains utility functions to assist the data import process.
  """

  def start_import_workers(paths, base_path, routine_func) do
    paths
    |> Enum.map(&Task.async(fn -> routine_func.(&1, base_path) end))
    |> Enum.map(&Task.await(&1))
  end
end
