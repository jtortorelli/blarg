defmodule Orga.Importer.Evaluator do
  @moduledoc """
  This module contains functions for opening and evaluating files containing Elixir code.
  """

  require Logger

  def evaluate(path) do
    path
    |> Code.eval_file()
    |> elem(0)
  rescue
    _e ->
      Logger.error("#{inspect(path)} failed to evaluate!")
  end
end
