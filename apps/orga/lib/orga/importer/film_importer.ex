defmodule Orga.Importer.FilmImporter do
  @moduledoc """
  This module handles importing film data that will be stored in the cache accessed via `SpaceHunter`.
  """
  alias NimbleCSV.RFC4180, as: CSV
  alias Orga.PathUtil
  alias Orga.Importer.{Evaluator, ImportUtil}
  @blank_row ["", "", "", ""]

  def import_film(path) do
    path
    |> PathUtil.derive_film_data_file_path()
    |> import_films_routine()
  end

  def import_films do
    PathUtil.films_path()
    |> PathUtil.get_paths()
    |> ImportUtil.start_import_workers(PathUtil.films_path(), &import_films_routine/2)
  end

  defp import_films_routine(path, base_path) do
    [base_path, path]
    |> Path.join()
    |> import_films_routine()
  end

  defp import_films_routine(path) do
    path
    |> Evaluator.evaluate()
    |> Aratrum.new_film()
    |> evaluate_draft_status()
    |> evaluate_synopsis(path)
    |> evaluate_credits(path)
    |> insert_film()
  end

  defp evaluate_draft_status(%{draft: true}), do: %{}
  defp evaluate_draft_status(film), do: film

  defp evaluate_synopsis(%{synopsis: true} = film, path) do
    PathUtil.derive_synopsis_file_path(path)
    |> import_synopsis()
    |> update_synopsis(film)
  end

  defp evaluate_synopsis(film, _path), do: film

  defp import_synopsis(path) do
    path
    |> File.read!()
    |> Earmark.as_html!()
  end

  defp evaluate_credits(%{credits: true} = film, path) do
    PathUtil.derive_credits_file_path(path)
    |> import_credits()
    |> update_credits(film)
  end

  defp evaluate_credits(film, _path), do: film

  defp import_credits(path) do
    path
    |> File.read!()
    |> CSV.parse_string(skip_headers: false)
    |> filter_blank_rows()
  end

  defp filter_blank_rows(parsed) do
    parsed
    |> Enum.filter(&(&1 != @blank_row))
  end

  defp update_synopsis(synopsis, film) do
    %{film | synopsis: synopsis}
  end

  defp update_credits(credits, film) do
    %{film | credits: credits}
  end

  defp insert_film(film) when map_size(film) != 0, do: SpaceHunter.insert_film(film)
  defp insert_film(_empty_film), do: :ok
end
