defmodule Orga.PathUtil do
  @moduledoc """
  This module contains utility functions for building and manipulating the file paths to the persistent data files used to power the X Cineaste web projects.
  """
  @root File.cwd!()
  @data_root Path.expand("#{@root}../../../data")
  @films_dir "films"
  @entity_dir "people"
  @synopses_dir "synopses"
  @credits_dir "credits"
  @bios_dir "bios"
  @awards_dir "awards"

  @japan_academy_prize_dir "japan-academy-prize"

  @data_ext ".exs"
  @synposis_ext ".md"
  @credits_ext ".csv"
  @bios_ext ".md"

  def films_path, do: Path.join(@data_root, @films_dir)

  def entities_path, do: Path.join(@data_root, @entity_dir)

  def synopsis_path, do: Path.join(@data_root, @synopses_dir)

  def credits_path, do: Path.join(@data_root, @credits_dir)

  def bios_path, do: Path.join(@data_root, @bios_dir)

  def awards_path(:japan_academy_prize),
    do: Path.join([@data_root, @awards_dir, @japan_academy_prize_dir])

  def get_paths(dir) do
    dir
    |> File.ls!()
    |> Enum.filter(&(!String.starts_with?(&1, ".")))
  end

  def derive_synopsis_file_path(path) do
    synopsis_path()
    |> Path.join(replace_extension(path, @synposis_ext))
  end

  def derive_credits_file_path(path) do
    credits_path()
    |> Path.join(replace_extension(path, @credits_ext))
  end

  def derive_bio_file_path(path) do
    bios_path()
    |> Path.join(replace_extension(path, @bios_ext))
  end

  def derive_film_data_file_path(path) do
    films_path()
    |> Path.join(replace_extension(path, @data_ext))
  end

  def derive_entity_data_file_path(path) do
    entities_path()
    |> Path.join(replace_extension(path, @data_ext))
  end

  def extract_base_file_name(path) do
    Path.basename(path, Path.extname(path))
  end

  defp replace_extension(path, ext) do
    [Path.basename(path, @data_ext), ext]
    |> Enum.join()
  end
end
