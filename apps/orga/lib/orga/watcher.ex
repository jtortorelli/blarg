defmodule Orga.Watcher do
  @moduledoc """
  This module manages a series of file watcher processes using the `FileSystem` library, and messages `Orga.Buffer` when changes are detected.
  """
  use GenServer
  alias Orga.{Buffer, PathUtil}

  require Logger

  def start_link(_) do
    GenServer.start_link(__MODULE__, [], name: __MODULE__)
  end

  def init(_) do
    {:ok, initialize_watchers()}
  end

  def handle_info({:file_event, worker_pid, {path, events}}, %{film_watcher: worker_pid} = state) do
    Logger.info("A film was changed")
    Logger.info(inspect(events))

    path
    |> PathUtil.extract_base_file_name()
    |> Buffer.film_updated()

    {:noreply, state}
  end

  def handle_info(
        {:file_event, worker_pid, {path, events}},
        %{people_watcher: worker_pid} = state
      ) do
    Logger.info("A person was changed")
    Logger.info(inspect(events))

    path
    |> PathUtil.extract_base_file_name()
    |> Buffer.person_updated()

    {:noreply, state}
  end

  def handle_info(
        {:file_event, worker_pid, {path, events}},
        %{synopses_watcher: worker_pid} = state
      ) do
    Logger.info("A synopsis was changed")
    Logger.info(inspect(events))

    path
    |> PathUtil.extract_base_file_name()
    |> Buffer.film_updated()

    {:noreply, state}
  end

  def handle_info(
        {:file_event, worker_pid, {path, events}},
        %{credits_watcher: worker_pid} = state
      ) do
    Logger.info("A credits was changed")
    Logger.info(inspect(events))

    path
    |> PathUtil.extract_base_file_name()
    |> Buffer.film_updated()

    {:noreply, state}
  end

  def handle_info({:file_event, worker_pid, {path, events}}, %{bios_watcher: worker_pid} = state) do
    Logger.info("A bio was changed!")
    Logger.info(inspect(events))

    path
    |> PathUtil.extract_base_file_name()
    |> Buffer.person_updated()

    {:noreply, state}
  end

  def handle_info(
        {:file_event, worker_pid, {path, events}},
        %{awards_watcher: worker_pid} = state
      ) do
    Logger.info("A awards was changed!")
    Logger.info(inspect(events))

    path
    |> PathUtil.extract_base_file_name()
    |> Buffer.award_updated()

    {:noreply, state}
  end

  def handle_info({:file_event, worker_pid, :stop}, %{film_watcher: worker_pid} = state) do
    Logger.info("Film watcher stopped, restarting")
    {:noreply, watch_films(state)}
  end

  def handle_info({:file_event, worker_pid, :stop}, %{people_watcher: worker_pid} = state) do
    Logger.info("People watcher stopped, restarting")
    {:noreply, watch_people(state)}
  end

  def handle_info({:file_event, worker_pid, :stop}, %{synopses_watcher: worker_pid} = state) do
    Logger.info("Synopses watcher stopped, restarting")
    {:noreply, watch_synopses(state)}
  end

  def handle_info({:file_event, worker_pid, :stop}, %{credits_watcher: worker_pid} = state) do
    Logger.info("Credits watcher stopped, restarting")
    {:noreply, watch_credits(state)}
  end

  def handle_info({:file_event, worker_pid, :stop}, %{bios_watcher: worker_pid} = state) do
    Logger.info("Bios watcher stopped, restarting")
    {:noreply, watch_bios(state)}
  end

  def handle_info({:file_event, worker_pid, :stop}, %{awards_watcher: worker_pid} = state) do
    Logger.info("Awards watcher stopped, restarting")
    {:noreply, watch_awards(state)}
  end

  defp initialize_state do
    %{
      film_watcher: nil,
      people_watcher: nil,
      synopses_watcher: nil,
      credits_watcher: nil,
      bios_watcher: nil,
      awards_watcher: nil
    }
  end

  defp initialize_watchers do
    initialize_state()
    |> watch_films()
    |> watch_people()
    |> watch_synopses()
    |> watch_credits()
    |> watch_bios()
    |> watch_awards()
  end

  defp watch_films(state) do
    {:ok, film_watcher_pid} =
      FileSystem.start_link(dirs: [PathUtil.films_path()], backend: :fs_poll)

    FileSystem.subscribe(film_watcher_pid)
    Logger.debug("Initialized film watcher")
    %{state | film_watcher: film_watcher_pid}
  end

  defp watch_people(state) do
    {:ok, people_watcher_pid} =
      FileSystem.start_link(dirs: [PathUtil.entities_path()], backend: :fs_poll)

    FileSystem.subscribe(people_watcher_pid)
    Logger.debug("Initialized people watcher")
    %{state | people_watcher: people_watcher_pid}
  end

  defp watch_synopses(state) do
    {:ok, synopses_watcher_pid} =
      FileSystem.start_link(dirs: [PathUtil.synopsis_path()], backend: :fs_poll)

    FileSystem.subscribe(synopses_watcher_pid)
    Logger.debug("Initialized synopses watcher")
    %{state | synopses_watcher: synopses_watcher_pid}
  end

  defp watch_credits(state) do
    {:ok, credits_watcher_pid} =
      FileSystem.start_link(dirs: [PathUtil.credits_path()], backend: :fs_poll)

    FileSystem.subscribe(credits_watcher_pid)
    Logger.debug("Initialized credits watcher")
    %{state | credits_watcher: credits_watcher_pid}
  end

  defp watch_bios(state) do
    {:ok, bios_watcher_pid} =
      FileSystem.start_link(dirs: [PathUtil.bios_path()], backend: :fs_poll)

    FileSystem.subscribe(bios_watcher_pid)
    Logger.debug("Initialized bios watcher")
    %{state | bios_watcher: bios_watcher_pid}
  end

  defp watch_awards(state) do
    {:ok, awards_watcher_pid} =
      FileSystem.start_link(dirs: [PathUtil.awards_path(:japan_academy_prize)], backend: :fs_poll)

    FileSystem.subscribe(awards_watcher_pid)
    Logger.debug("Initialized awards watcher")
    %{state | awards_watcher: awards_watcher_pid}
  end
end
