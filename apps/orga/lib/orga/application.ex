defmodule Orga.Application do
  @moduledoc """
  This module defines the supervision tree for `Orga` and starts the prerequisite child processes.
  """

  use Application

  def start(_type, _args) do
    children = [
      {Orga.Buffer, []},
      {Orga.Watcher, []}
    ]

    opts = [strategy: :one_for_one, name: Orga.Supervisor]
    Supervisor.start_link(children, opts)
  end
end
