defmodule Orga.Importer do
  @moduledoc """
  This module delegates importing of specific kinds of data to the respective importer modules.
  """
  alias Orga.Importer.{AwardsImporter, EntityImporter, FilmImporter}

  def import_film(path) do
    FilmImporter.import_film(path)
  end

  def import_films do
    FilmImporter.import_films()
  end

  def import_entity(path) do
    EntityImporter.import_entity(path)
  end

  def import_entities do
    EntityImporter.import_entities()
  end

  def import_awards do
    AwardsImporter.import_awards()
  end
end
