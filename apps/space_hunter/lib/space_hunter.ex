defmodule SpaceHunter do
  @moduledoc """
  This module expresses the API for interacting with the ETS cache containing the data for the X Cineaste projects.
  """
  alias SpaceHunter.{CacheOwner, CacheReader}
  def clear_cache, do: CacheOwner.clear()
  def get_film(film), do: CacheReader.lookup_film(film)
  def get_all_films, do: CacheReader.lookup_all_films()
  def get_person(person), do: CacheReader.lookup_person(person)
  def get_all_people, do: CacheReader.lookup_all_people()
  def get_all_awards(award), do: CacheReader.lookup_all_awards(award)
  def get_awards_ceremony(award, year), do: CacheReader.lookup_awards_ceremony(award, year)
  def insert_awards(ceremony, awards), do: CacheOwner.insert_awards(ceremony, awards)
  def insert_entity(entity), do: CacheOwner.insert_person(entity)
  def insert_film(film), do: CacheOwner.insert_film(film)
end
