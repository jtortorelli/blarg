defmodule SpaceHunter.CacheOwner do
  @moduledoc """
  This module creates and writes to a series of ETS caches. The caches contain Elixir terms used to populate the X Cineaste web projects.
  """
  use GenServer
  require Logger

  @cache_options ~w[set protected named_table]a
  @caches ~w[films film_ids people people_ids awards]a

  #######
  # API #
  #######

  def start_link(_) do
    GenServer.start_link(__MODULE__, [], name: __MODULE__)
  end

  def insert_film(film) do
    GenServer.cast(__MODULE__, {:insert_film, film})
  end

  def insert_person(person) do
    GenServer.cast(__MODULE__, {:insert_person, person})
  end

  def insert_awards(ceremony, awards) do
    GenServer.cast(__MODULE__, {:insert_awards, ceremony, awards})
  end

  def clear do
    GenServer.cast(__MODULE__, {:clear})
  end

  #############
  # Callbacks #
  #############

  def init(_) do
    initialize_caches()
    {:ok, []}
  end

  def handle_cast({:insert_film, film}, state) do
    add_film_to_cache(film)
    {:noreply, state}
  end

  def handle_cast({:insert_person, person}, state) do
    add_person_to_cache(person)
    {:noreply, state}
  end

  def handle_cast({:insert_awards, ceremony, awards}, state) do
    add_awards_to_cache(ceremony, awards)
    {:noreply, state}
  end

  def handle_cast({:clear}, state) do
    clear_all()
    {:noreply, state}
  end

  ####################
  # Helper Functions #
  ####################

  defp initialize_caches do
    Logger.debug("Initializing caches...")

    @caches
    |> Enum.each(&initialize_cache(&1))
  end

  defp initialize_cache(name), do: :ets.new(name, @cache_options)

  defp add_film_to_cache(%{path: path} = film) do
    film_id = Aratrum.derive_film_id(film)
    insert_to_cache(:film_ids, {path, film_id})
    insert_to_cache(:films, {film_id, film})
  end

  defp add_person_to_cache(%{path: path} = person) do
    entity_id = Aratrum.derive_entity_id(person)
    insert_to_cache(:people_ids, {path, entity_id})
    insert_to_cache(:people, {entity_id, person})
  end

  defp add_awards_to_cache(ceremony, awards) do
    insert_to_cache(:awards, {ceremony, awards})
  end

  defp insert_to_cache(table, object), do: :ets.insert(table, object)

  defp clear_all do
    @caches
    |> Enum.each(&clear_cache(&1))
  end

  defp clear_cache(name), do: :ets.delete_all_objects(name)
end
