defmodule SpaceHunter.CacheReader do
  @moduledoc """
  This module reads from the ETS cache created and managed by `SpaceHunter.CacheOwner`. It contains read-only functions.
  """

  def lookup_film({_title, _year} = key) do
    :ets.lookup(:films, key)
    |> handle()
  end

  def lookup_film(path) do
    :ets.lookup(:film_ids, path)
    |> handle(&lookup_film/1)
  end

  def lookup_person({_name, _qualifier} = key) do
    :ets.lookup(:people, key)
    |> handle()
  end

  def lookup_person(path) do
    :ets.lookup(:people_ids, path)
    |> handle(&lookup_person/1)
  end

  def lookup_all_films do
    lookup_all(:films)
    |> Enum.sort_by(& &1.sort, &<=/2)
  end

  def lookup_all_people do
    lookup_all(:people)
    |> Enum.sort_by(& &1.path, &<=/2)
  end

  def lookup_all_awards(award) do
    :ets.match_object(:awards, {{award, :_}, :_})
    |> Enum.map(&elem(&1, 1))
    |> Enum.sort(fn x, y -> Timex.after?(x.date, y.date) end)
  end

  def lookup_awards_ceremony(award, year) do
    :ets.lookup(:awards, {award, year})
    |> handle()
  end

  defp lookup_all(tab) do
    tab
    |> :ets.tab2list()
    |> Enum.map(&elem(&1, 1))
  end

  defp handle(result, func \\ fn val -> {:ok, val} end) do
    case result do
      [{_stored_id, val}] -> func.(val)
      [] -> {:error, :not_found}
    end
  end
end
