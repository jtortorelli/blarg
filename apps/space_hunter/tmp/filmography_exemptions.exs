MapSet.new([
  {"Hayao Miyazaki", "Original Story", {"The Wind Rises", 2013}},
  {"Hayao Miyazaki", "Original Story", {"Ponyo", 2008}},
  {"Hayao Miyazaki", "Original Story", {"Princess Mononoke", 1997}},
  {"Hayao Miyazaki", "Original Story", {"Nausicaä of the Valley of the Wind", 1984}},
  {"Hayao Miyazaki", "Original Story", {"My Neighbor Totoro", 1988}},
  {"Hayao Miyazaki", "Original Story", {"Castle in the Sky", 1986}},
  {"Hayao Miyazaki", "Original Story", {"Porco Rosso", 1992}},
  {"Hayao Miyazaki", "Original Story", {"Spirited Away", 2001}},
  {"Akira Kurosawa", "Original Screenplay", {"Hidden Fortress: The Last Princess", 2008}},
  {"Akira Kurosawa", "Screenplay", {"Tsubaki Sanjuro", 2007}}
])
